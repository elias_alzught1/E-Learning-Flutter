import 'package:e_learning/bloc/authentication/login_bloc/authentication_bloc.dart';
import 'package:e_learning/bloc/category_bloc/category_cubit.dart';
import 'package:e_learning/bloc/course_bloc/course_bloc.dart';
import 'package:e_learning/bloc/profile_bloc/profile_bloc.dart';
import 'package:e_learning/resources/provider/auth_provider.dart';
import 'package:e_learning/resources/provider/category_provider.dart';
import 'package:e_learning/resources/provider/course_provider.dart';
import 'package:e_learning/resources/provider/duration_provider.dart';
import 'package:e_learning/resources/provider/level_provider.dart';
import 'package:e_learning/resources/provider/teacher_provider.dart';
import 'package:e_learning/resources/provider/user_provider.dart';
import 'package:e_learning/themes/app_colors.dart';
import 'package:e_learning/ui/authentication/authentication_screen.dart';
import 'package:e_learning/ui/home/home_screen.dart';
import 'package:e_learning/ui/navigation/navigation.dart';
import 'package:e_learning/ui/splash/splash_screen.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'bloc/ads_bloc/ads_cubit.dart';
import 'bloc/application_bloc/application_cubit.dart';
import 'bloc/authentication/login_bloc/authentication_state.dart';
import 'bloc/duration_cubit/duration_cubit.dart';
import 'bloc/level_cubit/level_cubit.dart';
import 'bloc/teacher_cubit/teacher_cubit.dart';
import 'config/application.dart';
import 'controller/data_store.dart';
import 'localization/app_localization.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

Future<void> initialize() async {
  try {
    await Future.wait([
      DataStore.instance.init(),
      ApplicationCubit.init(),
    ]);
  } finally {
    GetIt.I.allowReassignment = true;
    GetIt.I.registerSingleton<AuthProvider>(AuthProvider());
    GetIt.I.registerSingleton<UserProvider>(UserProvider()); //CategoryProvider
    GetIt.I.registerSingleton<CategoryProvider>(CategoryProvider());
    GetIt.I.registerSingleton<CourseProvider>(CourseProvider());
    GetIt.I.registerSingleton<LevelProvider>(LevelProvider());
    GetIt.I.registerSingleton<TeacherProvider>(TeacherProvider());
    GetIt.I.registerSingleton<DurationProvider>(DurationProvider());
  }
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  try {
    await initialize();
  } finally{
    print('language loaded is : ${DataStore.instance.lang}');
    if (!DataStore.instance.hasToken) {
      print('there is no user');
    } else {
      print('user token is : ${DataStore.instance.token}');
    }
    runApp(
      MultiBlocProvider(
        providers: [
          BlocProvider(create: (context) => ApplicationCubit()),
          BlocProvider(create: (context) => AuthBloc()),
          BlocProvider(create: (context) => ProfileBloc()),
          BlocProvider(create: (context) => LevelCubit()),
          BlocProvider(create: (context) => DurationCubit()),
          BlocProvider(create: (context) => CourseBloc()),
          BlocProvider(create: (context) => TeacherCubit()),
          BlocProvider(create: (context) => CategoryCubit()),

          //TeacherCubit
        ],
        child: const AppMaterial(),
      ),
    );
  }
}

class AppMaterial extends StatefulWidget {
  const AppMaterial({Key key}) : super(key: key);

  @override
  _AppMaterialState createState() => _AppMaterialState();
}

class _AppMaterialState extends State<AppMaterial>{
  @override
  void initState() {
    // TODO: implement initState
    // BlocProvider.of<ApplicationCubit>(context).getTheme();
  }

  @override
  Widget build(BuildContext context){
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return BlocBuilder<ApplicationCubit, ApplicationState>(
        builder: (context, state) {
      return Sizer(
        builder: (context, orientation, deviceType) {
          return MaterialApp(
            theme: ThemeData(
              scaffoldBackgroundColor: AppColors.bGray,
              elevatedButtonTheme: ElevatedButtonThemeData(
                style: ElevatedButton.styleFrom(
                  primary: AppColors.bGray,
                  padding: const EdgeInsets.symmetric(
                    horizontal: 25,
                    vertical: 12,
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              ),
            ),
            navigatorKey: Application.instance.globalKey,
            title: "e-learning",
            home: BlocBuilder<AuthBloc, AuthState>(
              buildWhen: (context, state) =>
                  state is AuthenticationFail || state is AuthenticationSuccess,
              builder: (context, state) {
                if (state is AuthenticationFail){
                  return AuthenticateScreen();
                }
                if(state is AuthenticationSuccess) {
                  return     MultiBlocProvider(
                    providers: [
                      BlocProvider<CategoryCubit>(
                        create: (BuildContext context) => CategoryCubit(),
                      ),
                      BlocProvider<CourseBloc>(
                        create: (BuildContext context) => CourseBloc(),
                      ),
                      BlocProvider<AdsCubit>(
                        create: (BuildContext context) => AdsCubit(),
                      ),
                    ],
                    child:   HomeScreen(),
                  );
                }
                return SplashScreen();
              },
            ),
            debugShowCheckedModeBanner: false,
            supportedLocales: const [
              Locale('ar'),
              Locale('en'),
            ],

            localizationsDelegates: const [
              AppLocalizationsDelegate(),
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            locale: Locale(DataStore.instance.lang),
            localeResolutionCallback: (locale, locales) {
              for (Locale supportedLocale in locales) {
                if (supportedLocale.languageCode == locale.languageCode) {
                  return supportedLocale;
                }
              }
              return locales.first;
            },
          );
        },
      );
    });
  }
}
