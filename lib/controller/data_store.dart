import 'dart:developer';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../models/common/user_model.dart';

class DataStore {
  FlutterSecureStorage _storage = FlutterSecureStorage();

  static final DataStore _instance = DataStore._internal();

  static DataStore get instance => _instance;
  final storageKeyUser = 'userLocal';

  DataStore._internal();

  String _fcmToken;
  String _sessionId;
  String _token;
  String _lang;

  String _mode;

  String get fcmToken => _fcmToken;

  String get token => _token == null
      ? "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vMTg1LjE5NC4xMjQuODg6ODg4OS9hcGkvdXNlci9hdXRoL3ZlcmlmeS1waW4iLCJpYXQiOjE2ODg5MDA5MDAsIm5iZiI6MTY4ODkwMDkwMCwianRpIjoiSXdGUEpmY1pubks5UUJoUyIsInN1YiI6IjEyNiIsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.y98x6zMlmBh4O-c9kac3qaV8WdpQe7MLhSESZL6aOaY"
      : "Bearer $_token";

  String get lang => _lang ?? "en";

  String get sessionId => _sessionId;

  Future<void> init() async {
    _fcmToken = await _storage.read(key: 'messaging_token');
    _sessionId = await _storage.read(key: 'session_id');
    _token = await _storage.read(key: 'token');
    _lang = await _storage.read(key: 'lang');
    _mode = await _storage.read(key: 'mode');
    log("Datastore initialized", name: "$runtimeType");
  }

  bool get hasToken => token != null;

  bool get mode => _mode == null
      ? false
      : _mode == 'Dark'
          ? true
          : false;

  Future<void> deleteUserTokens() async {
    return Future.wait([
      // deleteMessagingToken(),
      // deleteSessionId(),
      deleteToken(),
    ]);
  }

  Future<void> deleteMessagingToken() async {
    _fcmToken = null;
    return _storage.delete(key: "messaging_token");
  }

  Future<void> putMessagingToken(String value) {
    _fcmToken = value;
    return _storage.write(key: "messaging_token", value: value);
  }

  Future<void> deleteToken() async {
    _token = null;
    return _storage.delete(key: "token");
  }

  Future<void> putToken(String value) {
    _token = value;
    return _storage.write(key: "token", value: value);
  }

  Future<void> deleteLang() {
    _lang = null;
    return _storage.delete(key: "lang");
  }

  Future<void> putLang(String value) {
    _lang = value;
    return _storage.write(key: "lang", value: value);
  }

  Future<String> getLang() {
    return _storage.read(key: "lang") ?? "en";
  }

  Future<void> deleteSessionId() {
    _sessionId = null;
    return _storage.delete(key: "session_id");
  }

  Future<void> putSessionId(String value) {
    _sessionId = value;
    return _storage.write(key: "session_id", value: value);
  }

  void saveThemeMode(bool isDarkMode) async {
    print("changerrr");
    String val = isDarkMode == true ? 'Dark' : 'Light';
    _mode = val;
    _storage.write(key: 'mode', value: val);
  }

  Future<User> getUser() async {
    var rawData = await _storage.read(key: storageKeyUser);
    if (rawData != null) {
      return User.fromMap(rawData);
    }
    return null;
  }

  void saveUser(User user) {
    _storage.write(key: storageKeyUser, value: user.toMap());
  }

/*
    // ThemeMode getThemeMode() {
  //   switchStatusColor();
  //   return isSavedDarkMode() ? ThemeMode.dark : ThemeMode.light;
  // }

  // bool isSavedDarkMode() {
  //   return _getStorage.read(storageKey) ?? false;
  // }
  //

  */
}
