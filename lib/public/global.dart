// String regularExpression= "^[A-Za-z][A-Za-z0-9_]{7,29}$";

import 'dart:math';

import 'package:flutter/material.dart';

import 'constants.dart';
import 'package:intl/intl.dart';

final otpRegExp = RegExp(
  PIN_REGEX,
  caseSensitive: false,
  multiLine: false,
);
final Random rc=Random();
final dateUiFormatter = DateFormat("yyyy-MM-dd");
List<Color> colorList = [
  Color(0xFF6E19D7),
  Color(0xFFD7195B),
  Color(0xFFD7B719),
  Color(0xFF19D765)
];
