import 'package:flutter/cupertino.dart';
import 'package:lottie/lottie.dart';

class Constants {
  static const INCH_TO_DP = 160;

  // Assets
  LottieBuilder splashLottie = Lottie.asset('assets/lottie/splash.json');
  LottieBuilder loadingLottie = Lottie.asset('assets/lottie/cat_sleeping.json');
  AssetImage primaryImage= const AssetImage("assets/img/primary_img.jpg");
  static const urlImageDefault = 'https://i.postimg.cc/Dfsg6VYJ/Hi-School.png';
  static const pageSize=10;

}
const PIN_REGEX = r'^\d{4}$';
