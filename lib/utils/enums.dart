class UserStatus {
  final int status;

  UserStatus._internal([this.status]);
  static final notAuth = UserStatus._internal();
}