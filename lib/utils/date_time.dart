class DateTimeParser {
  static DateTime parseDate(String date) {
    return DateTime(
      int.parse(date.substring(13, 14)),
      int.parse(date.substring(16, 18)),
      int.parse(date.substring(19, 23)),
    );
  }
}
