import 'package:flutter/material.dart';

class AppColors{
  static const white = Color(0xFFFFFFFF);
  static const grayAccentLight = Color(0xFFEDEDED);
  static const grayAccent = Color(0xFFdae0ee);

  static const gray = Color(0xFFAFAFAF);
  //#EBEBEB
  //#F0F1F3
  static const lighterGray = Color(0xFFEBEBEB);
  static const textFieldGray = Color(0xFFF0F1F3);
  static const textFieldGray2 = Color(0xFFC0B8B8);
  static const bGray = Color(0xFFFFFFFF);
  static const deepGray = Color(0xff646464);
  static const black = Color(0xFF000000);
  static const redAccent = Color(0xFFD46265);
  static const red = Color(0xFFCC002D);
  //#042B6F
  static const deepBlue = Color(0xFF042B6F);
  static const normalBlue = Color(0xFF074EC7);
  static const green50 = Color(0xFFe0f2f1);
  static const green100 = Color(0xFFb2dfdb);
  static const green200 = Color(0xFF80cbc4);
  static const green300 = Color(0xFF4db6ac);
  static const green700 = Color(0xFF00796b);
  static const darkGreen = Color(0xFF188546);
  static const normalGreen = Color(0xFF2AA421);
  static const blueAccent = Color(0xFF28B6F6);
  static const blue = Color(0xFF00B4CE);

  static const yellowAccent = Color(0xfffce100);
  static const yellow = Color(0xffb0a149);
  static const orangeAccent = Color(0xd0ff8f4a);
  static const deepPurple = Color(0xFF701685);


  static const purple = Color(0xFF774ebe);

  static const lightPurple = Color(0xFF754cbc);
  static const lightOrange = Color(0xFFffa456);
  static final transparent = Color(0x00000000);
}
