class FontFamily {
  FontFamily._();

  static const String lato = 'Lato';
  static const String allison = 'Allison';
  static const String dancing = 'DancingScript';
  static const  String cairo='Cairo';
  static const String cairoLight="Cario_light";

  static const String Cario_blod="Cario_blod";
}
