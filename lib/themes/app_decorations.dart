// import 'package:flutter/material.dart';
//
// import 'app_colors.dart';
//
//
// class AppDecoration {
//   final BoxDecoration decoration;
//   AppDecoration({ this.decoration});
//   factory AppDecoration.containerOnlyShadowTop(context) {
//     if (Theme.of(context).brightness == Brightness.dark) {
//       return AppDecoration(
//         decoration: BoxDecoration(
//           color: AppColors.black,
//           boxShadow: [
//             BoxShadow(
//               color: AppColors.black.withOpacity(.65),
//               offset: Offset(-2, -2),
//               blurRadius: 10,
//             ),
//           ],
//         ),
//       );
//     } else {
//       return AppDecoration(
//         decoration: BoxDecoration(
//           color: AppColors.black,
//           boxShadow: [
//             BoxShadow(
//               color: AppColors.black,
//               offset: Offset(-2, -2),
//               blurRadius: 10,
//             ),
//           ],
//         ),
//       );
//     }
//   }
//
//   factory AppDecoration.buttonActionCircle(context) {
//     if (Theme.of(context).brightness == Brightness.dark) {
//       return AppDecoration(
//         decoration: BoxDecoration(
//           shape: BoxShape.circle,
//           color: AppColors.black,
//           boxShadow: [
//             BoxShadow(
//               color: Colors.black.withOpacity(.8),
//               offset: Offset(1, 1),
//               blurRadius: 1,
//             ),
//             BoxShadow(
//               color: AppColors.black.withOpacity(.35),
//               offset: Offset(-1, -1),
//               blurRadius: 1,
//             ),
//           ],
//         ),
//       );
//     } else {
//       return AppDecoration(
//         decoration: BoxDecoration(
//           shape: BoxShape.circle,
//           color: AppColors.black,
//           boxShadow: [
//             BoxShadow(
//               color: AppColors.black,
//               offset: Offset(1, 1),
//               blurRadius: 1,
//             ),
//             BoxShadow(
//               color: AppColors.black,
//               offset: Offset(-1, -1),
//               blurRadius: 1,
//             ),
//           ],
//         ),
//       );
//     }
//   }
//
//   factory AppDecoration.buttonActionCircleActive(context) {
//     if (Theme.of(context).brightness == Brightness.dark) {
//       return AppDecoration(
//         decoration: BoxDecoration(
//           shape: BoxShape.circle,
//           color: Colors.black.withOpacity(.4),
//           boxShadow: [
//             BoxShadow(
//               color: Colors.grey.shade900,
//               offset: Offset(2, 2),
//               blurRadius: 2,
//               spreadRadius: -2,
//             ),
//           ],
//         ),
//       );
//     } else {
//       return AppDecoration(
//         decoration: BoxDecoration(
//           shape: BoxShape.circle,
//           color: AppColors.black,
//           boxShadow: [
//             BoxShadow(
//               color: AppColors.black,
//               offset: Offset(2, 2),
//               blurRadius: 2,
//               spreadRadius: -2,
//             ),
//           ],
//         ),
//       );
//     }
//   }
//
//   factory AppDecoration.buttonActionBorderActive(context, radius) {
//     if (Theme.of(context).brightness == Brightness.dark) {
//       return AppDecoration(
//         decoration: BoxDecoration(
//           borderRadius: BorderRadius.circular(radius),
//           color: Colors.black.withOpacity(.4),
//           boxShadow: [
//             BoxShadow(
//               color: Colors.grey.shade900,
//               offset: Offset(2, 2),
//               blurRadius: 2,
//               spreadRadius: -2,
//             ),
//           ],
//         ),
//       );
//     } else {
//       return AppDecoration(
//         decoration: BoxDecoration(
//           borderRadius: BorderRadius.circular(radius),
//           color: AppColors.black,
//           boxShadow: [
//             BoxShadow(
//               color: AppColors.black,
//               offset: Offset(2, 2),
//               blurRadius: 2,
//               spreadRadius: -2,
//             ),
//           ],
//         ),
//       );
//     }
//   }
//
//   factory AppDecoration.buttonActionCircleCall(context) {
//     if (Theme.of(context).brightness == Brightness.dark) {
//       return AppDecoration(
//         decoration: BoxDecoration(
//           shape: BoxShape.circle,
//           color: AppColors.black,
//           boxShadow: [
//             BoxShadow(
//               color: Colors.black.withOpacity(.8),
//               offset: Offset(2, 2),
//               blurRadius: 2,
//             ),
//             BoxShadow(
//               color: AppColors.black.withOpacity(.35),
//               offset: Offset(-2, -2),
//               blurRadius: 2,
//             ),
//           ],
//         ),
//       );
//     } else {
//       return AppDecoration(
//         decoration: BoxDecoration(
//           shape: BoxShape.circle,
//           color: AppColors.black.withOpacity(.1),
//           boxShadow: [
//             BoxShadow(
//               color: AppColors.black.withOpacity(.1),
//               offset: Offset(.5, .5),
//               blurRadius: .5,
//             ),
//             BoxShadow(
//               color: AppColors.black.withOpacity(.01),
//               offset: Offset(-1, -1),
//               blurRadius: .5,
//             ),
//           ],
//         ),
//       );
//     }
//   }
//
//   factory AppDecoration.buttonActionBorder(context, radius) {
//     if (Theme.of(context).brightness == Brightness.dark) {
//       return AppDecoration(
//         decoration: BoxDecoration(
//           borderRadius: BorderRadius.circular(radius),
//           color: AppColors.gray,
//           boxShadow: [
//             BoxShadow(
//               color: Colors.black.withOpacity(.4),
//               offset: Offset(4, 4),
//               blurRadius: 4,
//             ),
//             BoxShadow(
//               color: Colors.black.withOpacity(.15),
//               offset: Offset(-1, -1),
//               blurRadius: 20,
//             ),
//           ],
//         ),
//       );
//     } else {
//       return AppDecoration(
//         decoration: BoxDecoration(
//           borderRadius: BorderRadius.circular(radius),
//           color: AppColors.gray,
//           boxShadow: [
//             BoxShadow(
//               color: AppColors.black,
//               offset: Offset(5, 5),
//               blurRadius: 5,
//             ),
//             BoxShadow(
//               color: AppColors.black,
//               offset: Offset(-2, -2),
//               blurRadius: 2,
//             ),
//           ],
//         ),
//       );
//     }
//   }
//
//   factory AppDecoration.tabBarDecoration(context) {
//     if (Theme.of(context).brightness == Brightness.dark) {
//       return AppDecoration(
//           decoration: BoxDecoration(
//         color: AppColors.black.withOpacity(.85),
//         boxShadow: [
//           BoxShadow(
//             color: AppColors.black,
//             offset: Offset(-2, -2),
//             blurRadius: 2,
//           ),
//           BoxShadow(
//             color: AppColors.black.withOpacity(.8),
//             offset: Offset(2, 2),
//             blurRadius: 2,
//           ),
//         ],
//       ));
//     } else {
//       return AppDecoration(
//         decoration: BoxDecoration(
//           color: AppColors.black,
//           boxShadow: [
//             BoxShadow(
//               color: AppColors.black,
//               offset: Offset(2, 2),
//               blurRadius: 2,
//             ),
//             BoxShadow(
//               color: AppColors.black,
//               offset: Offset(-2, -2),
//               blurRadius: 2,
//             ),
//           ],
//         ),
//       );
//     }
//   }
//
//   factory AppDecoration.tabBarDecorationSecond(context) {
//     if (Theme.of(context).brightness == Brightness.dark) {
//       return AppDecoration(
//           decoration: BoxDecoration(
//         color: AppColors.black,
//         boxShadow: [
//           BoxShadow(
//             color: Colors.black.withOpacity(.6),
//             offset: Offset(1, 1),
//             blurRadius: 1,
//           ),
//         ],
//       ));
//     } else {
//       return AppDecoration(
//         decoration: BoxDecoration(
//           color: AppColors.black,
//           boxShadow: [
//             BoxShadow(
//               color: AppColors.black,
//               offset: Offset(2, 2),
//               blurRadius: 2,
//             ),
//           ],
//         ),
//       );
//     }
//   }
//
//   factory AppDecoration.inputChatDecoration(context) {
//     if (Theme.of(context).brightness == Brightness.dark) {
//       return AppDecoration(
//         decoration: BoxDecoration(
//           borderRadius: BorderRadius.circular(
//             8.0,
//           ),
//           color: Colors.black.withOpacity(.25),
//           boxShadow: [
//             BoxShadow(
//               color: AppColors.black.withOpacity(.1),
//               offset: Offset(2, 2),
//               blurRadius: 2,
//               spreadRadius: -2,
//             ),
//           ],
//         ),
//       );
//     } else {
//       return AppDecoration(
//         decoration: BoxDecoration(
//           borderRadius: BorderRadius.circular(
//             8.0,
//           ),
//           color: AppColors.black,
//           boxShadow: [
//             BoxShadow(
//               color: AppColors.black,
//               offset: Offset(2, 2),
//               blurRadius: 2,
//               spreadRadius: -2,
//             ),
//           ],
//         ),
//       );
//     }
//   }
// }
