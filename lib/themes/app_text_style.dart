import 'package:flutter/material.dart';

import 'app_colors.dart';
import 'app_font_size.dart';
import 'font_family.dart';

class AppTextStyle {
  static const xxLargeBlack = TextStyle(
      fontSize: AppFontSize.XX_LARGE,
      color: AppColors.black,
      fontWeight: FontWeight.normal,
      fontFamily: FontFamily.cairo);
  static const mediumYellow = TextStyle(
      fontSize: AppFontSize.MEDIUM,
      color: AppColors.yellow,
      fontWeight: FontWeight.normal,
      fontFamily: FontFamily.cairo);
  static const xxLargeBlackBold = TextStyle(
      fontSize: AppFontSize.XX_LARGE,
      color: AppColors.black,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);
  static const xLargeWhite = TextStyle(
      fontSize: AppFontSize.X_LARGE,
      color: AppColors.white,
      fontWeight: FontWeight.normal,
      fontFamily: FontFamily.cairo);
  static const xLargeLightWhite = TextStyle(
      fontSize: AppFontSize.X_LARGE,
      color: AppColors.white,
      fontWeight: FontWeight.normal,
      fontFamily: FontFamily.cairoLight);
  static const xxLargeWhite = TextStyle(
      fontSize: AppFontSize.XX_LARGE,
      color: AppColors.white,
      fontFamily: FontFamily.cairo);

  static const xxLargeWhiteBold = TextStyle(
      fontSize: AppFontSize.XX_LARGE,
      color: AppColors.white,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);

  static const largeWhiteBold = TextStyle(
      fontSize: AppFontSize.LARGE,
      color: AppColors.white,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);

  static const largeWhite = TextStyle(
      fontSize: AppFontSize.LARGE,
      color: AppColors.white,
      fontFamily: FontFamily.cairo);

  static const mediumWhite = TextStyle(
      fontSize: AppFontSize.MEDIUM,
      color: AppColors.white,
      fontFamily: FontFamily.cairo);

  static const mediumWhiteBold = TextStyle(
      fontSize: AppFontSize.MEDIUM,
      color: AppColors.white,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);

  static const smallWhite = TextStyle(
      fontSize: AppFontSize.SMALL,
      color: AppColors.white,
      fontFamily: FontFamily.cairo);

  static const smallWhiteBold = TextStyle(
      fontSize: AppFontSize.SMALL,
      color: AppColors.white,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);


  static const xSmallWhite = TextStyle(
      fontSize: AppFontSize.X_SMALL,
      color: Colors.white,
      fontFamily: FontFamily.cairo);
  static const xSmallBlue = TextStyle(
      fontSize: AppFontSize.X_SMALL,
      color: AppColors.blue,
      fontFamily: FontFamily.cairo);

  static const xSmallWhiteBold = TextStyle(
      fontSize: AppFontSize.X_SMALL,
      color: Colors.white,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);

  static const xxSmallWhite = TextStyle(
      fontSize: AppFontSize.XX_SMALL,
      color: AppColors.white,
      fontFamily: FontFamily.cairo);

//------------------------ gray
  static const mediumGrayBold = TextStyle(
      color: AppColors.gray,
      fontSize: AppFontSize.MEDIUM,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);

  static const smallGray = TextStyle(
      color: AppColors.gray,
      fontSize: AppFontSize.SMALL,
      fontFamily: FontFamily.cairo);
  static const smallDeepGray = TextStyle(
      color: AppColors.deepGray,
      fontSize: AppFontSize.SMALL,
      fontFamily: FontFamily.cairo);

  static const smallDeepGrayBold = TextStyle(
      color: AppColors.deepGray,
      fontSize: AppFontSize.SMALL,
      fontFamily: FontFamily.cairo,
      fontWeight: FontWeight.w600);
  static const smallGrayBold = TextStyle(
      color: AppColors.gray,
      fontSize: AppFontSize.SMALL,
      fontFamily: FontFamily.cairo,
      fontWeight: FontWeight.bold);

  static const mediumGray = TextStyle(
      color: AppColors.gray,
      fontSize: AppFontSize.MEDIUM,
      fontFamily: FontFamily.cairo);
  static const mediumDeepGray = TextStyle(
      color: AppColors.deepGray,
      fontSize: AppFontSize.MEDIUM,
      fontFamily: FontFamily.cairo);
  static const mediumDeepGrayBold = TextStyle(
    color: AppColors.deepGray,
    fontSize: AppFontSize.MEDIUM,
    fontFamily: FontFamily.cairo,
    fontWeight: FontWeight.bold,
  );

  static const largeGrayBold = TextStyle(
      color: AppColors.gray,
      fontSize: AppFontSize.LARGE,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);

  static const mediumBlack = TextStyle(
      fontSize: AppFontSize.MEDIUM,
      color: AppColors.black,
      fontWeight: FontWeight.normal,
      fontFamily: FontFamily.cairo);

  //cairoLight
  static TextStyle mediumLightBlack = TextStyle(
      fontSize: AppFontSize.MEDIUM,
      color: AppColors.black.withOpacity(0.7),
      fontWeight: FontWeight.normal,
      fontFamily: FontFamily.cairoLight);
  static const mediumBlackBold = TextStyle(
      fontSize: AppFontSize.MEDIUM,
      color: AppColors.black,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);

  static const smallBlack = TextStyle(
      fontSize: AppFontSize.SMALL,
      color: AppColors.black,
      fontFamily: FontFamily.cairo);
  static const smallRed = TextStyle(
      fontSize: AppFontSize.SMALL,
      color: AppColors.red,
      fontFamily: FontFamily.cairo);

  static const smallBlackBold = TextStyle(
      fontSize: AppFontSize.SMALL,
      color: AppColors.black,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);

  static const largeBlackBold = TextStyle(
      fontSize: AppFontSize.LARGE,
      color: AppColors.black,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);

  static const xLargeBlackBold = TextStyle(
      fontSize: AppFontSize.X_LARGE,
      color: AppColors.black,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);

  static const xxLargeRed = TextStyle(
      fontSize: AppFontSize.XX_LARGE,
      color: AppColors.red,
      fontFamily: FontFamily.cairo);

  static const xLargeGray = TextStyle(
      fontSize: AppFontSize.X_LARGE,
      color: AppColors.gray,
      fontFamily: FontFamily.cairo);
  static const xLargeDeepGray = TextStyle(
      fontSize: AppFontSize.X_LARGE,
      color: AppColors.deepGray,
      fontFamily: FontFamily.cairo);

  static const largeDeepGrayBold = TextStyle(
      fontSize: AppFontSize.X_LARGE,
      fontWeight: FontWeight.bold,
      color: AppColors.deepGray,
      fontFamily: FontFamily.cairo);
    static const largeDeepGray  = TextStyle(
      fontSize: AppFontSize.LARGE,

      color: AppColors.deepGray,
      fontFamily: FontFamily.cairo);
  static const largeGray = TextStyle(
      fontSize: AppFontSize.LARGE,
      color: AppColors.gray,
      fontFamily: FontFamily.cairo);

  static const xLargeGrayBold = TextStyle(
      fontSize: AppFontSize.X_LARGE,
      color: AppColors.gray,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);

  static const mediumRed = TextStyle(
      fontSize: AppFontSize.MEDIUM,
      color: AppColors.red,
      fontFamily: FontFamily.cairo);
  static const mediumRedBold = TextStyle(
      fontSize: AppFontSize.MEDIUM,
      fontWeight: FontWeight.bold,
      color: AppColors.red,
      fontFamily: FontFamily.cairo);

  static const xLargeRed = TextStyle(
      fontSize: AppFontSize.X_LARGE,
      color: AppColors.red,
      fontWeight: FontWeight.normal,
      fontFamily: FontFamily.cairo);

  static const xxLargeBlue = TextStyle(
      fontSize: AppFontSize.XX_LARGE,
      color: AppColors.blue,
      fontFamily: FontFamily.cairo);

  static const xxLargeBlueBold = TextStyle(
      fontSize: AppFontSize.XX_LARGE,
      color: AppColors.blue,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);

  static const xLargeBlue = TextStyle(
      fontSize: AppFontSize.X_LARGE,
      color: AppColors.blue,
      fontFamily: FontFamily.cairo);

  static const xLargeBlack = TextStyle(
      fontSize: AppFontSize.X_LARGE,
      color: AppColors.black,
      fontWeight: FontWeight.normal,
      fontFamily: FontFamily.cairo);

  static const largeBlack = TextStyle(
      fontSize: AppFontSize.LARGE,
      color: AppColors.black,
      fontWeight: FontWeight.normal,
      fontFamily: FontFamily.cairo);

  static const xSmallBlackBold = TextStyle(
    fontSize: AppFontSize.X_SMALL,
    color: AppColors.black,
    fontWeight: FontWeight.bold,
  );

  static const xSmallBlack = TextStyle(
    fontSize: AppFontSize.X_SMALL,
    color: AppColors.black,
  );

  static const xxSmallBlack = TextStyle(
    fontSize: AppFontSize.XX_SMALL,
    color: AppColors.black,
  );

////////////////////// blue

  static const smallBlue = TextStyle(
    color: AppColors.blue,
    fontSize: AppFontSize.SMALL,
  );

  static const smallBlueBold = TextStyle(
    color: AppColors.blue,
    fontSize: AppFontSize.SMALL,
    fontWeight: FontWeight.bold,
  );

  static const mediumBlueBold = TextStyle(
    color: AppColors.blue,
    fontSize: AppFontSize.MEDIUM,
    fontWeight: FontWeight.bold,
  );

  static const mediumBlue = TextStyle(
    color: AppColors.blue,
    fontSize: AppFontSize.MEDIUM,
  );

  static const xlargeBlue = TextStyle(
    color: AppColors.blue,
    fontSize: AppFontSize.X_LARGE,
  );

  static const xlargeBlueBold = TextStyle(
    color: AppColors.blue,
    fontSize: AppFontSize.X_LARGE,
    fontWeight: FontWeight.bold,
  );

  static const largeBlue = TextStyle(
      color: AppColors.blue,
      fontSize: AppFontSize.LARGE,
      fontFamily: FontFamily.cairo);

  static const xxxLargeBlack = TextStyle(
      fontSize: AppFontSize.XXX_LARGE,
      color: AppColors.black,
      fontWeight: FontWeight.normal,
      fontFamily: FontFamily.cairo);

  static const xxxLargeBlackBold = TextStyle(
      fontSize: AppFontSize.XXX_LARGE,
      color: AppColors.black,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);

//////////////////////Green

  static const smallGreen = TextStyle(
      color: AppColors.deepBlue,
      fontSize: AppFontSize.SMALL,
      fontFamily: FontFamily.cairo);

  static const smallGreenBold = TextStyle(
      color: AppColors.deepBlue,
      fontSize: AppFontSize.SMALL,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);
  static const smallBlueNormalBold = TextStyle(
      color: AppColors.normalBlue,
      fontSize: AppFontSize.SMALL,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);
  static const smallBlueAccentBold = TextStyle(
      color: AppColors.blueAccent,
      fontSize: AppFontSize.SMALL,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);

  static const mediumGreenBold = TextStyle(
      color: AppColors.deepBlue,
      fontSize: AppFontSize.MEDIUM,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);

  static const mediumGreen = TextStyle(
      color: AppColors.deepBlue,
      fontSize: AppFontSize.MEDIUM,
      fontFamily: FontFamily.cairo);
  static const mediumNormalBlue = TextStyle(
      color: Colors.blue,
      fontSize: AppFontSize.MEDIUM,
      fontFamily: FontFamily.cairo);
  static const mediumDarkGreen = TextStyle(
      color: AppColors.darkGreen,
      fontSize: AppFontSize.MEDIUM,
      fontFamily: FontFamily.cairo);

//darkGreen
  static const xlargeGreen = TextStyle(
      color: AppColors.deepBlue,
      fontSize: AppFontSize.X_LARGE,
      fontFamily: FontFamily.cairo);
//yellowAccent
  static const xlargeGreenBold = TextStyle(
      color: AppColors.deepBlue,
      fontSize: AppFontSize.X_LARGE,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);
  static const xlargeBlueNormal = TextStyle(
      color: Colors.blue,
      fontSize: AppFontSize.X_LARGE,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);
  static const xlargeGreenNormalBold = TextStyle(
      color: AppColors.normalGreen,
      fontSize: AppFontSize.X_LARGE,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);
  static const xlargeYellowBold = TextStyle(
      color: AppColors.yellowAccent,
      fontSize: AppFontSize.X_LARGE,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);

  static const largeGreen = TextStyle(
      color: AppColors.deepBlue,
      fontSize: AppFontSize.LARGE,
      fontFamily: FontFamily.cairo);
  static const largeGreenBold = TextStyle(
      color: AppColors.deepBlue,
      fontSize: AppFontSize.LARGE,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);
  static const largeRedBold = TextStyle(
      color: AppColors.red,
      fontSize: AppFontSize.LARGE,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);
  static const xxLargeGeenBold = TextStyle(
      color: AppColors.deepBlue,
      fontSize: AppFontSize.XX_LARGE,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);
  static const xxxLargeGeenBold = TextStyle(
      color: AppColors.deepBlue,
      fontSize: AppFontSize.XXX_LARGE,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);
  static const largeNormalBlue = TextStyle(
      color: AppColors.normalBlue,
      fontSize: AppFontSize.LARGE,
      fontWeight: FontWeight.normal,
      fontFamily: FontFamily.cairo);
  static const largeNormalBlueBold = TextStyle(
      color: AppColors.normalBlue,
      fontSize: AppFontSize.LARGE,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);
  static const mediumNormalBlueBold = TextStyle(
      color: AppColors.normalBlue,
      fontSize: AppFontSize.MEDIUM,
      fontWeight: FontWeight.bold,
      fontFamily: FontFamily.cairo);

  static const xLargeNormalBlue = TextStyle(
      color: AppColors.normalBlue,
      fontSize: AppFontSize.X_LARGE,
      fontWeight: FontWeight.normal,
      fontFamily: FontFamily.cairo);

  static const largeNormalBlueAccent = TextStyle(
      color: AppColors.blueAccent,
      fontSize: AppFontSize.LARGE,
      fontWeight: FontWeight.normal,
      fontFamily: FontFamily.cairo);
  static const largeYellowAccent = TextStyle(
      color: AppColors.yellowAccent,
      fontSize: AppFontSize.LARGE,
      fontWeight: FontWeight.normal,
      fontFamily: FontFamily.cairo);
  static const largeNormalLightPurple = TextStyle(
      color: AppColors.lightPurple,
      fontSize: AppFontSize.LARGE,
      fontWeight: FontWeight.normal,
      fontFamily: FontFamily.cairo);
//blueAccent
}
