abstract class AppFontSize {
  static const double XX_SMALL = 8.0;
  static const double X_SMALL = 10.0;
  static const double SMALL = 12;
  static const double MEDIUM = 14.0;
  static const double LARGE = 16.0;
  static const double X_LARGE = 18.0;
  static const double XX_LARGE = 20.0;
  static const double XXX_LARGE = 24.0;
}
