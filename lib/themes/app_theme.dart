
import 'package:flutter/material.dart';

import 'app_colors.dart';
import 'app_text_style.dart';

final ThemeData appTheme = ThemeData(
  colorScheme: const ColorScheme.light(
    primary: AppColors.blue,
    secondary: AppColors.blue,
  ),
  appBarTheme: const AppBarTheme(
    titleTextStyle: AppTextStyle.xxxLargeBlack,
    toolbarTextStyle: AppTextStyle.xxxLargeBlack,
    color: AppColors.white,
    elevation: 0,
  ),
  iconTheme: const IconThemeData(color: AppColors.black, size: 25),
  textTheme: const TextTheme(
    headline1: AppTextStyle.xxxLargeBlackBold,
    headline2: AppTextStyle.xxxLargeBlack,
    headline3: AppTextStyle.xxLargeBlack,
    headline4: AppTextStyle.xxLargeBlueBold,
    headline5: AppTextStyle.xLargeBlue,
    headline6: AppTextStyle.xLargeBlack,
    subtitle1: AppTextStyle.largeBlack,
    bodyText1: AppTextStyle.largeBlack,
    bodyText2: AppTextStyle.mediumBlue,
  ),
  inputDecorationTheme: InputDecorationTheme(
    border: UnderlineInputBorder(
      borderSide: BorderSide(color: AppColors.gray),
    ),
    focusedBorder: UnderlineInputBorder(
      borderSide: BorderSide(color: AppColors.blue),
    ),
    errorBorder: UnderlineInputBorder(
      borderSide: BorderSide(color: AppColors.red),
    ),
    floatingLabelBehavior: FloatingLabelBehavior.always,
    hintStyle: AppTextStyle.largeGray,
    labelStyle: AppTextStyle.xLargeBlue,
    errorStyle: AppTextStyle.mediumRed,
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ElevatedButton.styleFrom(
      primary: AppColors.blue,
      padding: EdgeInsets.symmetric(
        horizontal: 25,
        vertical: 12,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
    ),
  ),
);
