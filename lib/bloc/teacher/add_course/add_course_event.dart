part of 'add_course_bloc.dart';

@immutable
abstract class AddCourseEvent {}

class AddCourseTeacher extends AddCourseEvent {}

class GetAddCourseContent extends AddCourseEvent {}

class AddIncludeDescription extends AddCourseEvent {
  final int id;
  final int includeId;

  AddIncludeDescription(this.id, {this.includeId});
}

class DeleteIncludeEvent extends AddCourseEvent {
  final int id;
  final int idCourse;

  DeleteIncludeEvent(this.id, this.idCourse);
}

class AddAimsCourse extends AddCourseEvent {
  final int id;
  final int aimsId;

  AddAimsCourse(this.id, {this.aimsId});
}

class DeleteAimsEvent extends AddCourseEvent {
  final int id;
  final int idCourse;

  DeleteAimsEvent(this.id, this.idCourse);
}

class AddTargetCourse extends AddCourseEvent {
  final int id;
  final int targetId;

  AddTargetCourse(this.id, {this.targetId});
}

class DeleteTargetEvent extends AddCourseEvent {
  final int id;
  final int idCourse;

  DeleteTargetEvent(this.id, this.idCourse);
}

class AddPrerequisiteCourse extends AddCourseEvent {
  final int id;
  final int prerequisiteId;

  AddPrerequisiteCourse(this.id, {this.prerequisiteId});
}

class DeletePrerequisiteEvent extends AddCourseEvent {
  final int id;
  final int idCourse;

  DeletePrerequisiteEvent(this.id, this.idCourse);
}
class AddCurriculumCourse extends AddCourseEvent {
  final int id;
  AddCurriculumCourse(this.id );
}

class GetLessonEvent extends AddCourseEvent{
  final int id;
  GetLessonEvent(this.id );
}