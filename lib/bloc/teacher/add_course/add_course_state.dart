part of 'add_course_bloc.dart';

@immutable
abstract class AddCourseState {}

class AddCourseInitial extends AddCourseState {}

class AddCourseAwaitState extends AddCourseState {}

class AddCourseErrorState extends AddCourseState {
  final String message;

  AddCourseErrorState(this.message);
}

class AddCourseAcceptState extends AddCourseState {
  final AddCourseResponse response;

  AddCourseAcceptState(this.response);
}

class AddCourseValidateState extends AddCourseState {}

class GetAddCourseContentAwaitState extends AddCourseState {}

class GetAddCourseContentAcceptState extends AddCourseState {}

class GetAddCourseContentErrorState extends AddCourseState {
  final String message;

  GetAddCourseContentErrorState(this.message);
}

class AddIncludeDescriptionAwaitState extends AddCourseState {}

class AddIncludeDescriptionErrorState extends AddCourseState {
  final String message;

  AddIncludeDescriptionErrorState(this.message);
}

class AddIncludeDescriptionAcceptState extends AddCourseState{
  final AddCourseResponse response;

  AddIncludeDescriptionAcceptState(this.response);
}
class AddCurriculumAcceptState  extends AddCourseState{}

class AddIncludeDescriptionValidateState extends AddCourseState{}

class DeleteIncludeDescriptionAwaitState extends AddCourseState{}

class DeleteIncludeDescriptionErrorState extends AddCourseState{
  final String message;

  DeleteIncludeDescriptionErrorState(this.message);
}

class DeleteIncludeDescriptionAcceptState extends AddCourseState{
  final AddCourseResponse response;

  DeleteIncludeDescriptionAcceptState(this.response);
}

class AddAimsAwaitState extends AddCourseState {}

class AddAimsErrorState extends AddCourseState{
  final String message;

  AddAimsErrorState(this.message);
}

class AddAimsAcceptState extends AddCourseState{
  final AddCourseResponse response;

  AddAimsAcceptState(this.response);
}

class AddAimsValidateState extends AddCourseState{}
class AddTargetValidateState extends AddCourseState{}
class AddPrerequisiteValidateState extends AddCourseState{}
class AddCurriculumValidateState extends AddCourseState{}


class GetLessonAwaitState extends AddCourseState{}
class GetLessonAcceptState extends AddCourseState{}
class GetLessonEmptyState extends AddCourseState{}
class GetLessonErrorState extends AddCourseState{
  final String message;

  GetLessonErrorState(this.message);
}

