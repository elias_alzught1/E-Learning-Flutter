import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:e_learning/localization/app_localization.dart';
import 'package:e_learning/models/common/course_model.dart';
import 'package:e_learning/resources/provider/teacher_provider.dart';
import 'package:flutter/material.dart';

import 'package:get_it/get_it.dart';
import 'package:meta/meta.dart';

import '../../../models/common/language.dart';
import '../../../models/request/teacher/add_course_request.dart';
import '../../../models/response/teacher/add_course_response.dart';
import '../../../models/response/teacher/durations_response.dart';
import '../../../models/response/teacher/lessons_response.dart';
import '../../../models/response/teacher/levels_response.dart';

part 'add_course_event.dart';

part 'add_course_state.dart';

class AddCourseBloc extends Bloc<AddCourseEvent, AddCourseState> {
  final BuildContext context;
  final nameController = TextEditingController();
  final arabicNameController = TextEditingController();
  final englishNameController = TextEditingController();
  final shortNameController = TextEditingController();
  final arabicShortNameController = TextEditingController();
  final englishShortNameController = TextEditingController();
  final descriptionController = TextEditingController();
  final englishDescriptionController = TextEditingController();
  final arabicDescriptionController = TextEditingController();
  final startDateController = TextEditingController();
  final endDateController = TextEditingController();
  final coursePriceController = TextEditingController();

  //course details
  final arabicIncludeDescription = TextEditingController();
  final englishIncludeDescription = TextEditingController();

  //aims
  final arabicAims = TextEditingController();
  final englishAims = TextEditingController();

  //target
  final arabicTarget = TextEditingController();
  final englishTarget = TextEditingController();

  //prerequisite
  final arabicPrerequisite = TextEditingController();
  final englishPrerequisite = TextEditingController();

  //Curriculum
  final lessonTitle = TextEditingController();
  final lessonEnglishTitle = TextEditingController();
  final lessonArabicTitle = TextEditingController();
  final lessonTDescription = TextEditingController();
  final lessonEnglishDescription = TextEditingController();
  final lessonArabicDescription = TextEditingController();
  File imageLessonBloc;
  String fileLessonName;
  AddCourse addCourse;

  String language;
  bool visible = false;

  DateTime startDate;
  DateTime endTime;
  List<Levels> levels = [];
  List<Durations> durations = [];
  Durations durationsSelected;
  Levels levelSelected;
  List<String> items = [
    'English',
    'Arabic',
  ];
  String lanSelected;
  List<int> imageBytes = [];
  File test;
  String imageBase64;
  List<LessonsTeacher> lessonsTeacher = [];

  AddCourseBloc(this.context) : super(AddCourseInitial());
  String errorNameMessage;
  String errorArabicNameMessage;
  String errorEnglishNameMessage;
  String errorShortName;
  String errorArabicShortName;
  String errorEnglishShortName;
  String errorDescription;
  String errorArabicDescription;
  String errorEnglishDescription;
  String errorPriceMessage;
  String image64;
  Language local;
  File imageBloc;
  FormData formData;
  String fileName;

  String arabicErrorInclude;
  String englishErrorInclude;

  String arabicErrorAims;
  String englishErrorAims;

  String arabicErrorTarget;
  String englishErrorTarget;

  String arabicErrorPrerequisite;
  String englishErrorPrerequisite;

  String lessonTitleError;
  String lessonTitleEnglishError;
  String lessonTitleArabicError;
  String lessonDescriptionError;
  String lessonDescriptionEnglishError;
  String lessonDescriptionArabicError;
  String imageError;

  Future<void> validateIncludeDescription() async {
    arabicErrorInclude =
        arabicIncludeDescription.text.isEmpty ? "filed required" : null;
    englishErrorInclude =
        englishIncludeDescription.text.isEmpty ? "filed required" : null;
  }

  Future<void> validateAims() async {
    arabicErrorAims = arabicAims.text.isEmpty ? "filed required" : null;
    englishErrorAims = englishAims.text.isEmpty ? "filed required" : null;
  }

  Future<void> validateTarget() async {
    arabicErrorTarget = arabicTarget.text.isEmpty ? "filed required" : null;
    englishErrorTarget = englishTarget.text.isEmpty ? "filed required" : null;
  }

  Future<void> validatePrerequisite() async {
    arabicErrorPrerequisite =
        arabicPrerequisite.text.isEmpty ? "filed required" : null;
    englishErrorPrerequisite =
        englishPrerequisite.text.isEmpty ? "filed required" : null;
  }

  Future<void> validateCurriculum() async {
    lessonTitleError = lessonTitle.text.isEmpty ? "filed required" : null;
    lessonTitleEnglishError =
        lessonEnglishTitle.text.isEmpty ? "filed required" : null;
    lessonTitleArabicError =
        lessonArabicTitle.text.isEmpty ? "filed required" : null;
    lessonDescriptionError =
        lessonTDescription.text.isEmpty ? "filed required" : null;
    lessonDescriptionArabicError =
        lessonArabicDescription.text.isEmpty ? "filed required" : null;
    lessonDescriptionEnglishError =
        lessonEnglishDescription.text.isEmpty ? "filed required" : null;
  }

  bool get validDescription =>
      arabicErrorInclude == null && englishErrorInclude == null;

  bool get validAims => arabicErrorAims == null && englishErrorAims == null;

  bool get validTarget =>
      arabicErrorTarget == null && englishErrorTarget == null;

  bool get validPrerequisite =>
      arabicErrorPrerequisite == null && englishErrorPrerequisite == null;

  bool get validCurriculum =>
      lessonTitleError == null &&
      lessonTitleEnglishError == null &&
      lessonTitleArabicError == null &&
      lessonDescriptionError == null &&
      lessonDescriptionArabicError == null &&
      lessonDescriptionEnglishError == null;

  @override
  Stream<AddCourseState> mapEventToState(AddCourseEvent event) async* {
    if (event is GetAddCourseContent) {
      yield (GetAddCourseContentAwaitState());
      try {
        var response = await GetIt.I.get<TeacherProvider>().getLevels();
        levels = response.data.levels;
        var durationResponse =
            await GetIt.I.get<TeacherProvider>().getDurations();
        durations = durationResponse.data.durations;
        yield (GetAddCourseContentAcceptState());
      } catch (ex) {
        yield (GetAddCourseContentErrorState(ex.toString()));
      }
    } else if (event is AddCourseTeacher) {
      try {
        var formData = FormData.fromMap({
          "name": nameController.text.toString(),
          "name_ar": arabicNameController.text.toString(),
          "name_en": englishNameController.text.toString(),
          "shortname": shortNameController.text.toString(),
          "shortname_ar": arabicShortNameController.text.toString(),
          "shortname_en": englishShortNameController.text.toString(),
          "description": descriptionController.text.toString(),
          "description_ar": arabicDescriptionController.text.toString(),
          "descripiton_en": englishDescriptionController.text.toString(),
          "start_date": startDateController.text.toString(),
          "end_date": endDateController.text.toString(),
          "level": levelSelected.id,
          "duration": durationsSelected.id,
          "language": "english",
          "price": int.parse(coursePriceController.text.toString()),
          "visible": visible,
          "image":
              await MultipartFile.fromFile(imageBloc.path, filename: fileName),
        });
        yield (AddCourseAwaitState());
        var response = await GetIt.I.get<TeacherProvider>().addCourse(formData);
        yield (AddCourseAcceptState(response));
      } catch (ex) {
        yield (AddCourseErrorState(ex.toString()));
      }
    } else if (event is AddIncludeDescription) {
      validateIncludeDescription();
      yield AddIncludeDescriptionValidateState();
      if (validDescription) {
        try {
          yield (AddIncludeDescriptionAwaitState());
          var response = await GetIt.I.get<TeacherProvider>().editInclude(
              event.id,
              arabicIncludeDescription.text,
              englishIncludeDescription.text,
              includeId: event.includeId);

          var response2 =
              await GetIt.I.get<TeacherProvider>().getCourseById(event.id);

          yield (AddIncludeDescriptionAcceptState(response2));
        } catch (ex) {
          yield (AddIncludeDescriptionErrorState(ex.toString()));
        }
      }
    } else if (event is DeleteIncludeEvent) {
      try {
        yield DeleteIncludeDescriptionAwaitState();
        var response =
            await GetIt.I.get<TeacherProvider>().deleteInclude(event.id);

        var response2 =
            await GetIt.I.get<TeacherProvider>().getCourseById(event.idCourse);
        yield (DeleteIncludeDescriptionAcceptState(response2));
      } catch (ex) {
        DeleteIncludeDescriptionErrorState(ex.toString());
      }
    } else if (event is AddAimsCourse) {
      validateAims();
      yield AddAimsValidateState();
      if (validAims) {
        try {
          yield (AddIncludeDescriptionAwaitState());
          var response = await GetIt.I.get<TeacherProvider>().editAims(
              event.id, arabicAims.text, englishAims.text,
              aimsId: event.aimsId);

          var response2 =
              await GetIt.I.get<TeacherProvider>().getCourseById(event.id);

          yield (AddIncludeDescriptionAcceptState(response2));
        } catch (ex) {
          yield (AddIncludeDescriptionErrorState(ex.toString()));
        }
      }
    } else if (event is DeleteAimsEvent) {
      try {
        yield DeleteIncludeDescriptionAwaitState();
        var response =
            await GetIt.I.get<TeacherProvider>().deleteAims(event.id);

        var response2 =
            await GetIt.I.get<TeacherProvider>().getCourseById(event.idCourse);
        yield (DeleteIncludeDescriptionAcceptState(response2));
      } catch (ex) {
        DeleteIncludeDescriptionErrorState(ex.toString());
      }
    } else if (event is AddTargetCourse) {
      validateTarget();
      yield AddTargetValidateState();
      if (validTarget) {
        try {
          yield (AddIncludeDescriptionAwaitState());
          var response = await GetIt.I.get<TeacherProvider>().editTarget(
              event.id, arabicTarget.text, englishTarget.text,
              targetId: event.targetId);

          var response2 =
              await GetIt.I.get<TeacherProvider>().getCourseById(event.id);

          yield (AddIncludeDescriptionAcceptState(response2));
        } catch (ex) {
          yield (AddIncludeDescriptionErrorState(ex.toString()));
        }
      }
    } else if (event is DeleteTargetEvent) {
      try {
        yield DeleteIncludeDescriptionAwaitState();
        var response =
            await GetIt.I.get<TeacherProvider>().deleteTarget(event.id);

        var response2 =
            await GetIt.I.get<TeacherProvider>().getCourseById(event.idCourse);
        yield (DeleteIncludeDescriptionAcceptState(response2));
      } catch (ex) {
        DeleteIncludeDescriptionErrorState(ex.toString());
      }
    } else if (event is AddPrerequisiteCourse) {
      validatePrerequisite();
      yield AddPrerequisiteValidateState();
      if (validPrerequisite) {
        try {
          yield (AddIncludeDescriptionAwaitState());
          var response = await GetIt.I.get<TeacherProvider>().editPrerequisite(
              event.id, arabicPrerequisite.text, englishPrerequisite.text,
              prerequisiteId: event.prerequisiteId);

          var response2 =
              await GetIt.I.get<TeacherProvider>().getCourseById(event.id);

          yield (AddIncludeDescriptionAcceptState(response2));
        } catch (ex) {
          yield (AddIncludeDescriptionErrorState(ex.toString()));
        }
      }
    } else if (event is DeletePrerequisiteEvent) {
      try {
        yield DeleteIncludeDescriptionAwaitState();
        var response =
            await GetIt.I.get<TeacherProvider>().deletePrerequisite(event.id);

        var response2 =
            await GetIt.I.get<TeacherProvider>().getCourseById(event.idCourse);
        yield (DeleteIncludeDescriptionAcceptState(response2));
      } catch (ex) {
        DeleteIncludeDescriptionErrorState(ex.toString());
      }
    } else if (event is AddCurriculumCourse) {
      validateCurriculum();
      yield AddCurriculumValidateState();
      if (validCurriculum) {
        if (fileLessonName != '' && fileLessonName != null) {
          try {
            yield (AddIncludeDescriptionAwaitState());

            var formData = FormData.fromMap({
              "course_id": event.id,
              "title": lessonTitle.text,
              "title_ar": lessonArabicTitle.text,
              "title_en": lessonEnglishTitle.text,
              "description": lessonTDescription.text,
              "description_ar": lessonArabicDescription.text,
              "description_en": lessonEnglishDescription.text,
              "image": await MultipartFile.fromFile(imageLessonBloc.path,
                  filename: fileLessonName),
            });
            var response =
                await GetIt.I.get<TeacherProvider>().addLesson(formData);

            var response2 =
                await GetIt.I.get<TeacherProvider>().getCourseById(event.id);

            yield (AddIncludeDescriptionAcceptState(response2));
            yield AddCurriculumAcceptState();
          } catch (ex) {
            yield (AddIncludeDescriptionErrorState(ex.toString()));
          }
        } else {
          yield (AddIncludeDescriptionErrorState(
              AppLocalization.of(context).trans("select_file")));
        }
      }
    } else if (event is GetLessonEvent) {
      try {
        yield GetLessonAwaitState();
        var response = await GetIt.I
            .get<TeacherProvider>()
            .getLessonsOfCourseById(event.id);
        if (response.data.lessons.isEmpty) {
          yield GetLessonEmptyState();
        } else {
          lessonsTeacher = response.data.lessons;
          yield GetLessonAcceptState();
        }
      } catch (ex) {
        yield GetLessonErrorState(ex.toString());
      }
    }
  }
}
