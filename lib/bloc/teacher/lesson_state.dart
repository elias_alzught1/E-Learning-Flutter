part of 'lesson_cubit.dart';

@immutable
abstract class LessonState {}

class LessonInitial extends LessonState {}

class AddVideosAwaitState extends LessonState {}

class AddVideosErrorState extends LessonState {
  final String message;

  AddVideosErrorState(this.message);
}

class AddVideosAcceptState extends LessonState {}

class AddVideosValidateState extends LessonState {}

class GetVideosAwaitState extends LessonState {}

class GetVideosAcceptState extends LessonState {}

class GetVideosErrorState extends LessonState {
  final String message;

  GetVideosErrorState(this.message);
}

class GetBooksAwaitState extends LessonState {}

class GetBooksAcceptState extends LessonState {}

class GetBooksErrorState extends LessonState {
  final String message;

  GetBooksErrorState(this.message);
}

class AddQuizAwaitState extends LessonState {}

class AddQuizErrorState extends LessonState {
  final String message;

  AddQuizErrorState(this.message);
}

class AddQuizAcceptState extends LessonState {}

class AddQuizValidateState extends LessonState {}

class GetQuizAwaitState extends LessonState {}

class GetQuizAcceptState extends LessonState {}

class GetQuizErrorState extends LessonState {
  final String message;

  GetQuizErrorState(this.message);
}

class EditQuizInformationAwaitState extends LessonState {}

class EditQuizInformationAcceptState extends LessonState {}

class EditQuizInformationErrorState extends LessonState {
  final String message;

  EditQuizInformationErrorState(this.message);
}

class AddQuestionQuizAcceptState extends LessonState {}

class AddQuestionQuizErrorState extends LessonState {
  final String message;

  AddQuestionQuizErrorState(this.message);
}

class AddQuestionQuizAwaitState extends LessonState {}

class UpdateLessonInfoAwaitState extends LessonState {}

class UpdateLessonInfoAcceptState extends LessonState {}

class UpdateLessonInfoErrorState extends LessonState {
  final String message;

  UpdateLessonInfoErrorState(this.message);
}

class GetUpdatedLessonAwaitState extends LessonState {}

class GetUpdatedLessonAcceptState extends LessonState {
  final LessonsTeacher response;

  GetUpdatedLessonAcceptState(this.response);
}

class GetUpdatedLessonErrorState extends LessonState {
  final String message;

  GetUpdatedLessonErrorState(this.message);
}

class UpdateVideoInfoAwaitState extends LessonState {}

class UpdateVideoInfoAcceptState extends LessonState {
  final String message;

  UpdateVideoInfoAcceptState(this.message);
}

class UpdateVideoInfoErrorState extends LessonState {
  final String message;

  UpdateVideoInfoErrorState(this.message);
}

class UpdateBookInfoAwaitState extends LessonState {}

class UpdateBookInfoAcceptState extends LessonState {
  final String message;

  UpdateBookInfoAcceptState(this.message);
}

class UpdateBookInfoErrorState extends LessonState {
  final String message;

  UpdateBookInfoErrorState(this.message);
}

class ChangePdfAwaitState extends LessonState {}

class ChangePdfAcceptState extends LessonState {
  final String message;

  ChangePdfAcceptState(this.message);
}

class ChangePdfErrorState extends LessonState {
  final String message;

  ChangePdfErrorState(this.message);
}

class UpdateQuestionQuizAwaitState extends LessonState {}

class UpdateQuestionQuizAcceptState extends LessonState {}

class UpdateQuestionQuizErrorState extends LessonState {
  final String message;

  UpdateQuestionQuizErrorState(this.message);
}

class DeleteQuestionQuizAwaitState extends LessonState {}

class DeleteQuestionQuizAcceptState extends LessonState {}

class DeleteQuestionQuizErrorState extends LessonState {
  final String message;

  DeleteQuestionQuizErrorState(this.message);
}
