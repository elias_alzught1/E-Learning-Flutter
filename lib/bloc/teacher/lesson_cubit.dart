import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:e_learning/models/response/teacher/quiz_response.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';

import 'package:get_it/get_it.dart';
import 'package:meta/meta.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

import '../../models/response/shower_response.dart';
import '../../models/response/teacher/lessons_response.dart';
import '../../resources/provider/teacher_provider.dart';

part 'lesson_state.dart';

class LessonCubit extends Cubit<LessonState> {
  //update Lesson info
  final titleLessonInfoController = TextEditingController();
  final titleLessonInfoArabicController = TextEditingController();
  final titleLessonInfoEnglishController = TextEditingController();
  final descriptionLessonInfoController = TextEditingController();
  final descriptionLessonInfoArabicController = TextEditingController();
  final descriptionLessonInfoEnglishController = TextEditingController();

  //videos
  final titleController = TextEditingController();
  final titleArabicController = TextEditingController();
  final titleEnglishController = TextEditingController();
  final descriptionController = TextEditingController();
  final descriptionArabicController = TextEditingController();
  final descriptionEnglishController = TextEditingController();
  final subTitleArabic = TextEditingController();
  final subTitleEnglish = TextEditingController();

  //Quiz
  final titleQuizController = TextEditingController();
  final titleQuizArabicController = TextEditingController();
  final titleQuizEnglishController = TextEditingController();
  final descriptionQuizController = TextEditingController();
  final descriptionQuizArabicController = TextEditingController();
  final descriptionQuizEnglishController = TextEditingController();

  //AddQuiz
  final questionQuizController = TextEditingController();
  final firstAnswerQuizController = TextEditingController();
  final secondAnswerQuizController = TextEditingController();
  final thirdAnswerQuizController = TextEditingController();
  final fourthAnswerQuizController = TextEditingController();

  //book
  final titleBookController = TextEditingController();
  final titleBookArabicController = TextEditingController();
  final titleBookEnglishController = TextEditingController();
  final descriptionBookController = TextEditingController();
  final descriptionBookArabicController = TextEditingController();
  final descriptionBookEnglishController = TextEditingController();
  final bookPathController = TextEditingController();
  String errorQuestionQuiz;

  LessonCubit() : super(LessonInitial());
  String errorQuizTitle;
  String errorQuizTitleAr;
  String errorQuizTitleEn;
  String errorQuizDescription;
  String errorQuizDescriptionAr;
  String errorQuizDescriptionEn;
  File videosBloc;
  File videosEditBloc;
  String videosName;
  String errorController;
  String errorArabicController;
  String errorEnglishController;
  String errorDescription;
  String errorArabicDescription;
  String errorEnglishDescription;
  List<LessoneMedia> videos = [];
  List<LessoneMedia> books = [];
  List<QuizModel> quizList = [];
  List<Uint8List> videosImage = [];

  String errorInfoTitle;
  String errorInfoEnglishTitle;
  String errorInfoArabicTitle;
  String errorInfoDescription;
  String errorInfoEnglishDescription;
  String errorInfoArabicDescription;

  String errorSubTitleArabic;
  String errorSubTitleEnglish;
  String errorBookInfoTitle;
  String errorBookInfoEnglishTitle;
  String errorBookInfoArabicTitle;
  String errorBookInfoDescription;
  String errorBookInfoEnglishDescription;
  String errorBookInfoArabicDescription;

  Future<void> addVideos(int id) async {
    try {
      emit(AddVideosAwaitState());
      var formData = FormData.fromMap({
        "lesson_id": id,
        "title": titleController.text.toString(),
        "title_ar": titleArabicController.text.toString(),
        "title_en": titleEnglishController.text.toString(),
        "description": descriptionController.text.toString(),
        "description_ar": descriptionArabicController.text.toString(),
        "description_en": descriptionEnglishController.text.toString(),
        "media_type": "VIDEO",
        "media": await MultipartFile.fromFile(videosBloc.path),
      });
      var response = await GetIt.I.get<TeacherProvider>().addVideos(formData);
      emit(AddVideosAcceptState());
    } catch (ex) {
      emit(AddVideosErrorState(ex.error));
    }
  }

  Future<void> addQuiz(int id) async {
    try {
      emit(AddQuizAwaitState());
      var response = await GetIt.I.get<TeacherProvider>().addQuiz(
          id,
          titleQuizController.text,
          titleQuizArabicController.text,
          titleQuizEnglishController.text,
          descriptionQuizController.text,
          descriptionQuizArabicController.text,
          descriptionQuizEnglishController.text);
      emit(AddQuizAcceptState());
    } catch (ex) {
      emit(AddQuizErrorState(ex.error));
    }
  }

  Future<void> getVideos(int id) async {
    videosImage.clear();
    try {
      emit(GetVideosAwaitState());

      var response =
          await GetIt.I.get<TeacherProvider>().getVideosForLesson(id);
      videos = response.data.lessoneMedia;
      for (var v in videos) {
        final x = Uint8List.fromList(await VideoThumbnail.thumbnailData(
          video: v.mediaUrl,
          imageFormat: ImageFormat.JPEG,
          maxWidth: 500,
          quality: 25,
        ));
        videosImage.add(x);
      }
      emit(GetVideosAcceptState());
    } catch (ex) {
      emit(GetVideosErrorState(ex.error));
    }
  }

  Future<void> getBooks(int id) async {
    try {
      emit(GetBooksAwaitState());

      var response = await GetIt.I.get<TeacherProvider>().getBooksForLesson(id);
      books = response.data.lessoneMedia;

      emit(GetBooksAcceptState());
    } catch (ex) {
      emit(GetBooksErrorState(ex.error));
    }
  }

  Future<void> getQuiz(int id) async {
    try {
      emit(GetBooksAwaitState());

      var response = await GetIt.I.get<TeacherProvider>().getQuizForLesson(id);
      quizList = response.data;
      emit(GetBooksAcceptState());
    } catch (ex) {
      emit(GetBooksErrorState(ex.error));
    }
  }

  Future<void> editQuizInformation(int id) async {
    try {
      emit(EditQuizInformationAwaitState());

      var response = await GetIt.I.get<TeacherProvider>().editQuizInformation(
            id,
            titleQuizController.text,
            titleQuizArabicController.text.toString(),
            titleQuizEnglishController.text.toString(),
            descriptionQuizController.text.toString(),
            descriptionQuizArabicController.text.toString(),
            descriptionQuizEnglishController.text.toString(),
          );

      if (response == 200) {
        emit(EditQuizInformationAcceptState());
      } else {
        emit(EditQuizInformationErrorState("Sorry, something went wrong"));
      }
    } catch (ex) {
      emit(GetBooksErrorState(ex.error.toString()));
    }
  }

  Future<void> addQuestionForQuiz(
      int id,
      // String questionQuiz,
      // String firstAnswer,
      // String secondAnswer,
      // String thirdAnswer,
      // String fourthAnswer,
      String acceptedAnswer) async {
    try {
      emit(AddQuestionQuizAwaitState());

      var response = await GetIt.I.get<TeacherProvider>().addQuestionForQuiz(
          id,
          questionQuizController.text.toString(),
          firstAnswerQuizController.text.toString(),
          secondAnswerQuizController.text.toString(),
          thirdAnswerQuizController.text.toString(),
          fourthAnswerQuizController.text.toString(),
          acceptedAnswer);

      emit(AddQuestionQuizAcceptState());
    } catch (ex) {
      emit(AddQuestionQuizErrorState(ex.error.toString()));
    }
  }

  Future<void> updateQuestionForQuiz(int id, String acceptedAnswer) async {
    try {
      emit(UpdateQuestionQuizAwaitState());

      var response = await GetIt.I.get<TeacherProvider>().addQuestionForQuiz(
          id,
          questionQuizController.text.toString(),
          firstAnswerQuizController.text.toString(),
          secondAnswerQuizController.text.toString(),
          thirdAnswerQuizController.text.toString(),
          fourthAnswerQuizController.text.toString(),
          acceptedAnswer);

      emit(UpdateQuestionQuizAcceptState());
    } catch (ex) {
      emit(UpdateQuestionQuizErrorState(ex.error.toString()));
    }
  }

  Future<void> updateLessonInfo(
    int id,
  ) async {
    try {
      emit(UpdateLessonInfoAwaitState());

      var response = await GetIt.I.get<TeacherProvider>().editLessonInformation(
          id,
          titleLessonInfoController.text.toString(),
          titleLessonInfoArabicController.text.toString(),
          titleLessonInfoEnglishController.text.toString(),
          descriptionLessonInfoController.text.toString(),
          descriptionLessonInfoArabicController.text.toString(),
          descriptionLessonInfoEnglishController.text.toString());

      emit(UpdateLessonInfoAcceptState());
    } catch (ex) {
      emit(UpdateLessonInfoErrorState(ex.error.toString()));
    }
  }

  Future<void> getUpdatedLesson(
    int id,
  ) async {
    try {
      emit(GetUpdatedLessonAwaitState());

      final response = await GetIt.I.get<TeacherProvider>().getLesson(id);

      emit(GetUpdatedLessonAcceptState(response.data));
    } catch (ex) {
      emit(GetUpdatedLessonErrorState(ex.error.toString()));
    }
  }

  Future<void> updateVideosInfo(int id, String action) async {
    try {
      emit(UpdateVideoInfoAwaitState());

      if (action == "Basic Info") {
        var response = await GetIt.I
            .get<TeacherProvider>()
            .editVideoInformation(
                id,
                titleController.text.toString(),
                titleArabicController.text.toString(),
                titleEnglishController.text.toString(),
                descriptionController.text.toString(),
                descriptionArabicController.text.toString(),
                descriptionEnglishController.text.toString());
        emit(UpdateVideoInfoAcceptState(response.message));
      }
      if (action == "Edit Video") {
        var formData = FormData.fromMap({
          "id": id,
          "media": await MultipartFile.fromFile(videosEditBloc.path),
        });
        var response =
            await GetIt.I.get<TeacherProvider>().changeVideo(formData);
        emit(UpdateVideoInfoAcceptState(response.message));
      }
    } catch (ex) {
      emit(UpdateVideoInfoErrorState(ex.error.toString()));
    }
  }

  Future<void> changePdfFile(int id, String path) async {
    try {
      emit(UpdateBookInfoAwaitState());
      var formData = FormData.fromMap({
        "id": id,
        "media": await MultipartFile.fromFile(path),
      });
      var response =
          await GetIt.I.get<TeacherProvider>().changePdf(formData, id);
      emit(UpdateBookInfoAcceptState(response.message));
    } catch (ex) {
      emit(UpdateBookInfoErrorState(ex.error.toString()));
    }
  }

  Future<void> updateBookInfo(int id) async {
    try {
      emit(UpdateBookInfoAwaitState());

      var response = await GetIt.I.get<TeacherProvider>().editBookInformation(
          id,
          titleBookController.text.toString(),
          titleBookArabicController.text.toString(),
          titleBookEnglishController.text.toString(),
          descriptionBookController.text.toString(),
          descriptionBookArabicController.text.toString(),
          descriptionBookEnglishController.text.toString());
      emit(UpdateBookInfoAcceptState(response.message));
    } catch (ex) {
      emit(UpdateBookInfoErrorState(ex.error.toString()));
    }
  }

  Future<void> deleteQuestion(int id) async {
    try {
      emit(DeleteQuestionQuizAwaitState());

      var response = await GetIt.I.get<TeacherProvider>().deleteQuestion(id);
      emit(DeleteQuestionQuizAcceptState());
    } catch (ex) {
      emit(DeleteQuestionQuizErrorState(ex.error.toString()));
    }
  }
}
