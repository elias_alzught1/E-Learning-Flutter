part of 'course_details_bloc.dart';

@immutable
abstract class CourseDetailsEvent {}

class AddIncludesEvent extends CourseDetailsEvent {
  final int id;

  AddIncludesEvent(this.id);
}
