part of 'course_details_bloc.dart';

@immutable
abstract class CourseDetailsState {}

class CourseDetailsInitial extends CourseDetailsState {}

class AddIncludeAwaitState extends CourseDetailsState {}

class AddIncludeAcceptState extends CourseDetailsState {}

class AddIncludeValidateState extends CourseDetailsState {}

class AddIncludeErrorState extends CourseDetailsState {
  final String message;

  AddIncludeErrorState(this.message);
}
