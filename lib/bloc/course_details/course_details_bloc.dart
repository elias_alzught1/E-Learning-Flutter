import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:meta/meta.dart';

import '../../resources/provider/course_provider.dart';

part 'course_details_event.dart';

part 'course_details_state.dart';

class CourseDetailsBloc extends Bloc<CourseDetailsEvent, CourseDetailsState> {
  final arabicAddDescription = TextEditingController();
  final englishAddDescription = TextEditingController();

  CourseDetailsBloc() : super(CourseDetailsInitial());
  String arabicErrorInclude;
  String englishErrorInclude;
  @override
  Future<void> close() {
    arabicAddDescription.dispose();
    englishAddDescription.dispose();
    return super.close();
  }

  @override
  Stream<CourseDetailsState> mapEventToState(CourseDetailsEvent event) async* {
    if (event is AddIncludesEvent) {
      yield (AddIncludeAwaitState());
      try {
        final response = await GetIt.I.get<CourseProvider>().addInclude(
            event.id,
            arabicAddDescription.text.toString(),
            englishAddDescription.text.toString());

        yield (AddIncludeAcceptState());
      } catch (ex) {
        yield (AddIncludeErrorState(ex));
      }
    }
    // TODO: implement onEvent
    super.onEvent(event);
  }
}
