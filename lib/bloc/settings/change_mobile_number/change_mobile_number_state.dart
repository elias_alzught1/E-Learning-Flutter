part of 'change_mobile_number_bloc.dart';

@immutable
abstract class ChangeMobileNumberState {}

class ChangeMobileNumberInitial extends ChangeMobileNumberState {}

class ChangeMobileNumberAwaitState extends ChangeMobileNumberState {}

class ChangeMobileNumberAcceptState extends ChangeMobileNumberState {
  final String message;

  ChangeMobileNumberAcceptState(this.message);
}

class ChangeMobileNumberValidationState extends ChangeMobileNumberState {}

class ChangeMobileNumberErrorState extends ChangeMobileNumberState {
  final String message;

  ChangeMobileNumberErrorState(this.message);
}
