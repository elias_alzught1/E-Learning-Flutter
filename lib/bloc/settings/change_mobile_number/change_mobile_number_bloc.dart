import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:e_learning/resources/provider/user_provider.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:meta/meta.dart';
import 'package:phone_number/phone_number.dart';

import '../../../resources/api_client.dart';
import '../../../ui/common/widget/get_snack_bar.dart';

part 'change_mobile_number_event.dart';

part 'change_mobile_number_state.dart';

class ChangeMobileNumberBloc
    extends Bloc<ChangeMobileNumberEvent, ChangeMobileNumberState> {
  final mobileNumberController = TextEditingController();
  final phoneNumberUtil = PhoneNumberUtil();

  ChangeMobileNumberBloc() : super(ChangeMobileNumberInitial());

  @override
  Future<void> close() async {
    mobileNumberController.clear();
    mobileNumberController.dispose();
    return super.close();
  }

  String errorMobileNumberMsg;
  bool isValidMobileNumber = true;

  Future<void> _validate() async {
    String mobileNumber = mobileNumberController.text.trim();

    //validate for only syria
    try {
      // isValidMobileNumber = await phoneNumberUtil.validate(
      //   mobileNumber,
      //   'SY',
      // );
    } catch (e) {
      isValidMobileNumber = false;
    }

    errorMobileNumberMsg = mobileNumber.isEmpty
        ? "filed_required_msg"
        : !isValidMobileNumber
            ? "error_mobile_number_msg"
            : null;
  }

  bool get valid => isValidMobileNumber;

  Future<String> getPhoneNumber() async {
    String nationalNumber = mobileNumberController.text.trim();
    var phoneNumber = await phoneNumberUtil.parse(
      nationalNumber,
      regionCode: 'SY',
    );
    return phoneNumber.e164;
  }

  @override
  Stream<ChangeMobileNumberState> mapEventToState(event) async* {
    if (event is ChangeMobileNumber) {
      try {
        await _validate();
        yield ChangeMobileNumberValidationState();
        if (valid) {
          yield ChangeMobileNumberAwaitState();

          var response = await GetIt.I.get<UserProvider>().changeMobileNumber(
                await getPhoneNumber(),
              );

          yield ChangeMobileNumberAcceptState(response.message);
        }
      } on ValidationException catch (e) {
        var errors = e.errors;
        errorMobileNumberMsg = errors['mobile_number']?.join(" ");

        yield ChangeMobileNumberValidationState();
      } catch (e) {
        yield ChangeMobileNumberErrorState("$e");
      }
    }
  }
}
