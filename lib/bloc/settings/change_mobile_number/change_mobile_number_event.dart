part of 'change_mobile_number_bloc.dart';

@immutable
abstract class ChangeMobileNumberEvent {}
class ChangeMobileNumber extends ChangeMobileNumberEvent{}