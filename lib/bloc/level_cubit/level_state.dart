part of 'level_cubit.dart';

@immutable
abstract class LevelState {}

class LevelInitial extends LevelState {}

class GetLevelsAwaitState  extends LevelState {}
class GetLevelsAcceptState   extends LevelState {}
class GetLevelsErrorState extends LevelState {
  final String message;

  GetLevelsErrorState(this.message);
}
