import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:e_learning/models/common/course_model.dart';
import 'package:e_learning/resources/provider/level_provider.dart';
import 'package:get_it/get_it.dart';
import 'package:meta/meta.dart';

import '../../models/response/levels_response.dart';

part 'level_state.dart';

class LevelCubit extends Cubit<LevelState> {
  LevelCubit() : super(LevelInitial());
  List<Level> levels = [];

  Future<void> fetch() async {
    try {
      emit(GetLevelsAwaitState());
      var response = await GetIt.I.get<LevelProvider>().getLevels();
      levels = response.data.levels;
      emit(GetLevelsAcceptState());
    } on DioError catch (ex) {
      emit(GetLevelsErrorState(ex.error));
    }
  }
}
