import 'package:dio/dio.dart';
import 'package:e_learning/bloc/verification_bloc/verification_pin_factory.dart';
import 'package:e_learning/helpers/nav_context.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';

import 'package:phone_number/phone_number.dart';

import '../../controller/data_store.dart';
import '../../localization/app_localization.dart';

import '../../models/response/verify_response.dart';
import '../../public/global.dart';
import '../../resources/api_client.dart';
import '../../resources/provider/auth_provider.dart';
import '../profile_bloc/profile_bloc.dart';

part 'verification_event.dart';

part 'verification_state.dart';

class VerificationBloc extends Bloc<VerificationEvent, VerificationState>
    with NavContext {
  String sessionId;
  final int channelId;
  final int planId;
  final String phoneNumber;
  final String language;
  final controller = TextEditingController();
  final phoneNumberUtil = PhoneNumberUtil();

  @override
  Future<void> close() {
    controller.dispose();
    return super.close();
  }

  String otpMessage;

  void _validate() {
    String value = controller.text;

    otpMessage = value.isEmpty
        ? AppLocalization.of(context).trans("filed_required")
        : !otpRegExp.hasMatch(value)
            ? AppLocalization.of(context).trans("invalid_pin")
            : null;
  }

  bool get isValid => otpMessage == null;


  VerificationBloc(this.sessionId, this.phoneNumber, this.channelId,
      this.planId, this.language)
      : super(VerificationInitial()) {
    on<VerificationEvent>((event, emit) async {
      if (event is VerificationCommitEvent) {
        _validate();
        emit(VerificationValidationState());
        if (isValid) {
          try {
            emit(VerificationAwaitState());
            var response = await GetIt.I.get<AuthProvider>().verify(
                msisdn: phoneNumber,
                sessionId: sessionId,
                pin: controller.text);
            DataStore.instance.putToken(response.data.token);

            BlocProvider.of<ProfileBloc>(context).user = response.data?.user;
            DataStore.instance.saveUser(response.data.user);
            emit(VerificationAcceptState());
          } on DioError catch (ex) {
            var error = ex.error;
            if (error is ValidationException) {
              otpMessage = error.errors['pin']?.join(" ");
              emit(VerificationValidationState());
            } else {
              emit(VerificationErrorState(ex.error.toString()));
            }
          } on FactoryException catch (e) {
            if (e.code == 406) {
              emit(VerificationErrorState("$e"));
            }
            if (e.code == 302) {
              emit(VerificationErrorState("$e"));
            }
          }
        }
      } else if (event is VerificationResendCommitEvent) {
        try {
          emit(VerificationResendAwaitState());
          // var response = await GetIt.I.get<AuthRepository>().resendPin(
          //       msisdn: phoneNumber,
          //       operator: "MTN-SY",
          //     );
          //
          // sessionId = response.data.sessionId.toString();
          emit(VerificationResendAcceptState());
        } on DioError catch (ex) {
          emit(VerificationResendErrorState(ex.error.toString()));
        }
      }


    });
  }
}
