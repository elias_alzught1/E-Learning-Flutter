part of 'verification_bloc.dart';

@immutable
abstract class VerificationEvent {}

class VerificationCommitEvent extends VerificationEvent{}

class VerificationResendCommitEvent extends VerificationEvent{}

class VerificationPinLoginCommitEvent extends VerificationEvent{}