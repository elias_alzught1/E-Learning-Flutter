part of 'verification_bloc.dart';

@immutable
abstract class VerificationState {}

class VerificationInitial extends VerificationState {}

class VerificationValidationState extends VerificationState {}

class VerificationAwaitState extends VerificationState {}

class VerificationAcceptState extends VerificationState {}

class VerificationErrorState extends VerificationState {
  final String message;

  VerificationErrorState(this.message);
}

class VerificationResendAcceptState extends VerificationState {}

class VerificationResendAwaitState extends VerificationState {}

class VerificationResendErrorState extends VerificationState {
  final String message;

  VerificationResendErrorState(this.message);
}
class VerificationLoginAwaitState extends VerificationState {}

class VerificationLoginAcceptState extends VerificationState {
final  VerifyResponse response;

  VerificationLoginAcceptState(this.response);
}

class VerificationLoginErrorState extends VerificationState {
  final String message;

  VerificationLoginErrorState(this.message);
}

