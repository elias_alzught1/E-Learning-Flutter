// import 'package:flutter/cupertino.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:get_it/get_it.dart';
//
// import '../../layout/document/main_layout.dart';
// import '../../resource/repository/auth_repository.dart';
// import '../home_bloc/home_cubit.dart';
//
// abstract class VerificationFactory {
//   Future<void> verify(
//       String code, String sessionId, String msisdn, String countryCode) {
//     return GetIt.I.get<AuthRepository>().verificationPin(
//         sessionId: sessionId,
//         pin: code,
//         msisdn: msisdn,
//         countryCode: countryCode);
//   }
//
//   Future<void> resend();
//
//   void onVerified();
// }
//
// class VerificationOnAuthFactory extends VerificationFactory {
//   final BuildContext context;
//
//   VerificationOnAuthFactory(this.context);
//
//   @override
//   Future<void> resend() async {}
//
//   @override
//   void onVerified() {
//     Navigator.of(context).pushAndRemoveUntil(
//       CupertinoPageRoute(
//         builder: (context) => BlocProvider(
//           create: (context) => HomeCubit(),
//           child: const MainLayout(),
//         ),
//       ),
//       (route) => false,
//     );
//   }
// }
