import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:e_learning/models/common/course_model.dart';
import 'package:e_learning/resources/provider/course_provider.dart';
import 'package:get_it/get_it.dart';
import 'package:meta/meta.dart';

import '../../models/response/teacher_response.dart';

part 'teacher_state.dart';

class TeacherCubit extends Cubit<TeacherState> {
  TeacherCubit() : super(TeacherInitial());
  List<Courses> courses = [];

  Future<void> getTeacherById(int id) async {
    try {
      emit(GetTeacherAwaitState());
      var response = await GetIt.I.get<CourseProvider>().getTeacherById(id);

      emit(GetTeacherAcceptState(response));
    } on DioError catch (ex) {
      emit(GetTeacherErrorState(ex.error));
    }
  }

  Future<void> getCourseForTeacher() async {
    try {
      emit(GetCourseAwaitState());
      var response = await GetIt.I.get<CourseProvider>().getTeacherCourse(1);
      courses = response.data.courses;
      emit(GetCourseAcceptState());
    } on DioError catch (ex) {
      emit(GetCourseErrorState(ex.error));
    }
  }
}
//getTeacherById
