part of 'teacher_cubit.dart';

@immutable
abstract class TeacherState {}

class TeacherInitial extends TeacherState {}

class GetTeacherAwaitState extends TeacherState {}

class GetTeacherErrorState extends TeacherState {
  final String message;

  GetTeacherErrorState(this.message);
}

class GetTeacherAcceptState extends TeacherState {
  final TeacherResponse teacher;

  GetTeacherAcceptState(this.teacher);
}

class GetCourseAwaitState extends TeacherState {}

class GetCourseAcceptState extends TeacherState {}

class GetCourseErrorState extends TeacherState {
  final String message;

  GetCourseErrorState(this.message);
}
