part of 'ads_cubit.dart';

@immutable
abstract class AdsState {}

class AdsInitial extends AdsState {}

class GetAdsAwaitState extends AdsState {}

class GetAdsAcceptState extends AdsState {}

class GetAdsErrorState extends AdsState {
  final String message;

  GetAdsErrorState(this.message);
}
