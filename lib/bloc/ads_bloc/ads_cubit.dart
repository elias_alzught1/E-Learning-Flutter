import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:meta/meta.dart';

import '../../models/response/ads_response.dart';
import '../../resources/provider/user_provider.dart';

part 'ads_state.dart';

class AdsCubit extends Cubit<AdsState> {
  AdsCubit() : super(AdsInitial());
  List<AdsItem> adsItem = [];
  Future<void> fetchAds() async{
    emit (GetAdsAwaitState());
    try {
      final response = await GetIt.I.get<UserProvider>().getAds();
      adsItem = response.adsItem;
      emit (GetAdsAcceptState());
    } catch (ex) {
      emit (GetAdsErrorState(ex));
    }
  }
}
