import 'package:bloc/bloc.dart';
import 'package:e_learning/bloc/authentication/login_bloc/authentication_bloc.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';

import '../../controller/data_store.dart';

import '../authentication/login_bloc/authentication_event.dart';

part 'application_state.dart';

class ApplicationCubit extends Cubit<ApplicationState> {
  Key key = UniqueKey();
  static PackageInfo info;

  static Future<void> init() async {

    return info = await PackageInfo.fromPlatform();

  }

  ApplicationCubit() : super(ApplicationInitial());

  Future<void> refresh() async {
    key = UniqueKey();
    emit(ApplicationInitial());
  }

  Future<void> changeLanguage(String code) async {
    await DataStore.instance.putLang(code);
    emit(ApplicationInitial());
  }


}
