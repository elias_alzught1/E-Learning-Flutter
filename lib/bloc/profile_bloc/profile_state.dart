part of 'profile_bloc.dart';

@immutable
abstract class ProfileState {}

class ProfileInitial extends ProfileState {}
class UpdateUserInfoAwaitState  extends ProfileState {}
class UpdateUserInfoErrorState  extends ProfileState {final String message;

  UpdateUserInfoErrorState(this.message);}

class UpdateUserInfoAcceptState  extends ProfileState {}

class ChangeMobileNumberAwaitState extends ProfileState {}
class ChangeMobileNumberAcceptState extends ProfileState {}
class ChangeMobileNumberErrorState extends ProfileState {final String message;

  ChangeMobileNumberErrorState(this.message);}

class VerifyChangeMobileNumberAwaitState extends ProfileState {}
class VerifyChangeMobileNumberAcceptState extends ProfileState {}
class VerifyChangeMobileNumberErrorState extends ProfileState {final String message;

VerifyChangeMobileNumberErrorState(this.message);}