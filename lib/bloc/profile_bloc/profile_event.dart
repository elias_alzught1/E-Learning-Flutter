part of 'profile_bloc.dart';

@immutable
abstract class ProfileEvent {}



class UpdateUserInfo extends ProfileEvent {}

class ChangeMobileNumber extends ProfileEvent {}
class VerifyChangeMobileNumber extends ProfileEvent {}