import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:get_it/get_it.dart';
import 'package:meta/meta.dart';

import '../../localization/app_localization.dart';
import '../../models/common/user_model.dart';
import '../../resources/provider/user_provider.dart';

part 'profile_event.dart';

part 'profile_state.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  ProfileBloc() : super(ProfileInitial());
  User user;
  TextEditingController firstName = TextEditingController();
  TextEditingController lastName = TextEditingController();
  TextEditingController userName = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController phoneNumber = TextEditingController();
  TextEditingController pinController = TextEditingController();

  final _pinRegex = RegExp(
    '\\d{${4}}',
    caseSensitive: false,
  );
  String pinMessage;

  bool get valid => pinMessage == null;

  void _validate() {
    var pin = pinController.text.trim();
    pinMessage = !_pinRegex.hasMatch(pin)
        ? AppLocalization.instance.trans("invalid_pin")
        : null;
  }

  @override
  Stream<ProfileState> mapEventToState(ProfileEvent event) async* {
    if (event is UpdateUserInfo) {
      try {
        yield (UpdateUserInfoAwaitState());
        var response = await GetIt.I
            .get<UserProvider>()
            .changeUserInfo(firstName.text, lastName.text, userName.text);
        user.firstName = response.data.firstName;
        user.lastName = response.data.lastName;
        user.username = response.data.username;
        yield (UpdateUserInfoAcceptState());
      } catch (ex) {
        yield (UpdateUserInfoErrorState(ex.toString()));
      }
    } else if (event is ChangeMobileNumber) {
      yield ChangeMobileNumberAwaitState();
      yield ChangeMobileNumberAcceptState();
    } else if (event is VerifyChangeMobileNumber) {
      _validate();
      if (valid) {
        yield VerifyChangeMobileNumberAwaitState();
        yield VerifyChangeMobileNumberAcceptState();
      }
    }
  }
}
