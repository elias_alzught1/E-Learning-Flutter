part of 'search_cubit.dart';

@immutable
abstract class SearchState {}

class SearchInitial extends SearchState {}

class SearchCourseAwaitState extends SearchState {}

class SearchCourseAcceptState extends SearchState {}

class SearchCourseErrorState extends SearchState {
  final String message;

  SearchCourseErrorState(this.message);
}
