import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:meta/meta.dart';

import '../../models/common/course_model.dart';
import '../../resources/provider/course_provider.dart';

part 'search_state.dart';

class SearchCubit extends Cubit<SearchState> {
  List<Courses> searchResult = [];
  PagingController<int, Courses> pagingController =
      PagingController(firstPageKey: 1);

  SearchCubit() : super(SearchInitial());

  Future<void> fetch(String query, int page) async {
    try {
      emit(SearchCourseAwaitState());
      final response =
          await GetIt.I.get<CourseProvider>().searchCourse(query, page);
      if (response.data.courses.isNotEmpty) {
        final nextPageKey = page + 1;
        pagingController.appendPage(response.data.courses, nextPageKey);
      } else {
        pagingController.appendLastPage([]);
      }
      searchResult = response.data.courses;
      emit(SearchCourseAcceptState());
    } on DioError catch (ex) {
      emit(SearchCourseErrorState(ex.error));
    }
  }
}
