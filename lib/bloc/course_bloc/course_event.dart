part of 'course_bloc.dart';

@immutable
abstract class CourseEvent {}

class GetCourses extends CourseEvent {
  final Map<String, dynamic> filterRequest;

  GetCourses({this.filterRequest});
}

class GetFilterCourses extends CourseEvent {
  final Map<String, dynamic> filterRequest;

  GetFilterCourses({this.filterRequest});
}

class GetMoreCourses extends CourseEvent {}

class GetLessCourses extends CourseEvent {}

class FilterCourse extends CourseEvent {
  final List<int> filter;

  FilterCourse(this.filter);
}

class GetLessonsCourse extends CourseEvent {}

class GetQuizCourse extends CourseEvent {}

class FetchShowerContent extends CourseEvent {}

class TestEvents extends CourseEvent {
  final int id;
  final List<Map<String, dynamic>> data;

  TestEvents(this.id, this.data);
}

class FetchShowerQuiz extends CourseEvent {}

class CourseRating extends CourseEvent {
  final int courseId;
  final int ratingValue;

  CourseRating(this.courseId, this.ratingValue);
}

class LikeCourse extends CourseEvent {
  final int courseId;

  LikeCourse(this.courseId);
}

class UnLikeCourse extends CourseEvent {
  final int courseId;

  UnLikeCourse(this.courseId);
}

class CheckAnswer extends CourseEvent {}

class GetWishList extends CourseEvent {}

class GetMoreWishList extends CourseEvent {}

class GetLessWishList extends CourseEvent {}

class ToggleWishList extends CourseEvent {
  final int courseId;

  ToggleWishList(this.courseId);
}

class SearchCourse extends CourseEvent {
  final String query;

  SearchCourse(this.query);
}

class GetCartCourses extends CourseEvent {}
