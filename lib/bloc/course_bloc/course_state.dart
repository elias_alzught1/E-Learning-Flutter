part of 'course_bloc.dart';

@immutable
abstract class CourseState {}

class CourseInitial extends CourseState {}

class GetCourseAwaitState extends CourseState {
  final bool isFirstFetch;

  GetCourseAwaitState({this.isFirstFetch});
}

class GetCourseAcceptState extends CourseState {}

class GetCourseErrorState extends CourseState {
  final String message;

  GetCourseErrorState(this.message);
}

class GetFilterCourseAwaitState extends CourseState {
  final bool isFirstFetch;

  GetFilterCourseAwaitState({this.isFirstFetch});
}

class GetFilterCourseAcceptState extends CourseState {}

class GetFilterCourseErrorState extends CourseState {
  final String message;

  GetFilterCourseErrorState(this.message);
}

class GetCourseLessonsAwaitState extends CourseState {}

class GetCourseLessonsAcceptState extends CourseState {
  final List<Lessons> lessons;

  GetCourseLessonsAcceptState(this.lessons);
}

class GetCourseLessonsErrorState extends CourseState {
  final String message;

  GetCourseLessonsErrorState(this.message);
}

class GetShowerContentAwaitState extends CourseState {}

class GetShowerContentAcceptState extends CourseState {}

class GetShowerContentErrorState extends CourseState {
  final String message;

  GetShowerContentErrorState(this.message);
}

class FetchShowerQuizAwaitState extends CourseState {}

class FetchShowerQuizAcceptState extends CourseState {}

class FetchShowerQuizErrorState extends CourseState {
  final String message;

  FetchShowerQuizErrorState(this.message);
}

class RatingCourseAwaitState extends CourseState {}

class RatingCourseErrorState extends CourseState {
  final String message;

  RatingCourseErrorState(this.message);
}

class RatingCourseAcceptState extends CourseState {
  final RatingResponse ratingResponse;

  RatingCourseAcceptState(this.ratingResponse);
}

class LikeCourseAwaitState extends CourseState {}

class LikeCourseErrorState extends CourseState {
  final String message;

  LikeCourseErrorState(this.message);
}

class LikeCourseAcceptState extends CourseState {
  final CourseDetailsResponse courseDetailsResponse;

  LikeCourseAcceptState(this.courseDetailsResponse);
}

class UnLikeCourseAwaitState extends CourseState {}

class UnLikeCourseErrorState extends CourseState {
  final String message;

  UnLikeCourseErrorState(this.message);
}

class UnLikeCourseAcceptState extends CourseState {
  final CourseDetailsResponse courseDetailsResponse;

  UnLikeCourseAcceptState(this.courseDetailsResponse);
}

class CheckAnswerAwaitState extends CourseState {}

class CheckAnswerErrorState extends CourseState {
  final String message;

  CheckAnswerErrorState(this.message);
}

class CheckAnswerAcceptState extends CourseState {}

class GetWishListAcceptState extends CourseState {}

class GetWishListErrorState extends CourseState {
  final String message;

  GetWishListErrorState(this.message);
}

class GetWishListAwaitState extends CourseState {}

class ToggleCourseAwaitState extends CourseState {}

class ToggleCourseAcceptState extends CourseState {
  final String message;

  ToggleCourseAcceptState(this.message);
}

class ToggleCourseErrorState extends CourseState {
  final String message;

  ToggleCourseErrorState(this.message);
}

class TestAwaitState extends CourseState {}

class TestAcceptState extends CourseState {
  final TestResult result;

  TestAcceptState(this.result);
}

class TestErrorState extends CourseState {
  final String message;

  TestErrorState(this.message);
}



class GetCartAwait extends CourseState {}

class GetCartAccept extends CourseState {}

class GetCartError extends CourseState {
  final String message;
  GetCartError(this.message);
}
