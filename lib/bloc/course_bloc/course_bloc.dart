import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:e_learning/models/response/cart_response.dart';
import 'package:e_learning/models/response/lessones_response.dart';
import 'package:e_learning/models/response/shower_response.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:meta/meta.dart';

import '../../models/common/course_model.dart';

import '../../models/response/ads_response.dart';
import '../../models/response/course_details_response.dart';
import '../../models/response/lessons_response.dart';
import '../../models/response/quiz_result_response.dart';
import '../../models/response/quizs_response.dart';
import '../../models/response/rating_response.dart';
import '../../resources/provider/course_provider.dart';


part 'course_event.dart';

part 'course_state.dart';

class CourseBloc extends Bloc<CourseEvent, CourseState> {

  CourseBloc() : super(CourseInitial());
  int page = 1;
  int totalPages = 0;

  List<Courses> courses = [];
  List<CourseCart> cartCourses = [];
  List<Courses> searchResult = [];
  List<QuizQuestions> result = [];
  List<LessoneMedia> videoList = [];
  List<LessoneMedia> readList = [];
  LessonsInfo info;
  List<Courses> wishList = [];
  List<AdsItem> adsItem = [];
  int tempPage = 0;
  int subTotal = 0;
  int pageWishList = 1;
  int totalPageWishList;

  bool checkMaxValid(int currentPage, totalPages) {
    if (totalPages > currentPage) {
      return true;
    } else {
      return false;
    }
  }

  bool checkMinValid(int currentPage) {
    if (currentPage >= 1) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Stream<CourseState> mapEventToState(CourseEvent event) async* {
    if (event is GetCourses) {
      yield GetCourseAwaitState();
      try {
        var response = await GetIt.I
            .get<CourseProvider>()
            .getCourse(page, filter: event.filterRequest);
        totalPages = response.data.totalPages;

        if (courses.isNotEmpty) courses.clear();
        courses = response.data.courses;
        tempPage = page;
        yield GetCourseAcceptState();
      } on DioError catch (ex) {
        yield GetCourseErrorState(ex.message);
      }
    }
    else if (event is GetFilterCourses) {
      yield GetFilterCourseAwaitState();
      try {
        var response = await GetIt.I
            .get<CourseProvider>()
            .getCourse(page, filter: event.filterRequest);
        totalPages = response.data.totalPages;

        if (courses.isNotEmpty) courses.clear();
        courses = response.data.courses;
        searchResult = response.data.courses;
        tempPage = page;
        yield GetFilterCourseAcceptState();
      } on DioError catch (ex) {
        yield GetFilterCourseErrorState(ex.message);
      }
    }
      else if (event is FilterCourse) {
      yield GetCourseAwaitState();
      try {
        var response = await GetIt.I
            .get<CourseProvider>()
            .getFilterCourse(page, event.filter);
        courses = response.data.courses;
        page++;
        yield GetCourseAcceptState();
      } on DioError catch (ex) {
        yield GetCourseErrorState(ex.message);
      }
    } else if (event is GetLessonsCourse) {
      yield GetCourseLessonsAwaitState();
      try {
        final response =
            await GetIt.I.get<CourseProvider>().getLessons(10, 1, 5);
        // final response2 = await GetIt.I.get<CourseProvider>().getQuiz(4);
        yield GetCourseLessonsAcceptState(response.data.lessons);
      } catch (ex) {
        yield GetCourseLessonsErrorState(ex.toString());
      }
    } else if (event is FetchShowerContent) {
      yield (GetShowerContentAwaitState());
      try {
        videoList.clear();
        readList.clear();

        final response1 =
            await GetIt.I.get<CourseProvider>().getShowerVideosResponse(4);
        final response2 =
            await GetIt.I.get<CourseProvider>().getShowerReadingResponse(4);
        //getShowerLessonInfo
        final response =
            await GetIt.I.get<CourseProvider>().getShowerLessonInfo(4);
        info = response;
        videoList = response1.data.lessoneMedia;
        readList = response2.data.lessoneMedia;
        yield GetShowerContentAcceptState();
      } catch (ex) {
        yield GetShowerContentErrorState(ex.toString());
      }
    } else if (event is FetchShowerQuiz) {
      yield FetchShowerQuizAwaitState();
      try {
        final response2 = await GetIt.I.get<CourseProvider>().getQuiz(4);
        result = response2.data;
        yield FetchShowerQuizAcceptState();
      } catch (ex) {
        yield FetchShowerQuizErrorState(ex.toString());
      }
    } else if (event is CourseRating) {
      yield (RatingCourseAwaitState());

      try {
        final response = await GetIt.I
            .get<CourseProvider>()
            .changeRating(event.courseId, event.ratingValue);
        yield (RatingCourseAcceptState(response));
      } catch (ex) {
        yield (RatingCourseErrorState(ex));
      }
    }
    //likeCourse
    else if (event is LikeCourse) {
      yield (LikeCourseAwaitState());

      try {
        final response =
            await GetIt.I.get<CourseProvider>().likeCourse(event.courseId);
        final response2 = await GetIt.I
            .get<CourseProvider>()
            .getCourseDetails(event.courseId);
        yield (LikeCourseAcceptState(response2));
      } catch (ex) {
        yield (LikeCourseErrorState(ex));
      }
    } else if (event is UnLikeCourse){
      yield (UnLikeCourseAwaitState());
      try {
        final response =
            await GetIt.I.get<CourseProvider>().likeCourse(event.courseId);
        final response2 = await GetIt.I
            .get<CourseProvider>()
            .getCourseDetails(event.courseId);
        yield (UnLikeCourseAcceptState(response2));
      } catch (ex) {
        yield (UnLikeCourseErrorState(ex));
      }
    } else if (event is CheckAnswer){
      yield (CheckAnswerAwaitState());
      try {
        // final response = await GetIt.I.get<CourseProvider>().likeCourse(event.courseId);
        // final response2 = await GetIt.I.get<CourseProvider>().getCourseDetails(event.courseId);
        yield (CheckAnswerAcceptState());
      } catch (ex) {
        yield (CheckAnswerErrorState(ex));
      }
    }   else if (event is ToggleWishList) {
      yield (ToggleCourseAwaitState());
      try {
        final response = await GetIt.I
            .get<CourseProvider>()
            .toggleWishCourse(event.courseId);
        yield (ToggleCourseAcceptState(response.message));
      } catch (ex) {
        yield (ToggleCourseErrorState(ex));
      }
    } else if (event is TestEvents) {
      yield (TestAwaitState());
      try {
        final response = await GetIt.I
            .get<CourseProvider>()
            .getTestResult(event.id, event.data);

        yield (TestAcceptState(response));
      } catch (ex) {
        yield (TestErrorState(ex));
      }
    }  else if (event is GetCartCourses) {
      yield (GetCartAwait());
      try {
        searchResult.clear();
        subTotal = 0;
        final response = await GetIt.I.get<CourseProvider>().getCartCourse();
        cartCourses = response.data.courses;
        subTotal = response.data.subtotal;
        yield (GetCartAccept());
      } catch (ex) {
        yield (GetCartError(ex.toString()));
      }
    }
  }
}
