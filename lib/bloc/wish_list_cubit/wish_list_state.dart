part of 'wish_list_cubit.dart';

@immutable
abstract class WishListState {}

class WishListInitial extends WishListState {}

class GetWishListAwait  extends WishListState {}
class GetWishListAccept  extends WishListState {}
class GetWishListError  extends WishListState {
  final String errorMessage;

  GetWishListError(this.errorMessage);

}
