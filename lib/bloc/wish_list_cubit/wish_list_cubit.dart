import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:e_learning/bloc/course_bloc/course_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:meta/meta.dart';

import '../../models/common/course_model.dart';
import '../../resources/provider/course_provider.dart';

part 'wish_list_state.dart';

class WishListCubit extends Cubit<WishListState> {
  int currentPage;
  int totalPage;
  List<Courses> wishList = [];

  WishListCubit() : super(WishListInitial());

  Future<void> fetch() async {
    try {
      emit(GetWishListAwait());
      final response = await GetIt.I.get<CourseProvider>().getWishList(1);
      wishList = response.data.courses;
      currentPage = response.data.currentPage;
      totalPage=response.data.totalPages;
      emit(GetWishListAccept());
    } on DioError catch (ex) {
      emit(GetWishListError(ex.error));
    }
  }

  Future<void> fetchMore() async {
    if (currentPage < totalPage) {
      try {
        currentPage = currentPage++;
        emit(GetWishListAwait());
        final response =
            await GetIt.I.get<CourseProvider>().getWishList(currentPage);
        wishList += response.data.courses;
        currentPage = response.data.currentPage;
        totalPage=response.data.totalPages;
        emit(GetWishListAccept());
      } on DioError catch (ex) {
        emit(GetWishListError(ex.error));
      }
    }
  }

  Future<void> fetchLess() async {
if(currentPage>1){
  try {
    currentPage = currentPage--;
    emit(GetWishListAwait());
    final response = await GetIt.I.get<CourseProvider>().getWishList(1);
    wishList += response.data.courses;
    currentPage = response.data.currentPage;
    totalPage=response.data.totalPages;
    emit(GetWishListAccept());
  } on DioError catch (ex) {
    emit(GetWishListError(ex.error));
  }
}
  }
}
