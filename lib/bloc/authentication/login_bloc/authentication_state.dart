import 'package:e_learning/models/common/user_model.dart';

import '../../../models/response/otp_response.dart';

abstract class AuthState {}

class InitialAuthenticationState extends AuthState {}
//for check if auth
class AuthenticationSuccess extends AuthState {

}

class AuthenticationFail extends AuthState {}

//for auth

class LoginValidationState extends AuthState {}

class LoginAwaitState extends AuthState {}

class LoginAcceptState extends AuthState {
  final OtpResponse response;

  LoginAcceptState(this.response);
}

class LoginErrorState extends AuthState {
  final String message;

  LoginErrorState(this.message);
}
//For logout
class LogoutAwaitState extends AuthState {}

class LogoutErrorState extends AuthState {
  final String message;

  LogoutErrorState(this.message);
}

class LogoutAcceptState extends AuthState{}