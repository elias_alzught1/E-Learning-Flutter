import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:e_learning/bloc/application_bloc/application_cubit.dart';
import 'package:e_learning/config/application.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get_navigation/src/snackbar/snackbar.dart';

import 'package:get_it/get_it.dart';
import 'package:phone_number/phone_number.dart';

import '../../../controller/data_store.dart';
import '../../../helpers/nav_context.dart';
import '../../../models/common/user_model.dart';
import '../../../resources/api_client.dart';

import '../../../resources/provider/auth_provider.dart';

import '../../profile_bloc/profile_bloc.dart';
import 'authentication_event.dart';
import 'authentication_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> with NavContext {
  final mobileNumberController = TextEditingController();
  final phoneNumberUtil = PhoneNumberUtil();

  AuthBloc() : super(InitialAuthenticationState());

  bool isValidMobileNumber = true;
  String errorMessageNumber;

  @override
  Future<void> close() {
    mobileNumberController.dispose();
    return super.close();
  }

  Future<void> _validate() async {
    String mobileNumber = mobileNumberController.text.trim();

    //validate for only syria
    try {
      isValidMobileNumber = await phoneNumberUtil.validate(
        mobileNumber,
        'SY',
      );
    } catch (e) {
      isValidMobileNumber = false;
    }

    errorMessageNumber = mobileNumber.isEmpty
        ? "filed_required_msg"
        : !isValidMobileNumber
            ? "error_mobile_number_msg"
            : null;
  }

  bool get valid => isValidMobileNumber;

  Future<String> getPhoneNumber()async{


    String nationalNumber = mobileNumberController.text.trim();
    var phoneNumber = await phoneNumberUtil.parse(
      nationalNumber,
      regionCode: 'SY',
    );
    return "0${phoneNumber.nationalNumber}";
  }

  @override
  Stream<AuthState> mapEventToState(event) async* {
    if (event is OnAuthCheck) {
      bool isLogined = await _onAuthCheck();

      if (isLogined) {
        // user = await DataStore.instance.getUser();
        BlocProvider.of<ProfileBloc>(Application.instance.context).user =
            await DataStore.instance.getUser();
        yield AuthenticationSuccess();
      } else {
        yield AuthenticationFail();
      }
    }
    if (event is LoginEvent) {
      await _validate();
      yield LoginValidationState();
      if (valid) {
        yield LoginAwaitState();
        // AppNavigator.pop();
        try {
          var response = await GetIt.I.get<AuthProvider>().login(
                await getPhoneNumber(),
              );

          yield LoginAcceptState(response);
        } on DioError catch (ex) {
          yield LoginErrorState(ex.message);
          if (ex.response != null) {
            yield LoginErrorState(ex.message);
          } else {
            var e = ex.error;
            if (e is ValidationException) {
              var errors = e.errors;
              if (errors['mobile_number'] != null) {
                isValidMobileNumber = false;
                errorMessageNumber = errors['mobile_number'].join(" ");
              }
              yield LoginValidationState();
            } else {

              yield LoginErrorState("$e");
            }
          }
        }

      }
    }

    if (event is LogOutEvent) {
      try {
        yield LogoutAwaitState();
        var response = await GetIt.I.get<AuthProvider>().logout();
        yield LogoutAcceptState();
        await DataStore.instance.deleteToken();
      } catch (ex) {
        yield LogoutErrorState(ex.toString());
      }
    }
  }

  Future<bool> _onAuthCheck() async {
    if (DataStore.instance.hasToken) {
      return true;
    } else {
      return false;
    }
  }

  Future<void> _getUserInfo() async {
    var response = await GetIt.I.get<AuthProvider>().getInformation();
    BlocProvider.of<ProfileBloc>(context).user = response.data.user;
    //  user = response.data.user;
    //DataStore.instance.saveUser(user);
  }
}
