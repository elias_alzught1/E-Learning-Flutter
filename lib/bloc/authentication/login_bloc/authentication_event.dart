import 'dart:io';

abstract class AuthEvent {}

class OnAuthCheck extends AuthEvent {}

//for login
class LoginEvent extends AuthEvent {}

class DeleteAccount extends AuthEvent {}

class LogOutEvent extends AuthEvent {}


