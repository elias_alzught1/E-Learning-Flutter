import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:get_it/get_it.dart';
import 'package:meta/meta.dart';
import 'package:phone_number/phone_number.dart';

import '../../../controller/data_store.dart';
import '../../../helpers/nav_context.dart';
import '../../../models/common/user_model.dart';
import '../../../resources/api_client.dart';
import '../../../resources/provider/auth_provider.dart';
import '../../profile_bloc/profile_bloc.dart';

part 'register_event.dart';

part 'register_state.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState>  with NavContext{
  final mobileNumberController = TextEditingController();
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final phoneNumberUtil = PhoneNumberUtil();

  RegisterBloc() : super(RegisterInitial());
  bool isValidMobileNumber = true;
  bool isValidFirstName = true;
  bool isValidLastName = true;

  String errorMobileNumber;
  String errorFirstName;
  String errorLastName;

  @override
  Future<void> close() {
    mobileNumberController.dispose();
    firstNameController.dispose();
    lastNameController.dispose();
    return super.close();
  }

  Future<void> _validate() async {
    String mobileNumber = mobileNumberController.text.trim();
    String firstName = firstNameController.text.trim();
    String lastName = lastNameController.text.trim();
    //validate for only syria
    try {
      // isValidMobileNumber = await phoneNumberUtil.validate(
      //   mobileNumber,
      //   '963',
      // );
    } catch (e) {
      isValidMobileNumber = false;
    }
    isValidFirstName = firstName.isNotEmpty;
    isValidLastName = lastName.isNotEmpty;
    isValidMobileNumber =mobileNumber.isNotEmpty;
     errorMobileNumber = mobileNumber.isEmpty
        ? "filed_required_msg"
        : !isValidMobileNumber
            ? "error_mobile_number_msg"
            : null;
    errorFirstName = firstName.isEmpty ? "filed_required_msg" : null;
    errorLastName = lastName.isEmpty ? "filed_required_msg" : null;
  }

  bool get valid => isValidMobileNumber && isValidFirstName && isValidLastName;

  Future<String> getPhoneNumber() async {
    String nationalNumber = mobileNumberController.text.trim();
    var phoneNumber = await phoneNumberUtil.parse(
      nationalNumber,
      regionCode: 'SY',
    );
    return phoneNumber.e164;
  }

  @override
  Stream<RegisterState> mapEventToState(event) async* {
    if (event is RegisterCommitEvent) {
      await _validate();
      yield RegisterValidationState();
      if (valid) {
        yield RegisterAwaitState();
        try {
          var response = await GetIt.I.get<AuthProvider>().register(
              await getPhoneNumber(),
              firstNameController.text.trim(),
              lastNameController.text.trim());
          BlocProvider.of<ProfileBloc>(context).user = response.data.user;
          DataStore.instance.saveUser(response.data.user);
          await DataStore.instance.putToken(response.data.token);
          yield RegisterAcceptState();
        } on DioError catch (ex) {
          if (ex.response != null) {
            if (ex.response.statusCode == 401) {
              yield RegisterErrorState(ex.message);
            } else {
              yield RegisterErrorState(ex.message);
            }
          } else {
            var e = ex.error;
            if (e is ValidationException) {
              var errors = e.errors;
              if (errors['mobile_number'] != null) {
                isValidMobileNumber = false;
                errorMobileNumber = errors['mobile_number'].join(" ");
              }
              yield RegisterValidationState();
            } else {
            String e=ex.message;
              yield RegisterErrorState("$e");
            }
          }
        }
  }
    }
    if (event is RegisterEvent) {}
  }
}
