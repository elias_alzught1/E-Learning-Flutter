part of 'category_cubit.dart';

@immutable
abstract class CategoryState {}

class CategoryInitial extends CategoryState {}

class CategoryAwaitState extends CategoryState {
  final bool isFirstFetch;

  CategoryAwaitState(this.isFirstFetch);
}

class CategoryAcceptState extends CategoryState {}

class CategoryErrorState extends CategoryState {
  final String message;
  CategoryErrorState(this.message);
}

class SearchAwaitState extends CategoryState {
  final bool isFirstFetch;

  SearchAwaitState(this.isFirstFetch);
}

class SearchErrorState extends CategoryState {
  final String message;
  SearchErrorState(this.message);
}

class SearchAcceptState extends CategoryState {}
