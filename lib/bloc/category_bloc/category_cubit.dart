import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:e_learning/resources/provider/category_provider.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:meta/meta.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../models/common/category_model.dart';

part 'category_state.dart';

class CategoryCubit extends Cubit<CategoryState> {
  final RefreshController controller = RefreshController();
  final TextEditingController searchController = TextEditingController();
  final List<String> _suggestions = [];

  bool get enablePullUp => totalPage > page;

  bool get enableSearchPullUp => totalPageSearch > pageSearch;

  CategoryCubit() : super(CategoryInitial()) {
    searchController.addListener(setupSearch);
  }

  List<Category> categories = [] ;
  List<Category> categoriesSearch = [];
  int page = 1;
  int pageSearch = 1;
  int totalPage;
  int totalPageSearch;
  String _searchSubstring = "";
  Timer _timer;

  @override
  Future<void> close() {
    searchController.dispose();
    controller.dispose();
    return super.close();
  }

  void removeSuggestion(String value) {
    _suggestions.remove(value);
    // emit(SearchSuggestionsState());
  }

  void setupSearch() {
    var substring = searchController.text.trim();
    if (_searchSubstring == substring) return;

    _timer?.cancel();
    if (substring.length >= 3) {
      _timer = Timer(
          const Duration(milliseconds: 500), () => applySearch(substring));
    } else {
      emit(CategoryInitial());
    }
  }

  void applySearch(String value) {
    _searchSubstring = value;
    _suggestions.remove(value);
    _suggestions.insert(0, value);
    search();
  }

  List<String> getSuggestions() {
    final substring = searchController.text.trim();
    final regex = RegExp(substring, caseSensitive: false);
    return _suggestions
        .where((element) => element.contains(regex))
        .take(5)
        .toList();
  }

  Future<void> fetch() async {
    try {
      emit(CategoryAwaitState(page == 1));
      var response = await GetIt.I.get<CategoryProvider>().getCategory(page);
      totalPage = response.data.totalPages;
      categories=response.data.categories;
      page++;
      controller.loadComplete();
      emit(CategoryAcceptState());
    } on DioError catch (ex) {
      controller.loadFailed();
      emit(CategoryErrorState(ex.error));
    }
  }

  Future<void> search() async {
    try {
      emit(SearchAwaitState(true));
      var response = await GetIt.I.get<CategoryProvider>().fetchSearch(
          pageSearch, _searchSubstring, _searchSubstring, _searchSubstring);
      totalPageSearch = response.data.totalPages;
      categoriesSearch.addAll(response.data.categories);
      pageSearch++;
      controller.loadComplete();
      emit(SearchAcceptState());
    } on DioError catch (ex) {
      controller.loadFailed();
      emit(SearchErrorState(ex.error));
    }
  }

  Future<void> refresh() async {
    try {
      emit(CategoryAwaitState(page == 1));
      var response = await GetIt.I.get<CategoryProvider>().getCategory(page);
      totalPage = response.data.totalPages;
      categories=response.data.categories;
      for (int i = 1; i < 39; i++) {
        categories.addAll(response.data.categories);
      }
      page++;
      controller.refreshCompleted();
      emit(CategoryAcceptState());
    } on DioError catch (ex) {
      controller.refreshFailed();
      emit(CategoryErrorState(ex.error));
    }
  }


}
