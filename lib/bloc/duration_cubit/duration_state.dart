part of 'duration_cubit.dart';

@immutable
abstract class DurationState {}

class DurationInitial extends DurationState {}

class GetDurationAwaitState extends DurationState {}

class GetDurationAcceptState extends DurationState {}

class GetDurationErrorState extends DurationState {
  final String message;

  GetDurationErrorState(this.message);
}
