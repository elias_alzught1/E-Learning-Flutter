import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:e_learning/models/response/durations_response.dart';

import 'package:get_it/get_it.dart';
import 'package:meta/meta.dart';

import '../../resources/provider/duration_provider.dart';

part 'duration_state.dart';

class DurationCubit extends Cubit<DurationState> {
  DurationCubit() : super(DurationInitial());
  List<Durations>  durations=[];
  Future<void> fetch() async {
    try {
      emit(GetDurationAwaitState());
      var response = await GetIt.I.get<DurationProvider>().getDurations();

      durations = response.data.durations;
      emit(GetDurationAcceptState());
    } on DioError catch (ex) {
      emit(GetDurationErrorState(ex.error));
    }
  }
}
