part of 'quiz_bloc.dart';

@immutable
abstract class QuizState {}

class QuizInitial extends QuizState {}
class GetResultAwaitState  extends QuizState {}
class GetResultAcceptState  extends QuizState {}
class GetResultErrorState  extends QuizState {
  final String message;
  GetResultErrorState(this.message);
}