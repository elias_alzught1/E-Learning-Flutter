part of 'quiz_bloc.dart';

@immutable
abstract class QuizEvent {}

class GetResult extends  QuizEvent {}
