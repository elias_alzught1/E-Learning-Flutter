import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';

import '../../localization/app_localization.dart';
import '../../themes/app_colors.dart';

class ReloadWidget extends StatelessWidget {
  final String content;
  final VoidCallback onPressed;


    ReloadWidget(  this.content, this.onPressed) ;



  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return SizedBox(
      height: 180,
      width: 200,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            content,
            style: TextStyle(
              color: Theme.of(context).textTheme.bodyText1.color,
              fontSize: 12.sp,
              fontWeight: FontWeight.w600,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          ElevatedButton(
              onPressed: () {
                onPressed;
              },

              child: Text(
                AppLocalization.of(context).trans("retry"),
                style: TextStyle(fontSize: 15.sp, color: AppColors.black),
              )),
        ],
      ),
    );
  }
}
