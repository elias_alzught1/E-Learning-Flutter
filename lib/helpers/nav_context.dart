import 'package:flutter/material.dart';

import '../config/application.dart';

mixin NavContext {
  BuildContext get context => Application.instance.globalKey.currentContext;
}