import 'package:e_learning/localization/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import '../themes/app_colors.dart';


Widget getShimmer({double height, double width}) {
  return Shimmer.fromColors(
    baseColor: Colors.grey[300],
    highlightColor: Colors.grey[100],
    enabled: true,
    period: const Duration(milliseconds: 1500),
    child: Container(
      color: AppColors.blue,
      height: height,
      width: width,
    ),
  );
}
class ExpandedSingleChildScrollView extends StatelessWidget {
  final Widget child;

  const ExpandedSingleChildScrollView({
    Key  key,
      this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraint) {
        return SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(minHeight: constraint.maxHeight),
            child: child,
          ),
        );
      },
    );
  }
}
Widget imageErrorBuilder(ctx, error, _) =>  Container(
      color: AppColors.gray,

    );


