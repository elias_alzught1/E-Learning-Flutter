import 'package:e_learning/ui/widgets/loader/inkdrop_loader.dart';
import 'package:flutter/material.dart';

class StackLoaderIndicator {
  GlobalKey _key;

  Future show(BuildContext context, {bool dismissible = false}) {
    if (_key != null) return null;
    _key = GlobalKey();
    return showDialog(
      context: context,
      builder: (context) {
        return WillPopScope(
          key: _key,
          onWillPop: () async => dismissible,
          child: InkDropLoader(),
        );
      },
      barrierDismissible: dismissible,
    );
  }

  bool dismiss() {
    if (_key == null) {
      return false;
    } else {
      if (_key.currentState != null) {
        Navigator.of(_key.currentContext).pop();
        _key = null;
        return true;
      } else {
        _key = null;
        return false;
      }
    }
  }
}
