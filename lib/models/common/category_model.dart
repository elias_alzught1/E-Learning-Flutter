class Category {
  int     id;
  String  name;
  String  nameAr;
  String  nameEn;
  String  shortname;
  String shortnameAr;
  String  shortnameEn;
  String  cODE;

  Category(
      {
        this.id,
        this.name,
        this.nameAr,
        this.nameEn,
        this.shortname,
        this.shortnameAr,
        this.shortnameEn,
        this.cODE
      });

  factory Category.fromJson(Map<String, dynamic> json)=> Category (
    id :json['id'],
    name : json['name'],
    nameAr : json['name_ar'],
    nameEn : json['name_en'],
    shortname : json['shortname'],
    shortnameAr : json['shortname_ar'],
    shortnameEn : json['shortname_en'],
    cODE : json['CODE'],
  );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['name_ar'] = this.nameAr;
    data['name_en'] = this.nameEn;
    data['shortname'] = this.shortname;
    data['shortname_ar'] = this.shortnameAr;
    data['shortname_en'] = this.shortnameEn;
    data['CODE'] = this.cODE;
    return data;
  }
}