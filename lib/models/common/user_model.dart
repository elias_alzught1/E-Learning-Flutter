import 'dart:convert';

import '../../public/constants.dart';

class User {
  int id;
  String firstName;
  String lastName;
  String username;
  String mSISDN;
  String profileImage;
  String email;
  String metaData;
  String role;

  User(
      {this.id,
      this.firstName,
      this.lastName,
      this.username,
      this.mSISDN,
      this.profileImage,
      this.email,
      this.metaData,
      this.role});

  User.fromJson(Map<String, dynamic> json){
    id = json['id'];
    firstName = json['first_name'] ?? " ";
    lastName = json['last_name'] ?? " ";
    username = json['username'] ?? " ";
    mSISDN = json['MSISDN'] ?? " ";
    profileImage = json['profile_image'] ?? Constants.urlImageDefault;
    email = json['email'] ?? " ";
    metaData = json['metaData'] ?? " ";
    role = json['role'] ?? " ";
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['username'] = this.username;
    data['MSISDN'] = this.mSISDN;
    data['profile_image'] = this.profileImage;
    data['email'] = this.email;
    data['metaData'] = this.metaData;
    return data;
  }

  factory User.fromMap(String source) => User.fromJson(json.decode(source));

  String toMap() => json.encode(toJson());
}
