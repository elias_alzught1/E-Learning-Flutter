class Levels {
  int  id;
  String levelAr;
  String  levelEn;
  int numberOfCourses;

  Levels({this.id, this.levelAr, this.levelEn, this.numberOfCourses});

  factory Levels.fromJson(Map<String, dynamic> json) =>Levels (
    id : json['id'],
    levelAr : json['level_ar'],
    levelEn : json['level_en'],
    numberOfCourses :  json['number_of_courses'],
  );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['level_ar'] = this.levelAr;
    data['level_en'] = this.levelEn;
    data['number_of_courses'] = this.numberOfCourses;
    return data;
  }
}