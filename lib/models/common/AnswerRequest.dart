
class AnswerRequest {
  int  testId;
  List<UserAnswers>    userAnswers;

  AnswerRequest({this.testId, this.userAnswers});

  // AnswerRequest.fromJson(Map<String, dynamic> json) {
  //   testId = json['test_id'];
  //   if (json['userAnswers'] != null) {
  //     userAnswers = <UserAnswers>[];
  //     json['userAnswers'].forEach((v) {
  //       userAnswers .add(new UserAnswers.fromJson(v));
  //     });
  //   }
  // }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['test_id'] = this.testId;
    if (this.userAnswers != null) {
      data['userAnswers'] = this.userAnswers .map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class UserAnswers {
  int  answerId;
  int  questionId;

  UserAnswers({this.answerId, this.questionId});

  UserAnswers.fromJson(Map<String, dynamic> json) {
    answerId = json['answer_id'];
    questionId = json['question_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['answer_id'] = this.answerId;
    data['question_id'] = this.questionId;
    return data;
  }
}

