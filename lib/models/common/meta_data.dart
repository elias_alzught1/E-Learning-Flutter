class MetaData {
  String  positionEn;
  String  positionAr;
  String  descriptionEn;
  String  descriptionAr;

  MetaData(
      {this.positionEn,
        this.positionAr,
        this.descriptionEn,
        this.descriptionAr});

  MetaData.fromJson(Map<String, dynamic> json) {
    positionEn = json['position_en'] ??" ";
    positionAr = json['position_ar']??" ";
    descriptionEn = json['description_en']??" ";
    descriptionAr = json['description_ar']??" ";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['position_en'] = this.positionEn;
    data['position_ar'] = this.positionAr;
    data['description_en'] = this.descriptionEn;
    data['description_ar'] = this.descriptionAr;
    return data;
  }
}
