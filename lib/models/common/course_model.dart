import 'package:e_learning/models/common/auther_model.dart';

import '../response/levels_response.dart';

class Courses {
  int id;
  String name;
  String nameAr;
  String nameEn;
  String shortname;
  String shortnameAr;
  String shortnameEn;
  String description;
  String descriptionAr;
  String descriptionEn;
  String language;
  String code;
  bool visible;
  String startDate;
  String endDate;
  String image;
  int price;
  Duartion duartion;
  Level level;
  bool isLike;
  bool inWishList;
  bool isSubscribe;
  String rating;
  List<Categories> categories;
  List<Aims> aims;
  List<Includes> includes;
  List<Authors> authors;
  List<Introudction> introudction;
  List<Prerequisites> prerequisites;
  List<TarqetAudience> tarqetAudience;
  int userRating;
  List<dynamic> ratingDetails;
  int lessonsCount;
  int currentLesson;
  List<Courses>  relatedCourses;
  Courses(
      {this.id,
      this.name,
      this.nameAr,
      this.nameEn,
      this.shortname,
      this.shortnameAr,
      this.shortnameEn,
      this.description,
      this.descriptionAr,
      this.descriptionEn,
      this.language,
      this.code,
      this.visible,
      this.startDate,
      this.endDate,
      this.image,
      this.price,
      this.duartion,
      this.level,
      this.isLike,
      this.inWishList,
      this.isSubscribe,
      this.rating,
      this.categories,
      this.aims,
      this.includes,
      this.introudction,
      this.prerequisites,
      this.tarqetAudience,
      this.ratingDetails,
      this.userRating,
      this.currentLesson,
      this.lessonsCount,
      this.authors});

  Courses.fromJson(Map<String, dynamic> json) {
    currentLesson = 1;
    lessonsCount = 2;
    id = json['id'];
    name = json['name'];
    nameAr = json['name_ar'];
    nameEn = json['name_en'];
    shortname = json['shortname'];
    shortnameAr = json['shortname_ar'];
    shortnameEn = json['shortname_en'];
    description = json['description'];
    descriptionAr = json['description_ar'];
    descriptionEn = json['description_en'];
    language = json['language'];
    code = json['code'];
    visible = json['visible'];
    startDate = json['start_date'];
    endDate = json['end_date'];
    image = json['image'];
    price = json['price'];

    duartion = json['durations'] != null
        ? new Duartion.fromJson(json['durations'])
        : null;
    level = json['level'] != null ? new Level.fromJson(json['level']) : null;
    isLike = json['isLike'];
    inWishList = json['inWishList'];
    isSubscribe = json['isSubscribe'];
    ratingDetails = json['ratingDetails'];
    userRating = json['user_rating'];
    rating = json['rating'].toString();
    if (json['categories'] != null) {
      categories = <Categories>[];
      json['categories'].forEach((v) {
        categories.add(new Categories.fromJson(v));
      });
    }
    if (json['authors'] != null) {
      authors = [];

      json['authors'].forEach((v) {

        authors.add(new Authors.fromJson(v));

      });
    }
    if (json['relatedCourses'] != null) {
      relatedCourses = <Courses>[];
      json['relatedCourses'].forEach((v) {
        relatedCourses .add(new Courses.fromJson(v));
      });
    }
    if (json['aims'] != null) {
      aims = <Aims>[];
      json['aims'].forEach((v) {
        aims.add(new Aims.fromJson(v));
      });
    }
    if (json['includes'] != null) {
      includes = <Includes>[];
      json['includes'].forEach((v) {
        includes.add(new Includes.fromJson(v));
      });
    }
    if (json['introudction'] != null) {
      introudction = <Introudction>[];
      json['introudction'].forEach((v) {
        introudction.add(new Introudction.fromJson(v));
      });
    }
    if (json['prerequisites'] != null) {
      prerequisites = <Prerequisites>[];
      json['prerequisites'].forEach((v) {
        prerequisites.add(new Prerequisites.fromJson(v));
      });
    }
    if (json['targetAudience'] != null) {
      tarqetAudience = <TarqetAudience>[];
      json['targetAudience'].forEach((v) {
        tarqetAudience.add(new TarqetAudience.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['name_ar'] = this.nameAr;
    data['name_en'] = this.nameEn;
    data['shortname'] = this.shortname;
    data['shortname_ar'] = this.shortnameAr;
    data['shortname_en'] = this.shortnameEn;
    data['description'] = this.description;
    data['description_ar'] = this.descriptionAr;
    data['description_en'] = this.descriptionEn;
    data['language'] = this.language;
    data['code'] = this.code;
    data['visible'] = this.visible ?? "0";
    data['start_date'] = this.startDate;
    data['end_date'] = this.endDate;
    data['image'] = this.image;
    data['price'] = this.price;
    if (this.duartion != null) {
      data['duartion'] = this.duartion.toJson();
    }
    if (this.level != null) {
      data['level'] = this.level.toJson();
    }
    data['isLike'] = this.isLike;
    data['inWishList'] = this.inWishList;
    data['isSubscribe'] = this.isSubscribe;
    data['rating'] = this.rating;
    if (this.categories != null) {
      data['categories'] = this.categories.map((v) => v.toJson()).toList();
    }
    if (this.aims != null) {
      data['aims'] = this.aims.map((v) => v.toJson()).toList();
    }
    if (this.includes != null) {
      data['includes'] = this.includes.map((v) => v.toJson()).toList();
    }
    if (this.introudction != null) {
      data['introudction'] = this.introudction.map((v) => v.toJson()).toList();
    }
    if (this.prerequisites != null) {
      data['prerequisites'] =
          this.prerequisites.map((v) => v.toJson()).toList();
    }
    if (this.tarqetAudience != null) {
      data['tarqetAudience'] =
          this.tarqetAudience.map((v) => v.toJson()).toList();
    }

    return data;
  }
}

class Duartion {
  int id;
  String durationAr;
  String durationEn;
  int numberOfCourses;

  Duartion({this.id, this.durationAr, this.durationEn, this.numberOfCourses});

  Duartion.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    durationAr = json['duration_ar'];
    durationEn = json['duration_en'];
    numberOfCourses = json['number_of_courses'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['duration_ar'] = this.durationAr;
    data['duration_en'] = this.durationEn;
    data['number_of_courses'] = this.numberOfCourses;
    return data;
  }
}



class Categories {
  int id;
  String name;
  String nameAr;
  String nameEn;
  String shortname;
  String shortnameAr;
  String shortnameEn;
  String code;
  int coursesCount;

  Categories(
      {this.id,
      this.name,
      this.nameAr,
      this.nameEn,
      this.shortname,
      this.shortnameAr,
      this.shortnameEn,
      this.code,
      this.coursesCount});

  Categories.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    nameAr = json['name_ar'];
    nameEn = json['name_en'];
    shortname = json['shortname'];
    shortnameAr = json['shortname_ar'];
    shortnameEn = json['shortname_en'];
    code = json['code'];
    coursesCount = json['courses_count'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['name_ar'] = this.nameAr;
    data['name_en'] = this.nameEn;
    data['shortname'] = this.shortname;
    data['shortname_ar'] = this.shortnameAr;
    data['shortname_en'] = this.shortnameEn;
    data['code'] = this.code;
    data['courses_count'] = this.coursesCount;
    return data;
  }
}

class Aims {
  int id;
  String aimAr;
  String aimEn;

  Aims({this.id, this.aimAr, this.aimEn});

  Aims.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    aimAr = json['aim_ar'];
    aimEn = json['aim_en'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['aim_ar'] = this.aimAr;
    data['aim_en'] = this.aimEn;
    return data;
  }
}

class Includes {
  int id;
  String includeAr;
  String includeEn;

  Includes({this.id, this.includeAr, this.includeEn});

  Includes.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    includeAr = json['include_ar'];
    includeEn = json['include_en'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['include_ar'] = this.includeAr;
    data['include_en'] = this.includeEn;
    return data;
  }
}

class Introudction {
  int id;
  String descriptionEn;
  String descriptionAr;
  String media;

  Introudction({this.id, this.descriptionEn, this.descriptionAr, this.media});

  Introudction.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    descriptionEn = json['description_en'];
    descriptionAr = json['description_ar'];
    media = json['media'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['description_en'] = this.descriptionEn;
    data['description_ar'] = this.descriptionAr;
    data['media'] = this.media;
    return data;
  }
}

class Prerequisites {
  int id;
  String prerequisiteEn;
  String prerequisiteAr;

  Prerequisites({this.id, this.prerequisiteEn, this.prerequisiteAr});

  Prerequisites.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    prerequisiteEn = json['prerequisite_en'];
    prerequisiteAr = json['prerequisite_ar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['prerequisite_en'] = this.prerequisiteEn;
    data['prerequisite_ar'] = this.prerequisiteAr;
    return data;
  }
}

class TarqetAudience {
  int id;
  String targetEn;
  String targetAr;

  TarqetAudience({this.id, this.targetEn, this.targetAr});

  TarqetAudience.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    targetEn = json['target_en'];
    targetAr = json['target_ar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['target_en'] = this.targetEn;
    data['target_ar'] = this.targetAr;
    return data;
  }
}
