import 'package:e_learning/models/common/meta_data.dart';

class Authors {
  int id;
  String  firstName;
  String  lastName;
  String  mSISDN;
  String  profileImage;
  String  email;
  MetaData  metaData;

  Authors(
      {this.id,
        this.firstName,
        this.lastName,
        this.mSISDN,
        this.profileImage,
        this.email,
        this.metaData});

  Authors.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    mSISDN = json['MSISDN'];
    profileImage = json['profile_image'];
    email = json['email'];
    metaData = json['metaData'] != null
        ? new MetaData.fromJson(json['metaData'])
        : MetaData(descriptionAr: "",descriptionEn: "",positionAr: "",positionEn: "");
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['MSISDN'] = this.mSISDN;
    data['profile_image'] = this.profileImage;
    data['email'] = this.email;
    if (this.metaData != null) {
      data['metaData'] = this.metaData.toJson();
    }
    return data;
  }
}