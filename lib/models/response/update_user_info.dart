class UpdateUserInfoResponse {
  Data  data;
  String  message;
  int  code;

  UpdateUserInfoResponse({this.data, this.message, this.code});

  UpdateUserInfoResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data .toJson();
    }
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}

class Data {
  int  id;
  String  firstName;
  String lastName;
  String  username;
  String  mSISDN;
  String  profileImage;
  String   email;
  int  rating;
  String  metaData;

  Data(
      {this.id,
        this.firstName,
        this.lastName,
        this.username,
        this.mSISDN,
        this.profileImage,
        this.email,
        this.rating,
        this.metaData});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    username = json['username'];
    mSISDN = json['MSISDN'];
    profileImage = json['profile_image'];
    email = json['email'] ?? " ";
    rating = json['rating'];
    metaData = json['metaData']?? " ";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['username'] = this.username;
    data['MSISDN'] = this.mSISDN;
    data['profile_image'] = this.profileImage;
    data['email'] = this.email;
    data['rating'] = this.rating;
    data['metaData'] = this.metaData;
    return data;
  }
}