class QuizResponse {
  List<QuizQuestions> data;
  String message;
  int code;

  QuizResponse({this.data, this.message, this.code});

  QuizResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <QuizQuestions>[];
      json['data'].forEach((v) {
        data .add(new QuizQuestions.fromJson(v));
      });
    }
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}

class QuizQuestions {
  int id;
  String title;
  String titleAr;
  String titleEn;
  String description;
  String descriptionAr;
  String descriptionEn;
  String duration;
  int questionCount;
  List<Questions> questions;

  QuizQuestions(
      {this.id,
        this.title,
        this.titleAr,
        this.titleEn,
        this.description,
        this.descriptionAr,
        this.descriptionEn,
        this.duration,
        this.questionCount,
        this.questions});

  QuizQuestions.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    titleAr = json['title_ar'];
    titleEn = json['title_en'];
    description = json['description'];
    descriptionAr = json['description_ar'];
    descriptionEn = json['description_en'];
    duration = json['duration'];
    questionCount = json['questionCount'];
    if (json['questions'] != null) {
      questions = <Questions>[];
      json['questions'].forEach((v) {
        questions.add(new Questions.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['title_ar'] = this.titleAr;
    data['title_en'] = this.titleEn;
    data['description'] = this.description;
    data['description_ar'] = this.descriptionAr;
    data['description_en'] = this.descriptionEn;
    data['duration'] = this.duration;
    data['questionCount'] = this.questionCount;
    if (this.questions != null) {
      data['questions'] = this.questions.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Questions {
  int  id;
  String questionText;
  // Null? questionMedia;
  // Null? description;
  List<Answers> answers;

  Questions(
      {this.id,
        this.questionText,

        this.answers});

  Questions.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    questionText = json['question_text'];

    if (json['answers'] != null) {
      answers = <Answers>[];
      json['answers'].forEach((v) {
        answers.add(new Answers.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['question_text'] = this.questionText;

    if (this.answers != null) {
      data['answers'] = this.answers.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Answers {
  int   id;
  String  value;


  Answers({this.id, this.value});

  Answers.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    value = json['value'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['value'] = this.value;

    return data;
  }
}