import '../common/course_model.dart';

class CourseResponse {
  Data data;
  String message;
  int code;

  CourseResponse({this.data, this.message, this.code});

  factory CourseResponse.fromJson(Map<String, dynamic> json) =>
      CourseResponse(
          data: json['data'] != null ? Data.fromJson(json['data']) : null,
          message: json['message'],
          code: json['code']);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['message'] = message;
    data['code'] = code;
    return data;
  }
}

class Data {
  List<Courses> courses;
  int totalPages;
  int currentPage;

  Data({this.courses, this.totalPages, this.currentPage});

  factory Data.fromJson(Map<String, dynamic> json)=>
      Data(


          courses: List<Courses>.from(
              json["courses"].map((x) => Courses.fromJson(x))),
          totalPages: json['total_pages'],
          currentPage: json['current_page']);



Map<String, dynamic> toJson() {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  if (courses != null) {
    data['courses'] = this.courses.map((v) => v.toJson()).toList();
  }
  data['total_pages'] = this.totalPages;
  data['current_page'] = this.currentPage;
  return data;
}}
