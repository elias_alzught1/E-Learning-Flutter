class AdsResponse {
  List<AdsItem> adsItem;
  String message;
  int code;

  AdsResponse({this.adsItem, this.message, this.code});

  AdsResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      adsItem = <AdsItem>[];
      json['data'].forEach((v) {
        adsItem.add(new AdsItem.fromJson(v));
      });
    }
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.adsItem != null) {
      data['data'] = this.adsItem.map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}

class AdsItem {
  int id;
  String mediaUrl;
  String titleAr;
  String titleEn;
  String descriptionAr;
  String descriptionEn;
  Owner owner;

  AdsItem(
      {this.id,
      this.mediaUrl,
      this.titleAr,
      this.titleEn,
      this.descriptionAr,
      this.descriptionEn,
      this.owner});

  AdsItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    mediaUrl = json['media_url'];
    titleAr = json['title_ar'];
    titleEn = json['title_en'];
    descriptionAr = json['description_ar'];
    descriptionEn = json['description_en'];
    owner = json['owner'] != null ? new Owner.fromJson(json['owner']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['media_url'] = this.mediaUrl;
    data['title_ar'] = this.titleAr;
    data['title_en'] = this.titleEn;
    data['description_ar'] = this.descriptionAr;
    data['description_en'] = this.descriptionEn;
    if (this.owner != null) {
      data['owner'] = this.owner.toJson();
    }
    return data;
  }
}

class Owner {
  int id;
  String firstName;
  String lastName;
  String username;
  String mSISDN;
  String profileImage;
  String email;
  String rating;
  String role;
  String metaData;

  Owner(
      {this.id,
      this.firstName,
      this.lastName,
      this.username,
      this.mSISDN,
      this.profileImage,
      this.email,
      this.rating,
      this.role,
      this.metaData});

  Owner.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    username = json['username'];
    mSISDN = json['MSISDN'];
    profileImage = json['profile_image'];
    email = json['email'];
    rating = json['rating'];
    role = json['role'];
    metaData = json['metaData'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['username'] = this.username;
    data['MSISDN'] = this.mSISDN;
    data['profile_image'] = this.profileImage;
    data['email'] = this.email;
    data['rating'] = this.rating;
    data['role'] = this.role;
    data['metaData'] = this.metaData;
    return data;
  }
}
/////////////////////////////////////////////////////////////////////////////////////
// class AdsResponse {
//   List<AdsItem>  data;
//   String  message;
//   int  code;
//
//   AdsResponse({this.data, this.message, this.code});
//
//   AdsResponse.fromJson(Map<String, dynamic> json) {
//     if (json['data'] != null) {
//       data = <AdsItem>[];
//       json['data'].forEach((v) {
//         data .add(new AdsItem.fromJson(v));
//       });
//     }
//     message = json['message'];
//     code = json['code'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     if (this.data != null) {
//       data['data'] = this.data .map((v) => v.toJson()).toList();
//     }
//     data['message'] = this.message;
//     data['code'] = this.code;
//     return data;
//   }
// }
//
// class AdsItem {
//   int  id;
//   String  mediaUrl;
//   String  titleAr;
//   String  titleEn;
//   String  descriptionAr;
//   String  descriptionEn;
//
//   AdsItem(
//       {this.id,
//         this.mediaUrl,
//         this.titleAr,
//         this.titleEn,
//         this.descriptionAr,
//         this.descriptionEn});
//
//   AdsItem.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     mediaUrl = json['media_url'];
//     titleAr = json['title_ar'];
//     titleEn = json['title_en'];
//     descriptionAr = json['description_ar'];
//     descriptionEn = json['description_en'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['media_url'] = this.mediaUrl;
//     data['title_ar'] = this.titleAr;
//     data['title_en'] = this.titleEn;
//     data['description_ar'] = this.descriptionAr;
//     data['description_en'] = this.descriptionEn;
//     return data;
//   }
// }
