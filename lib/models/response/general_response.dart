import 'dart:convert';

GeneralResponse generalResponseFromJson(String str) =>
    GeneralResponse.fromJson(json.decode(str));

String generalResponseToJson(GeneralResponse data) =>
    json.encode(data.toJson());

class GeneralResponse {
  GeneralResponse({
    this.data,
    this.message,
    this.code,
  });

  dynamic data;
  String message;
  int code;

  factory GeneralResponse.fromJson(Map<String, dynamic> json) =>
      GeneralResponse(
        data: json["data"],
        message: json["message"] == null ? null : json["message"],
        code: json["code"] == null ? null : json["code"],
      );

  Map<String, dynamic> toJson() => {
        "data": data,
        "message": message == null ? null : message,
        "code": code == null ? null : code,
      };
}
