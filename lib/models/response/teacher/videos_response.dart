import 'package:e_learning/models/response/shower_response.dart';

class VideosResponse {
  Data data;
  String message;
  int code;

  VideosResponse({this.data, this.message, this.code});

  VideosResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}

class Data {
  List<LessoneMedia> lessoneMedia;

  Data({this.lessoneMedia});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['lessoneMedia'] != null) {
      lessoneMedia = <LessoneMedia>[];
      json['lessoneMedia'].forEach((v) {
        lessoneMedia.add(new LessoneMedia.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.lessoneMedia != null) {
      data['lessoneMedia'] = this.lessoneMedia.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

