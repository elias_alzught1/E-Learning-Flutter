class BasicResponse {
  String  data;
  String  message;
  int  code;

  BasicResponse({this.data, this.message, this.code});

  BasicResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'];
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['data'] = this.data;
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}