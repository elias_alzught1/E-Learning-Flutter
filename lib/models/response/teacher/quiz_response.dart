class QuizTeacherResponse {
  List<QuizModel>  data;
  String  message;
  int  code;

  QuizTeacherResponse({this.data, this.message, this.code});

  QuizTeacherResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <QuizModel>[];
      json['data'].forEach((v) {
        data .add(new QuizModel.fromJson(v));
      });
    }
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data .map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}

class QuizModel {
  int id;
  String  title;
  String  titleAr;
  String  titleEn;
  String  description;
  String  descriptionAr;
  String  descriptionEn;
  String  duration;
  int  questionCount;
  List<Questions>  questions;
  int tested;

  QuizModel(
      {this.id,
        this.title,
        this.titleAr,
        this.titleEn,
        this.description,
        this.descriptionAr,
        this.descriptionEn,
        this.duration,
        this.questionCount,
        this.questions,
        this.tested});

  QuizModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    titleAr = json['title_ar'];
    titleEn = json['title_en'];
    description = json['description'];
    descriptionAr = json['description_ar'];
    descriptionEn = json['description_en'];
    duration = json['duration'];
    questionCount = json['questionCount'];
    if (json['questions'] != null) {
      questions = <Questions>[];
      json['questions'].forEach((v) {
        questions .add(new Questions.fromJson(v));
      });
    }
    tested = json['tested'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['title_ar'] = this.titleAr;
    data['title_en'] = this.titleEn;
    data['description'] = this.description;
    data['description_ar'] = this.descriptionAr;
    data['description_en'] = this.descriptionEn;
    data['duration'] = this.duration;
    data['questionCount'] = this.questionCount;
    if (this.questions != null) {
      data['questions'] = this.questions.map((v) => v.toJson()).toList();
    }
    data['tested'] = this.tested;
    return data;
  }
}

class Questions {
  int id;
  String questionText;

  List<Answers> answers;

  Questions(
      {this.id,
        this.questionText,

        this.answers});

  Questions.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    questionText = json['question_text'];

    if (json['answers'] != null) {
      answers = <Answers>[];
      json['answers'].forEach((v) {
        answers.add(new Answers.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['question_text'] = this.questionText;

    if (this.answers != null) {
      data['answers'] = this.answers.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Answers {
  int id;
  String value;

  bool isCorrect;

  Answers({this.id, this.value, this.isCorrect});

  Answers.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    value = json['value'];

    isCorrect = json['is_correct'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['value'] = this.value;

    data['is_correct'] = this.isCorrect;
    return data;
  }
}