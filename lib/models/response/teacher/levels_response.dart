class LevelsResponse {
  Data  data;
  String  message;
  int  code;

  LevelsResponse({this.data, this.message, this.code});

  LevelsResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data .toJson();
    }
    data['message'] =  message;
    data['code'] =  code;
    return data;
  }
}

class Data {
  List<Levels>  levels;

  Data({this.levels});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['levels'] != null) {
      levels = <Levels>[];
      json['levels'].forEach((v) {
        levels .add(  Levels.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if ( levels != null) {
      data['levels'] =  levels .map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Levels {
  int id;
  String  levelAr;
  String levelEn;
  int  numberOfCourses;

  Levels({this.id, this.levelAr, this.levelEn, this.numberOfCourses});

  Levels.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    levelAr = json['level_ar'];
    levelEn = json['level_en'];
    numberOfCourses = json['number_of_courses'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] =   id;
    data['level_ar'] =  levelAr;
    data['level_en'] =  levelEn;
    data['number_of_courses'] =  numberOfCourses;
    return data;
  }
}