import 'lessons_response.dart';

class GetUpdateLessonResponse {
  LessonsTeacher data;
  String message;
  int code;

  GetUpdateLessonResponse({this.data, this.message, this.code});

  GetUpdateLessonResponse.fromJson(Map<String, dynamic> json) {
    data =
        json['data'] != null ?   LessonsTeacher.fromJson(json['data']) : null;
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =   Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data .toJson();
    }
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}
