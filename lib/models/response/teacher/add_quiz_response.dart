class AddQuizResponse {
  Data  data;
  String  message;
  int  code;

  AddQuizResponse({this.data, this.message, this.code});

  AddQuizResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data .toJson();
    }
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}

class Data {
  int  id;
  String  title;
  String  titleAr;
  String  titleEn;
  String  description;
  String descriptionAr;
  String  descriptionEn;
  String  duration;
  int  questionCount;

  int  tested;

  Data(
      {this.id,
        this.title,
        this.titleAr,
        this.titleEn,
        this.description,
        this.descriptionAr,
        this.descriptionEn,
        this.duration,
        this.questionCount,

        this.tested});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    titleAr = json['title_ar'];
    titleEn = json['title_en'];
    description = json['description'];
    descriptionAr = json['description_ar'];
    descriptionEn = json['description_en'];
    duration = json['duration'];
    questionCount = json['questionCount'];

    tested = json['tested'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['title_ar'] = this.titleAr;
    data['title_en'] = this.titleEn;
    data['description'] = this.description;
    data['description_ar'] = this.descriptionAr;
    data['description_en'] = this.descriptionEn;
    data['duration'] = this.duration;
    data['questionCount'] = this.questionCount;

    data['tested'] = this.tested;
    return data;
  }
}


