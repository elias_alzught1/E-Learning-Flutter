import 'package:e_learning/models/common/course_model.dart';

class SearchResponse {
  Data data;
  String message;
  int code;

  SearchResponse({this.data, this.message, this.code});

  SearchResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ?   Data.fromJson(json['data']) : null;
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =   <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['message'] =  message;
    data['code'] =  code;
    return data;
  }
}

class Data {
  List<Courses> courses;
  int totalPages;
  int currentPage;

  Data({this.courses, this.totalPages, this.currentPage});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['courses'] != null) {
      courses = <Courses>[];
      json['courses'].forEach((v) {
        courses.add(Courses.fromJson(v));
      });
    }
    totalPages = json['total_pages'];
    currentPage = json['current_page'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =   Map<String, dynamic>();
    if ( courses != null) {
      data['courses'] = courses.map((v) => v.toJson()).toList();
    }
    data['total_pages'] =  totalPages;
    data['current_page'] =  currentPage;
    return data;
  }
}
