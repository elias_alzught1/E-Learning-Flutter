class LessonsResponse {
  Data  data;
  String  message;
  int  code;

  LessonsResponse({this.data, this.message, this.code});

  LessonsResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data .toJson();
    }
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}

class Data {
  List<Lessons>  lessons;
  int   totalPages;
  int  currentPage;

  Data({this.lessons, this.totalPages, this.currentPage});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['Lessons'] != null) {
      lessons = <Lessons>[];
      json['Lessons'].forEach((v) {
        lessons .add(new Lessons.fromJson(v));
      });
    }
    totalPages = json['total_pages'];
    currentPage = json['current_page'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.lessons != null) {
      data['Lessons'] = this.lessons .map((v) => v.toJson()).toList();
    }
    data['total_pages'] = this.totalPages;
    data['current_page'] = this.currentPage;
    return data;
  }
}

class Lessons {
  int  id;
  String  title;
  String  titleAr;
  String  titleEn;
  String  description;
  String  descriptionAr;
  String  descriptionEn;
  String  image;
  List<LessonMedia>  lessonMedia;
  int  lessonMediaNumber;
  int  videosNumber;
  int  readsNumber;
  int  testsNumber;

  Lessons(
      {this.id,
        this.title,
        this.titleAr,
        this.titleEn,
        this.description,
        this.descriptionAr,
        this.descriptionEn,
        this.image,
        this.lessonMedia,
        this.lessonMediaNumber,
        this.videosNumber,
        this.readsNumber,
        this.testsNumber});

  Lessons.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    titleAr = json['title_ar'];
    titleEn = json['title_en'];
    description = json['description'];
    descriptionAr = json['description_ar'];
    descriptionEn = json['description_en'];
    image = json['image'];
    if (json['lessonMedia'] != null) {
      lessonMedia = <LessonMedia>[];
      json['lessonMedia'].forEach((v) {
        lessonMedia .add(new LessonMedia.fromJson(v));
      });
    }
    lessonMediaNumber = json['lesson_media_number'];
    videosNumber = json['videos_number'];
    readsNumber = json['reads_number'];
    testsNumber = json['tests_number'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['title_ar'] = this.titleAr;
    data['title_en'] = this.titleEn;
    data['description'] = this.description;
    data['description_ar'] = this.descriptionAr;
    data['description_en'] = this.descriptionEn;
    data['image'] = this.image;
    if (this.lessonMedia != null) {
      data['lessonMedia'] = this.lessonMedia .map((v) => v.toJson()).toList();
    }
    data['lesson_media_number'] = this.lessonMediaNumber;
    data['videos_number'] = this.videosNumber;
    data['reads_number'] = this.readsNumber;
    data['tests_number'] = this.testsNumber;
    return data;
  }
}

class LessonMedia {
  int id;
  String title;
  String titleAr;
  String titleEn;
  String duration;
  String mediaUrl;
  String mediaType;
  String description;
  String descriptionAr;
  String descriptionEn;

  LessonMedia({this.id,
    this.title,
    this.titleAr,
    this.titleEn,
    this.duration,
    this.mediaUrl,
    this.mediaType,
    this.description,
    this.descriptionAr,
    this.descriptionEn});

  LessonMedia.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    titleAr = json['title_ar'];
    titleEn = json['title_en'];
    duration = json['duration'];
    mediaUrl = json['media_url'];
    mediaType = json['media_type'];
    description = json['description'];
    descriptionAr = json['description_ar'];
    descriptionEn = json['description_en'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['title_ar'] = this.titleAr;
    data['title_en'] = this.titleEn;
    data['duration'] = this.duration;
    data['media_url'] = this.mediaUrl;
    data['media_type'] = this.mediaType;
    data['description'] = this.description;
    data['description_ar'] = this.descriptionAr;
    data['description_en'] = this.descriptionEn;
    return data;
  }
}