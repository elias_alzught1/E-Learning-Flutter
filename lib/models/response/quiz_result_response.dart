class TestResult {
  Data  data;
  String  message;
  int  code;

  TestResult({this.data, this.message, this.code});

  TestResult.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data .toJson();
    }
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}

class Data {
  List<Report>  report;
  int  correctAnswersNumber;
  int  incorrectAnswersNumber;
  int  emptyAnswersNumber;

  Data(
      {this.report,
        this.correctAnswersNumber,
        this.incorrectAnswersNumber,
        this.emptyAnswersNumber});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['report'] != null) {
      report = <Report>[];
      json['report'].forEach((v) {
        report .add(new Report.fromJson(v));
      });
    }
    correctAnswersNumber = json['correct_answers_number'];
    incorrectAnswersNumber = json['incorrect_answers_number'];
    emptyAnswersNumber = json['empty_answers_number'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.report != null) {
      data['report'] = this.report .map((v) => v.toJson()).toList();
    }
    data['correct_answers_number'] = this.correctAnswersNumber;
    data['incorrect_answers_number'] = this.incorrectAnswersNumber;
    data['empty_answers_number'] = this.emptyAnswersNumber;
    return data;
  }
}

class Report {
  String  question;
  String  userAnswer;
  String  expectedAnswer;
  bool  isCorrect;
  String  type;

  Report(
      {this.question,
        this.userAnswer,
        this.expectedAnswer,
        this.isCorrect,
        this.type});

  Report.fromJson(Map<String, dynamic> json) {
    question = json['question'];
    userAnswer = json['user_answer'];
    expectedAnswer = json['expected_answer'];
    isCorrect = json['isCorrect'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['question'] = this.question;
    data['user_answer'] = this.userAnswer;
    data['expected_answer'] = this.expectedAnswer;
    data['isCorrect'] = this.isCorrect;
    data['type'] = this.type;
    return data;
  }
}