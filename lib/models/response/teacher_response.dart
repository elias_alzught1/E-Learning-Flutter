import '../common/course_model.dart';

class TeacherResponse {
  Data  data;
  String  message;
  int  code;

  TeacherResponse({this.data, this.message, this.code});

  TeacherResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data .toJson();
    }
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}

class Data {
  int  id;
  String  firstName;
  String  lastName;
  String  username;
  String  mSISDN;
  String  profileImage;
  String  email;
  int  rating;
  MetaData  metaData;
  List<Courses>  courses;

  Data(
      {this.id,
        this.firstName,
        this.lastName,
        this.username,
        this.mSISDN,
        this.profileImage,
        this.email,
        this.rating,
        this.metaData,
        this.courses});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    username = json['username'];
    mSISDN = json['MSISDN'];
    profileImage = json['profile_image'] ??" ";
    email = json['email']??" ";
    rating = json['rating'] ??" ";
    metaData = json['metaData'] != null
        ? new MetaData.fromJson(json['metaData'])
        : MetaData(descriptionAr: "",descriptionEn: "",positionAr: "",positionEn: "");
    if (json['courses'] != null) {
      courses = <Courses>[];
      json['courses'].forEach((v) {
        courses .add(new Courses.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['username'] = this.username;
    data['MSISDN'] = this.mSISDN;
    data['profile_image'] = this.profileImage;
    data['email'] = this.email;
    data['rating'] = this.rating;
    if (this.metaData != null) {
      data['metaData'] = this.metaData .toJson();
    }
    if (this.courses != null) {
      data['courses'] = this.courses .map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class MetaData {
  String  positionEn;
  String  positionAr;
  String  descriptionEn;
  String  descriptionAr;

  MetaData(
      {this.positionEn,
        this.positionAr,
        this.descriptionEn,
        this.descriptionAr});

  MetaData.fromJson(Map<String, dynamic> json) {
    positionEn = json['position_en'];
    positionAr = json['position_ar'];
    descriptionEn = json['description_en'];
    descriptionAr = json['description_ar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['position_en'] = this.positionEn;
    data['position_ar'] = this.positionAr;
    data['description_en'] = this.descriptionEn;
    data['description_ar'] = this.descriptionAr;
    return data;
  }
}


