class ShowerResponse {
  Data  data;
  String  message;
  int  code;

  ShowerResponse({this.data, this.message, this.code});

  ShowerResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data .toJson();
    }
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}

class Data {
  List<LessoneMedia>  lessoneMedia;

  Data({this.lessoneMedia});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['lessoneMedia'] != null) {
      lessoneMedia = <LessoneMedia>[];
      json['lessoneMedia'].forEach((v) {
        lessoneMedia .add(new LessoneMedia.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.lessoneMedia != null) {
      data['lessoneMedia'] = this.lessoneMedia .map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class LessoneMedia {
  int  id;
  String  title;
  String  titleAr;
  String  titleEn;
  String   duration;
  String  mediaUrl;
  String  mediaType;
  String  description;
  String  descriptionAr;
  String descriptionEn;

  LessoneMedia(
      {this.id,
        this.title,
        this.titleAr,
        this.titleEn,
        this.duration,
        this.mediaUrl,
        this.mediaType,
        this.description,
        this.descriptionAr,
        this.descriptionEn});

  LessoneMedia.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    titleAr = json['title_ar'];
    titleEn = json['title_en'];
    duration = json['duration'];
    mediaUrl = json['media_url'];
    mediaType = json['media_type'];
    description = json['description'];
    descriptionAr = json['description_ar'];
    descriptionEn = json['description_en'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['title_ar'] = this.titleAr;
    data['title_en'] = this.titleEn;
    data['duration'] = this.duration;
    data['media_url'] = this.mediaUrl;
    data['media_type'] = this.mediaType;
    data['description'] = this.description;
    data['description_ar'] = this.descriptionAr;
    data['description_en'] = this.descriptionEn;
    return data;
  }
}