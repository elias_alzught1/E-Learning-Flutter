import '../common/course_model.dart';

class CourseDetailsResponse {
  Courses data;
  String message;
  int code;

  CourseDetailsResponse({this.data, this.message, this.code});

  CourseDetailsResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Courses.fromJson(json['data']) : null;
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}
