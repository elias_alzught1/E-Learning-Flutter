class LessonsInfo {
  Data data;
  String message;
  int code;

  LessonsInfo({this.data, this.message, this.code});

  LessonsInfo.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}

class Data {
  int id;
  String title;
  String titleAr;
  String titleEn;
  String description;
  String descriptionAr;
  String descriptionEn;
  String image;

  int lessonMediaNumber;
  int videosNumber;
  int readsNumber;
  int testsNumber;

  Data(
      {this.id,
      this.title,
      this.titleAr,
      this.titleEn,
      this.description,
      this.descriptionAr,
      this.descriptionEn,
      this.image,
      this.lessonMediaNumber,
      this.videosNumber,
      this.readsNumber,
      this.testsNumber});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    titleAr = json['title_ar'];
    titleEn = json['title_en'];
    description = json['description'];
    descriptionAr = json['description_ar'];
    descriptionEn = json['description_en'];
    image = json['image'];

    lessonMediaNumber = json['lesson_media_number'];
    videosNumber = json['videos_number'];
    readsNumber = json['reads_number'];
    testsNumber = json['tests_number'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['title_ar'] = this.titleAr;
    data['title_en'] = this.titleEn;
    data['description'] = this.description;
    data['description_ar'] = this.descriptionAr;
    data['description_en'] = this.descriptionEn;
    data['image'] = this.image;

    data['lesson_media_number'] = this.lessonMediaNumber;
    data['videos_number'] = this.videosNumber;
    data['reads_number'] = this.readsNumber;
    data['tests_number'] = this.testsNumber;
    return data;
  }
}
