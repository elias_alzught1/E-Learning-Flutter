class LevelsResponse {
  Data data;
  String message;
  int code;

  LevelsResponse({this.data, this.message, this.code});

  LevelsResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}

class Data {
  List<Level> levels;

  Data({this.levels});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['levels'] != null) {
      levels = <Level>[];
      json['levels'].forEach((v) {
        levels.add(new Level.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.levels != null) {
      data['levels'] = this.levels.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Level {
  int id;
  String levelAr;
  String levelEn;
  int numberOfCourses;

  Level({this.id, this.levelAr, this.levelEn, this.numberOfCourses});

  Level.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    levelAr = json['level_ar'];
    levelEn = json['level_en'];
    numberOfCourses = json['number_of_courses'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['level_ar'] = this.levelAr;
    data['level_en'] = this.levelEn;
    data['number_of_courses'] = this.numberOfCourses;
    return data;
  }
}
