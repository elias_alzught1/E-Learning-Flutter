class DurationsResponse {
  Data  data;
  String  message;
  int  code;

  DurationsResponse({this.data, this.message, this.code});

  DurationsResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data .toJson();
    }
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}

class Data {
  List<Durations>  durations;

  Data({this.durations});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['durations'] != null) {
      durations = <Durations>[];
      json['durations'].forEach((v) {
        durations .add(new Durations.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.durations != null) {
      data['durations'] = this.durations .map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Durations {
  int  id;
  String  durationAr;
  String  durationEn;
  int  numberOfCourses;

  Durations({this.id, this.durationAr, this.durationEn, this.numberOfCourses});

  Durations.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    durationAr = json['duration_ar'];
    durationEn = json['duration_en'];
    numberOfCourses = json['number_of_courses'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['duration_ar'] = this.durationAr;
    data['duration_en'] = this.durationEn;
    data['number_of_courses'] = this.numberOfCourses;
    return data;
  }
}