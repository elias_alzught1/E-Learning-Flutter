class OtpResponse {
  Data data;
  String message;
  int code;

  OtpResponse({this.data, this.message, this.code});

  OtpResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}

class Data {
  String sessionId;
  String mSISDN;
  String verifyType;
  String type;

  Data({this.sessionId, this.mSISDN, this.verifyType, this.type});

  Data.fromJson(Map<String, dynamic> json) {
    sessionId = json['SessionId'];
    mSISDN = json['MSISDN'];
    verifyType = json['verifyType'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['SessionId'] = this.sessionId;
    data['MSISDN'] = this.mSISDN;
    data['verifyType'] = this.verifyType;
    data['type'] = this.type;
    return data;
  }
}
