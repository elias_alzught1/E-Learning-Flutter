import '../common/auther_model.dart';

import 'durations_response.dart';

class CartResponse {
  Data data;
  String message;
  int code;

  CartResponse({this.data, this.message, this.code});

  CartResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    message = json['message'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}

class Data {
  List<CourseCart> courses;
  int subtotal;

  Data({this.courses, this.subtotal});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['courses'] != null) {
      courses = <CourseCart>[];
      json['courses'].forEach((v) {
        courses.add(new CourseCart.fromJson(v));
      });
    }
    subtotal = json['subtotal'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.courses != null) {
      data['courses'] = this.courses.map((v) => v.toJson()).toList();
    }
    data['subtotal'] = this.subtotal;
    return data;
  }
}
class CourseCart {
  int  id;
  String  name;
  String  nameAr;
  String  nameEn;
  String description;
  String  descriptionAr;
  String  descriptionEn;
  String  price;
  List<Authors>  authors;
  Durations  durations;
  String  image;

  CourseCart(
      {this.id,
        this.name,
        this.nameAr,
        this.nameEn,
        this.description,
        this.descriptionAr,
        this.descriptionEn,
        this.price,
        this.authors,
        this.durations,
        this.image});

  CourseCart.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    nameAr = json['name_ar'];
    nameEn = json['name_en'];
    description = json['description'];
    descriptionAr = json['description_ar'];
    descriptionEn = json['description_en'];
    price = json['price'];
    if (json['authors'] != null) {
      authors = <Authors>[];
      json['authors'].forEach((v) {
        authors .add(new Authors.fromJson(v));
      });
    }
    durations = json['durations'] != null
        ? new Durations.fromJson(json['durations'])
        : null;
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['name_ar'] = this.nameAr;
    data['name_en'] = this.nameEn;
    data['description'] = this.description;
    data['description_ar'] = this.descriptionAr;
    data['description_en'] = this.descriptionEn;
    data['price'] = this.price;
    if (this.authors != null) {
      data['authors'] = this.authors .map((v) => v.toJson()).toList();
    }
    if (this.durations != null) {
      data['durations'] = this.durations .toJson();
    }
    data['image'] = this.image;
    return data;
  }
}
