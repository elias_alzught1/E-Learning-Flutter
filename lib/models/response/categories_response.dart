import '../common/category_model.dart';

class CategoriesResponse {
  Data data;
  String message;
  int code;

  CategoriesResponse({this.data, this.message, this.code});

  factory CategoriesResponse.fromJson(Map<String, dynamic> json) =>
      CategoriesResponse(
        data: json['data'] != null ? Data.fromJson(json['data']) : null,
        message: json["message"],
        code: json["code"],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['message'] = this.message;
    data['code'] = this.code;
    return data;
  }
}

class Data {
  List<Category> categories;
  int totalPages;
  int currentPage;

  Data({this.categories, this.totalPages, this.currentPage});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['categories'] != null) {
      categories = <Category>[];
      json['categories'].forEach((v) {
        categories.add(Category.fromJson(v));
      });
    }
    totalPages = json['total_pages'];
    currentPage = json['current_page'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (categories != null) {
      data['categories'] = this.categories.map((v) => v.toJson()).toList();
    }
    data['total_pages'] = this.totalPages;
    data['current_page'] = this.currentPage;
    return data;
  }
}
