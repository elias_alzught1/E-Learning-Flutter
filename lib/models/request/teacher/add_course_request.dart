import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';

class AddCourse {
  final String name;
  final String arabicName;
  final String englishName;
  final String shortName;
  final String arabicShortName;
  final String englishShortName;
  final String description;
  final String arabicDescription;
  final String englishDescription;
  final String startTime;
  final String endTime;
  final int level;
  final int duration;
  final String language;
  final int price;
  final bool visible;
  final FormData  image64;

  AddCourse(
      this.name,
      this.arabicName,
      this.englishName,
      this.shortName,
      this.arabicShortName,
      this.englishShortName,
      this.description,
      this.arabicDescription,
      this.englishDescription,
      this.startTime,
      this.endTime,
      this.level,
      this.duration,
      this.language,
      this.price,
      this.visible,
      this.image64);

  Map<String, dynamic> toJson() => {
        "name": name,
        "name_ar": arabicName,
        "name_en": englishName,
        "shortname": shortName,
        "shortname_ar": arabicShortName,
        "shortname_en": englishShortName,
        "description": description,
        "description_ar": arabicDescription,
        "descripiton_en": englishDescription,
        "start_date": startTime,
        "end_date": endTime,
        "level": level,
        "duration": duration,
        "language": language,
        "price": price,
    "visible":visible,
    "image":image64
      };
}
