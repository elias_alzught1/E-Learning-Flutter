class FilterRequest {
  List<int> selectedCategories;

  List<int> selectedLevels;

  List<int> selectedDurations;

  List<double> selectedPriceRange;

  FilterRequest(
      {this.selectedCategories,
      this.selectedDurations,
      this.selectedLevels,
      this.selectedPriceRange});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['categories'] = this.selectedCategories;
    data['levels'] = this.selectedLevels;
    data['durations'] = this.selectedDurations;
    data['price'] = this.selectedPriceRange;
    return data;
  }
}
