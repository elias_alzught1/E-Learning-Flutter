abstract class Links {
  static const BASE = "http://185.194.124.88:8889/";

// Authentication
  static const LOGIN_URL = "api/user/auth/login";
  static const REGISTER_URL = "api/user/auth/register";
  static const PROFILE_URL = "api/user/auth/profile";
  static const LOGOUT_URL = "api/user/auth/logout";

//setting
  static const CHANGEMOBILENUMBER_URL = "api/user/auth/change-msisdn";
  static const CHANGEUSERINFO_URL = "api/user/auth/change-profile";

//level
  static const LEVELS_URL = "api/user/level";

//Durations
  static const DURATIONS_URL = "api/user/duration";

//user
  static const CATEGORY_URL = "api/user/categories";
  static const CATEGORY_SEARCH = "api/user/search/category";
  static const COURSES_URL = "api/user/course";
  static const EDIT_INLUCDE_URL = "api/teacher/includes";
  static const MEDIA_COURSE = "api/user/lessones/course";
  static const QUIZ_COURSE = "api/user/lessones/test";
  static const Lessones_Details = "api/user/lessones";
  static const Lessones_Vedios = "api/user/lessones/lessone-viedos";
  static const Lessones_Books = "api/user/lessones/lessone-books";
  static const Lessones_Reading = "api/user/lessones/lessone-books";
  static const Rating_Course = "api/user/course/rateing";
  static const Like_Reaction = "api/user/reaction/like";
  static const UnLike_Reaction = "api/user/reaction/unlike";
  static const VERFIYY_LINK = "api/user/auth/verify-pin";
  static const TEST_URL = "api/user/lessones/test/test-report";
  static const WISH_LIST = "api/user/wishList";
  static const Toggle_Wish_List = "api/user/wishList/toggle";
  static const Ads_User = "api/user/Ad";
  static const ADD_COURSE = "api/teacher/course";
  static const ADD_LESSON = "api/teacher/lesson";
  static const ADD_MEDIA = "api/teacher/lesson-media";
  static const GET_COURSE_BY_ID = "api/teacher/course";
  static const DELETE_INCLUDE_BY_ID = "api/teacher/includes/delete";
  static const DELETE_AIMS_BY_ID = "api/teacher/aims/delete";
  static const DELETE_TARGET_BY_ID = "api/teacher/target_audience/delete";
  static const DELETE_PREREQUISITE_BY_ID = "api/teacher/prerequisite/delete";
  static const EDIT_AIMS_BY_ID = "api/teacher/aims";
  static const EDIT_TARGET_BY_ID = "api/teacher/target_audience";
  static const ADD_QUIZ = "api/teacher/lesson-quiz";
  static const EDIT_PREREQUISITE_BY_ID = "api/teacher/prerequisite";
  static const GET_TEACHER_COURSE = "api/teacher/course";
  static const TEST_Quiz = "api/user/lessones/test/test-report";
  static const ADD_INCLUDE = "api/teacher/includes";
  static const SEARCH_URL = "api/user/search/course";
  static const Update_Quiz_Link='api/teacher/lesson-quiz/question/update';
  static const Delete_Question='api/teacher/lesson-quiz/question/delete';
  static const CART_URL="api/common/cart";
//api/teacher/includes
  //api/user/Ad
  //api/user/lessones/lessone-media
  static String gteLessons(int id) => "$MEDIA_COURSE/$id";

  static String getQuizs(int id) => "$QUIZ_COURSE/$id";

  static String getLessonesDetails(int id) => "$Lessones_Details/$id";

  static String getLessonesVideos(int id) => "$Lessones_Vedios/$id";

  static String getLessonesBooks(int id) => "$Lessones_Books/$id";

  static String getLessonesReadings(int id) => "$Lessones_Reading/$id";

  static String putCourseRateing(int id) => "$Rating_Course/$id";

  static String getCourseDetails(int id) => "$COURSES_URL/$id";

  static String putLikeCourse(int id) => "$Like_Reaction/$id";

  static String putUnLikeCourse(int id) => "$UnLike_Reaction/$id";

  static String getTeacherById(int id) => "api/user/teacher/$id";

  static String getCourseById(int id) => "api/teacher/course/$id";

  static String getLessonById(int id) => "api/user/lessones/course/$id";

  static String getQuizById(int id) => "api/user/lessones/test/$id";

  static String addQuestionForQuiz() => "api/teacher/lesson-quiz/question/add";

  static String editQuizInformation(int id) => "api/teacher/lesson-quiz/$id";

  static String updateLessonInformation(int id) => "api/teacher/lesson/$id";
  static String updateMediaInformation(int id) => "api/teacher/lesson-media/$id";

  static String getUpdatedLesson(int id) => "api/user/lessones/$id";

  static String getLessonVideos(int id) =>
      "api/user/lessones/lessone-viedos/$id";
/*
  http://185.194.124.88:8889/api/user/teacher/125
  */
}
