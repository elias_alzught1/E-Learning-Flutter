import 'dart:developer';

import "package:dio/dio.dart";

import '../controller/data_store.dart';
import '../localization/app_localization.dart';
import 'links.dart';

class ValidationException implements Exception {
  final Map<String, dynamic> errors;

  ValidationException(this.errors);

  @override
  String toString() {
    return errors.toString();
  }
}

class InternetException implements Exception {
  final Map<String, dynamic> errors;

  InternetException(this.errors);

  @override
  String toString() {
    return errors.toString();
  }
}

class FactoryException implements Exception {
  final data;
  final int code;

  FactoryException(data)
      : this.data = data["message"],
        this.code = data["code"];

  @override
  String toString() {
    if (data is List) {
      return data.join(" ");
    } else {
      return data;
    }
  }
}

class BaseClient {
  static const UNAUTHORIZED = 401;
  static const UNPROCESSABLE = 422;
  static const TECHNICALERROR = 500;
  static const INTERNETERROR = 101;

  Dio client = Dio();

  BaseClient() {
    client.interceptors.add(LogInterceptor());
    client.interceptors.add(ClientInterceptor());
    client.options.baseUrl = Links.BASE;
  }

// Future<bool> refreshToken() async {
//   final refreshToken = await _storage.read(key: 'refreshToken');
//   final response = await api
//       .post('/auth/refresh', data: {'refreshToken': refreshToken});
//
//   if (response.statusCode == 201) {
//     accessToken = response.data;
//     return true;
//   } else {
//     // refresh token is wrong
//     accessToken = null;
//     _storage.deleteAll();
//     return false;
//   }
// }

}

class LogInterceptor extends Interceptor {
  static final function = "FUNCTION".padRight(10).padLeft(15);
  static final status = "STATUS".padRight(10).padLeft(15);
  static final error = "ERROR".padRight(10).padLeft(15);
  static final path = "PATH".padRight(10).padLeft(15);
  static final method = "METHOD".padRight(10).padLeft(15);
  static final headers = "HEADERS".padRight(10).padLeft(15);
  static final query = "QUERY".padRight(10).padLeft(15);
  static final body = "BODY".padRight(10).padLeft(15);

  @override
  void onRequest(options, handler) {
    log("---------------------------");

    log("REQUEST", name: function);
    log(options.uri.path, name: path);
    log(options.method, name: method);

    if (options.queryParameters.isNotEmpty) {
      log(options.queryParameters.toString(), name: query);
    }
    if (options.headers.isNotEmpty) {
      log(options.headers.toString(), name: headers);
    }
    if (options.data != null) {
      log(options.data.toString(), name: body);
    }
    handler.next(options);
  }

  Future<Response<dynamic>> _retry(RequestOptions requestOptions) async {
    final options = Options(
      method: requestOptions.method,
      headers: requestOptions.headers,
    );
    return BaseClient().client.request<dynamic>(requestOptions.path,
        data: requestOptions.data,
        queryParameters: requestOptions.queryParameters,
        options: options);
  }

  @override
  Future<void> onError(DioError err, ErrorInterceptorHandler handler) async {
    log("---------------------------");
    log("ERROR", name: function);
    log(err.requestOptions.path, name: path);
    log(err.message, name: error);
    if (err.response != null) {
      log(err.response.statusCode.toString(), name: status);
      log(err.response.headers.toString(), name: headers);
      log(err.response.data.toString(), name: body);
    }

    handler.next(err);
  }

  Future<bool> refreshToken() async {
    final response = await BaseClient().client.get('/api/user/auth/refresh');

    if (response.statusCode == 201) {
      DataStore.instance.putToken(response.data['data']['token']);
      return true;
    } else {
      // refresh token is wrong
      DataStore.instance.putToken(null);
      // _storage.deleteAll();
      return false;
    }
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    log("---------------------------");
    log("RESPONSE", name: function);
    log(response.realUri.path, name: path);
    log(response.headers.toString(), name: headers);
    log(response.data.toString(), name: body);
    handler.next(response);
  }
}

class ClientInterceptor extends Interceptor {
  @override
  void onRequest(options, handler) {
    options.headers = {
      "Accept-Language": DataStore.instance.lang,
      "Content-Type": Headers.jsonContentType,
      "Authorization": DataStore.instance.token,
      'Accept': '*/*',
      'Accept-Encoding': 'gzip, deflate, br',
    };
    handler.next(options);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    if (err.type == DioErrorType.response) {
      var response = err.response;
      if (response.statusCode == BaseClient.UNPROCESSABLE) {
        var errors = response.data["errors"];
        if (errors != null) {
          err.error = ValidationException(response.data["errors"]);
        } else {
          err.error = response.data["message"];
        }
      } else {
        err.error = response.data["message"];
      }
    } else if (err.type == DioErrorType.connectTimeout ||
        err.type == DioErrorType.receiveTimeout ||
        err.type == DioErrorType.other) {
      err.error = AppLocalization.instance.trans("no internet");
    }
    handler.next(err);
  }
}
