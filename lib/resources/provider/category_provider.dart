import 'package:e_learning/resources/api_client.dart';

import '../../models/response/categories_response.dart';
import '../../public/constants.dart';
import '../links.dart';

class CategoryProvider extends BaseClient {
  Future<CategoriesResponse> getCategory(int currentPage) async {
    var queryParameters = {
      "page": currentPage,
      "page_size": Constants.pageSize,
    };
    final response =
        await client.get(Links.CATEGORY_URL, queryParameters: queryParameters);
    return CategoriesResponse.fromJson(response.data);
  }

  Future<CategoriesResponse> fetchSearch(
      int currentPage, String name, String shortname, String code) async {
    var queryParameters = {
      "page": currentPage,
      "page_size": Constants.pageSize,
      "name": name,
      "shortname": shortname,
      "code": code
    };
    final response = await client.post(Links.CATEGORY_SEARCH,
        queryParameters: queryParameters);
    return CategoriesResponse.fromJson(response.data);
  }
}
