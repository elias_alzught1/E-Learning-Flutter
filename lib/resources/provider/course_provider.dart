import 'package:dio/dio.dart';
import 'package:e_learning/models/response/cart_response.dart';
import 'package:e_learning/models/response/rating_response.dart';

import '../../models/request/filter_request.dart';
import '../../models/response/course_details_response.dart';
import '../../models/response/courses_response.dart';
import '../../models/response/general_response.dart';
import '../../models/response/lessones_response.dart';
import '../../models/response/lessons_response.dart';
import '../../models/response/quiz_result_response.dart';
import '../../models/response/quizs_response.dart';
import '../../models/response/reaction_response.dart';
import '../../models/response/search_course_response.dart';
import '../../models/response/shower_response.dart';
import '../../models/response/teacher/teacher_course_response.dart';
import '../../models/response/teacher_response.dart';
import '../../models/response/toggle_response.dart';
import '../../models/response/wishlist_response.dart';
import '../../public/constants.dart';
import '../api_client.dart';
import '../links.dart';

class CourseProvider extends BaseClient {
  Future<CourseResponse> getCourse(int currentPage,
      {Map<String, dynamic> filter}) async {
    try {
      final response = await client.post(Links.COURSES_URL, data: {
        "filters": filter,
        "page": currentPage,
        "pageSize": Constants.pageSize,
      });
      return CourseResponse.fromJson(response.data);
    } on DioError catch (ex) {
      print(ex.toString());
      throw ex.error;
    }
  }

  Future<CourseResponse> getFilterCourse(
      int currentPage, List<int> temp) async {
    try {
      final response = await client.post(Links.COURSES_URL, data: {
        "page": currentPage,
        "pageSize": Constants.pageSize,
        "categoryList": temp
      });
      return CourseResponse.fromJson(response.data);
    } on DioError catch (ex) {
      throw ex.error;
    }
  }

  Future<LessonsResponse> getLessons(
      int id, int currentPage, int pageSize) async {
    try {
      final response = await client.get(Links.gteLessons(id), queryParameters: {
        "page": currentPage,
        "pageSize": Constants.pageSize,
      });
      return LessonsResponse.fromJson(response.data);
    } on DioError catch (ex) {
      throw ex.error;
    }
  }

  Future<QuizResponse> getQuiz(int id) async {
    try {
      final response = await client.get(Links.getQuizs(id));
      return QuizResponse.fromJson(response.data);
    } on DioError catch (ex) {
      throw ex.error;
    }
  }

  Future<ShowerResponse> getShowerVideosResponse(int id) async {
    try {
      final response = await client.get(Links.getLessonesVideos(id));
      return ShowerResponse.fromJson(response.data);
    } on DioError catch (ex) {
      throw ex.error;
    }
  }

  Future<ShowerResponse> getShowerReadingResponse(int id) async {
    try {
      final response = await client.get(Links.getLessonesReadings(id));
      return ShowerResponse.fromJson(response.data);
    } on DioError catch (ex) {
      throw ex.error;
    }
  }

  Future<LessonsInfo> getShowerLessonInfo(int id) async {
    try {
      final response = await client.get(Links.getLessonesDetails(id));
      return LessonsInfo.fromJson(response.data);
    } on DioError catch (ex) {
      throw ex.error;
    }
  }

  Future<RatingResponse> changeRating(int id, int ratingValue) async {
    try {
      final response = await client
          .post(Links.putCourseRateing(id), data: {"value": ratingValue});
      return RatingResponse.fromJson(response.data);
    } on DioError catch (ex) {
      throw ex.error;
    }
  }

  Future<CourseDetailsResponse> getCourseDetails(int id) async {
    try {
      final response = await client.get(Links.getCourseDetails(id));
      return CourseDetailsResponse.fromJson(response.data);
    } on DioError catch (ex) {
      throw ex.error;
    }
  }

  Future<ReactionResponse> likeCourse(int id) async {
    try {
      final response = await client.get(Links.putLikeCourse(id));
      return ReactionResponse.fromJson(response.data);
    } on DioError catch (ex) {
      throw ex.error;
    }
  }

  Future<ReactionResponse> unlikeCourse(int id) async {
    try {
      final response = await client.get(Links.putUnLikeCourse(id));
      return ReactionResponse.fromJson(response.data);
    } on DioError catch (ex) {
      throw ex.error;
    }
  }

  Future<WishListResponse> getWishList(int page) async {
    try {
      final response = await client.post(Links.WISH_LIST, data: {"page": page,"page_size":"8"});
      return WishListResponse.fromJson(response.data);
    } on DioError catch (ex) {
      throw ex.error;
    }
  }

  Future<ToggleWishList> toggleWishCourse(int id) async {
    try {
      final response =
          await client.post(Links.Toggle_Wish_List, data: {"course_id": id});
      return ToggleWishList.fromJson(response.data);
    } on DioError catch (ex) {
      throw ex.error;
    }
  }

  Future<TeacherResponse> getTeacherById(int id) async {
    try {
      final response = await client.get(Links.getTeacherById(id));
      return TeacherResponse.fromJson(response.data);
    } on DioError catch (ex) {
      throw ex.error;
    }
  }

  Future<TeacherCourseResponse> getTeacherCourse(int pageNumber) async {
    try {
      final response = await client.get(Links.GET_TEACHER_COURSE,
          options: Options(headers: {"page": "pageNumber", "page_size": "10"}));
      return TeacherCourseResponse.fromJson(response.data);
    } on DioError catch (ex) {
      throw ex.error;
    }
  }

  Future<TestResult> getTestResult(
      int testId, List<Map<String, dynamic>> data) async {
    try {
      final response = await client.post(Links.TEST_Quiz,
          data: {"test_id": testId.toString(), "userAnswers": data});
      return TestResult.fromJson(response.data);
    } on DioError catch (ex) {
      throw ex.error;
    }
  }

  Future<GeneralResponse> addInclude(
      int courseId, String includeAr, String includeEn) async {
    try {
      final response = await client.post(Links.TEST_Quiz, data: {
        "course_id": courseId.toString(),
        "include_ar": includeAr,
        "include_en": includeEn,
        "id": null
      });
      return GeneralResponse.fromJson(response.data);
    } on DioError catch (ex) {
      throw ex.error;
    }
  }

  Future<SearchResponse> searchCourse(String query,int page) async {
    try {
      final response = await client.post(Links.SEARCH_URL, data: {
        "page": page,
        "query": query,
      });
      return SearchResponse.fromJson(response.data);
    } on DioError catch (ex) {
      throw ex.error;
    }
  }

  Future<CartResponse> getCartCourse() async {
    try {
      final response = await client.get(Links.CART_URL);
      return CartResponse.fromJson(response.data);
    } on DioError catch (ex) {
      throw ex.error;
    }
  }
}
