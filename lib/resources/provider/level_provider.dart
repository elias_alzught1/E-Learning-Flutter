import 'package:e_learning/models/response/levels_response.dart';

import '../api_client.dart';
import '../links.dart';

class LevelProvider  extends BaseClient {
  Future<LevelsResponse> getLevels() async {

    final response =
    await client.get(Links.LEVELS_URL);
    return LevelsResponse.fromJson(response.data);
  }

}