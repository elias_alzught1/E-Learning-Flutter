import '../../models/response/durations_response.dart';
import '../api_client.dart';
import '../links.dart';

class DurationProvider  extends BaseClient {
  Future<DurationsResponse> getDurations() async {

    final response =
    await client.get(Links.DURATIONS_URL);
    return DurationsResponse.fromJson(response.data);
  }

}