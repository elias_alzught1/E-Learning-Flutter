import 'package:dio/dio.dart';
import '../../models/response/ads_response.dart';
import '../../models/response/general_response.dart';
import '../../models/response/update_user_info.dart';
import '../../models/response/user.dart';
import '../api_client.dart';
import '../links.dart';

class UserProvider extends BaseClient {
  Future<GeneralResponse> changeMobileNumber(String phoneNumber) async {
    final response = await client.post(Links.CHANGEMOBILENUMBER_URL, data: {
      'new_MSISDN': phoneNumber,
    });
    return GeneralResponse.fromJson(response.data);
  }

  Future<UpdateUserInfoResponse> changeUserInfo(
      String firstName, String lastName, String userName) async {
    final response = await client.post(Links.CHANGEUSERINFO_URL, data: {
      'first_name': firstName,
      'last_name': lastName,
      'userName': userName
    });
    return UpdateUserInfoResponse.fromJson(response.data);
  }

  Future<AdsResponse> getAds() async {
    try {
      final response = await client.get(Links.Ads_User);
      return AdsResponse.fromJson(response.data);
    } on DioError catch (ex) {
      throw ex.error;
    }
  }
}
