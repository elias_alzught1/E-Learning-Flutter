import 'dart:io';

import 'package:dio/dio.dart';
import 'package:e_learning/models/response/teacher/add_quiz_response.dart';
import 'package:e_learning/models/response/teacher/basic_response.dart';
import 'package:e_learning/models/response/teacher/books_response.dart';
import 'package:e_learning/models/response/teacher/videos_response.dart';

import '../../models/common/course_model.dart';
import '../../models/request/teacher/add_course_request.dart';
import '../../models/response/teacher/add_course_response.dart';
import '../../models/response/teacher/add_video_response.dart';
import '../../models/response/teacher/durations_response.dart';
import '../../models/response/teacher/lessons_response.dart';
import '../../models/response/teacher/levels_response.dart';
import '../../models/response/teacher/quiz_response.dart';
import '../../models/response/teacher/upadte_lesson_information.dart';
import '../../models/response/teacher/updated_lesson.dart';
import '../api_client.dart';
import '../links.dart';

class TeacherProvider extends BaseClient {
  Future<LevelsResponse> getLevels() async {
    try {
      final response = await client.get(Links.LEVELS_URL);
      return LevelsResponse.fromJson(response.data);
    } catch (ex) {
      throw ex;
    }
  }

  Future<DurationsResponse> getDurations() async {
    try {
      final response = await client.get(Links.DURATIONS_URL);
      return DurationsResponse.fromJson(response.data);
    } catch (ex) {
      throw ex;
    }
  }

  Future<AddCourseResponse> addCourse(var addCourse) async {
    Map<String, String> headers = <String, String>{
      'Content-Type': 'multipart/form-data'
    };
    try {
      final response = await client.post(
        Links.ADD_COURSE,
        options: Options(headers: headers),
        data: addCourse,
      );
      return AddCourseResponse.fromJson(response.data);
    } catch (ex) {
      throw ex;
    }
  }

  Future<BasicResponse> editInclude(
      int idCourse, String includeAr, String includeEr,
      {int includeId}) async {
    try {
      final response = await client.post(Links.EDIT_INLUCDE_URL, data: {
        "course_id": idCourse,
        "id": includeId,
        "include_ar": includeAr,
        "include_en": includeEr
      });
      return BasicResponse.fromJson(response.data);
    } catch (ex) {
      throw ex;
    }
  }

  Future<BasicResponse> editAims(int idCourse, String aimsAr, String aimsEn,
      {int aimsId}) async {
    try {
      final response = await client.post(Links.EDIT_AIMS_BY_ID, data: {
        "aim_ar": aimsAr,
        "aim_en": aimsEn,
        "course_id": idCourse,
        "id": aimsId
      });
      return BasicResponse.fromJson(response.data);
    } catch (ex) {
      throw ex;
    }
  }

  Future<AddCourseResponse> getCourseById(int id) async {
    final response = await client.get(Links.getCourseById(id));
    return AddCourseResponse.fromJson(response.data);
  }

  Future<BasicResponse> deleteInclude(int idInclude) async {
    try {
      final response = await client.post(Links.DELETE_INCLUDE_BY_ID, data: {
        "id": idInclude,
      });
      return BasicResponse.fromJson(response.data);
    } catch (ex) {
      throw ex;
    }
  }

  Future<BasicResponse> deleteAims(int idAims) async {
    try {
      final response = await client.post(Links.DELETE_AIMS_BY_ID, data: {
        "id": idAims,
      });
      return BasicResponse.fromJson(response.data);
    } catch (ex) {
      throw ex;
    }
  }

  Future<BasicResponse> editTarget(
      int idCourse, String targetAr, String targetEn,
      {int targetId}) async {
    try {
      final response = await client.post(Links.EDIT_TARGET_BY_ID, data: {
        "target_ar": targetAr,
        "target_en": targetEn,
        "course_id": idCourse,
        "id": targetId
      });
      return BasicResponse.fromJson(response.data);
    } catch (ex) {
      throw ex;
    }
  }

  Future<BasicResponse> deleteTarget(int idTarget) async {
    try {
      final response = await client.post(Links.DELETE_TARGET_BY_ID, data: {
        "id": idTarget,
      });
      return BasicResponse.fromJson(response.data);
    } catch (ex) {
      throw ex;
    }
  }

  Future<BasicResponse> editPrerequisite(
      int idCourse, String prerequisiteAr, String prerequisiteEn,
      {int prerequisiteId}) async {
    try {
      final response = await client.post(Links.EDIT_PREREQUISITE_BY_ID, data: {
        "prerequisite_ar": prerequisiteAr,
        "prerequisite_en": prerequisiteEn,
        "course_id": idCourse,
        "id": prerequisiteId
      });
      return BasicResponse.fromJson(response.data);
    } catch (ex) {
      throw ex;
    }
  }

  Future<BasicResponse> deletePrerequisite(int idPrerequisite) async {
    try {
      final response = await client.post(Links.DELETE_TARGET_BY_ID, data: {
        "id": idPrerequisite,
      });
      return BasicResponse.fromJson(response.data);
    } catch (ex) {
      throw ex;
    }
  }

  Future<AddCourseResponse> addLesson(var addLesson) async {
    Map<String, String> headers = <String, String>{
      'Content-Type': 'multipart/form-data'
    };
    try {
      final response = await client.post(
        Links.ADD_LESSON,
        options: Options(headers: headers),
        data: addLesson,
      );
      return AddCourseResponse.fromJson(response.data);
    } catch (ex) {
      throw ex;
    }
  }

  Future<AddVideoResponse> addVideos(var addVideos) async {
    Map<String, String> headers = <String, String>{
      'Content-Type': 'multipart/form-data'
    };
    try {
      final response = await client.post(
        Links.ADD_MEDIA,
        options: Options(headers: headers),
        data: addVideos,
      );
      return AddVideoResponse.fromJson(response.data);
    } catch (ex) {
      throw ex;
    }
  }

  Future<AddQuizResponse> addQuiz(
      int id,
      String title,
      String titleAr,
      String titleEn,
      String description,
      String descriptionAr,
      String descriptionEn) async {
    try {
      final response = await client.post(Links.ADD_QUIZ, data: {
        "lesson_id": id,
        "title": title,
        "title_ar": titleAr,
        "title_en": titleEn,
        "description": description,
        "description_ar": descriptionAr,
        "description_en": descriptionEn,
      });
      return AddQuizResponse.fromJson(response.data);
    } catch (ex) {
      throw ex;
    }
  }

  Future<LessonTeacherResponse> getLessonsOfCourseById(int id) async {
    final response = await client.get(Links.getLessonById(id));
    return LessonTeacherResponse.fromJson(response.data);
  }

  Future<VideosResponse> getVideosForLesson(int id) async {
    final response = await client.get(Links.getLessonesVideos(id));
    return VideosResponse.fromJson(response.data);
  }

  //BooksResponse
  Future<BooksResponse> getBooksForLesson(int id) async {
    final response = await client.get(Links.getLessonesBooks(id));
    return BooksResponse.fromJson(response.data);
  }

  Future<QuizTeacherResponse> getQuizForLesson(int id) async {
    final response = await client.get(Links.getQuizById(id));
    return QuizTeacherResponse.fromJson(response.data);
  }

  Future<String> addQuestionForQuiz(
      int id,
      String questionQuiz,
      String firstAnswer,
      String secondAnswer,
      String thirdAnswer,
      String fourthAnswer,
      String acceptedAnswer) async {
    final response = await client.post(Links.addQuestionForQuiz(), data: {
      "acceptedAnswer": acceptedAnswer,
      "firstAnswer": firstAnswer,
      "fourthAnswer": fourthAnswer,
      "question": questionQuiz,
      "quiz_id": id.toString(),
      "secondAnswer": secondAnswer,
      "thirdAnswer": thirdAnswer
    });
    if (response.statusCode == 200) {
      return "Success";
    } else {
      return "False";
    }
  }

  Future<String> updateQuestionForQuiz(
      int id,
      String questionQuiz,
      String firstAnswer,
      String secondAnswer,
      String thirdAnswer,
      String fourthAnswer,
      String acceptedAnswer) async {
    final response = await client.post(Links.addQuestionForQuiz(), data: {
      "acceptedAnswer": acceptedAnswer,
      "firstAnswer": firstAnswer,
      "fourthAnswer": fourthAnswer,
      "question": questionQuiz,
      "question_id": id.toString(),
      "secondAnswer": secondAnswer,
      "thirdAnswer": thirdAnswer
    });
    if (response.statusCode == 200) {
      return "Success";
    } else {
      return "False";
    }
  }

  Future<int> editQuizInformation(
      int id,
      String title,
      String titleAr,
      String titleEn,
      String description,
      String descriptionAr,
      String descriptionEn) async {
    final response = await client.post(Links.editQuizInformation(id), data: {
      "id": id.toString(),
      "title": title,
      "title_ar": titleAr,
      "title_en": titleEn,
      "description": description,
      "description_ar": descriptionAr,
      "description_en": descriptionEn
    });
    return response.statusCode;
  }

  Future<GeneralResponse> editLessonInformation(
      int id,
      String title,
      String titleAr,
      String titleEn,
      String description,
      String descriptionAr,
      String descriptionEn) async {
    final response =
        await client.post(Links.updateLessonInformation(id), data: {
      "title": title,
      "title_ar": titleAr,
      "title_en": titleEn,
      "description": description,
      "description_ar": descriptionAr,
      "description_en": descriptionEn,
      "id": id.toString()
    });
    return GeneralResponse.fromJson(response.data);
  }

  Future<GeneralResponse> editVideoInformation(
      int id,
      String title,
      String titleAr,
      String titleEn,
      String description,
      String descriptionAr,
      String descriptionEn) async {
    final response = await client.post(Links.updateMediaInformation(id), data: {
      "title": title,
      "title_ar": titleAr,
      "title_en": titleEn,
      "description": description,
      "description_ar": descriptionAr,
      "description_en": descriptionEn,
      "id": id.toString()
    });
    return GeneralResponse.fromJson(response.data);
  }

  Future<AddVideoResponse> changeVideo(var changeVideos) async {
    Map<String, String> headers = <String, String>{
      'Content-Type': 'multipart/form-data'
    };
    try {
      final response = await client.post(
        Links.updateMediaInformation(94),
        options: Options(headers: headers),
        data: changeVideos,
      );
      return AddVideoResponse.fromJson(response.data);
    } catch (ex) {
      throw ex;
    }
  }

  Future<GetUpdateLessonResponse> getLesson(int id) async {
    final response = await client.get(Links.getUpdatedLesson(id));
    print(response);
    return GetUpdateLessonResponse.fromJson(response.data);
  }

  Future<GeneralResponse> changePdf(var changePdf, int id) async {
    Map<String, String> headers = <String, String>{
      'Content-Type': 'multipart/form-data'
    };
    try {
      final response = await client.post(
        Links.updateMediaInformation(id),
        options: Options(headers: headers),
        data: changePdf,
      );
      return GeneralResponse.fromJson(response.data);
    } catch (ex) {
      throw ex;
    }
  }

  Future<GeneralResponse> editBookInformation(
      int id,
      String title,
      String titleAr,
      String titleEn,
      String description,
      String descriptionAr,
      String descriptionEn) async {
    final response = await client.post(Links.updateMediaInformation(id), data: {
      "title": title,
      "title_ar": titleAr,
      "title_en": titleEn,
      "description": description,
      "description_ar": descriptionAr,
      "description_en": descriptionEn,
      "id": id.toString()
    });
    return GeneralResponse.fromJson(response.data);
  }

  Future<void> deleteQuestion(
    int id,
  ) async {
    final response = await client.post(Links.Delete_Question, data: {
      "question_id": id.toString(),
    });
  }
}
