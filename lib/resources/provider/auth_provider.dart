import 'dart:io';

import 'package:dio/dio.dart';
import '../../models/response/general_response.dart';
import '../../models/response/otp_response.dart';
import '../../models/response/user.dart';
import '../../models/response/verify_response.dart';
import '../api_client.dart';
import '../links.dart';

class AuthProvider extends BaseClient {
  Future<OtpResponse> login(String phoneNumber) async {
    try {
      final response = await client.post(Links.LOGIN_URL, data: {
        'MSISDN': phoneNumber,
      });
      return OtpResponse.fromJson(response.data);
    } catch (ex) {
      throw ex;
    }
  }

  //VERFIYY_LINK

  Future<VerifyResponse> verify(
      {String sessionId, String pin, String msisdn}) async {
    try {
      final response = await client.post(Links.VERFIYY_LINK, data: {
        'SessionId':sessionId,
        'MSISDN': msisdn,
        'Pin':pin
      });
      return VerifyResponse.fromJson(response.data);
    } catch (ex) {
      throw ex;
    }
  }

  Future<LoginResponse> register(
      String phoneNumber, String firstName, String lastName) async {
    final response = await client.post(Links.REGISTER_URL, data: {
      'MSISDN': phoneNumber,
      'first_name': firstName,
      'last_name': lastName
    });
    return LoginResponse.fromJson(response.data);
  }

  Future<LoginResponse> getInformation() async {
    final response = await client.get(Links.PROFILE_URL);
    return LoginResponse.fromJson(response.data);
  }

  Future<GeneralResponse> logout() async {
    final response = await client.get(Links.LOGOUT_URL);
    return GeneralResponse.fromJson(response.data);
  }
//GeneralResponse
}
