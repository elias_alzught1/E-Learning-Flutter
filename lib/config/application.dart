import 'dart:convert';
import 'dart:developer';
import 'dart:io';


import 'package:device_info_plus/device_info_plus.dart';

import 'package:flutter/material.dart' hide Key;
import 'package:get_it/get_it.dart';

class Application {
  final GlobalKey<NavigatorState> globalKey = GlobalKey();
  final Map<String, String> keys = {};

  String _deviceId;

  static final Application instance = Application._internal();

  Application._internal();

  BuildContext get context => globalKey.currentContext;

  String get deviceId => _deviceId;

  Future<void> init() async {
    _deviceId = await _getDeviceId();
    // Permission.location.request();
  }

  bool contains(String version) {
    return keys.containsKey(version);
  }



  Future<String> _getDeviceId() async {
    var deviceInfo = DeviceInfoPlugin();
    if (Platform.isIOS) {
      var iosDeviceInfo = await deviceInfo.iosInfo;
      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
    } else if (Platform.isAndroid) {
      var androidDeviceInfo = await deviceInfo.androidInfo;
      return androidDeviceInfo.androidId; // unique ID on Android
    }
    return null;
  }

  // Future<bool> _isLocationGranted() async {
  //   var permission = await Geolocator.checkPermission();
  //   return permission == LocationPermission.whileInUse ||
  //       permission == LocationPermission.always;
  // }

  // Future<String> getLocation() async {
  //   try {
  //     if (await _isLocationGranted() &&
  //         await Geolocator.isLocationServiceEnabled()) {
  //       var value = await Geolocator.getCurrentPosition();
  //       return "${value.latitude} ${value.longitude}";
  //     } else {
  //       return null;
  //     }
  //   } catch (e) {
  //     return null;
  //   }
  // }
}
