import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

//class AppLocalization {
//   AppLocalization(this.locale);
//
//   final Locale locale;
//
//   static AppLocalization of(BuildContext context) {
//     return Localizations.of<AppLocalization>(context, AppLocalization);
//   }
//
//   Map<String, String> _sentences;
//
//   Future<bool> load() async {
//     String data =
//         await rootBundle.loadString('assets/lang/${locale.languageCode}.json');
//     Map<String, dynamic> _result = json.decode(data);
//
//     _sentences = {};
//     _result.forEach((key, value) {
//       _sentences[key] = value;
//     });
//     return true;
//   }
//
//   String trans(String key) {
//     return _sentences[key] ?? '???';
//   }
//
//   String sentence(List<String> keys, [String separator = " "]) {
//     return keys.map((key) => trans(key)).join(separator);
//   }
// }

class AppLocalization {
  static AppLocalization instance = AppLocalization._internal();

  AppLocalization._internal();

  Locale locale;

  static AppLocalization of(BuildContext context) {
    return Localizations.of<AppLocalization>(context, AppLocalization);
  }

  Map<String, String> _sentences;

  Future<bool> load(Locale locale) async {
    this.locale = locale;
    var data =
    await rootBundle.loadString('assets/lang/${locale.languageCode}.json');
    Map<String, dynamic> _result = json.decode(data);
    _sentences = new Map();
    _result.forEach((String key, dynamic value) {
      _sentences[key] = value.toString();
    });
    return true;
  }

  String trans(String key) {
    return _sentences[key] ?? '???';
  }

  String sentence(List<String> keys, [String separator = " "]) {
    return keys.map((key) => trans(key)).join(separator);
  }
}

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalization> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'ar'].contains(locale.languageCode);

  @override
  Future<AppLocalization> load(Locale locale) async {
    AppLocalization localizations = AppLocalization.instance;
    await localizations.load(locale);
    print("Load ${locale.languageCode}");
    return localizations;
  }

  @override
  bool shouldReload(AppLocalizationsDelegate old) => true;
}

