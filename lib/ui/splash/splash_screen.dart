import 'package:e_learning/bloc/authentication/login_bloc/authentication_bloc.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:video_player/video_player.dart';
import '../../bloc/authentication/login_bloc/authentication_event.dart';
import '../../public/constants.dart';
import '../../themes/app_colors.dart';
import '../../themes/font_family.dart';

class SplashScreen extends StatefulWidget {
  static final route = "/splash";

  @override
  State<StatefulWidget> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  AuthBloc _bloc;
  VideoPlayerController _controller;
  bool _isFinished = false;

  // @override
  // void initState() {
  //   _bloc = BlocProvider.of(context);
  //   Future.delayed(const Duration(seconds: 3), () {
  //     _bloc.add(OnAuthCheck());
  //   });
  //
  //   super.initState();
  // }

  @override
  void initState() {
    super.initState();
    _bloc = BlocProvider.of(context);
    _controller = VideoPlayerController.asset("assets/videos/splach_videos.mp4");
    _controller.initialize().then((_) {
      setState((){
        _controller.play();
        _controller.addListener(_listener);
      });
    });
  }

  bool get isVideoCompleted =>
      _controller.value.isInitialized &&
      _controller.value.position == _controller.value.duration;

  void _listener(){
    if (isVideoCompleted && !_isFinished) {
      print('video Ended');
      _isFinished = true;
      _bloc.add(OnAuthCheck());
    }
  }
  @override
  dispose() {
    _controller?.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    final aspectRatio = size.width / size.height;
    return Scaffold(
      body: _controller.value.isInitialized
          ? Center(child: Container(height:size.height,width: size.width,child: VideoPlayer(_controller)))
          : Container(),
    );
  }
}
