import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';

import '../../../themes/app_colors.dart';
import '../../../themes/app_text_style.dart';
import '../../widgets/textfield/DirectionalTextField.dart';

Widget buildTextFailedForm(TextEditingController controller, IconData icon,
    String errorMessage, String label,
    {String errorText,Color iconColor}) {
  return Padding(
    padding: const EdgeInsets.only(top: 4.0, bottom: 4.0),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
            margin: EdgeInsetsDirectional.only(start: 3.w, end: 2.w),
            child: Text(
              label,
              style: AppTextStyle.smallDeepGray,
            )),
        SizedBox(
          height: 1.h,
        ),
        Container(
          height: errorText == null ? 40.sp : 60.sp,
          margin: EdgeInsetsDirectional.only(start: 2.w, end: 2.w),
          child: DirectionalTextField(
            controller: controller,
            decoration: InputDecoration(
              errorText: errorText,
filled: true,
              fillColor: AppColors.lighterGray.withOpacity(0.8),
              prefixIcon: Icon(
                icon,
                color: iconColor ?? AppColors.purple,
                size: 20,
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.lighterGray.withOpacity(0.35)),
                borderRadius: BorderRadius.circular(12.0),
              ),
              disabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.lighterGray.withOpacity(0.35)),
                borderRadius: BorderRadius.circular(12.0),
              ),
              enabledBorder: OutlineInputBorder(
              borderSide:   BorderSide(color: AppColors.lighterGray.withOpacity(0.35)),
  borderRadius: BorderRadius.circular(12.0),
  ),
              border: OutlineInputBorder(
                borderSide:   BorderSide(color: AppColors.lighterGray.withOpacity(0.35)),
                borderRadius: BorderRadius.circular(12.0),
              ),
            ),
          ),
        ),
      ],
    ),
  );
}
