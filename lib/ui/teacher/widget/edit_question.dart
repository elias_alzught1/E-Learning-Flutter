import 'package:e_learning/bloc/teacher/lesson_cubit.dart';
import 'package:e_learning/models/response/teacher/quiz_response.dart';
import 'package:e_learning/ui/widgets/common/Flush_bar.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../localization/app_localization.dart';

import '../../../themes/app_colors.dart';
import '../../../themes/app_text_style.dart';
import '../../widgets/appbar/abbBarAPP.dart';
import '../../widgets/appbar/abbBottomBar.dart';
import '../../widgets/textfield/DirectionalTextField.dart';

class EditQuestionDetails extends StatefulWidget {
  final Questions question;

  const EditQuestionDetails({Key key, this.question}) : super(key: key);

  @override
  State<EditQuestionDetails> createState() => _EditQuestionDetailsState();
}

class _EditQuestionDetailsState extends State<EditQuestionDetails> {
  String selectedValue = 'Option 1';
  bool answer1 = false;
  bool answer2 = false;
  bool answer3 = false;
  bool answer4 = false;

  void setCorrectAnswer() {
    if (widget.question.answers[0].isCorrect) {
      setAnswer(1);
    } else if (widget.question.answers[1].isCorrect) {
      setAnswer(2);
    } else if (widget.question.answers[2].isCorrect) {
      setAnswer(3);
    } else if (widget.question.answers[3].isCorrect) {
      setAnswer(4);
    }
  }

  void setSelectedValue(String value) {
    setState(() {
      selectedValue = value;
    });
  }

  void setAnswer(int number) {
    if (number == 1) {
      setState(() {
        answer1 = true;
        answer2 = false;
        answer3 = false;
        answer4 = false;
      });
    } else if (number == 2) {
      setState(() {
        answer1 = false;
        answer2 = true;
        answer3 = false;
        answer4 = false;
      });
    } else if (number == 3) {
      setState(() {
        answer1 = false;
        answer2 = false;
        answer3 = true;
        answer4 = false;
      });
    } else if (number == 4) {
      setState(() {
        answer1 = false;
        answer2 = false;
        answer3 = false;
        answer4 = true;
      });
    }
  }

  LessonCubit _bloc;

  @override
  void initState() {
    _bloc = BlocProvider.of<LessonCubit>(context);
    _bloc.questionQuizController.text = widget.question.questionText;
    _bloc.firstAnswerQuizController.text = widget.question.answers[0].value;
    _bloc.secondAnswerQuizController.text = widget.question.answers[1].value;
    _bloc.thirdAnswerQuizController.text = widget.question.answers[2].value;
    _bloc.fourthAnswerQuizController.text = widget.question.answers[3].value;
    // setAnswer(widget.question.answers[0].is)
    setCorrectAnswer();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<LessonCubit, LessonState>(
      listenWhen: (_, current) =>
          current is UpdateQuestionQuizAwaitState ||
          current is UpdateQuestionQuizErrorState ||
          current is UpdateQuestionQuizAcceptState,
      listener: (context, state) {
        if (state is UpdateQuestionQuizAwaitState) {
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            duration: Duration(milliseconds: 10),
            behavior: SnackBarBehavior.floating,
            content: Text(
              "Loading ...",
              style: AppTextStyle.largeWhiteBold,
            ),
            backgroundColor: AppColors.purple,
          ));
        }
        if (state is UpdateQuestionQuizErrorState) {
          showCustomFlashbar(context, "sorry_error", "error");
        } else if (state is UpdateQuestionQuizAcceptState) {
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            behavior: SnackBarBehavior.floating,
            content: Text(
              "Success Proccess",
              style: AppTextStyle.largeWhiteBold,
            ),
            backgroundColor: AppColors.purple,
          ));
          Navigator.pop(context);
        }
      },
      child: Scaffold(
        appBar: AppBarApp(),
        bottomNavigationBar: AppBottomBar(),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: SingleChildScrollView(
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              const SizedBox(height: 25),
                  Text(
                    AppLocalization.of(context).trans("edit_question"),
                    style: AppTextStyle.mediumBlackBold,
                  ),
              Container(
                  margin: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    AppLocalization.of(context).trans("question"),
                    style: AppTextStyle.mediumGray,
                  )),
              Container(
                width: MediaQuery.of(context).size.width / 1,
                height: _bloc.errorQuestionQuiz == null ? 40.sp : 60.sp,
                margin: EdgeInsetsDirectional.only(start: 2.w, end: 2.w),
                child: DirectionalTextField(
                  controller: _bloc.questionQuizController,
                  decoration: InputDecoration(
                    errorText: _bloc.errorQuestionQuiz,
                    filled: true,
                    fillColor: AppColors.lighterGray,
                    prefixIcon: const Icon(
                      Icons.question_mark_sharp,
                      color: AppColors.lightPurple,
                      size: 20,
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color:AppColors.lighterGray),
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                    disabledBorder: OutlineInputBorder(
                      borderSide:
                          const BorderSide(color:AppColors.lighterGray),
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                    enabledBorder:  OutlineInputBorder(
                      borderSide:
                      const BorderSide(color:AppColors.lighterGray),
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                    border: OutlineInputBorder(
                      borderSide:
                          const BorderSide(color:AppColors.lighterGray),
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 30),
              Container(
                  margin: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    AppLocalization.of(context).trans("first_answer"),
                    style: AppTextStyle.mediumGray,
                  )),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Flexible(
                    child: Container(
                      width: MediaQuery.of(context).size.width / 1,
                      height: _bloc.errorQuestionQuiz == null ? 40.sp : 60.sp,
                      margin: EdgeInsetsDirectional.only(start: 2.w, end: 2.w),
                      child: DirectionalTextField(
                        controller: _bloc.firstAnswerQuizController,
                        decoration: InputDecoration(
                          errorText: _bloc.errorQuestionQuiz,
                          filled: true,
                          fillColor: AppColors.lighterGray,
                          prefixIcon: const Icon(
                            Icons.question_answer,
                            color: AppColors.lightPurple,
                            size: 20,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                const BorderSide(color:AppColors.lighterGray),
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderSide:
                                const BorderSide(color: AppColors.lighterGray),
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          enabledBorder:OutlineInputBorder(
                            borderSide:
                            const BorderSide(color: AppColors.lighterGray),
                            borderRadius: BorderRadius.circular(12.0),
                          ) ,
                          border: const OutlineInputBorder(
                            borderSide:
                                BorderSide(color: AppColors.lighterGray),
                          ),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setAnswer(1);
                    },
                    child: SizedBox(
                      height: 25,
                      width: 25,
                      child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(100),
                              border: Border.all(color: AppColors.gray)),
                          child: answer1 == true
                              ? Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(4.0),
                                    child: Container(
                                      decoration: BoxDecoration(
                                          color: AppColors.lightPurple,
                                          shape: BoxShape.circle),
                                    ),
                                  ),
                                )
                              : null),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 15),
              Container(
                  margin: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    AppLocalization.of(context).trans("second_answer"),
                    style: AppTextStyle.mediumGray,
                  )),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Flexible(
                    child: Container(
                      width: MediaQuery.of(context).size.width / 1,
                      height: _bloc.errorQuestionQuiz == null ? 40.sp : 60.sp,
                      margin: EdgeInsetsDirectional.only(start: 2.w, end: 2.w),
                      child: DirectionalTextField(
                        controller: _bloc.secondAnswerQuizController,
                        decoration: InputDecoration(
                          errorText: _bloc.errorQuestionQuiz,
                          filled: true,
                          fillColor: AppColors.lighterGray,
                          prefixIcon: const Icon(
                            Icons.question_answer,
                            color: AppColors.lightPurple,
                            size: 20,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                const BorderSide(color: AppColors.lighterGray),
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderSide:
                                const BorderSide(color:  AppColors.lighterGray),
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          enabledBorder:OutlineInputBorder(
                            borderSide:
                            const BorderSide(color: AppColors.lighterGray),
                            borderRadius: BorderRadius.circular(12.0),
                          ) ,
                          border: OutlineInputBorder(
                            borderSide:
                                const BorderSide(color: AppColors.lighterGray),
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setAnswer(2);
                    },
                    child: SizedBox(
                      height: 25,
                      width: 25,
                      child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(100),
                              border: Border.all(color: AppColors.gray)),
                          child: answer2 == true
                              ? Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(4.0),
                                    child: Container(
                                      decoration: const BoxDecoration(
                                          color: AppColors.lightPurple,
                                          shape: BoxShape.circle),
                                    ),
                                  ),
                                )
                              : null),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 15),
              Container(
                  margin: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    AppLocalization.of(context).trans("third_answer"),
                    style: AppTextStyle.mediumGray,
                  )),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Flexible(
                    child: Container(
                      width: MediaQuery.of(context).size.width / 1,
                      height: _bloc.errorQuestionQuiz == null ? 40.sp : 60.sp,
                      margin: EdgeInsetsDirectional.only(start: 2.w, end: 2.w),
                      child: DirectionalTextField(
                        controller: _bloc.thirdAnswerQuizController,
                        decoration: InputDecoration(
                          errorText: _bloc.errorQuestionQuiz,
                          filled: true,
                          fillColor: AppColors.lighterGray,
                          prefixIcon: const Icon(
                            Icons.question_answer,
                            color: AppColors.lightPurple,
                            size: 20,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                const BorderSide(color: AppColors.lighterGray),
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderSide:
                                const BorderSide(color:AppColors.lighterGray),
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          enabledBorder:OutlineInputBorder(
                            borderSide:
                            const BorderSide(color: AppColors.lighterGray),
                            borderRadius: BorderRadius.circular(12.0),
                          ) ,
                          border: OutlineInputBorder(
                            borderSide:
                                const BorderSide(color: AppColors.lighterGray),
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setAnswer(3);
                    },
                    child: SizedBox(
                      height: 25,
                      width: 25,
                      child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(100),
                              border: Border.all(color: AppColors.gray)),
                          child: answer3 == true
                              ? Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(4.0),
                                    child: Container(
                                      decoration: const BoxDecoration(
                                          color: AppColors.lightPurple,
                                          shape: BoxShape.circle),
                                    ),
                                  ),
                                )
                              : null),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 15),
              Container(
                  margin: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    AppLocalization.of(context).trans("fourth_answer"),
                    style: AppTextStyle.mediumGray,
                  )),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Flexible(
                    child: Container(
                      width: MediaQuery.of(context).size.width / 1,
                      height: _bloc.errorQuestionQuiz == null ? 40.sp : 60.sp,
                      margin: EdgeInsetsDirectional.only(start: 2.w, end: 2.w),
                      child: DirectionalTextField(
                        controller: _bloc.fourthAnswerQuizController,
                        decoration: InputDecoration(
                          errorText: _bloc.errorQuestionQuiz,
                          filled: true,
                          fillColor: AppColors.lighterGray,
                          prefixIcon: const Icon(
                            Icons.question_answer,
                            color: AppColors.lightPurple,
                            size: 20,
                          ),
                          enabledBorder:OutlineInputBorder(
                            borderSide:
                            const BorderSide(color: AppColors.lighterGray),
                            borderRadius: BorderRadius.circular(12.0),
                          ) ,
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                const BorderSide(color: AppColors.lighterGray),
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderSide:
                                const BorderSide(color:AppColors.lighterGray),
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          border: OutlineInputBorder(
                            borderSide:
                                const BorderSide(color:AppColors.lighterGray),
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setAnswer(4);
                    },
                    child:
                    SizedBox(
                      height: 25,
                      width: 25,
                      child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(100),
                              border: Border.all(color: AppColors.gray)),
                          child: answer4 == true
                              ? Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(4.0),
                                    child: Container(
                                      decoration: const BoxDecoration(
                                          color: AppColors.lightPurple,
                                          shape: BoxShape.circle),
                                    ),
                                  ),
                                )
                              : null),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 35,
              ),
              Row(
                children: [
                  Expanded(
                      child: GestureDetector(
                        onTap: (){
                          Navigator.pop(context);
                        },
                        child: Container(
                    decoration: BoxDecoration(
                          color: AppColors.gray,
                          borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Center(
                          child: Text(
                            AppLocalization.of(context).trans("cancel"),
                            style: AppTextStyle.mediumWhiteBold,
                          ),
                        ),
                    ),
                  ),
                      )),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                      child: GestureDetector(
                    onTap: () {
                      _bloc.updateQuestionForQuiz(
                          widget.question.id,
                          answer1
                              ? "firstAnswer"
                              : answer2
                                  ? "secondAnswer"
                                  : answer3
                                      ? "thirdAnswer"
                                      : answer4
                                          ? "fourthAnswer"
                                          : "");
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: AppColors.lightPurple,
                          borderRadius: BorderRadius.circular(10)),
                      child: Padding(
                        padding: EdgeInsets.all(12.0),
                        child: Center(
                          child: Text(
                            AppLocalization.of(context).trans("edit"),
                            style: AppTextStyle.mediumWhiteBold,
                          ),
                        ),
                      ),
                    ),
                  )),
                ],
              )
            ]),
          ),
        ),
      ),
    );
  }
}
