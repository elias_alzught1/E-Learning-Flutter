import 'package:flutter/material.dart';

class BodyPainter extends CustomPainter {
  final bottomPadding = 48;
  @override
  void paint(Canvas canvas, Size size) {
    Path path = Path()
      ..lineTo(size.width, 0)
      ..lineTo(size.width, size.height * .65) //65& on left
      ..lineTo(0, size.height - bottomPadding)
      ..lineTo(0, 0);

    Paint paint = Paint()..color = Colors.blue;

    canvas.drawPath(path, paint);
    // bottom line

    paint
      ..color = Colors.green
      ..strokeWidth = 20;
    canvas.drawLine(Offset(-20, size.height - bottomPadding),
        Offset(size.width + 20, size.height * .65), paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => false;
}