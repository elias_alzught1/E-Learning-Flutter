import 'package:e_learning/themes/app_text_style.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';

import '../../../themes/app_colors.dart';
import '../../../themes/elearning_icons.dart';

class UnorderedListItemTeacher extends StatelessWidget {
  UnorderedListItemTeacher(this.text, this.icon,
      {Key key, this.padding, this.appTextStyle, this.funDelete, this.funEdit})
      : super(key: key);
  final Icon icon;
  final String text;
  final double padding;
  final TextStyle appTextStyle;
  final funDelete;
  final funEdit;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        icon,
        SizedBox(
          width: 1.3.w,
        ),
        Expanded(
          child: Text(
            text,
            style: appTextStyle,
          ),
        ),
        Spacer(),
        InkWell(
          onTap: () {
            funEdit();
          },
          child: Icon(
            Icons.edit,
            color: AppColors.normalGreen,
            size: 15.sp,
          ),
        ),
        SizedBox(
          width: 6.w,
        ),
        InkWell(
          onTap: () {
            funDelete();
          },
          child: Icon(
            Elearning.cancel,
            color: AppColors.red,
            size: 12.sp,
          ),
        )
      ],
    );
  }
}
