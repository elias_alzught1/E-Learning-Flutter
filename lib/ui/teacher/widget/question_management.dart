import 'package:e_learning/localization/app_localization.dart';
import 'package:e_learning/themes/app_colors.dart';
import 'package:e_learning/ui/teacher/widget/dialog_show.dart';
import 'package:e_learning/ui/teacher/widget/edit_question.dart';
import 'package:e_learning/ui/widgets/common/Flush_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../bloc/teacher/lesson_cubit.dart';
import '../../../themes/app_text_style.dart';
import '../../widgets/appbar/abbBarAPP.dart';
import '../../widgets/appbar/abbBottomBar.dart';

class QuestionManagement extends StatefulWidget {
  const QuestionManagement({Key key}) : super(key: key);

  @override
  State<QuestionManagement> createState() => _QuestionManagementState();
}

class _QuestionManagementState extends State<QuestionManagement> {
  LessonCubit _bloc;
  int selectedIndex = 0;

  @override
  void initState() {
    _bloc = BlocProvider.of<LessonCubit>(context);
    print(_bloc.quizList[0].questions.length.toString());
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<LessonCubit, LessonState>(
      listenWhen: (_, current) =>
          current is DeleteQuestionQuizAwaitState ||
          current is DeleteQuestionQuizAcceptState ||
          current is DeleteQuestionQuizErrorState,
      listener: (context, state) {
        if (state is DeleteQuestionQuizAwaitState) {
        } else if (state is DeleteQuestionQuizAcceptState) {
          showCustomFlashbar(context, "", "Success_Add");
          Navigator.of(context).pop();
          Navigator.of(context).pop();
        } else if (state is DeleteQuestionQuizErrorState) {}
        // TODO: implement listener
      },
      child: Scaffold(
          appBar: AppBarApp(),
          bottomNavigationBar: AppBottomBar(),
          body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  AppLocalization.of(context).trans("edit_question"),
                  style: AppTextStyle.largeBlackBold,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      border: Border.all(
                        color: AppColors.gray,
                      ),
                      borderRadius: BorderRadius.circular(15)),
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(children: [
                        Container(
                          child: Column(children: [
                            Row(
                              children: [
                                const Icon(Icons.question_mark_sharp,
                                    size: 19, color: AppColors.deepBlue),
                                Text(
                                  _bloc.quizList[0].questions[selectedIndex]
                                      .questionText
                                      .toString(),
                                  style: AppTextStyle.xLargeBlack,
                                ),
                                Spacer(),
                                InkWell(
                                  onTap: () {
                                    Navigator.of(context).push(
                                      CupertinoPageRoute(
                                        builder: (context) => BlocProvider(
                                          create: (context) => LessonCubit(),
                                          child: EditQuestionDetails(
                                              question: _bloc.quizList[0]
                                                  .questions[selectedIndex]),
                                        ),
                                      ),
                                    );
                                  },
                                  child: const Icon(Icons.edit,
                                      size: 19, color: AppColors.deepBlue),
                                ),
                                SizedBox(
                                  width: 15,
                                ),
                                InkWell(
                                  onTap: () {
                                    myDialog(
                                        context,
                                        AppLocalization.of(context).trans(
                                          "delete_question",
                                        ),
                                        Text(AppLocalization.of(context)
                                            .trans("delete_question_body")),
                                        () => _bloc.deleteQuestion(_bloc
                                            .quizList[0]
                                            .questions[selectedIndex]
                                            .id),
                                        icon: Icons.delete);
                                  },
                                  child: const Icon(Icons.delete,
                                      size: 19, color: AppColors.red),
                                ),
                              ],
                            ),
                            ListView.builder(
                              shrinkWrap: true,
                              itemCount: _bloc.quizList[0]
                                  .questions[selectedIndex].answers.length,
                              itemBuilder: (context, index) {
                                return Container(
                                  decoration: BoxDecoration(
                                      color: _bloc
                                              .quizList[0]
                                              .questions[selectedIndex]
                                              .answers[index]
                                              .isCorrect
                                          ? Colors.green
                                          : AppColors.lighterGray,
                                      borderRadius: BorderRadius.circular(10)),
                                  height: 50,
                                  margin:
                                      const EdgeInsets.only(top: 5, bottom: 5),
                                  child: Padding(
                                    padding: const EdgeInsetsDirectional.only(
                                        start: 5),
                                    child: Row(
                                      children: [
                                        _bloc
                                                .quizList[0]
                                                .questions[selectedIndex]
                                                .answers[index]
                                                .isCorrect
                                            ? const Center(
                                                child: Icon(
                                                    Icons.check_circle_outline),
                                              )
                                            : SizedBox(
                                                height: 20,
                                                width: 20,
                                                child: Container(
                                                    decoration: BoxDecoration(
                                                        shape: BoxShape.circle,
                                                        border: Border.all(
                                                            color: AppColors
                                                                .gray)),
                                                    child: null),
                                              ),
                                        SizedBox(
                                          width: 8,
                                        ),
                                        Text(
                                          _bloc
                                              .quizList[0]
                                              .questions[selectedIndex]
                                              .answers[index]
                                              .value
                                              .toString(),
                                          style: AppTextStyle.xLargeBlack,
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              },
                            ),
                            SizedBox(
                              height: 25,
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 5),
                              child: Row(
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        selectedIndex--;
                                      });
                                    },
                                    child: Row(
                                      children: [
                                        Icon(Icons.arrow_back_ios_rounded,
                                            size: 15,
                                            color: AppColors.deepGray),
                                        Container(
                                          child: Text("Back",
                                              style: TextStyle(
                                                  color: AppColors.deepGray,
                                                  fontWeight: FontWeight.bold)),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Spacer(),
                                  GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        ++selectedIndex;
                                      });
                                    },
                                    child: Row(
                                      children: [
                                        Container(
                                          child: Text(
                                            "Next",
                                            style: TextStyle(
                                                color: AppColors.deepGray,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        Icon(
                                          Icons.arrow_forward_ios_rounded,
                                          size: 15,
                                          color: AppColors.deepGray,
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 25,
                            ),
                          ]),
                        ),
                      ]),
                    ),
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
