import 'package:e_learning/bloc/teacher/lesson_cubit.dart';
import 'package:e_learning/bloc/teacher_cubit/teacher_cubit.dart';
import 'package:e_learning/ui/teacher/my_course.dart';
import 'package:e_learning/ui/teacher/widget/text_input.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../localization/app_localization.dart';
import '../../../themes/app_colors.dart';
import '../../../themes/app_text_style.dart';
import '../../widgets/appbar/abbBarAPP.dart';
import '../../widgets/appbar/abbBottomBar.dart';
import '../../widgets/common/Flush_bar.dart';
import '../../widgets/textfield/DirectionalTextField.dart';

class AddQuestionPage extends StatefulWidget {
  final int quizId;

  const AddQuestionPage({Key key, this.quizId}) : super(key: key);

  @override
  State<AddQuestionPage> createState() => _AddQuestionPageState();
}

class _AddQuestionPageState extends State<AddQuestionPage>{
  String selectedValue = 'Option 1';
  bool answer1 = false;
  bool answer2 = false;
  bool answer3 = false;
  bool answer4 = false;

  void setSelectedValue(String value) {
    setState(() {
      selectedValue = value;
    });
  }

  void setAnswer(int number) {
    if (number == 1) {
      setState(() {
        answer1 = true;
        answer2 = false;
        answer3 = false;
        answer4 = false;
      });
    } else if (number == 2) {
      setState(() {
        answer1 = false;
        answer2 = true;
        answer3 = false;
        answer4 = false;
      });
    } else if (number == 3) {
      setState(() {
        answer1 = false;
        answer2 = false;
        answer3 = true;
        answer4 = false;
      });
    } else if (number == 4) {
      setState(() {
        answer1 = false;
        answer2 = false;
        answer3 = false;
        answer4 = true;
      });
    }
  }

  LessonCubit _bloc;

  @override
  void initState() {
    _bloc = BlocProvider.of<LessonCubit>(context);
    // TODO: implement initState
    super.initState();
  }

  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBarApp(),
      bottomNavigationBar: AppBottomBar(),
      body: BlocListener<LessonCubit, LessonState>(
        listenWhen: (previous, current) =>
            current is AddQuestionQuizAcceptState ||
            current is AddQuestionQuizErrorState ||
            current is AddQuestionQuizAwaitState,
        listener: (context, state) {
          if (state is AddQuestionQuizAwaitState) {
            setState(() {
              isLoading = true;
            });
          }
          if (state is AddQuestionQuizAcceptState) {
            setState(() {
              isLoading = false;
            });
            Navigator.of(context).pushReplacement(
              CupertinoPageRoute(
                builder: (context) => BlocProvider(
                  create: (context) => TeacherCubit(),
                  child: MyCourse(),
                ),
              ),
            );
          }
          if (state is AddQuestionQuizErrorState) {
            setState(() {
              isLoading = false;
            });
            showCustomFlashbar(context, state.message, "Error");
          }
        },
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: SingleChildScrollView(
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              SizedBox(height: 25),
              Container(
                  margin: EdgeInsets.only(bottom: 5),
                  child: Text(
                    AppLocalization.of(context).trans("question"),
                    style: AppTextStyle.mediumGray,
                  )),
              Container(
                width: MediaQuery.of(context).size.width / 1,
                height: _bloc.errorQuestionQuiz == null ? 40.sp : 60.sp,
                margin: EdgeInsetsDirectional.only(start: 2.w, end: 2.w),
                child: DirectionalTextField(
                  controller: _bloc.questionQuizController,
                  decoration: InputDecoration(
                    errorText: _bloc.errorQuestionQuiz,
                    filled: true,
                    fillColor: AppColors.lighterGray,
                    prefixIcon: const Icon(
                      Icons.question_mark_sharp,
                      color: AppColors.deepBlue,
                      size: 20,
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.deepBlue),
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.lighterGray),
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.lighterGray),
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 30),
              Container(
                  margin: EdgeInsets.only(bottom: 5),
                  child: Text(
                    AppLocalization.of(context).trans("first_answer"),
                    style: AppTextStyle.mediumGray,
                  )),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Flexible(
                    child: Container(
                      width: MediaQuery.of(context).size.width / 1,
                      height: _bloc.errorQuestionQuiz == null ? 40.sp : 60.sp,
                      margin: EdgeInsetsDirectional.only(start: 2.w, end: 2.w),
                      child: DirectionalTextField(
                        controller: _bloc.firstAnswerQuizController,
                        decoration: InputDecoration(
                          errorText: _bloc.errorQuestionQuiz,
                          filled: true,
                          fillColor: AppColors.lighterGray,
                          prefixIcon: const Icon(
                            Icons.question_answer,
                            color: AppColors.deepBlue,
                            size: 20,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: AppColors.deepBlue),
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: AppColors.lighterGray),
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          border: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: AppColors.lighterGray),
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setAnswer(1);
                    },
                    child: SizedBox(
                      height: 25,
                      width: 25,
                      child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(100),
                              border: Border.all(color: AppColors.gray)),
                          child: answer1 == true
                              ? Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(4.0),
                                    child: Container(
                                      decoration: BoxDecoration(
                                          color: AppColors.blue,
                                          shape: BoxShape.circle),
                                    ),
                                  ),
                                )
                              : null),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 15),
              Container(
                  margin: EdgeInsets.only(bottom: 5),
                  child: Text(
                    AppLocalization.of(context).trans("second_answer"),
                    style: AppTextStyle.mediumGray,
                  )),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Flexible(
                    child: Container(
                      width: MediaQuery.of(context).size.width / 1,
                      height: _bloc.errorQuestionQuiz == null ? 40.sp : 60.sp,
                      margin: EdgeInsetsDirectional.only(start: 2.w, end: 2.w),
                      child: DirectionalTextField(
                        controller: _bloc.secondAnswerQuizController ,
                        decoration: InputDecoration(
                          errorText: _bloc.errorQuestionQuiz,
                          filled: true,
                          fillColor: AppColors.lighterGray,
                          prefixIcon: const Icon(
                            Icons.question_answer,
                            color: AppColors.deepBlue,
                            size: 20,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: AppColors.deepBlue),
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: AppColors.lighterGray),
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          border: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: AppColors.lighterGray),
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setAnswer(2);
                    },
                    child: SizedBox(
                      height: 25,
                      width: 25,
                      child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(100),
                              border: Border.all(color: AppColors.gray)),
                          child: answer2 == true
                              ? Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(4.0),
                                    child: Container(
                                      decoration: BoxDecoration(
                                          color: AppColors.blue,
                                          shape: BoxShape.circle),
                                    ),
                                  ),
                                )
                              : null),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 15),
              Container(
                  margin: EdgeInsets.only(bottom: 5),
                  child: Text(
                    AppLocalization.of(context).trans("third_answer"),
                    style: AppTextStyle.mediumGray,
                  )),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Flexible(
                    child: Container(
                      width: MediaQuery.of(context).size.width / 1,
                      height: _bloc.errorQuestionQuiz == null ? 40.sp : 60.sp,
                      margin: EdgeInsetsDirectional.only(start: 2.w, end: 2.w),
                      child: DirectionalTextField(
                        controller: _bloc.thirdAnswerQuizController,
                        decoration: InputDecoration(
                          errorText: _bloc.errorQuestionQuiz,
                          filled: true,
                          fillColor: AppColors.lighterGray,
                          prefixIcon: const Icon(
                            Icons.question_answer,
                            color: AppColors.deepBlue,
                            size: 20,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: AppColors.deepBlue),
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: AppColors.lighterGray),
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          border: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: AppColors.lighterGray),
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setAnswer(3);
                    },
                    child: SizedBox(
                      height: 25,
                      width: 25,
                      child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(100),
                              border: Border.all(color: AppColors.gray)),
                          child: answer3 == true
                              ? Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(4.0),
                                    child: Container(
                                      decoration: BoxDecoration(
                                          color: AppColors.blue,
                                          shape: BoxShape.circle),
                                    ),
                                  ),
                                )
                              : null),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 15),
              Container(
                  margin: EdgeInsets.only(bottom: 5),
                  child: Text(
                    AppLocalization.of(context).trans("fourth_answer"),
                    style: AppTextStyle.mediumGray,
                  )),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Flexible(
                    child: Container(
                      width: MediaQuery.of(context).size.width / 1,
                      height: _bloc.errorQuestionQuiz == null ? 40.sp : 60.sp,
                      margin: EdgeInsetsDirectional.only(start: 2.w, end: 2.w),
                      child: DirectionalTextField(
                        controller: _bloc.fourthAnswerQuizController,
                        decoration: InputDecoration(
                          errorText: _bloc.errorQuestionQuiz,
                          filled: true,
                          fillColor: AppColors.lighterGray,
                          prefixIcon: const Icon(
                            Icons.question_answer,
                            color: AppColors.deepBlue,
                            size: 20,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: AppColors.deepBlue),
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: AppColors.lighterGray),
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          border: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: AppColors.lighterGray),
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setAnswer(4);
                    },
                    child: SizedBox(
                      height: 25,
                      width: 25,
                      child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(100),
                              border: Border.all(color: AppColors.gray)),
                          child: answer4 == true
                              ? Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(4.0),
                                    child: Container(
                                      decoration: BoxDecoration(
                                          color: AppColors.blue,
                                          shape: BoxShape.circle),
                                    ),
                                  ),
                                )
                              : null),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 35,
              ),
              Row(
                children: [
                  Expanded(
                      child: Container(
                    decoration: BoxDecoration(
                        color: AppColors.gray,
                        borderRadius: BorderRadius.circular(10)),
                    child: const Padding(
                      padding: EdgeInsets.all(12.0),
                      child: Center(
                        child: Text(
                          "Back",
                          style: AppTextStyle.mediumWhiteBold,
                        ),
                      ),
                    ),
                  )),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                      child: GestureDetector(
                    onTap: () {
                      _bloc.addQuestionForQuiz(
                          widget.quizId,
                          answer1
                              ? "firstAnswer"
                              : answer2
                                  ? "secondAnswer"
                                  : answer3
                                      ? "thirdAnswer"
                                      : answer4
                                          ? "fourthAnswer"
                                          : "");
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: AppColors.deepBlue,
                          borderRadius: BorderRadius.circular(10)),
                      child: Padding(
                        padding: EdgeInsets.all(12.0),
                        child: Center(
                          child: isLoading
                              ? CircularProgressIndicator()
                              : Text(
                                  "Add",
                                  style: AppTextStyle.mediumWhiteBold,
                                ),
                        ),
                      ),
                    ),
                  )),
                ],
              )
            ]),
          ),
        ),
      ),
    );
  }
}
