import 'package:e_learning/ui/widgets/common/Flush_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../bloc/teacher/lesson_cubit.dart';
import '../../../bloc/teacher_cubit/teacher_cubit.dart';
import '../../../localization/app_localization.dart';
import '../../../models/response/teacher/quiz_response.dart';
import '../../../themes/app_colors.dart';
import '../../../themes/app_text_style.dart';
import '../../widgets/appbar/abbBarAPP.dart';
import '../../widgets/appbar/abbBottomBar.dart';
import '../../widgets/textfield/DirectionalTextField.dart';
import '../my_course.dart';

class EditQuizDetails extends StatefulWidget {
  final QuizModel quiz;

  const EditQuizDetails({Key key, this.quiz}) : super(key: key);

  @override
  State<EditQuizDetails> createState() => _EditQuizDetailsState();
}

class _EditQuizDetailsState extends State<EditQuizDetails> {
  LessonCubit _bloc;

  @override
  void initState() {
    _bloc = BlocProvider.of<LessonCubit>(context);
    _bloc.titleQuizController.text = widget.quiz.title;
    _bloc.titleQuizArabicController.text = widget.quiz.titleAr;
    _bloc.titleQuizEnglishController.text = widget.quiz.titleEn;
    _bloc.descriptionQuizController.text = widget.quiz.description;
    _bloc.descriptionQuizArabicController.text = widget.quiz.descriptionAr;
    _bloc.descriptionQuizEnglishController.text = widget.quiz.descriptionEn;
    // TODO: implement initState
    super.initState();
  }


  bool isLoading = false;
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBarApp(),
      bottomNavigationBar: AppBottomBar(),
      body: BlocListener<LessonCubit, LessonState>(
        listenWhen: (context, state) =>
            state is EditQuizInformationAcceptState ||
            state is EditQuizInformationAwaitState ||
            state is EditQuizInformationErrorState,
        listener: (context, state) {
          if (state is EditQuizInformationAwaitState) {
            setState(() {
              isLoading = true;
            });
          }
          if (state is EditQuizInformationAcceptState) {
            setState(() {
              isLoading = false;
            });
            Navigator.of(context).pushReplacement(
              CupertinoPageRoute(
                builder: (context) => BlocProvider(
                  create: (context) => TeacherCubit(),
                  child: MyCourse(),
                ),
              ),
            );
          }
          if (state is EditQuizInformationErrorState) {
            setState(() {
              isLoading = false;
            });
            showCustomFlashbar(context, state.message, "Error");
          }
          // TODO: implement listener
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    margin: EdgeInsets.only(bottom: 5),
                    child: Text(
                      AppLocalization.of(context).trans("title"),
                      style: AppTextStyle.mediumBlack,
                    )),
                DirectionalTextField(
                  controller: _bloc.titleQuizController,
                  decoration: InputDecoration(
                    errorText: _bloc.errorQuizTitle,
                    labelText: AppLocalization.of(context).trans("title"),
                    labelStyle: const TextStyle(color: Colors.grey),
                    prefixIcon: const Icon(
                      Icons.title,
                      color: AppColors.gray,
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.gray),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
                Divider(
                  color: AppColors.deepBlue,
                ),
                Container(
                    margin: EdgeInsets.only(bottom: 5),
                    child: Text(
                      AppLocalization.of(context).trans("title_arabic"),
                      style: AppTextStyle.mediumBlack,
                    )),
                DirectionalTextField(
                  controller: _bloc.titleQuizArabicController,
                  decoration: InputDecoration(
                    errorText: _bloc.errorQuizTitleAr,
                    labelText:
                        AppLocalization.of(context).trans("title_arabic"),
                    labelStyle: const TextStyle(color: Colors.grey),
                    prefixIcon: const Icon(
                      Icons.title,
                      color: AppColors.gray,
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.gray),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
                Divider(
                  color: AppColors.deepBlue,
                ),
                Container(
                    margin: EdgeInsets.only(bottom: 5),
                    child: Text(
                      AppLocalization.of(context).trans("title_english"),
                      style: AppTextStyle.mediumBlack,
                    )),
                DirectionalTextField(
                  controller: _bloc.titleQuizEnglishController,
                  decoration: InputDecoration(
                    errorText: _bloc.errorQuizTitleEn,
                    labelText:
                        AppLocalization.of(context).trans("title_english"),
                    labelStyle: const TextStyle(color: Colors.grey),
                    prefixIcon: const Icon(
                      Icons.title,
                      color: AppColors.gray,
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.gray),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
                Divider(
                  color: AppColors.deepBlue,
                ),
                Container(
                    margin: EdgeInsets.only(bottom: 5),
                    child: Text(
                      AppLocalization.of(context).trans("description"),
                      style: AppTextStyle.mediumBlack,
                    )),
                DirectionalTextField(
                  controller: _bloc.descriptionQuizController,
                  decoration: InputDecoration(
                    errorText: _bloc.errorQuizDescription,
                    labelText: AppLocalization.of(context).trans("description"),
                    labelStyle: const TextStyle(color: Colors.grey),
                    prefixIcon: const Icon(
                      Icons.title,
                      color: AppColors.gray,
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.gray),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
                Divider(
                  color: AppColors.deepBlue,
                ),
                Container(
                    margin: EdgeInsets.only(bottom: 5),
                    child: Text(
                      AppLocalization.of(context).trans("description_arabic"),
                      style: AppTextStyle.mediumBlack,
                    )),
                DirectionalTextField(
                  controller: _bloc.descriptionQuizArabicController,
                  decoration: InputDecoration(
                    errorText: _bloc.errorQuizDescriptionAr,
                    labelText:
                        AppLocalization.of(context).trans("description_arabic"),
                    labelStyle: const TextStyle(color: Colors.grey),
                    prefixIcon: const Icon(
                      Icons.title,
                      color: AppColors.gray,
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.gray),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
                Divider(
                  color: AppColors.deepBlue,
                ),
                Container(
                    margin: EdgeInsets.only(bottom: 5),
                    child: Text(
                      AppLocalization.of(context).trans("description_english"),
                      style: AppTextStyle.mediumBlack,
                    )),
                DirectionalTextField(
                  controller: _bloc.descriptionQuizEnglishController,
                  decoration: InputDecoration(
                    errorText: _bloc.errorQuizDescriptionEn,
                    labelText: AppLocalization.of(context)
                        .trans("description_english"),
                    labelStyle: const TextStyle(color: Colors.grey),
                    prefixIcon: const Icon(
                      Icons.title,
                      color: AppColors.gray,
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.gray),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
                Divider(
                  color: AppColors.deepBlue,
                ),
                Row(
                  children: [
                    Expanded(
                        child: Container(
                      decoration: BoxDecoration(
                          color: AppColors.gray,
                          borderRadius: BorderRadius.circular(10)),
                      child: const Padding(
                        padding: EdgeInsets.all(12.0),
                        child: Center(
                          child: Text(
                            "Back",
                            style: AppTextStyle.mediumWhiteBold,
                          ),
                        ),
                      ),
                    )),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                        child: GestureDetector(
                      onTap: () {
                        _bloc.editQuizInformation(widget.quiz.id);
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            color: AppColors.deepBlue,
                            borderRadius: BorderRadius.circular(10)),
                        child:   Padding(
                          padding: EdgeInsets.all(12.0),
                          child: Center(
                            child:
                            isLoading  ?const  CircularProgressIndicator():const Text(
                              "Edit",
                              style: AppTextStyle.mediumWhiteBold,
                            ),
                          ),
                        ),
                      ),
                    )),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
