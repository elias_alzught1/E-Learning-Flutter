import 'package:e_learning/ui/teacher/lesson_details.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../bloc/teacher/lesson_cubit.dart';
import '../../../controller/data_store.dart';
import '../../../localization/app_localization.dart';
import '../../../models/response/teacher/lessons_response.dart';
import '../../../themes/app_colors.dart';
import '../../../themes/app_font_size.dart';
import '../../../themes/app_text_style.dart';
import '../../../themes/elearning_icons.dart';
import '../../../themes/font_family.dart';

class CurriculumCard extends StatefulWidget {
  final LessonsTeacher lesson;

  const CurriculumCard({Key key, this.lesson}) : super(key: key);

  @override
  State<CurriculumCard> createState() => _CurriculumCardState();
}

class _CurriculumCardState extends State<CurriculumCard> {
  bool _isVisible = false;

  @override
  Widget build(BuildContext context) {
    String lang = DataStore.instance.lang;
    return InkWell(
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(8)),
        ),
        child: Padding(
          padding: EdgeInsetsDirectional.only(start: 2.w, end: 2.w),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  height: 5.sp,
                ),
                Row(
                  children: [
                    SvgPicture.asset("assets/img/svg/tag-name.svg",color: AppColors.red.withOpacity(0.8),),
                    SizedBox(width: 5),
                    Text(
                      lang == "en"
                          ? widget.lesson.title.toString()
                          : widget.lesson.titleAr,
                      style: AppTextStyle.largeBlackBold,
                    ),
                    Spacer(),
                    Icon(
                      _isVisible == true
                          ? Icons.keyboard_arrow_up
                          : Icons.keyboard_arrow_down,
                      color: AppColors.deepBlue,
                    ),
                  ],
                ),
                SizedBox(
                  height: 3,
                ),
                Visibility(
                    visible: _isVisible,
                    child: Text(
                      lang == "en"
                          ? widget.lesson.descriptionEn
                          : widget.lesson.descriptionAr,
                      style: AppTextStyle.largeDeepGray,
                    )),
                const SizedBox(
                  height: 8,
                ),
                Visibility(
                    visible: _isVisible,
                    child: Text(
                      lang == "en" ? "What is include" : "ماذا يتضمن ",
                      style: AppTextStyle.largeBlackBold,
                    )),
                SizedBox(
                  height: 1.h,
                ),
                Visibility(
                  visible: _isVisible,
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Icon(Elearning.play_circle_outline,
                              color: AppColors.red.withOpacity(0.8)),
                          SizedBox(
                            width: 1.w,
                          ),
                          Text(
                            lang == "en"
                                ? "${widget.lesson.videosNumber} Videos"
                                : "${widget.lesson.videosNumber} فيديو ",
                            style: const TextStyle(
                              fontSize: AppFontSize.LARGE,
                              color: Colors.black,
                              fontWeight: FontWeight.normal,
                              fontFamily: FontFamily.cairo,
                            ),
                          ),
                          SizedBox(
                            width: 4.w,
                          ),
                          Icon(Elearning.book,
                              color: AppColors.red.withOpacity(0.8)),
                          SizedBox(
                            width: 1.w,
                          ),
                          Text(
                            lang == "en"
                                ? "${widget.lesson.readsNumber} Reading"
                                : "${widget.lesson.readsNumber} كتب ",
                            style: const TextStyle(
                              fontSize: AppFontSize.LARGE,
                              color: Colors.black,
                              fontWeight: FontWeight.normal,
                              fontFamily: FontFamily.cairo,
                            ),
                          ),
                          SizedBox(
                            width: 4.w,
                          ),
                          Icon(Elearning.live_help,
                              color: AppColors.red.withOpacity(0.8)),
                          SizedBox(
                            width: 1.w,
                          ),
                          Expanded(
                            child: Text(
                              lang == "en"
                                  ? "${widget.lesson.testsNumber} Quizs"
                                  : "${widget.lesson.testsNumber} اختبارات ",
                              style: const TextStyle(
                                fontSize: AppFontSize.LARGE,
                                color: Colors.black,
                                fontWeight: FontWeight.normal,
                                fontFamily: FontFamily.cairo,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 2.h,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 1.h,
                      ),
                      GestureDetector(
                        child: Container(
                          height: 6.h,
                          width: 100.w,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8)),
                              border: Border.all(color: AppColors.purple)),
                          child: Center(
                            child: Text(
                              AppLocalization.of(context)
                                  .trans("view_information"),
                              style: const TextStyle(
                                fontSize: AppFontSize.X_LARGE,
                                color:AppColors.purple,
                                fontWeight: FontWeight.normal,
                                fontFamily: FontFamily.cairo,
                              ),
                            ),
                          ),
                        ),
                        onTap: () {
                          Navigator.of(context).push(
                            CupertinoPageRoute(
                              builder: (context) => BlocProvider(
                                create: (context) => LessonCubit(),
                                child: LessonDetails(lesson: widget.lesson),
                              ),
                            ),
                          );
                          //LessonCubit
                        },
                      ),
                      SizedBox(
                        height: 2.h,
                      ),
                    ],
                  ),
                ),
              ]),
        ),
      ),
      onTap: () {
        setState(() {
          _isVisible = !_isVisible;
        });
      },
    );
  }
}
