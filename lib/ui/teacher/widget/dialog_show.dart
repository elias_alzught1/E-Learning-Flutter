import 'package:e_learning/themes/app_text_style.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';

import '../../../localization/app_localization.dart';
import '../../../themes/app_colors.dart';

Widget myDialog(context, String title, Widget content,  fun,{IconData icon}) {
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          actionsAlignment: MainAxisAlignment.end,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(15.0))),
          title: Column(
            children: [
              Row(
                children: [
                    Icon(
                    icon ??   Icons.edit,
                    color:  AppColors.purple,
                    size: 20,
                  ),
                  const SizedBox(
                    width: 15,
                  ),
                  Text(
                    title,
                    style: AppTextStyle.xLargeBlackBold,
                  ),
                ],
              ),
              Divider(
                color: Colors.black.withOpacity(0.4),
              )
            ],
          ),
          content: content,
          actions: [
            ElevatedButton(
                style: ElevatedButton.styleFrom(

                    primary: AppColors.red.withOpacity(0.8),
                    padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 8),
                    textStyle:
                      AppTextStyle.mediumWhiteBold),
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text(AppLocalization.of(context).trans("cancel"))),
            ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: AppColors.normalGreen.withOpacity(0.6),
                    padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 8),
                    textStyle:
                    AppTextStyle.mediumWhiteBold),
                onPressed: () {
                  fun();
                },
                child: Text(AppLocalization.of(context).trans("confirm"))),
          ],
        );
      });
}
