import 'package:e_learning/bloc/teacher/lesson_cubit.dart';
import 'package:e_learning/helpers/stack_loader_indicator.dart';
import 'package:e_learning/localization/app_localization.dart';
import 'package:e_learning/models/response/shower_response.dart';
import 'package:e_learning/ui/teacher/widget/text_input.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:file_picker/file_picker.dart';
import '../../helpers/Pdf/pdf_api.dart';
import '../../themes/app_colors.dart';
import '../../themes/app_text_style.dart';
import '../../themes/elearning_icons.dart';
import '../widgets/common/Flush_bar.dart';
import '../widgets/textfield/DirectionalTextField.dart';

class EditBook extends StatefulWidget {
  final LessoneMedia book;

  const EditBook({Key key, this.book}) : super(key: key);

  @override
  State<EditBook> createState() => _EditBookState();
}

class _EditBookState extends State<EditBook> {
  LessonCubit _bloc;
  PDFViewController pdfViewController;
  PlatformFile pdfFile;
  final StackLoaderIndicator _loader = StackLoaderIndicator();

  @override
  void initState() {
    _bloc = BlocProvider.of<LessonCubit>(context);
    _bloc.bookPathController.text = widget.book.mediaUrl;
    _bloc.titleBookController.text = widget.book.title;
    _bloc.titleBookArabicController.text = widget.book.titleAr;
    _bloc.titleBookEnglishController.text = widget.book.titleEn;
    _bloc.descriptionBookController.text = widget.book.description;
    _bloc.descriptionBookArabicController.text = widget.book.descriptionAr;
    _bloc.descriptionBookEnglishController.text = widget.book.descriptionEn;
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<LessonCubit, LessonState>(
      listenWhen: (_, current) =>
          current is UpdateBookInfoAwaitState ||
          current is UpdateBookInfoAcceptState ||
          current is UpdateBookInfoErrorState  ,
      listener: (context, state) {
        if (state is UpdateBookInfoAwaitState) {
        } else if (state is UpdateBookInfoErrorState) {
          showCustomFlashbar(context, state.message, "error");
        } else if (state is UpdateBookInfoAcceptState) {
          showCustomFlashbar(context, state.message, "Success_Add");

          Navigator.pop(context);
        }
        // TODO: implement listener
      },
      child: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
              bottom: const TabBar(
                unselectedLabelColor: Colors.grey,
                indicatorColor: Colors.green,
                labelColor: Colors.black,
                tabs: [
                  Tab(
                    icon: Icon(Icons.edit_calendar_outlined),
                    text: "Edit Details",
                  ),
                  Tab(icon: Icon(Elearning.book), text: "Edit Book"),
                ],
              ),
              centerTitle: true,
              leading: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: const Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black,
                ),
              ),
              backgroundColor: Colors.white,
              title: const Text(
                "Edit Book Details",
                style: TextStyle(color: Colors.black),
              )),
          body: TabBarView(children: [
            SingleChildScrollView(child: editBooksDetails()),
            Container(height: 200, child: changeBook())
          ]),
        ),
      ),
    );
  }

  Widget changeBook() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 15,
        ),
        Container(
            margin: EdgeInsetsDirectional.only(start: 3.w, end: 2.w),
            child: const Text(
              "Book",
              style: AppTextStyle.smallDeepGray,
            )),
        SizedBox(
          height: 1.h,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Flexible(
              child: Container(
                margin: EdgeInsetsDirectional.only(start: 2.w, end: 2.w),
                height: 60.sp,
                child: DirectionalTextField(
                  readOnly: true,
                  controller: _bloc.bookPathController,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: AppColors.lighterGray.withOpacity(0.8),
                    prefixIcon: const Icon(
                      Icons.description,
                      color: AppColors.gray,
                      size: 20,
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: AppColors.lighterGray.withOpacity(0.35)),
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: AppColors.lighterGray.withOpacity(0.35)),
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: AppColors.lighterGray.withOpacity(0.35)),
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: AppColors.lighterGray.withOpacity(0.35)),
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 48.sp,
              child: ElevatedButton(
                onPressed: () async {
                  if (pdfFile != null) {
                    setState(() {
                      pdfFile = null;
                      _bloc.bookPathController.text = widget.book.mediaUrl;
                    });
                  } else {
                    _loader.show(context);
                    final file = await PDFApi.loadNetwork(widget.book.mediaUrl);
                    _loader.dismiss();
                    PDFApi.openPDF(context, file);
                  }
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.green,
                  onPrimary: Colors.grey,
                  // Disable color
                ),
                child: Text(
                  pdfFile != null
                      ? AppLocalization.of(context).trans("clear")
                      : AppLocalization.of(context).trans("preview"),
                  style: const TextStyle(color: Colors.white),
                ),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 15,
        ),
        Center(
          child: ElevatedButton(
            onPressed: () {
              pdfFile != null
                  ? _bloc.changePdfFile(widget.book.id, pdfFile.path.toString())
                  : pickSinglePDFFile();
            },
            style: ElevatedButton.styleFrom(
              padding: EdgeInsets.symmetric(horizontal: 40, vertical: 13),
              primary: Colors.green,
              onPrimary: Colors.grey,
              // Disable color
            ),
            child: Text(
              pdfFile != null
                  ? AppLocalization.of(context).trans("submit")
                  : AppLocalization.of(context).trans("edit"),
              style: const TextStyle(color: Colors.white),
            ),
          ),
        )
      ],
    );
  }

  Future<void> pickSinglePDFFile() async {
    try {
      FilePickerResult result = await FilePicker.platform.pickFiles(
        type: FileType.custom,
        allowedExtensions: ['pdf'],
        allowMultiple: false, // Set this to false to select only one file
      );

      if (result != null) {
        // Handle the selected PDF file here
        setState(() {
          pdfFile = result.files.first;
          _bloc.bookPathController.text = pdfFile.path;
        });
        print('Selected PDF file path: ${pdfFile.path}');
      } else {
        // User canceled the file picker
      }
    } catch (e) {
      // Handle any errors that occur during file picking
      print('Error picking a PDF file: $e');
    }
  }

  Widget editBooksDetails() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        buildTextFailedForm(
            _bloc.titleBookController,
            Icons.title,
            _bloc.errorBookInfoTitle,
            AppLocalization.of(context).trans("title"),
            errorText: _bloc.errorBookInfoTitle,
            iconColor: Colors.green),
        buildTextFailedForm(
            _bloc.titleBookArabicController,
            Icons.title,
            _bloc.errorBookInfoArabicTitle,
            AppLocalization.of(context).trans("title_arabic"),
            errorText: _bloc.errorBookInfoArabicTitle,
            iconColor: Colors.green),
        buildTextFailedForm(
            _bloc.titleBookEnglishController,
            Icons.title,
            _bloc.errorBookInfoEnglishTitle,
            AppLocalization.of(context).trans("title_english"),
            errorText: _bloc.errorBookInfoEnglishTitle,
            iconColor: Colors.green),
        Divider(
          color: Colors.green.withOpacity(0.4),
        ),
        buildTextFailedForm(
            _bloc.descriptionBookController,
            Icons.description,
            _bloc.errorBookInfoDescription,
            AppLocalization.of(context).trans("description"),
            errorText: _bloc.errorBookInfoDescription,
            iconColor: Colors.green),
        buildTextFailedForm(
            _bloc.descriptionBookArabicController,
            Icons.description,
            _bloc.errorBookInfoArabicDescription,
            AppLocalization.of(context).trans("description_arabic"),
            errorText: _bloc.errorBookInfoArabicDescription,
            iconColor: Colors.green),
        buildTextFailedForm(
            _bloc.descriptionBookEnglishController,
            Icons.description,
            _bloc.errorBookInfoEnglishDescription,
            AppLocalization.of(context).trans("description_english"),
            errorText: _bloc.errorBookInfoEnglishDescription,
            iconColor: Colors.green),
        const SizedBox(
          height: 25,
        ),
        Center(
          child: ElevatedButton(
            onPressed: () {
              _bloc.updateBookInfo(widget.book.id);
            },
            style: ElevatedButton.styleFrom(
              padding: EdgeInsets.symmetric(horizontal: 40, vertical: 13),
              primary: Colors.green,
              onPrimary: Colors.grey,
              // Disable color
            ),
            child: Text(
              AppLocalization.of(context).trans("edit"),
              style: const TextStyle(color: Colors.white),
            ),
          ),
        )
      ],
    );
  }
}
