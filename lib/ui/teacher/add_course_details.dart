import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:e_learning/themes/app_text_style.dart';
import 'package:e_learning/themes/elearning_icons.dart';
import 'package:e_learning/ui/teacher/widget/curriculum_card.dart';
import 'package:e_learning/ui/teacher/widget/dialog_show.dart';
import 'package:e_learning/ui/teacher/widget/text_input.dart';
import 'package:e_learning/ui/teacher/widget/unordered_listItem.dart';
import 'package:e_learning/ui/widgets/widget.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';

import '../../bloc/teacher/add_course/add_course_bloc.dart';
import '../../controller/data_store.dart';
import '../../helpers/helpers.dart';
import '../../helpers/stack_loader_indicator.dart';
import '../../localization/app_localization.dart';
import '../../models/response/teacher/add_course_response.dart';
import '../../themes/app_colors.dart';
import '../../themes/app_font_size.dart';
import '../../themes/font_family.dart';
import '../course/widget/unordered_listItem.dart';
import '../widgets/appbar/abbBarAPP.dart';
import '../widgets/appbar/abbBarAPP.dart';
import '../widgets/appbar/abbBottomBar.dart';
import '../widgets/common/Flush_bar.dart';

class AddCourseDetails extends StatefulWidget {
  final AddCourseResponse course;

  AddCourseDetails({Key key, this.course}) : super(key: key);

  @override
  State<AddCourseDetails> createState() => _AddCourseDetailsState();
}

class _AddCourseDetailsState extends State<AddCourseDetails> {
  AddCourseBloc _bloc;
  final StackLoaderIndicator _loader = StackLoaderIndicator();
  final ImagePicker _picker = ImagePicker();
  File _imageFile; // picked image will be store here.
  Uint8List imageBytes;

  void initState() {
    // TODO: implement initState
    _bloc = BlocProvider.of<AddCourseBloc>(context);
    _bloc.add(GetLessonEvent(widget.course.data.id));
  }

  void _pickImageBase64(setState) async {
    try {
      final XFile image = await _picker.pickImage(source: ImageSource.gallery);
      if (image == null) return;
      Uint8List imagebytes = await image.readAsBytes();
      String _base64String = base64.encode(imagebytes);
      print(_base64String);
      String fileName = image.path.split('/').last;
      FormData formData = FormData.fromMap({
        "file": await MultipartFile.fromFile(image.path, filename: fileName),
      });
      print(fileName);
      final imageTemp = File(image.path);
      setState(() {
        _bloc.imageLessonBloc = imageTemp;
        _bloc.fileLessonName = fileName;
        this.imageBytes = imagebytes;
        this._imageFile =
            imageTemp; // setState to image the UI and show picked image on screen.
      });
    } on PlatformException catch (e) {
      if (kDebugMode) {
        print('error');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    String lang = DataStore.instance.lang;
    return BlocConsumer<AddCourseBloc, AddCourseState>(
      listenWhen: (previous, current) =>
          current is AddIncludeDescriptionAcceptState ||
          current is AddIncludeDescriptionErrorState ||
          current is DeleteIncludeDescriptionAcceptState ||
          current is AddIncludeDescriptionAwaitState ||
          current is DeleteIncludeDescriptionErrorState ||
          current is DeleteIncludeDescriptionAwaitState ||
          current is AddCurriculumAcceptState,
      listener: (context, state) {
        if (state is AddIncludeDescriptionAcceptState) {
          Navigator.pop(context);
          _loader.dismiss();
          setState(() {
            widget.course.data = state.response.data;
          });
        } else if (state is DeleteIncludeDescriptionAcceptState) {
          Navigator.pop(context);
          _loader.dismiss();
          setState(() {
            widget.course.data = state.response.data;
          });
        } else if (state is DeleteIncludeDescriptionErrorState) {
          _loader.dismiss();
          showCustomFlashbar(context, state.message,
              AppLocalization.of(context).trans("failure"));
        } else if (state is AddIncludeDescriptionErrorState) {
          _loader.dismiss();
          showCustomFlashbar(context, state.message,
              AppLocalization.of(context).trans("failure"));
        } else if (state is AddIncludeDescriptionAwaitState ||
            state is DeleteIncludeDescriptionAwaitState) {
          _loader.show(context);
        } else if (state is AddCurriculumAcceptState) {
          _bloc.add(GetLessonEvent(widget.course.data.id));
        }

        // TODO: implement listener
      },
      builder: (context, state) {
        return Scaffold(
            appBar: AppBarApp(),
            body: SingleChildScrollView(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Stack(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image: CachedNetworkImageProvider(
                                  widget.course.data.image,
                                  errorListener: () {
                                    Text(
                                      'Loading',
                                      style: TextStyle(
                                        fontSize: 20,
                                      ),
                                    );
                                  },
                                )),
                          ),
                          child: Stack(
                            children: [
                              Positioned.fill(
                                child: Container(
                                  width: 100.w,
                                  color: Colors.black.withOpacity(
                                      0.8), // Adjust opacity as needed
                                ),
                              ),
                              Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  children: [
                                    Container(
                                      margin: EdgeInsetsDirectional.only(
                                          start: 2.w, top: 4.w),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Flexible(
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              child: CachedNetworkImage(
                                                width: 20.h,
                                                height: 20.h,
                                                imageUrl:
                                                    widget.course.data.image,
                                                fit: BoxFit.fill,
                                                placeholder: (context, url) =>
                                                    getShimmer(),
                                                errorWidget:
                                                    (context, url, error) =>
                                                        const Center(
                                                  child: Icon(Icons.error),
                                                ),
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 2.w,
                                          ),
                                          Flexible(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  lang == "en"
                                                      ? widget
                                                          .course.data.nameEn
                                                      : widget
                                                          .course.data.nameAr,
                                                  style: AppTextStyle
                                                      .largeWhiteBold,
                                                ),
                                                Text(
                                                  lang == "en"
                                                      ? widget.course.data
                                                          .descriptionEn
                                                      : widget.course.data
                                                          .descriptionAr,
                                                  style: AppTextStyle
                                                      .mediumWhiteBold,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: 2.h,
                                    ),
                                    SizedBox(
                                      height: 1.h,
                                    ),
                                    Row(
                                      children: [
                                        Padding(
                                          padding: EdgeInsetsDirectional.only(
                                              start: 3.w),
                                          child: Text(
                                            AppLocalization.of(context)
                                                .trans("Created_By"),
                                            style: AppTextStyle.mediumYellow,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      margin: EdgeInsetsDirectional.only(
                                          start: 2.w, end: 2.w),
                                      height: 20.sp,
                                      child: ListView.builder(
                                        scrollDirection: Axis.horizontal,
                                        itemCount:
                                            widget.course.data.authors.length,
                                        itemBuilder: (context, i) {
                                          return Container(
                                            alignment: Alignment.center,
                                            margin: const EdgeInsets.only(
                                                left: 2, right: 2),
                                            child: Text(
                                              "  ${widget.course.data.authors[i].firstName} ${widget.course.data.authors[i].lastName}  ",
                                              style: AppTextStyle
                                                  .largeYellowAccent,
                                            ),
                                          );
                                        },
                                      ),
                                    ),
                                    SizedBox(
                                      height: 2.h,
                                    ),
                                    lang == "en"
                                        ? RichText(
                                            text: TextSpan(
                                              children: [
                                                WidgetSpan(
                                                  child: Padding(
                                                    padding:
                                                        EdgeInsetsDirectional
                                                            .only(
                                                                start: 3.w,
                                                                end: 1.w),
                                                    child: Icon(
                                                      Icons.language,
                                                      size: 20,
                                                      color: AppColors.red,
                                                    ),
                                                  ),
                                                ),
                                                TextSpan(
                                                    text: widget
                                                        .course.data.language,
                                                    style: AppTextStyle
                                                        .largeWhite),
                                              ],
                                            ),
                                          )
                                        : RichText(
                                            text: TextSpan(
                                              children: [
                                                TextSpan(
                                                    text: widget
                                                        .course.data.language,
                                                    style: AppTextStyle
                                                        .largeGreen),
                                                WidgetSpan(
                                                  child: Padding(
                                                    padding:
                                                        EdgeInsetsDirectional
                                                            .only(
                                                                start: 3.w,
                                                                end: 1.w),
                                                    child: const Icon(
                                                      Icons.language,
                                                      size: 20,
                                                      color: AppColors.red,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                    SizedBox(
                                      height: 2.h,
                                    ),
                                    Container(
                                      width: 100.w,
                                      child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            SizedBox(
                                              height: 4.h,
                                            ),
                                            Padding(
                                              padding:
                                                  EdgeInsetsDirectional.only(
                                                      start: 4.w),
                                              child: Text(
                                                widget.course.data.rating,
                                                style: AppTextStyle.largeWhite,
                                              ),
                                            ),
                                            SizedBox(
                                              height: 2.h,
                                            ),
                                            Padding(
                                              padding:
                                                  EdgeInsetsDirectional.only(
                                                      start: 2.w),
                                              child: Center(
                                                child: Row(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: List.generate(
                                                        5,
                                                        (index) => buildStar(
                                                            context,
                                                            index,
                                                            double.parse(widget
                                                                .course
                                                                .data
                                                                .rating),
                                                            size: 21))),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 8,
                                            ),
                                          ]),
                                    ),
                                  ]),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12),
                        color: AppColors.red,
                      ),
                      margin: EdgeInsetsDirectional.only(
                          start: 4.w, end: 4.w, top: 2.h, bottom: 2.h),
                      child: (Center(
                        child: Padding(
                            padding: EdgeInsets.only(top: 1.5.h, bottom: 1.5.h),
                            child: Text(
                              AppLocalization.of(context).trans("pending"),
                              style: AppTextStyle.largeWhiteBold,
                            )),
                      )),
                    ),
                    SizedBox(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 25),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 4.h,
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Icon(
                                  Icons.access_time,
                                  color: AppColors.red,
                                  size: 17.sp,
                                ),
                                SizedBox(width: 2.w),
                                Text(
                                  AppLocalization.of(context)
                                      .trans("Course_Duration"),
                                  style: const TextStyle(
                                      color: AppColors.deepGray,
                                      fontSize: AppFontSize.LARGE,
                                      fontFamily: FontFamily.cairo,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(width: 4.w),
                                Spacer(),
                                Text(
                                  widget.course.data.duartion == null
                                      ? "1-4 Weeks"
                                      : lang == "en"
                                          ? widget
                                              .course.data.duartion.durationEn
                                          : widget
                                              .course.data.duartion.durationAr,
                                  style: const TextStyle(
                                      color: AppColors.lightPurple,
                                      fontSize: AppFontSize.LARGE,
                                      fontFamily: FontFamily.cairo),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 1.h,
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Icon(
                                  Icons.leaderboard_sharp,
                                  color: AppColors.red,
                                  size: 17.sp,
                                ),
                                SizedBox(width: 2.w),
                                Text(
                                  AppLocalization.of(context)
                                      .trans("Course_Level"),
                                  style: const TextStyle(
                                      color: AppColors.deepGray,
                                      fontSize: AppFontSize.LARGE,
                                      fontFamily: FontFamily.cairo,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(width: 4.w),
                                Spacer(),
                                Text(
                                  widget.course.data.level == null
                                      ? lang == "en"
                                          ? "Beginner"
                                          : "مبتدئ"
                                      : lang == "en"
                                          ? widget.course.data.level.levelEn
                                          : widget.course.data.level.levelAr,
                                  style: const TextStyle(
                                      color: AppColors.lightPurple,
                                      fontSize: AppFontSize.LARGE,
                                      fontFamily: FontFamily.cairo),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 1.h,
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Icon(
                                  Icons.supervised_user_circle_sharp,
                                  color: AppColors.red,
                                  size: 17.sp,
                                ),
                                SizedBox(width: 2.w),
                                Text(
                                  AppLocalization.of(context)
                                      .trans("Students_Enrolled"),
                                  style: const TextStyle(
                                      color: AppColors.deepGray,
                                      fontSize: AppFontSize.LARGE,
                                      fontFamily: FontFamily.cairo,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(width: 4.w),
                                const Spacer(),
                                const Text(
                                  "44.99",
                                  style: const TextStyle(
                                      color: AppColors.lightPurple,
                                      fontSize: AppFontSize.LARGE,
                                      fontFamily: FontFamily.cairo),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 1.h,
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Icon(
                                  Icons.language,
                                  color: AppColors.red,
                                  size: 17.sp,
                                ),
                                SizedBox(width: 2.w),
                                Text(
                                  AppLocalization.of(context).trans("language"),
                                  style: const TextStyle(
                                      color: AppColors.deepGray,
                                      fontSize: AppFontSize.LARGE,
                                      fontFamily: FontFamily.cairo,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(width: 4.w),
                                const Spacer(),
                                Text(
                                  widget.course.data.language,
                                  style: const TextStyle(
                                      color: AppColors.lightPurple,
                                      fontSize: AppFontSize.LARGE,
                                      fontFamily: FontFamily.cairo),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsetsDirectional.only(start: 4.w, end: 4.w),
                      child: editButton(),
                    ),
                    Container(
                      child: Padding(
                        padding:
                            EdgeInsetsDirectional.only(start: 4.w, end: 4.w),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              SizedBox(
                                height: 2.h,
                              ),
                              Text(
                                AppLocalization.of(context)
                                    .trans("Course_included"),
                                style: AppTextStyle.xLargeBlackBold,
                              ),
                              widget.course.data.includes != null &&
                                      widget.course.data.includes.isNotEmpty
                                  ? Container(
                                      margin: EdgeInsetsDirectional.only(
                                          start: 3.w),
                                      child: ListView.builder(
                                          physics:
                                              const NeverScrollableScrollPhysics(),
                                          scrollDirection: Axis.vertical,
                                          shrinkWrap: true,
                                          itemCount: widget
                                              .course.data.includes.length,
                                          itemBuilder: (BuildContext context,
                                              int index) {
                                            return Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: <Widget>[
                                                Icon(
                                                  Icons.circle,
                                                  color:AppColors.deepGray,
                                                  size: 15.sp,
                                                ),
                                                SizedBox(
                                                  width: 1.3.w,
                                                ),
                                                Expanded(
                                                  child: Text(
                                                    lang == "en"
                                                        ? widget
                                                            .course
                                                            .data
                                                            .includes[index]
                                                            .includeEn
                                                            .toString()
                                                        : widget
                                                            .course
                                                            .data
                                                            .includes[index]
                                                            .includeAr
                                                            .toString(),
                                                    style:const  TextStyle(
                                                        color:AppColors.deepGray,
                                                        fontSize:
                                                            AppFontSize.MEDIUM,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontFamily:
                                                            FontFamily.cairo),
                                                  ),
                                                ),
                                                const Spacer(),
                                                InkWell(
                                                  onTap: () {
                                                    showDialog(
                                                      content: Form(
                                                          child: BlocProvider
                                                              .value(
                                                        value: _bloc,
                                                        child: BlocBuilder<
                                                            AddCourseBloc,
                                                            AddCourseState>(
                                                          buildWhen: (previous,
                                                                  current) =>
                                                              current is AddIncludeDescriptionErrorState ||
                                                              current
                                                                  is AddIncludeDescriptionValidateState ||
                                                              current
                                                                  is AddIncludeDescriptionAcceptState ||
                                                              current
                                                                  is AddIncludeDescriptionAwaitState,
                                                          builder:
                                                              (context, state) {
                                                            return Container(
                                                              height: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .height /
                                                                  3.5,
                                                              child: Column(
                                                                children: [
                                                                  buildTextFailedForm(
                                                                      _bloc
                                                                          .arabicIncludeDescription,
                                                                      Icons
                                                                          .description,
                                                                      _bloc
                                                                          .arabicErrorInclude,
                                                                      AppLocalization.of(
                                                                              context)
                                                                          .trans(
                                                                              "arabic_description_add_course"),
                                                                      errorText:
                                                                          _bloc
                                                                              .arabicErrorInclude),
                                                                  buildTextFailedForm(
                                                                      _bloc
                                                                          .englishIncludeDescription,
                                                                      Icons
                                                                          .description,
                                                                      _bloc
                                                                          .englishErrorInclude,
                                                                      AppLocalization.of(
                                                                              context)
                                                                          .trans(
                                                                              "english_description_add_course"),
                                                                      errorText:
                                                                          _bloc
                                                                              .englishErrorInclude),
                                                                ],
                                                              ),
                                                            );
                                                          },
                                                        ),
                                                      )),
                                                      fun: () => _bloc.add(
                                                          AddIncludeDescription(
                                                              widget.course.data
                                                                  .id,
                                                              includeId: widget
                                                                  .course
                                                                  .data
                                                                  .includes[
                                                                      index]
                                                                  .id)),
                                                      title: AppLocalization.of(
                                                              context)
                                                          .trans(
                                                              "edit_course_information"),
                                                    );
                                                  },
                                                  child: Icon(
                                                    Icons.edit,
                                                    color: AppColors.normalGreen,
                                                    size: 15.sp,
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 6.w,
                                                ),
                                                InkWell(
                                                  child: Icon(
                                                    Elearning.cancel,
                                                    color: AppColors.red,
                                                    size: 12.sp,
                                                  ),
                                                  onTap: () {
                                                    showDialog(
                                                      content: Text(
                                                        AppLocalization.of(
                                                                context)
                                                            .trans(
                                                                "delete_include"),
                                                        style: AppTextStyle
                                                            .mediumGray,
                                                      ),
                                                      fun: () => _bloc.add(
                                                          DeleteIncludeEvent(
                                                              widget
                                                                  .course
                                                                  .data
                                                                  .includes[
                                                                      index]
                                                                  .id,
                                                              widget.course.data
                                                                  .id)),
                                                      title: AppLocalization.of(
                                                              context)
                                                          .trans(
                                                              "edit_course_information"),
                                                    );
                                                  },
                                                ),
                                              ],
                                            );
                                          }),
                                    )
                                  : Center(
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                          left: 13.w,
                                          right: 13.w,
                                          bottom: 2.h,
                                          top: 3.h,
                                        ),
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          child: Container(
                                            child: SvgPicture.asset(
                                              'assets/img/svg/illustration_empty_content.svg',
                                              width: 85.sp,
                                              height: 85.sp,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                              widget.course.data.includes != null &&
                                      widget.course.data.includes.isNotEmpty
                                  ? SizedBox()
                                  : Center(
                                      child: Text(
                                        AppLocalization.of(context)
                                            .trans("empty_content"),
                                        style: AppTextStyle.mediumDeepGrayBold,
                                      ),
                                    ),
                              SizedBox(
                                height: 3.h,
                              ),
                              editButton(
                                  fun: () => _bloc.add(AddIncludeDescription(
                                      widget.course.data.id)),
                                  content: Form(
                                      child: BlocProvider.value(
                                    value: _bloc,
                                    child: BlocBuilder<AddCourseBloc,
                                        AddCourseState>(
                                      buildWhen: (previous, current) =>
                                          current is AddIncludeDescriptionErrorState ||
                                          current
                                              is AddIncludeDescriptionValidateState ||
                                          current
                                              is AddIncludeDescriptionAcceptState ||
                                          current
                                              is AddIncludeDescriptionAwaitState,
                                      builder: (context, state) {
                                        return Container(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height /
                                              3.5,
                                          child: Column(
                                            children: [
                                              buildTextFailedForm(
                                                  _bloc
                                                      .arabicIncludeDescription,
                                                  Icons.description,
                                                  _bloc.arabicErrorInclude,
                                                  AppLocalization.of(context).trans(
                                                      "arabic_description_add_course"),
                                                  errorText:
                                                      _bloc.arabicErrorInclude),
                                              buildTextFailedForm(
                                                  _bloc
                                                      .englishIncludeDescription,
                                                  Icons.description,
                                                  _bloc.englishErrorInclude,
                                                  AppLocalization.of(context).trans(
                                                      "english_description_add_course"),
                                                  errorText: _bloc
                                                      .englishErrorInclude),
                                            ],
                                          ),
                                        );
                                      },
                                    ),
                                  )),
                                  titleButton:
                                      AppLocalization.of(context).trans("add"),
                                  icon: Icons.add)

                              // Container(color: Colors.black,width: 20,)
                            ]),
                      ),
                    ),
                    Container(
                      child: Padding(
                        padding:
                            EdgeInsetsDirectional.only(start: 4.w, end: 4.w),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SizedBox(
                              height: 2.h,
                            ),
                            Text(
                              AppLocalization.of(context)
                                  .trans("What_Will_Learn"),
                              style: AppTextStyle.xLargeBlackBold,
                            ),
                            widget.course.data.aims.isEmpty
                                ? Center(
                                    child: Padding(
                                      padding: EdgeInsets.only(
                                        left: 13.w,
                                        right: 13.w,
                                        bottom: 2.h,
                                        top: 3.h,
                                      ),
                                      child: Column(
                                        children: [
                                          ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            child: Container(
                                              child: SvgPicture.asset(
                                                'assets/img/svg/illustration_empty_content.svg',
                                                width: 85.sp,
                                                height: 85.sp,
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            height: 2.h,
                                          ),
                                          Center(
                                            child: Text(
                                              AppLocalization.of(context)
                                                  .trans("empty_content"),
                                              style: AppTextStyle
                                                  .mediumDeepGrayBold,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                                : Container(
                                    margin:
                                        EdgeInsetsDirectional.only(start: 3.w),
                                    child: ListView.builder(
                                        physics:
                                            const NeverScrollableScrollPhysics(),
                                        scrollDirection: Axis.vertical,
                                        shrinkWrap: true,
                                        itemCount:
                                            widget.course.data.aims.length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return UnorderedListItemTeacher(
                                              lang == "en"
                                                  ? widget.course.data
                                                      .aims[index].aimEn
                                                      .toString()
                                                  : widget.course.data
                                                      .aims[index].aimAr
                                                      .toString(),
                                              Icon(
                                                Icons.circle,
                                                color: AppColors.deepGray,
                                                size: 15.sp,
                                              ),
                                              padding: 4,
                                              appTextStyle: const TextStyle(
                                                  color:AppColors.deepGray,
                                                  fontSize:
                                                  AppFontSize.MEDIUM,
                                                  fontWeight:
                                                  FontWeight.bold,
                                                  fontFamily:
                                                  FontFamily.cairo),
                                              funDelete: () => showDialog(
                                                    content: Text(
                                                      AppLocalization.of(
                                                              context)
                                                          .trans(
                                                              "delete_include"),
                                                      style: AppTextStyle
                                                          .mediumGray,
                                                    ),
                                                    fun: () => _bloc.add(
                                                        DeleteAimsEvent(
                                                            widget.course.data
                                                                .aims[index].id,
                                                            widget.course.data
                                                                .id)),
                                                    title: AppLocalization.of(
                                                            context)
                                                        .trans(
                                                            "edit_course_information"),
                                                  ),
                                              funEdit: () => showDialog(
                                                    content: Form(
                                                        child:
                                                            BlocProvider.value(
                                                      value: _bloc,
                                                      child: BlocBuilder<
                                                          AddCourseBloc,
                                                          AddCourseState>(
                                                        buildWhen: (previous,
                                                                current) =>
                                                            current is AddIncludeDescriptionErrorState ||
                                                            current
                                                                is AddAimsValidateState ||
                                                            current
                                                                is AddIncludeDescriptionAcceptState ||
                                                            current
                                                                is AddIncludeDescriptionAwaitState,
                                                        builder:
                                                            (context, state) {
                                                          return Container(
                                                            height: MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .height /
                                                                3.5,
                                                            child: Column(
                                                              children: [
                                                                buildTextFailedForm(
                                                                    _bloc
                                                                        .arabicAims,
                                                                    Icons
                                                                        .description,
                                                                    _bloc
                                                                        .arabicErrorAims,
                                                                    AppLocalization.of(
                                                                            context)
                                                                        .trans(
                                                                            "aim_in_arabic_language"),
                                                                    errorText: _bloc
                                                                        .arabicErrorAims),
                                                                buildTextFailedForm(
                                                                  _bloc
                                                                      .englishAims,
                                                                  Icons
                                                                      .description,
                                                                  _bloc
                                                                      .englishErrorAims,
                                                                  AppLocalization.of(
                                                                          context)
                                                                      .trans(
                                                                          "aim_in_english_language"),
                                                                  errorText: _bloc
                                                                      .englishErrorAims,
                                                                ),
                                                              ],
                                                            ),
                                                          );
                                                        },
                                                      ),
                                                    )),
                                                    fun: () => _bloc.add(
                                                        AddAimsCourse(
                                                            widget
                                                                .course.data.id,
                                                            aimsId: widget
                                                                .course
                                                                .data
                                                                .aims[index]
                                                                .id)),
                                                    title: AppLocalization.of(
                                                            context)
                                                        .trans(
                                                            "edit_course_information"),
                                                  ));
                                        }),
                                  ),
                            editButton(
                              fun: () => _bloc
                                  .add(AddAimsCourse(widget.course.data.id)),
                              content: Form(
                                  child: BlocProvider.value(
                                value: _bloc,
                                child:
                                    BlocBuilder<AddCourseBloc, AddCourseState>(
                                  buildWhen: (previous, current) =>
                                      current
                                          is AddIncludeDescriptionErrorState ||
                                      current is AddAimsValidateState ||
                                      current
                                          is AddIncludeDescriptionAcceptState ||
                                      current
                                          is AddIncludeDescriptionAwaitState,
                                  builder: (context, state) {
                                    return Container(
                                      height:
                                          MediaQuery.of(context).size.height /
                                              3.5,
                                      child: Column(
                                        children: [
                                          buildTextFailedForm(
                                              _bloc.arabicAims,
                                              Icons.description,
                                              _bloc.arabicErrorAims,
                                              AppLocalization.of(context).trans(
                                                  "aim_in_arabic_language"),
                                              errorText: _bloc.arabicErrorAims),
                                          buildTextFailedForm(
                                            _bloc.englishAims,
                                            Icons.description,
                                            _bloc.englishErrorAims,
                                            AppLocalization.of(context).trans(
                                                "aim_in_english_language"),
                                            errorText: _bloc.englishErrorAims,
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                ),
                              )),
                            ),
                            // editButton(),
                          ],
                          //What_Will_Learn
                        ),
                      ),
                    ),
                    Container(
                      child: Padding(
                        padding:
                            EdgeInsetsDirectional.only(start: 4.w, end: 4.w),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SizedBox(
                              height: 2.h,
                            ),
                            Text(
                              AppLocalization.of(context)
                                  .trans("who_this_course"),
                              style: AppTextStyle.xLargeBlackBold,
                            ),
                            widget.course.data.tarqetAudience.isEmpty
                                ? Center(
                                    child: Padding(
                                      padding: EdgeInsets.only(
                                        left: 13.w,
                                        right: 13.w,
                                        bottom: 2.h,
                                        top: 3.h,
                                      ),
                                      child: Column(
                                        children: [
                                          ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            child: Container(
                                              child: SvgPicture.asset(
                                                'assets/img/svg/illustration_empty_content.svg',
                                                width: 85.sp,
                                                height: 85.sp,
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            height: 2.h,
                                          ),
                                          Center(
                                            child: Text(
                                              AppLocalization.of(context)
                                                  .trans("empty_content"),
                                              style: AppTextStyle
                                                  .mediumDeepGrayBold,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                                : Container(
                                    margin:
                                        EdgeInsetsDirectional.only(start: 3.w),
                                    child: Container(
                                      child: ListView.builder(
                                          physics:
                                              const NeverScrollableScrollPhysics(),
                                          scrollDirection: Axis.vertical,
                                          shrinkWrap: true,
                                          itemCount: widget.course.data
                                              .tarqetAudience.length,
                                          itemBuilder: (BuildContext context,
                                              int index) {
                                            return UnorderedListItemTeacher(
                                                lang == "en"
                                                    ? widget
                                                        .course
                                                        .data
                                                        .tarqetAudience[index]
                                                        .targetEn
                                                        .toString()
                                                    : widget
                                                        .course
                                                        .data
                                                        .tarqetAudience[index]
                                                        .targetAr
                                                        .toString(),
                                                Icon(
                                                  Icons.circle,
                                                  color:AppColors.deepGray,
                                                  size: 15.sp,
                                                ),
                                                appTextStyle: const TextStyle(
                                                    color:AppColors.deepGray,
                                                    fontSize:
                                                    AppFontSize.MEDIUM,
                                                    fontWeight:
                                                    FontWeight.bold,
                                                    fontFamily:
                                                    FontFamily.cairo),
                                                funDelete: () => showDialog(
                                                      content: Text(
                                                        AppLocalization.of(
                                                                context)
                                                            .trans(
                                                                "delete_include"),
                                                        style: AppTextStyle
                                                            .mediumGray,
                                                      ),
                                                      fun: () => _bloc.add(
                                                          DeleteTargetEvent(
                                                              widget
                                                                  .course
                                                                  .data
                                                                  .tarqetAudience[
                                                                      index]
                                                                  .id,
                                                              widget.course.data
                                                                  .id)),
                                                      title: AppLocalization.of(
                                                              context)
                                                          .trans(
                                                              "edit_course_information"),
                                                    ),
                                                funEdit: () => showDialog(
                                                      content: Form(
                                                          child: BlocProvider
                                                              .value(
                                                        value: _bloc,
                                                        child: BlocBuilder<
                                                            AddCourseBloc,
                                                            AddCourseState>(
                                                          buildWhen: (previous,
                                                                  current) =>
                                                              current is AddIncludeDescriptionErrorState ||
                                                              current
                                                                  is AddTargetValidateState ||
                                                              current
                                                                  is AddIncludeDescriptionAcceptState ||
                                                              current
                                                                  is AddIncludeDescriptionAwaitState,
                                                          builder:
                                                              (context, state) {
                                                            return Container(
                                                              height: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .height /
                                                                  3.5,
                                                              child: Column(
                                                                children: [
                                                                  buildTextFailedForm(
                                                                      _bloc
                                                                          .arabicTarget,
                                                                      Icons
                                                                          .description,
                                                                      _bloc
                                                                          .arabicErrorTarget,
                                                                      AppLocalization.of(
                                                                              context)
                                                                          .trans(
                                                                              "target_in_arabic_language"),
                                                                      errorText:
                                                                          _bloc
                                                                              .arabicErrorTarget),
                                                                  buildTextFailedForm(
                                                                      _bloc
                                                                          .englishTarget,
                                                                      Icons
                                                                          .description,
                                                                      _bloc
                                                                          .englishErrorTarget,
                                                                      AppLocalization.of(
                                                                              context)
                                                                          .trans(
                                                                              "target_in_english_language"),
                                                                      errorText:
                                                                          _bloc
                                                                              .englishErrorTarget),
                                                                ],
                                                              ),
                                                            );
                                                          },
                                                        ),
                                                      )),
                                                      fun: () => _bloc.add(
                                                          AddTargetCourse(
                                                              widget.course.data
                                                                  .id,
                                                              targetId: widget
                                                                  .course
                                                                  .data
                                                                  .tarqetAudience[
                                                                      index]
                                                                  .id)),
                                                      title: AppLocalization.of(
                                                              context)
                                                          .trans(
                                                              "edit_course_information"),
                                                    ));
                                          }),
                                    ),
                                  ),
                            editButton(
                              fun: () => _bloc
                                  .add(AddTargetCourse(widget.course.data.id)),
                              content: Form(
                                  child: BlocProvider.value(
                                value: _bloc,
                                child:
                                    BlocBuilder<AddCourseBloc, AddCourseState>(
                                  buildWhen: (previous, current) =>
                                      current
                                          is AddIncludeDescriptionErrorState ||
                                      current is AddTargetValidateState ||
                                      current
                                          is AddIncludeDescriptionAcceptState ||
                                      current
                                          is AddIncludeDescriptionAwaitState,
                                  builder: (context, state) {
                                    return Container(
                                      height:
                                          MediaQuery.of(context).size.height /
                                              3.5,
                                      child: Column(
                                        children: [
                                          buildTextFailedForm(
                                              _bloc.arabicTarget,
                                              Icons.description,
                                              _bloc.arabicErrorTarget,
                                              AppLocalization.of(context).trans(
                                                  "target_in_arabic_language"),
                                              errorText:
                                                  _bloc.arabicErrorTarget),
                                          buildTextFailedForm(
                                              _bloc.englishTarget,
                                              Icons.description,
                                              _bloc.englishErrorTarget,
                                              AppLocalization.of(context).trans(
                                                  "target_in_english_language"),
                                              errorText:
                                                  _bloc.englishErrorTarget),
                                        ],
                                      ),
                                    );
                                  },
                                ),
                              )),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                          ],
                          //What_Will_Learn
                        ),
                      ),
                    ),
                    Container(
                      child: Padding(
                        padding:
                            EdgeInsetsDirectional.only(start: 4.w, end: 4.w),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SizedBox(
                              height: 2.h,
                            ),
                            Text(
                              AppLocalization.of(context)
                                  .trans("course_requirements"),
                              style: AppTextStyle.xLargeBlackBold,
                            ),
                            widget.course.data.prerequisites.isEmpty
                                ? Center(
                                    child: Padding(
                                      padding: EdgeInsets.only(
                                        left: 13.w,
                                        right: 13.w,
                                        bottom: 2.h,
                                        top: 3.h,
                                      ),
                                      child: Column(
                                        children: [
                                          ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            child: Container(
                                              child: SvgPicture.asset(
                                                'assets/img/svg/illustration_empty_content.svg',
                                                width: 85.sp,
                                                height: 85.sp,
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            height: 2.h,
                                          ),
                                          Center(
                                            child: Text(
                                              AppLocalization.of(context)
                                                  .trans("empty_content"),
                                              style: AppTextStyle
                                                  .mediumDeepGrayBold,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                                : Container(
                                    margin:
                                        EdgeInsetsDirectional.only(start: 3.w),
                                    child: Container(
                                      child: ListView.builder(
                                          physics:
                                              const NeverScrollableScrollPhysics(),
                                          scrollDirection: Axis.vertical,
                                          shrinkWrap: true,
                                          itemCount: widget
                                              .course.data.prerequisites.length,
                                          itemBuilder: (BuildContext context,
                                              int index) {
                                            return UnorderedListItemTeacher(
                                                lang == "en"
                                                    ? widget
                                                        .course
                                                        .data
                                                        .prerequisites[index]
                                                        .prerequisiteEn
                                                        .toString()
                                                    : widget
                                                        .course
                                                        .data
                                                        .prerequisites[index]
                                                        .prerequisiteAr
                                                        .toString(),
                                                Icon(
                                                  Icons.circle,
                                                  color: AppColors.deepGray,
                                                  size: 15.sp,
                                                ),
                                                appTextStyle: const  TextStyle(
                                                color:AppColors.deepGray,
                                                fontSize:
                                                AppFontSize.MEDIUM,
                                                fontWeight:
                                                FontWeight.bold,
                                                fontFamily:
                                                FontFamily.cairo),
                                                funDelete: () => showDialog(
                                                      content: Text(
                                                        AppLocalization.of(
                                                                context)
                                                            .trans(
                                                                "delete_include"),
                                                        style: AppTextStyle
                                                            .mediumGray,
                                                      ),
                                                      fun: () => _bloc.add(
                                                          DeletePrerequisiteEvent(
                                                              widget
                                                                  .course
                                                                  .data
                                                                  .prerequisites[
                                                                      index]
                                                                  .id,
                                                              widget.course.data
                                                                  .id)),
                                                      title: AppLocalization.of(
                                                              context)
                                                          .trans(
                                                              "edit_course_information"),
                                                    ),
                                                funEdit: () => showDialog(
                                                      content: Form(
                                                          child: BlocProvider
                                                              .value(
                                                        value: _bloc,
                                                        child: BlocBuilder<
                                                            AddCourseBloc,
                                                            AddCourseState>(
                                                          buildWhen: (previous,
                                                                  current) =>
                                                              current is AddIncludeDescriptionErrorState ||
                                                              current
                                                                  is AddPrerequisiteValidateState ||
                                                              current
                                                                  is AddIncludeDescriptionAcceptState ||
                                                              current
                                                                  is AddIncludeDescriptionAwaitState,
                                                          builder:
                                                              (context, state) {
                                                            return Container(
                                                              height: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .height /
                                                                  3.5,
                                                              child: Column(
                                                                children: [
                                                                  buildTextFailedForm(
                                                                      _bloc
                                                                          .arabicPrerequisite,
                                                                      Icons
                                                                          .description,
                                                                      _bloc
                                                                          .arabicErrorPrerequisite,
                                                                      AppLocalization.of(
                                                                              context)
                                                                          .trans(
                                                                              "prerequisite_in_arabic"),
                                                                      errorText:
                                                                          _bloc
                                                                              .arabicErrorPrerequisite),
                                                                  buildTextFailedForm(
                                                                      _bloc
                                                                          .englishPrerequisite,
                                                                      Icons
                                                                          .description,
                                                                      _bloc
                                                                          .englishErrorPrerequisite,
                                                                      AppLocalization.of(
                                                                              context)
                                                                          .trans(
                                                                              "prerequisite_in_english"),
                                                                      errorText:
                                                                          _bloc
                                                                              .englishErrorPrerequisite),
                                                                ],
                                                              ),
                                                            );
                                                          },
                                                        ),
                                                      )),
                                                      fun: () => _bloc.add(
                                                          AddPrerequisiteCourse(
                                                              widget.course.data
                                                                  .id,
                                                              prerequisiteId: widget
                                                                  .course
                                                                  .data
                                                                  .prerequisites[
                                                                      index]
                                                                  .id)),
                                                      title: AppLocalization.of(
                                                              context)
                                                          .trans(
                                                              "edit_course_information"),
                                                    ));
                                          }),
                                    ),
                                  ),
                            editButton(
                              fun: () => _bloc.add(
                                  AddPrerequisiteCourse(widget.course.data.id)),
                              content: Form(
                                  child: BlocProvider.value(
                                value: _bloc,
                                child:
                                    BlocBuilder<AddCourseBloc, AddCourseState>(
                                  buildWhen: (previous, current) =>
                                      current
                                          is AddIncludeDescriptionErrorState ||
                                      current is AddPrerequisiteValidateState ||
                                      current
                                          is AddIncludeDescriptionAcceptState ||
                                      current
                                          is AddIncludeDescriptionAwaitState,
                                  builder: (context, state) {
                                    return Container(
                                      height:
                                          MediaQuery.of(context).size.height /
                                              3.5,
                                      child: Column(
                                        children: [
                                          buildTextFailedForm(
                                              _bloc.arabicPrerequisite,
                                              Icons.description,
                                              _bloc.arabicErrorPrerequisite,
                                              AppLocalization.of(context).trans(
                                                  "prerequisite_in_arabic"),
                                              errorText: _bloc
                                                  .arabicErrorPrerequisite),
                                          buildTextFailedForm(
                                              _bloc.englishPrerequisite,
                                              Icons.description,
                                              _bloc.englishErrorPrerequisite,
                                              AppLocalization.of(context).trans(
                                                  "prerequisite_in_english"),
                                              errorText: _bloc
                                                  .englishErrorPrerequisite),
                                        ],
                                      ),
                                    );
                                  },
                                ),
                              )),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                          ],
                          //What_Will_Learn
                        ),
                      ),
                    ),
                    Container(
                      child: Padding(
                        padding:
                            EdgeInsetsDirectional.only(start: 4.w, end: 4.w),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SizedBox(
                              height: 2.h,
                            ),
                            Row(
                              children: [
                                Text(
                                  AppLocalization.of(context)
                                      .trans("curriculum"),
                                  style: AppTextStyle.xLargeBlackBold,
                                ),
                                Spacer(),
                                GestureDetector(
                                  onTap: () {
                                    showDialog(
                                        fun: () => _bloc.add(
                                            AddCurriculumCourse(
                                                widget.course.data.id)),
                                        icon: Icons.add,
                                        title: AppLocalization.of(context)
                                            .trans("add_new_lesson"),
                                        content: BlocProvider.value(
                                          value: _bloc,
                                          child: StatefulBuilder(builder:
                                              (context, StateSetter setState) {
                                            return BlocBuilder<AddCourseBloc,
                                                AddCourseState>(
                                              buildWhen: (previous, current) =>
                                                  current is AddIncludeDescriptionErrorState ||
                                                  current
                                                      is AddCurriculumValidateState ||
                                                  current
                                                      is AddIncludeDescriptionAcceptState ||
                                                  current
                                                      is AddIncludeDescriptionAwaitState,
                                              builder: (context, state) {
                                                return SingleChildScrollView(
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    mainAxisSize:
                                                        MainAxisSize.min,
                                                    children: [
                                                      buildTextFailedForm(
                                                          _bloc.lessonTitle,
                                                          Icons.description,
                                                          _bloc
                                                              .lessonTitleError,
                                                          AppLocalization.of(
                                                                  context)
                                                              .trans(
                                                                  "lesson_title"),
                                                          errorText: _bloc
                                                              .lessonTitleError),
                                                      buildTextFailedForm(
                                                        _bloc.lessonArabicTitle,
                                                        Icons.description,
                                                        _bloc
                                                            .lessonTitleArabicError,
                                                        AppLocalization.of(
                                                                context)
                                                            .trans(
                                                                "lesson_title_in_arabic"),
                                                        errorText: _bloc
                                                            .lessonTitleArabicError,
                                                      ),
                                                      buildTextFailedForm(
                                                        _bloc
                                                            .lessonEnglishTitle,
                                                        Icons.description,
                                                        _bloc
                                                            .lessonTitleEnglishError,
                                                        AppLocalization.of(
                                                                context)
                                                            .trans(
                                                                "lesson_title_in_english"),
                                                        errorText: _bloc
                                                            .lessonTitleEnglishError,
                                                      ),
                                                      const SizedBox(
                                                        height: 10,
                                                      ),
                                                      Divider(
                                                        color: Colors.black
                                                            .withOpacity(0.4),
                                                      ),
                                                      SizedBox(
                                                        height: 10,
                                                      ),
                                                      buildTextFailedForm(
                                                        _bloc
                                                            .lessonTDescription,
                                                        Icons.description,
                                                        _bloc
                                                            .lessonDescriptionError,
                                                        AppLocalization.of(
                                                                context)
                                                            .trans(
                                                                "lesson_description"),
                                                        errorText: _bloc
                                                            .lessonDescriptionError,
                                                      ),
                                                      buildTextFailedForm(
                                                        _bloc
                                                            .lessonArabicDescription,
                                                        Icons.description,
                                                        _bloc
                                                            .lessonDescriptionError,
                                                        AppLocalization.of(
                                                                context)
                                                            .trans(
                                                                "lesson_description_in_arabic"),
                                                        errorText: _bloc
                                                            .lessonDescriptionError,
                                                      ),
                                                      buildTextFailedForm(
                                                        _bloc
                                                            .lessonEnglishDescription,
                                                        Icons.description,
                                                        _bloc
                                                            .lessonDescriptionEnglishError,
                                                        AppLocalization.of(
                                                                context)
                                                            .trans(
                                                                "lesson_description_in_english"),
                                                        errorText: _bloc
                                                            .lessonDescriptionEnglishError,
                                                      ),
                                                      GestureDetector(
                                                        onTap: () {
                                                          _pickImageBase64(
                                                              setState);
                                                        },
                                                        child: Container(
                                                          margin:
                                                              EdgeInsetsDirectional
                                                                  .only(
                                                                      top:
                                                                          15.sp,
                                                                      start:
                                                                          3.w,
                                                                      end: 2.w),
                                                          decoration: BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          12),
                                                              border: Border.all(
                                                                  color: AppColors
                                                                      .lighterGray)),
                                                          child: Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(8.0),
                                                            child: Center(
                                                                child: Column(
                                                              children: [
                                                                _imageFile !=
                                                                        null
                                                                    ? Stack(
                                                                        children: [
                                                                          Image.memory(
                                                                              Uint8List.fromList(imageBytes))
                                                                        ],
                                                                      )
                                                                    : Container(
                                                                        child: SvgPicture
                                                                            .asset(
                                                                          'assets/img/svg/bg.svg',
                                                                          height:
                                                                              150.sp,
                                                                        ),
                                                                      ),
                                                                SizedBox(
                                                                  height: 20.sp,
                                                                ),
                                                                Text(
                                                                  AppLocalization.of(
                                                                          context)
                                                                      .trans(
                                                                          "Drop_Select_File"),
                                                                  style: AppTextStyle
                                                                      .xLargeBlackBold,
                                                                ),
                                                                SizedBox(
                                                                  height: 20.sp,
                                                                ),
                                                                Text(
                                                                  AppLocalization.of(
                                                                          context)
                                                                      .trans(
                                                                    "browse_thorough_your_machine",
                                                                  ),
                                                                  style: AppTextStyle
                                                                      .smallBlack,
                                                                ),
                                                                SizedBox(
                                                                  height: 20.sp,
                                                                ),
                                                              ],
                                                            )),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                );
                                              },
                                            );
                                          }),
                                        ));
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: AppColors.red.withOpacity(0.8),
                                    ),
                                    child: Center(
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          "+    ${AppLocalization.of(context).trans("add_new_lesson")}",
                                          style: AppTextStyle.mediumWhiteBold,
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                            SizedBox(height: 15.sp),
                            BlocBuilder<AddCourseBloc, AddCourseState>(
                              buildWhen: (previous, current) =>
                                  current is GetLessonAwaitState ||
                                  current is GetLessonEmptyState ||
                                  current is GetLessonAcceptState ||
                                  current is GetLessonErrorState,
                              builder: (context, state) {
                                if (state is GetLessonAwaitState) {
                                  return Center(
                                    child: Container(
                                        width: 10.sp,
                                        height: 10.sp,
                                        child: const CircularProgressIndicator(
                                          color: Colors.grey,
                                        )),
                                  );
                                } else if (state is GetLessonErrorState) {
                                  return Text(state.message);
                                } else if (state is GetLessonEmptyState) {
                                  Center(
                                    child: Padding(
                                      padding: EdgeInsets.only(
                                        left: 13.w,
                                        right: 13.w,
                                        bottom: 2.h,
                                        top: 3.h,
                                      ),
                                      child: Column(
                                        children: [
                                          ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            child: Container(
                                              child: SvgPicture.asset(
                                                'assets/img/svg/illustration_empty_content.svg',
                                                width: 85.sp,
                                                height: 85.sp,
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            height: 2.h,
                                          ),
                                          Center(
                                            child: Text(
                                              AppLocalization.of(context)
                                                  .trans("empty_content"),
                                              style: AppTextStyle
                                                  .mediumDeepGrayBold,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                }
                                return Center(
                                  child: Container(
                                    decoration: const BoxDecoration(
                                      boxShadow: [
                                        BoxShadow(
                                          color: AppColors.lighterGray,
                                          blurRadius: 09,
                                          offset:
                                              Offset(2, 2), // Shadow position
                                        ),
                                      ],
                                    ),
                                    child: ListView.builder(
                                        shrinkWrap: true,
                                        //    <-- Set this to true
                                        physics: ScrollPhysics(),
                                        padding: const EdgeInsets.all(8),
                                        itemCount: _bloc.lessonsTeacher.length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return CurriculumCard(
                                              lesson:
                                                  _bloc.lessonsTeacher[index]);
                                        }),
                                  ),
                                );
                              },
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                          ],
                          //What_Will_Learn
                        ),
                      ),
                    ),
                    AppBottomBar()
                  ]),
            ));
      },
    );
  }

  Widget editButton(
      {Widget content, String title, fun, String titleButton, IconData icon}) {
    return GestureDetector(
      onTap: () {
        myDialog(
            context,
            title ??
                AppLocalization.of(context).trans("edit_course_information"),
            content ?? Container(),
            fun);
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: AppColors.normalGreen.withOpacity(0.6),
        ),
        margin: EdgeInsetsDirectional.only(top: 2.h, bottom: 2.h),
        child: (Center(
          child: Padding(
              padding: EdgeInsets.only(top: 1.h, bottom: 1.h),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    icon ?? Icons.edit,
                    color: Colors.white,
                    size: 15,
                  ),
                  SizedBox(
                    width: 1.w,
                  ),
                  Text(
                    titleButton ?? AppLocalization.of(context).trans("edit"),
                    style: AppTextStyle.largeWhiteBold,
                  ),
                ],
              )),
        )),
      ),
    );
  }

  Widget showDialog({Widget content, String title, fun, IconData icon}) {
    return myDialog(
        context,
        title ?? AppLocalization.of(context).trans("edit_course_information"),
        content ?? Container(),
        fun,
        icon: icon);
  }
}
