import 'dart:io';

import 'package:chewie/chewie.dart';
import 'package:e_learning/helpers/stack_loader_indicator.dart';
import 'package:e_learning/themes/app_colors.dart';
import 'package:e_learning/themes/elearning_icons.dart';
import 'package:e_learning/ui/teacher/widget/text_input.dart';
import 'package:e_learning/ui/widgets/common/Flush_bar.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:video_player/video_player.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

import '../../bloc/teacher/lesson_cubit.dart';
import '../../localization/app_localization.dart';
import '../../models/response/shower_response.dart';
import '../../models/response/teacher/lessons_response.dart';
import '../../themes/app_text_style.dart';
import '../widgets/appbar/abbBarAPP.dart';
import '../widgets/appbar/abbBottomBar.dart';

class EditVedioPage extends StatefulWidget {
  LessoneMedia lesson;

  EditVedioPage({Key key, this.lesson}) : super(key: key);

  @override
  State<EditVedioPage> createState() => _EditVedioPageState();
}

class _EditVedioPageState extends State<EditVedioPage> with TickerProviderStateMixin {
  TabController tabController;
  LessonCubit _bloc;
  VideoPlayerController _videoPlayerController;
  ChewieController _chewieController;
  Uint8List imageBytes;
  final StackLoaderIndicator _loader = StackLoaderIndicator();
  final picker = ImagePicker();

  Future getVideo(ImageSource img, setState) async {
    final pickedFile = await picker.pickVideo(
        source: img,
        preferredCameraDevice: CameraDevice.front,
        maxDuration: const Duration(minutes: 10));
    XFile xFilePick = pickedFile;
    ScaffoldMessenger.of(context).showSnackBar(// is this context <<<
        const SnackBar(
      behavior: SnackBarBehavior.floating,
      content: Text(
        "Wait to prepare ...",
        style: AppTextStyle.largeWhiteBold,
      ),
      backgroundColor: AppColors.green300,
    ));
    await VideoThumbnail.thumbnailData(
      video: pickedFile.path,
      imageFormat: ImageFormat.JPEG,
      // specify the width of the thumbnail, let the height auto-scaled to keep the source aspect ratio
      quality: 25,
    ).then((value) => {
          setState(
            () {
              if (xFilePick != null) {
                imageBytes = value;
                _bloc.videosEditBloc = File(pickedFile.path);
                ScaffoldMessenger.of(context)
                    .showSnackBar(// is this context <<<

                        const SnackBar(
                  behavior: SnackBarBehavior.floating,
                  content: Text(
                    "Ready",
                    style: AppTextStyle.largeWhiteBold,
                  ),
                  backgroundColor: AppColors.green300,
                ));
              } else {
                ScaffoldMessenger.of(context)
                    .showSnackBar(// is this context <<<
                        const SnackBar(content: Text('Nothing is selected!')));
              }
            },
          )
        });
  }

  @override
  void initState() {
    _bloc = BlocProvider.of<LessonCubit>(context);
    _bloc.titleController.text = widget.lesson.title;
    _bloc.titleArabicController.text = widget.lesson.titleAr;
    _bloc.titleEnglishController.text = widget.lesson.titleAr;
    _bloc.descriptionController.text = widget.lesson.description;
    _bloc.descriptionArabicController.text = widget.lesson.descriptionAr;
    _bloc.descriptionEnglishController.text = widget.lesson.descriptionEn;
    tabController = TabController(
      initialIndex: 0,
      length: 3,
      vsync: this,
    );
    _videoPlayerController =
        VideoPlayerController.network(widget.lesson.mediaUrl);
    _chewieController = ChewieController(
      videoPlayerController: _videoPlayerController,
      aspectRatio: 16 / 9,
      // Adjust aspect ratio as needed
      autoInitialize: true,
      looping: false,
      // Set to true if you want the video to loop
      allowFullScreen: true,
      // Enable full-screen option
      showControls: true, // Show playback controls
    );

    _videoPlayerController.addListener(() {
      setState(() {}); // Update UI when video state changes
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    _videoPlayerController.dispose();
    _chewieController.dispose();
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<LessonCubit, LessonState>(
      listenWhen: (_, current) =>
          current is UpdateVideoInfoAcceptState ||
          current is UpdateVideoInfoErrorState ||
          current is UpdateVideoInfoAwaitState,
      listener: (context, state) {
        if (state is UpdateLessonInfoAwaitState) {
          _loader.show(context);
        } else if (state is UpdateVideoInfoErrorState) {
          _loader.dismiss();
          showCustomFlashbar(context, state.message, "error");
        } else if (state is UpdateVideoInfoAcceptState) {
          showCustomFlashbar(context, state.message, "Success_Add");
          _loader.dismiss();
          Navigator.pop(context);
        }

      },
      child: DefaultTabController(
        length: 3,
        child: Scaffold(
            appBar: AppBar(
                bottom: const TabBar(
                  unselectedLabelColor: Colors.grey,
                  indicatorColor: Colors.orange,
                  labelColor: Colors.black,
                  tabs: [
                    Tab(
                      icon: Icon(Icons.edit_calendar_outlined),
                      text: "Edit Details",
                    ),
                    Tab(
                        icon: Icon(Elearning.play_circled),
                        text: "Edit Videos"),
                    Tab(
                        icon: Icon(Icons.edit_attributes_outlined),
                        text: "Edit Subtitle"),
                  ],
                ),
                centerTitle: true,
                leading: IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: const Icon(
                    Icons.arrow_back_ios,
                    color: Colors.black,
                  ),
                ),
                backgroundColor: Colors.white,
                title: const Text(
                  "Edit Videos Details",
                  style: TextStyle(color: Colors.black),
                )),
            body: TabBarView(
              children: [
                SingleChildScrollView(child: editVideosDetails()),
                Column(
                  children: [
                    const SizedBox(
                      height: 15,
                    ),
                    _bloc.videosEditBloc != null
                        ? Container(

                          child: Expanded(

                              child: Padding(
                                padding: EdgeInsets.all(50),
                                child: Image.memory(
                                  Uint8List.fromList(imageBytes),
                                  fit: BoxFit.contain,

                                ),
                              )),
                        )
                        : Container(
                            height: MediaQuery.of(context).size.height / 4,
                            child: Chewie(
                              controller: _chewieController,
                            ),
                          ),
                    SizedBox(
                      height: 10,
                    ),
                    Center(
                      child: ElevatedButton(
                        onPressed: () {
                          _bloc.videosEditBloc != null
                              ? _bloc.updateVideosInfo(
                                  widget.lesson.id, "Edit Video")
                              : getVideo(ImageSource.gallery, setState);
                        },
                        style: ElevatedButton.styleFrom(
                          padding:const  EdgeInsets.symmetric(
                              horizontal: 40, vertical: 13),
                          primary: Colors.green,
                          onPrimary: Colors.grey,
                          // Disable color
                        ),
                        child:  Text(
                          _bloc.videosEditBloc != null ? "Continue" : "Edit",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
                subTitleDetails()
              ],
            )),
      ),
    );
  }

  Widget subTitleDetails() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        buildTextFailedForm(
            _bloc.subTitleEnglish,
            Icons.subtitles,
            _bloc.errorSubTitleArabic,
            AppLocalization.of(context).trans("sub_title_arabic"),
            errorText: _bloc.errorSubTitleArabic,
            iconColor: Colors.orange),
        Divider(
          color: Colors.black.withOpacity(0.4),
        ),
        buildTextFailedForm(
            _bloc.subTitleEnglish,
            Icons.subtitles,
            _bloc.errorSubTitleEnglish,
            AppLocalization.of(context).trans("sub_title_english"),
            errorText: _bloc.errorSubTitleEnglish,
            iconColor: Colors.orange),
        const SizedBox(
          height: 25,
        ),
        Center(
          child: ElevatedButton(
            onPressed: () {},
            style: ElevatedButton.styleFrom(
              padding: EdgeInsets.symmetric(horizontal: 40, vertical: 13),
              primary: Colors.green,
              onPrimary: Colors.grey,
              // Disable color
            ),
            child: Text(
              AppLocalization.of(context).trans("edit"),
              style: const TextStyle(color: Colors.white),
            ),
          ),
        )
      ],
    );
  }

  Widget editVideosDetails() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        buildTextFailedForm(_bloc.titleController, Icons.title,
            _bloc.errorController, AppLocalization.of(context).trans("title"),
            errorText: _bloc.errorController, iconColor: Colors.orange),
        buildTextFailedForm(
            _bloc.titleArabicController,
            Icons.title,
            _bloc.errorArabicController,
            AppLocalization.of(context).trans("title_arabic"),
            errorText: _bloc.errorArabicController,
            iconColor: Colors.orange),
        buildTextFailedForm(
            _bloc.titleEnglishController,
            Icons.title,
            _bloc.errorEnglishController,
            AppLocalization.of(context).trans("title_english"),
            errorText: _bloc.errorEnglishController,
            iconColor: Colors.orange),
        Divider(
          color: Colors.black.withOpacity(0.4),
        ),
        buildTextFailedForm(
            _bloc.descriptionController,
            Icons.description,
            _bloc.errorDescription,
            AppLocalization.of(context).trans("description"),
            errorText: _bloc.errorDescription,
            iconColor: Colors.orange),
        buildTextFailedForm(
            _bloc.descriptionArabicController,
            Icons.description,
            _bloc.errorArabicDescription,
            AppLocalization.of(context).trans("description_arabic"),
            errorText: _bloc.errorArabicDescription,
            iconColor: Colors.orange),
        buildTextFailedForm(
            _bloc.descriptionEnglishController,
            Icons.description,
            _bloc.errorEnglishController,
            AppLocalization.of(context).trans("description_english"),
            errorText: _bloc.errorEnglishController,
            iconColor: Colors.orange),
        const SizedBox(
          height: 25,
        ),
        Center(
          child: ElevatedButton(
            onPressed: () {
              _bloc.updateVideosInfo(widget.lesson.id, "Basic Info");
            },
            style: ElevatedButton.styleFrom(
              padding: EdgeInsets.symmetric(horizontal: 40, vertical: 13),
              primary: Colors.green,
              onPrimary: Colors.grey,
              // Disable color
            ),
            child: Text(
              AppLocalization.of(context).trans("edit"),
              style: const TextStyle(color: Colors.white),
            ),
          ),
        )
      ],
    );
  }
}
