import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:e_learning/localization/app_localization.dart';
import 'package:e_learning/themes/app_text_style.dart';
import 'package:e_learning/themes/elearning_icons.dart';
import 'package:e_learning/ui/teacher/widget/text_input.dart';
import 'package:e_learning/ui/widgets/appbar/abbBarAPP.dart';
import 'package:e_learning/ui/widgets/common/Flush_bar.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import '../../bloc/teacher/add_course/add_course_bloc.dart';
import '../../controller/data_store.dart';
import '../../models/common/language.dart';
import '../../models/response/teacher/durations_response.dart';
import '../../models/response/teacher/levels_response.dart';
import '../../public/global.dart';
import '../../themes/app_colors.dart';
import '../widgets/loader/inkdrop_loader.dart';
import '../widgets/textfield/DirectionalTextField.dart';
import 'add_course_details.dart';

class AddCourse extends StatefulWidget {
  const AddCourse({Key key}) : super(key: key);

  @override
  State<AddCourse> createState() => _AddCourseState();
}

class _AddCourseState extends State<AddCourse> {
  AddCourseBloc _bloc;
  bool checkBoxValue = false;
  final ImagePicker _picker = ImagePicker();
  File _imageFile; // picked image will be store here.
  Uint8List imageBytes;

  @override
  void initState() {
    // TODO: implement initState
    _bloc = BlocProvider.of<AddCourseBloc>(context);
    _bloc.add(GetAddCourseContent());
  }

  void _pickImageBase64() async {
    try {
      // pick image from gallery, change ImageSource.camera if you want to capture image from camera.
      final XFile image = await _picker.pickImage(source: ImageSource.gallery);
      if (image == null) return;
      // read picked image byte data.
      Uint8List imagebytes = await image.readAsBytes();
      // using base64 encoder convert image into base64 string.
      String _base64String = base64.encode(imagebytes);
      String fileName = image.path.split('/').last;
      FormData formData = FormData.fromMap({
        "file": await MultipartFile.fromFile(image.path, filename: fileName),
      });
      print(fileName);
      final imageTemp = File(image.path);
      setState(() {
        _bloc.fileName = fileName;
        _bloc.image64 = _base64String;
        _bloc.imageBloc = imageTemp;
        _bloc.formData = formData;
        this.imageBytes = imagebytes;
        this._imageFile =
            imageTemp; // setState to image the UI and show picked image on screen.
      });
    } on PlatformException catch (e) {
      if (kDebugMode) {
        print('error');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const AppBarApp(),
      body: BlocConsumer<AddCourseBloc, AddCourseState>(
        listenWhen: (_, current) =>
            current is AddCourseAcceptState || current is AddCourseErrorState,
        listener: (context, state) {
          if (state is AddCourseErrorState) {
            print(state.message.toString());
            showCustomFlashbar(context, state.message,
                AppLocalization.of(context).trans("failure"));
          }
          if (state is AddCourseAcceptState) {
            Navigator.of(context).push(
              CupertinoPageRoute(
                builder: (context) => BlocProvider(
                  create: (context) => AddCourseBloc(context),
                  child: AddCourseDetails(course: state.response),
                ),
              ),
            );

            print("yes");
          }
          // TODO: implement listener
        },
        buildWhen: (_, current) =>
            current is GetAddCourseContentAwaitState ||
            current is GetAddCourseContentAcceptState ||
            current is GetAddCourseContentErrorState,
        builder: (context, state) {
          if (state is GetAddCourseContentAwaitState) {
            return Center(
              child: InkDropLoader(),
            );
          } else if (state is GetAddCourseContentErrorState) {
            return Center(
              child: Text(
                state.message,
                style: AppTextStyle.mediumGreenBold,
              ),
            );
          }
          return SingleChildScrollView(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    color: AppColors.textFieldGray,
                    child: Padding(
                      padding: EdgeInsetsDirectional.only(
                          start: 4.w, end: 4.w, top: 2.h, bottom: 2.h),
                      child: Text(
                        AppLocalization.of(context).trans("Add_Course_Title"),
                        style: AppTextStyle.mediumDeepGray,
                      ),
                    ),
                  ),
                  Container(
                    height: 2.h,
                  ),
                  _buildAddCardForm(),
                  // Container(c)
                ]),
          );
        },
      ),
    );
  }

  Widget _buildAddCardForm() {
    return Container(
      margin: EdgeInsetsDirectional.only(start: 4.w, end: 4.w),
      width: 100.w,
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: AppColors.lighterGray),
        borderRadius: BorderRadius.circular(10),
        boxShadow: const [
          BoxShadow(
            color: AppColors.lighterGray,
            blurRadius: 09,
            offset: Offset(2, 2), // Shadow position
          ),
        ],
      ),
      child: Padding(
        padding: EdgeInsetsDirectional.only(start: 4.w, end: 4.w),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                height: 1.h,
              ),
              /*
                "Course_information": "Course information",
  "Add_course_Form": "Fill form below with course information"
              */
              Text(
                AppLocalization.of(context).trans("Course_information"),
                style: AppTextStyle.largeGreenBold,
              ),
              //Add_course_Form
              Text(
                AppLocalization.of(context).trans("Add_course_Form"),
                style: AppTextStyle.mediumDeepGray,
              ),

              //basic_name_course
//arabic_name_course
              buildTextFailedForm(
                _bloc.nameController,
                Elearning.graduation_hat,
                _bloc.errorNameMessage,
                AppLocalization.of(context).trans("basic_name_course"),
              ),
              buildTextFailedForm(
                _bloc.arabicNameController,
                Elearning.graduation_hat,
                _bloc.errorArabicNameMessage,
                AppLocalization.of(context).trans("arabic_name_course"),
              ),
              buildTextFailedForm(
                _bloc.englishNameController,
                Elearning.graduation_hat,
                _bloc.errorEnglishNameMessage,
                AppLocalization.of(context).trans("english_name_course"),
              ),
              SizedBox(
                height: 1.h,
              ),
              Container(
                  margin: EdgeInsetsDirectional.only(start: 3.w, end: 2.w),
                  child: Divider(
                    color: AppColors.gray,
                  )),
              buildTextFailedForm(
                _bloc.shortNameController,
                Elearning.language,
                _bloc.errorShortName,
                AppLocalization.of(context).trans("short_name_course"),
              ),
              buildTextFailedForm(
                _bloc.arabicShortNameController,
                Elearning.language,
                _bloc.errorArabicShortName,
                AppLocalization.of(context).trans("arabic_short_name_course"),
              ),
              buildTextFailedForm(
                _bloc.englishShortNameController,
                Elearning.language,
                _bloc.errorEnglishShortName,
                AppLocalization.of(context).trans("english_short_name_course"),
              ),
              SizedBox(
                height: 1.h,
              ),
              Container(
                  margin: EdgeInsetsDirectional.only(start: 3.w, end: 2.w),
                  child: const Divider(
                    color: AppColors.gray,
                  )),
              buildTextFailedForm(
                _bloc.descriptionController,
                Icons.description,
                _bloc.errorDescription,
                AppLocalization.of(context).trans("description_add_course"),
              ),
              buildTextFailedForm(
                _bloc.arabicDescriptionController,
                Icons.description,
                _bloc.errorArabicDescription,
                AppLocalization.of(context)
                    .trans("arabic_description_add_course"),
              ),
              buildTextFailedForm(
                _bloc.englishDescriptionController,
                Icons.description,
                _bloc.errorEnglishDescription,
                AppLocalization.of(context)
                    .trans("english_description_add_course"),
              ),
              buildTextFailedForm(
                _bloc.coursePriceController,
                Icons.price_change,
                _bloc.errorPriceMessage,
                AppLocalization.of(context).trans("course_price"),
              ),
              getStartDatePicker(),
              getEndDatePicker(),
              GestureDetector(
                onTap: () {
                  _pickImageBase64();
                  // CustomImagePicker().openImagePicker(
                  //   context: context,
                  //   handleFinish: (File image) async {
                  //     List<int> temp=[];
                  //     // showDialogLoading(context);
                  //     temp = await image.readAsBytesSync();
                  //     String base64Image = base64Encode(await image.readAsBytes());
                  //     print("ss"+base64Image.toString());
                  //     setState(() {
                  //       _bloc.imageBytes=temp;
                  //       _bloc.test=image;
                  // _bloc.imageBase64=base64Image;
                  //     });
                  //     // AppBloc.authBloc.add(
                  //     //   UpdateAvatarUser(avatar: image),
                  //     // );
                  //   },
                  // );
                },
                child: Container(
                  margin: EdgeInsetsDirectional.only(
                      top: 15.sp, start: 3.w, end: 2.w),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      border: Border.all(color: AppColors.lighterGray)),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                        child: Column(
                      children: [
                        _imageFile != null
                            ? Stack(
                                children: [
                                  Image.memory(Uint8List.fromList(imageBytes))
                                ],
                              )
                            : Container(
                                child: SvgPicture.asset(
                                  'assets/img/svg/bg.svg',
                                  width: 150.sp,
                                  height: 150.sp,
                                ),
                              ),
                        SizedBox(
                          height: 20.sp,
                        ),
                        Text(
                          AppLocalization.of(context).trans("Drop_Select_File"),
                          style: AppTextStyle.xLargeBlackBold,
                        ),
                        SizedBox(
                          height: 20.sp,
                        ),
                        Text(
                          AppLocalization.of(context).trans(
                            "browse_thorough_your_machine",
                          ),
                          style: AppTextStyle.smallBlack,
                        ),
                        SizedBox(
                          height: 20.sp,
                        ),
                      ],
                    )),
                  ),
                ),
              ),
              _buildDurationDropDown(),
              _buildLevelDropDown(),
              _buildLanguageDropDown(),
              SizedBox(
                height: 20.sp,
              ),
              ListTile(
                leading: Checkbox(
                  checkColor: AppColors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(2)),
                  activeColor: AppColors.deepBlue,
                  value: _bloc.visible,
                  onChanged: (value) {
                    setState(() {
                      _bloc.visible = !_bloc.visible;
                    });
                  },
                ),
                contentPadding: EdgeInsets.all(0.0),
                title: Text(
                  AppLocalization.of(context).trans("Is_Course_visible"),
                  style: AppTextStyle.mediumBlack,
                ),
              ),
              SizedBox(
                height: 20.sp,
              ),
              Row(
                children: [
                  Expanded(
                    // Place `Expanded` inside `Row`
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 1.w),
                      child: ElevatedButton(
                        child: Row(
                          children: [
                            Icon(
                              Elearning.retweet,
                              size: 14.sp,
                              color: AppColors.white,
                            ),
                            SizedBox(
                              width: 8.sp,
                            ),
                            Flexible(
                              child: Text(
                                  AppLocalization.of(context)
                                      .trans("reset_form"),
                                  style: AppTextStyle.mediumWhite),
                            ),
                          ],
                        ),
                        onPressed: () {}, //
                        style: ElevatedButton.styleFrom(
                            primary: AppColors.red,
                            textStyle: const TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight
                                    .bold)), // Every button need a callback
                      ),
                    ),
                  ),
                  Expanded(
                    // Place `Expanded` inside `Row`
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 1.w),
                      child: BlocBuilder<AddCourseBloc, AddCourseState>(
                        buildWhen: (_, current) =>
                            current is AddCourseAwaitState ||
                            current is AddCourseErrorState ||
                            current is AddCourseAcceptState,
                        builder: (context, state) {
                          return ElevatedButton(
                            onPressed: () {
                              _bloc.add(AddCourseTeacher());
                            }, //
                            style: ElevatedButton.styleFrom(
                                primary: AppColors.blueAccent,
                                textStyle: const TextStyle(
                                    fontSize: 30, fontWeight: FontWeight.bold)),
                            child: state is AddCourseAwaitState
                                ? CircularProgressIndicator()
                                : Row(
                                    children: [
                                      Icon(
                                        Icons.check,
                                        size: 14.sp,
                                        color: AppColors.white,
                                      ),
                                      SizedBox(
                                        width: 1.w,
                                      ),
                                      Flexible(
                                        child: Text(
                                          AppLocalization.of(context)
                                              .trans("create_course"),
                                          style: AppTextStyle.mediumWhite,
                                        ),
                                      ),
                                    ],
                                  ), // Every button need a callback
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20.sp,
              ),

              //_bloc
            ]),
      ),
    );
  }

  Widget getStartDatePicker() {
    // print(    expireDateUiFormatterNew.format(_bloc.expiryDate));
    var mediaQuery = MediaQuery.of(context);

    return Padding(
      padding: const EdgeInsets.only(top: 4.0, bottom: 4.0),
      child: InkWell(
          onTap: () {
            FocusScope.of(context).unfocus();
            DatePicker.showDatePicker(
              context,
              showTitleActions: true,
              minTime: DateTime.now(),
              currentTime: _bloc.startDate,
              locale: DataStore.instance.lang == "ar"
                  ? LocaleType.ar
                  : LocaleType.en,
              theme: DatePickerTheme(
                itemStyle: Theme.of(context).textTheme.bodyText1,
                doneStyle: Theme.of(context).textTheme.bodyText2,
                hideDay: false,
              ),
              onConfirm: (date) {
                setState(() {
                  _bloc.startDate = date;
                  _bloc.startDateController.text = dateUiFormatter.format(date);
                });
                // _bloc.add(AddCardSelectExpiryDateEvent(date));
              },
            );
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                  margin: EdgeInsetsDirectional.only(start: 3.w, end: 2.w),
                  child: Text(
                    AppLocalization.of(context).trans("start_date_add_course"),
                    style: AppTextStyle.smallDeepGray,
                  )),
              SizedBox(
                height: 1.h,
              ),
              Container(
                height: 40.sp,
                width: 100.w,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    color: AppColors.lighterGray,
                    border: Border.all(color: AppColors.deepBlue)),
                margin: EdgeInsetsDirectional.only(start: 3.w, end: 2.w),
                child: Row(
                  children: [
                    Padding(
                      padding: EdgeInsetsDirectional.only(start: 10.sp),
                      child: Icon(
                        Icons.date_range,
                        color: AppColors.deepBlue,
                        size: 20,
                      ),
                    ),
                    SizedBox(width: 12.sp),
                    Text(
                      _bloc.startDate == null
                          ? dateUiFormatter.format(DateTime.now())
                          : dateUiFormatter.format(_bloc.startDate),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }

  Widget getEndDatePicker() {
    return Padding(
      padding: const EdgeInsets.only(top: 4.0, bottom: 4.0),
      child: InkWell(
          onTap: () {
            FocusScope.of(context).unfocus();
            DatePicker.showDatePicker(
              context,
              showTitleActions: true,
              minTime: DateTime.now(),
              currentTime: _bloc.endTime,
              locale: DataStore.instance.lang == "ar"
                  ? LocaleType.ar
                  : LocaleType.en,
              theme: DatePickerTheme(
                itemStyle: Theme.of(context).textTheme.bodyText1,
                doneStyle: Theme.of(context).textTheme.bodyText2,
                hideDay: false,
              ),
              onConfirm: (date) {
                _bloc.endTime = date;
                _bloc.endDateController.text = dateUiFormatter.format(date);
                print(_bloc.endDateController.text);
                // _bloc.add(AddCardSelectExpiryDateEvent(date));
              },
            );
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                  margin: EdgeInsetsDirectional.only(start: 3.w, end: 2.w),
                  child: Text(
                    AppLocalization.of(context).trans("end_date_add_course"),
                    style: AppTextStyle.smallDeepGray,
                  )),
              SizedBox(
                height: 1.h,
              ),
              Container(
                height: 40.sp,
                width: 100.w,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    color: AppColors.lighterGray,
                    border: Border.all(color: AppColors.deepBlue)),
                margin: EdgeInsetsDirectional.only(start: 3.w, end: 2.w),
                child: Row(
                  children: [
                    Padding(
                      padding: EdgeInsetsDirectional.only(start: 10.sp),
                      child: Icon(
                        Icons.date_range,
                        color: AppColors.deepBlue,
                        size: 20,
                      ),
                    ),
                    SizedBox(width: 12.sp),
                    Text(
                      _bloc.endTime == null
                          ? dateUiFormatter.format(DateTime.now())
                          : dateUiFormatter.format(_bloc.startDate),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }

  Widget _buildLanguageDropDown() {
    return Container(
      width: 100.w,
      height: 43.sp,
      margin: EdgeInsetsDirectional.only(start: 3.w, end: 2.w, top: 2.h),
      child: DropdownButtonHideUnderline(
        child: DropdownButton2<Language>(
          dropdownStyleData: DropdownStyleData(
            maxHeight: 200,
            width: 90.w,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(14),
              color: Colors.white,
            ),
            offset: Offset(-13, MediaQuery.of(context).size.height / 2),
            scrollbarTheme: ScrollbarThemeData(
              radius: const Radius.circular(40),
              thickness: MaterialStateProperty.all(6),
              thumbVisibility: MaterialStateProperty.all(true),
            ),
          ),
          hint: Row(
            children: [
              Container(
                child: Text(
                  AppLocalization.of(context).trans("choose_the_language"),
                  style: AppTextStyle.smallDeepGray,
                ),
              ),
            ],
          ),
          menuItemStyleData: MenuItemStyleData(
            height: MediaQuery.of(context).size.height / 20,
          ),
          buttonStyleData: ButtonStyleData(
            height: 50,
            width: 160,
            padding: const EdgeInsets.only(left: 14, right: 14),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(14),
              border: Border.all(
                color: AppColors.lighterGray,
              ),
              color: Colors.white,
            ),
            elevation: 2,
          ),
          iconStyleData: const IconStyleData(
            icon: Icon(
              Icons.arrow_drop_down,
              size: 16,
              color: AppColors.deepGray,
            ),
          ),
          selectedItemBuilder: (BuildContext context) {
            return Language.languageList().map((Language item) {
              return Container(
                alignment: Alignment.center,
                child: Text(
                  item.languageCode,
                ),
              );
            }).toList();
          },
          style: const TextStyle(color: Colors.black),
          onChanged: (value) async{
            // setState(() {
            //   _bloc.local = value;
            // });
          },
          value: _bloc.local,
          items: Language.languageList().map((Language items) {
            return DropdownMenuItem(
              value: items,
              child: Row(children: <Widget>[
                Text(items.flag),
                const Spacer(),
                Text(
                  items.name,
                  style: AppTextStyle.smallGreenBold,
                ),
              ]),
            );
          }).toList(),
        ),
      ),
    );
  }

  Widget _buildDurationDropDown() {
    return Container(
      width: 100.w,
      height: 43.sp,
      margin: EdgeInsetsDirectional.only(start: 3.w, end: 2.w, top: 2.h),
      child: DropdownButtonHideUnderline(
        child: DropdownButton2<Durations>(
          isExpanded: true,
          hint: Row(
            children: [
              Container(
                child: Text(
                  AppLocalization.of(context).trans("select_duration"),
                  style: AppTextStyle.smallDeepGray,
                ),
              ),
            ],
          ),
          items: _bloc.durations
              .map<DropdownMenuItem<Durations>>((Durations value) {
            return DropdownMenuItem<Durations>(
              value: value,
              child: Container(
                child: Text(
                  DataStore.instance.lang == "en"
                      ? value.durationEn
                      : value.durationAr,
                ),
              ),
            );
          }).toList(),
          value: _bloc.durationsSelected,
          // value: selectedValue,
          onChanged: (value) {
            setState(() {
              _bloc.durationsSelected = value;
            });
          },
          buttonStyleData: ButtonStyleData(
            height: 50,
            width: 160,
            padding: const EdgeInsets.only(left: 14, right: 14),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(14),
              border: Border.all(
                color: AppColors.lighterGray,
              ),
              color: Colors.white,
            ),
            elevation: 2,
          ),
          iconStyleData: const IconStyleData(
            icon: Icon(
              Icons.arrow_drop_down,
              size: 16,
              color: AppColors.deepGray,
            ),
          ),
          dropdownStyleData: DropdownStyleData(
            maxHeight: 200,
            width: 90.w,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(14),
              color: Colors.white,
            ),
            offset: Offset(-13, MediaQuery.of(context).size.height / 2),
            scrollbarTheme: ScrollbarThemeData(
              radius: const Radius.circular(40),
              thickness: MaterialStateProperty.all(6),
              thumbVisibility: MaterialStateProperty.all(true),
            ),
          ),
          menuItemStyleData: const MenuItemStyleData(
            height: 40,
            padding: EdgeInsets.only(left: 14, right: 14),
          ),
        ),
      ),
    );
  }

  Widget _buildLevelDropDown() {
    return Container(
      width: 100.w,
      height: 43.sp,
      margin: EdgeInsetsDirectional.only(start: 3.w, end: 2.w, top: 2.h),
      child: DropdownButtonHideUnderline(
        child: DropdownButton2<Levels>(
          isExpanded: true,
          hint: Row(
            children: [
              Container(
                child: Text(
                  AppLocalization.of(context).trans("select_levels"),
                  style: AppTextStyle.smallDeepGray,
                ),
              ),
            ],
          ),
          items: _bloc.levels.map<DropdownMenuItem<Levels>>((Levels value) {
            return DropdownMenuItem<Levels>(
              value: value,
              child: Container(
                child: Text(
                  DataStore.instance.lang == "en"
                      ? value.levelEn
                      : value.levelAr,
                ),
              ),
            );
          }).toList(),
          value: _bloc.levelSelected,
          // value: selectedValue,
          onChanged: (value) {
            setState(() {
              _bloc.levelSelected = value;
            });
          },
          buttonStyleData: ButtonStyleData(
            height: 50,
            width: 160,
            padding: const EdgeInsets.only(left: 14, right: 14),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(14),
              border: Border.all(
                color: AppColors.lighterGray,
              ),
              color: Colors.white,
            ),
            elevation: 2,
          ),
          iconStyleData: const IconStyleData(
            icon: Icon(
              Icons.arrow_drop_down,
              size: 16,
              color: AppColors.deepGray,
            ),
          ),
          dropdownStyleData: DropdownStyleData(
            maxHeight: 200,
            width: 90.w,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(14),
              color: Colors.white,
            ),
            offset: Offset(-13, MediaQuery.of(context).size.height / 2),
            scrollbarTheme: ScrollbarThemeData(
              radius: const Radius.circular(40),
              thickness: MaterialStateProperty.all(6),
              thumbVisibility: MaterialStateProperty.all(true),
            ),
          ),
          menuItemStyleData: const MenuItemStyleData(
            height: 40,
            padding: EdgeInsets.only(left: 14, right: 14),
          ),
        ),
      ),
    );
  }
}
