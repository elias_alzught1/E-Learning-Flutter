import 'package:e_learning/bloc/teacher/add_course/add_course_bloc.dart';
import 'package:e_learning/bloc/teacher_cubit/teacher_cubit.dart';
import 'package:e_learning/models/response/teacher/add_course_response.dart';
import 'package:e_learning/ui/course/widget/my_course_card.dart';
import 'package:e_learning/ui/teacher/add_course_details.dart';
import 'package:e_learning/ui/widgets/appbar/abbBarAPP.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/course_details/course_details_bloc.dart';
import '../../public/global.dart';
import '../course/widget/course_card.dart';
import '../widgets/loader/inkdrop_loader.dart';

class MyCourse extends StatefulWidget {
  const MyCourse({Key key}) : super(key: key);

  @override
  State<MyCourse> createState() => _MyCourseState();
}

class _MyCourseState extends State<MyCourse> {
  TeacherCubit _bloc;

  @override
  void initState() {
    _bloc = BlocProvider.of<TeacherCubit>(context);
    _bloc.getCourseForTeacher();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: const AppBarApp(),
        body: BlocBuilder<TeacherCubit, TeacherState>(
          builder: (context, state) {
            if (state is GetCourseAwaitState) {
              return Center(
                child: InkDropLoader(),
              );
            } else if (state is GetCourseErrorState) {
              Center(
                child: Text(state.message),
              );
            }

            return Container(
              color: Color(0xFFF0F1F3),
              child: GridView.builder(
                  itemBuilder: (context, index) {
                    return MyCourseCard(
color:  colorList[index % 4],
                      course: _bloc.courses[index],
                    );
                  },
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: 1.0,
                    crossAxisSpacing: 0.0,
                    mainAxisSpacing: 5,
                    mainAxisExtent: 250,
                  ),
                  itemCount: _bloc.courses.length),
            );
          },
        ));
  }
}
