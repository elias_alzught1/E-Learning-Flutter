import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:e_learning/controller/data_store.dart';
import 'package:e_learning/helpers/helpers.dart';
import 'package:e_learning/models/response/teacher/lessons_response.dart';
import 'package:e_learning/themes/app_colors.dart';
import 'package:e_learning/ui/teacher/edit_book.dart';
import 'package:e_learning/ui/teacher/edit_vedios.dart';
import 'package:e_learning/ui/teacher/widget/add_question.dart';
import 'package:e_learning/ui/teacher/widget/dialog_show.dart';
import 'package:e_learning/ui/teacher/widget/edit_quiz_details.dart';
import 'package:e_learning/ui/teacher/widget/question_management.dart';
import 'package:e_learning/ui/teacher/widget/text_input.dart';
import 'package:e_learning/ui/widgets/appbar/abbBarAPP.dart';
import 'package:e_learning/ui/widgets/appbar/abbBottomBar.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';

import '../../bloc/teacher/lesson_cubit.dart';
import '../../helpers/stack_loader_indicator.dart';
import '../../localization/app_localization.dart';
import '../../themes/app_text_style.dart';
import '../../themes/elearning_icons.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

class LessonDetails extends StatefulWidget {
  LessonsTeacher lesson;

  LessonDetails({Key key, this.lesson}) : super(key: key);

  @override
  State<LessonDetails> createState() => _LessonDetailsState();
}

class _LessonDetailsState extends State<LessonDetails>
    with SingleTickerProviderStateMixin {
  final picker = ImagePicker();
  TabController _tabController;
  final StackLoaderIndicator _loader = StackLoaderIndicator();
  LessonCubit _bloc;
  Uint8List imageBytes;
  AnimationController _controller;
  Animation<Offset> _animation;
  Animation<Offset> _animation2;
  Animation<Offset> _animation3;
  Animation<Offset> _animation4;

  void initState() {
    // TODO: implement initState
    _bloc = BlocProvider.of<LessonCubit>(context);
    _bloc.getVideos(widget.lesson.id);
    _bloc.getBooks(widget.lesson.id);
    _bloc.getQuiz(widget.lesson.id);
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 2),
    );

    _animation = Tween<Offset>(
      begin: const Offset(-1.0, 0.0), // Start off-screen to the left
      end: Offset.zero, // End at the container's position
    ).animate(_controller);
    _animation2 = Tween<Offset>(
      begin: const Offset(1.0, 0.0), // Start off-screen to the left
      end: Offset.zero, // End at the container's position
    ).animate(_controller);
    _animation3 = Tween<Offset>(
      begin: const Offset(-1.0, 0.0), // Start off-screen to the left
      end: Offset.zero, // End at the container's position
    ).animate(_controller);
    _animation4 = Tween<Offset>(
      begin: const Offset(1.0, 0.0), // Start off-screen to the left
      end: Offset.zero, // End at the container's position
    ).animate(_controller);
    _controller.forward();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    String lang = DataStore.instance.lang;
    return Scaffold(
      appBar: AppBarApp(),
      bottomNavigationBar: AppBottomBar(),
      backgroundColor: AppColors.textFieldGray,
      body: BlocConsumer<LessonCubit, LessonState>(
        listenWhen: (previous, current) =>
            current is AddVideosAcceptState ||
            current is AddVideosErrorState ||
            current is AddVideosAwaitState ||
            current is GetVideosAwaitState ||
            current is GetVideosAcceptState ||
            current is GetVideosErrorState ||
            current is GetBooksAwaitState ||
            current is GetBooksAcceptState ||
            current is GetBooksErrorState ||
            current is AddQuizAwaitState ||
            current is AddQuizErrorState ||
            current is AddQuizErrorState ||
            current is GetQuizAwaitState ||
            current is GetQuizAcceptState ||
            current is GetQuizErrorState ||
            current is UpdateLessonInfoAcceptState ||
            current is GetUpdatedLessonAcceptState,
        listener: (context, state) {
          if (state is AddVideosAcceptState) {
            _loader.dismiss();
            Navigator.of(context).pop();
            _bloc.getVideos(widget.lesson.id);
          } else if (state is AddQuizAcceptState) {
            _loader.dismiss();
            Navigator.of(context).pop();
            _bloc.getQuiz(widget.lesson.id);
          } else if (state is AddVideosErrorState ||
              state is AddQuizErrorState) {
            _loader.dismiss();
          } else if (state is AddVideosAwaitState ||
              state is AddQuizAwaitState) {
            _loader.show(context);
          } else if (state is GetVideosAwaitState ||
              state is GetBooksAwaitState ||
              state is GetQuizAwaitState) {
            _loader.show(context);
          } else if (state is GetVideosAcceptState ||
              state is GetBooksAcceptState ||
              state is GetQuizAcceptState) {
            _loader.dismiss();
          } else if (state is GetVideosErrorState ||
              state is GetQuizErrorState) {
            _loader.dismiss();
          } else if (state is UpdateLessonInfoAcceptState) {
            Navigator.of(context).pop();
            _bloc.getUpdatedLesson(widget.lesson.id);
          } else if (state is GetUpdatedLessonAcceptState) {
            setState(() {
              widget.lesson = state.response;
            });
          }
          // TODO: implement listener
        },
        builder: (context, state) {
          return SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: CachedNetworkImageProvider(
                          widget.lesson.image ?? "",
                          errorListener: () {
                            const Text(
                              'Loading',
                              style: TextStyle(
                                fontSize: 20,
                              ),
                            );
                          },
                        )),
                  ),
                  child: Stack(
                    children: [
                      Positioned.fill(
                        child: Container(
                          width: 100.w,
                          color: AppColors.black
                              .withOpacity(0.6), // Adjust opacity as needed
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SlideTransition(
                            position: _animation,
                            child: Padding(
                              padding: EdgeInsets.only(
                                left: 2.w,
                                right: 2.w,
                                bottom: 5.h,
                                top: 2.h,
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(10),
                                child: CachedNetworkImage(
                                  imageUrl: widget.lesson.image ?? "",
                                  fit: BoxFit.fill,
                                  placeholder: (context, url) => getShimmer(),
                                  errorWidget: (context, url, error) =>
                                      const Center(
                                    child: Icon(Icons.error),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SlideTransition(
                            position: _animation2,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 4.w),
                                  child: Container(
                                    child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Text(
                                            lang == "en"
                                                ? widget.lesson.titleAr
                                                : widget.lesson.titleEn,
                                            style: AppTextStyle
                                                .xlargeGreenNormalBold,
                                          ),
                                          const Spacer(),
                                          IconButton(
                                            onPressed: () {
                                              myDialog(
                                                  context,
                                                  AppLocalization.of(context).trans(
                                                      "edit_course_information"),
                                                  _updateLessonInfo(),
                                                  () => _bloc.updateLessonInfo(
                                                      widget.lesson.id));
                                            },
                                            icon: const Icon(Icons.edit,
                                                color: AppColors.normalGreen,
                                                size: 18),
                                          )
                                        ]),
                                  ),
                                ),
                                SizedBox(
                                  height: 1.h,
                                ),
                                Padding(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 5.w),
                                  child: Text(
                                    widget.lesson.descriptionEn,
                                    style: AppTextStyle.largeWhiteBold,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 1.h,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10.sp,
                ),
                SlideTransition(
                  position: _animation3,
                  child: Padding(
                    padding: EdgeInsetsDirectional.only(end: 10.sp),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        lang == "en"
                            ? SvgPicture.asset(
                                'assets/img/svg/tag.svg',
                                color: Colors.orange,
                              )
                            : Transform(
                                alignment: Alignment.center,
                                transform: Matrix4.identity()..scale(-1.0, 1.0),
                                // Rotate by 180 degrees (pi radians)
                                child:
                                    SvgPicture.asset('assets/img/svg/tag.svg'),
                              ),
                        SizedBox(
                          width: 8,
                        ),
                        Icon(Elearning.monitor,
                            size: 20.sp, color: AppColors.black),
                        SizedBox(width: 10.sp),
                        Text(
                          AppLocalization.of(context).trans("list Videos"),
                          style: AppTextStyle.xLargeBlackBold,
                        ),
                        Spacer(),
                        GestureDetector(
                          onTap: () {
                            _buildAddVideoDialog();
                            // _showPicker(context: context);
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: Colors.orange,
                            ),
                            child: Center(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "+    ${AppLocalization.of(context).trans("add_video")}",
                                  style: AppTextStyle.mediumWhiteBold,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 10.sp,
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 15, horizontal: 15),
                    child: GridView.builder(
                        physics: const NeverScrollableScrollPhysics(),
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        gridDelegate:
                            const SliverGridDelegateWithMaxCrossAxisExtent(
                                maxCrossAxisExtent: 256,
                                mainAxisSpacing: 5,
                                crossAxisSpacing: 5),
                        itemCount: _bloc.videos.length,
                        itemBuilder: (BuildContext ctx, index) {
                          return GestureDetector(
                            onTap: () {
                              Navigator.of(context).push(
                                CupertinoPageRoute(
                                  builder: (context) => BlocProvider(
                                    create: (context) => LessonCubit(),
                                    child:
                                        EditVedioPage(lesson: _bloc.videos[0]),
                                  ),
                                ),
                              );
                              // Navigator.push(context, MaterialPageRoute(builder: (context) => EditVideos(),));
                            },
                            child: Container(
                              decoration: const BoxDecoration(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(15),
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.white,
                                    blurRadius: 0,
                                    offset: Offset(0, 2), // Shadow position
                                  ),
                                ],
                              ),
                              child: Column(
                                children: [
                                  Container(
                                      width: MediaQuery.of(context).size.width,
                                      decoration: const BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(15),
                                        ),
                                      ),
                                      child: ClipRRect(
                                        borderRadius: const BorderRadius.only(
                                            topRight: Radius.circular(15),
                                            topLeft: Radius.circular(15)),
                                        child: _bloc.videosImage.isEmpty
                                            ? Center(
                                                child: getShimmer(
                                                    width: 200, height: 100),
                                              )
                                            : Image.memory(
                                                Uint8List.fromList(
                                                    _bloc.videosImage[index]),
                                                fit: BoxFit.cover,
                                                height: 100,
                                              ),
                                      )),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Padding(
                                    padding:
                                        EdgeInsetsDirectional.only(start: 2.w),
                                    child: SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: Text(_bloc.videos[index].title)),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Padding(
                                    padding:
                                        EdgeInsetsDirectional.only(start: 2.w),
                                    child: SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: Text(
                                          "- ${_bloc.videos[index].description}",
                                          style:
                                              TextStyle(color: Colors.orange),
                                        )),
                                  )
                                ],
                              ),
                            ),
                          );
                        }),
                  ),
                ),
                SizedBox(
                  height: 10.sp,
                ),
                SlideTransition(
                  position: _animation4,
                  child: Padding(
                    padding: EdgeInsetsDirectional.only(end: 10.sp),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        lang == "en"
                            ? SvgPicture.asset(
                                'assets/img/svg/tag.svg',
                                color: Colors.green,
                              )
                            : Transform(
                                alignment: Alignment.center,
                                transform: Matrix4.identity()..scale(-1.0, 1.0),
                                // Rotate by 180 degrees (pi radians)
                                child: SvgPicture.asset(
                                  'assets/img/svg/tag.svg',
                                  color: Colors.green,
                                ),
                              ),
                        SizedBox(
                          width: 8,
                        ),
                        Icon(Elearning.book,
                            size: 20.sp, color: AppColors.black),
                        SizedBox(width: 10.sp),
                        Text(
                          AppLocalization.of(context).trans("list Books"),
                          style: AppTextStyle.xLargeBlackBold,
                        ),
                        Spacer(),
                        GestureDetector(
                          onTap: () {
                            _buildAddVideoDialog();
                            // _showPicker(context: context);
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: Colors.green,
                            ),
                            child: Center(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "+    ${AppLocalization.of(context).trans("add_book")}",
                                  style: AppTextStyle.mediumWhiteBold,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 10.sp,
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 15, horizontal: 15),
                    child: GridView.builder(
                        physics: const NeverScrollableScrollPhysics(),
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        gridDelegate:
                            const SliverGridDelegateWithMaxCrossAxisExtent(
                                maxCrossAxisExtent: 256,
                                mainAxisSpacing: 5,
                                crossAxisSpacing: 5),
                        itemCount: _bloc.books.length,
                        itemBuilder: (BuildContext ctx, index) {
                          return GestureDetector(
                            onTap: () {
                              Navigator.of(context).push(
                                CupertinoPageRoute(
                                  builder: (context) => BlocProvider(
                                    create: (context) => LessonCubit(),
                                    child: EditBook(book: _bloc.books[index]),
                                  ),
                                ),
                              );
                            },
                            child: Container(
                              decoration: const BoxDecoration(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(15),
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.white,
                                    blurRadius: 0,
                                    offset: Offset(0, 2), // Shadow position
                                  ),
                                ],
                              ),
                              child: Column(
                                children: [
                                  Container(
                                      width: MediaQuery.of(context).size.width,
                                      decoration: const BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(15),
                                        ),
                                      ),
                                      child: ClipRRect(
                                          borderRadius: const BorderRadius.only(
                                              topRight: Radius.circular(15),
                                              topLeft: Radius.circular(15)),
                                          child: SvgPicture.asset(
                                            "assets/img/svg/Book.svg",
                                            width: 50,
                                            height: 100,
                                          ))),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Padding(
                                    padding:
                                        EdgeInsetsDirectional.only(start: 2.w),
                                    child: SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: Text(_bloc.books[index].title)),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Padding(
                                    padding:
                                        EdgeInsetsDirectional.only(start: 2.w),
                                    child: SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: Text(
                                          "- ${_bloc.books[index].description}",
                                          style: TextStyle(color: Colors.green),
                                        )),
                                  )
                                ],
                              ),
                            ),
                          );
                        }),
                  ),
                ),
                SizedBox(
                  height: 10.sp,
                ),
                Padding(
                  padding: EdgeInsetsDirectional.only(end: 10.sp),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      lang == "en"
                          ? SvgPicture.asset(
                              'assets/img/svg/tag.svg',
                              color: AppColors.lightPurple,
                            )
                          : Transform(
                              alignment: Alignment.center,
                              transform: Matrix4.identity()..scale(-1.0, 1.0),
                              // Rotate by 180 degrees (pi radians)
                              child: SvgPicture.asset(
                                'assets/img/svg/tag.svg',
                                color: AppColors.lightPurple,
                              ),
                            ),
                      SizedBox(
                        width: 8,
                      ),
                      Icon(Elearning.live_help,
                          size: 20.sp, color: AppColors.black),
                      SizedBox(width: 10.sp),
                      Text(
                        AppLocalization.of(context).trans("quiz"),
                        style: AppTextStyle.xLargeBlackBold,
                      ),
                      Spacer(),
                      _bloc.quizList.length == 0 || _bloc.quizList.isEmpty
                          ? GestureDetector(
                              onTap: () {
                                _buildAddQuizDialog();
                                // _showPicker(context: context);
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: AppColors.blueAccent,
                                ),
                                child: Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      "+    ${AppLocalization.of(context).trans("add_quiz")}",
                                      style: AppTextStyle.mediumWhiteBold,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          : SizedBox()
                    ],
                  ),
                ),
                SizedBox(
                  height: 10.sp,
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 15, horizontal: 15),
                    child: GridView.builder(
                        physics: const NeverScrollableScrollPhysics(),
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        gridDelegate:
                            const SliverGridDelegateWithMaxCrossAxisExtent(
                                maxCrossAxisExtent: 256,
                                mainAxisSpacing: 5,
                                crossAxisSpacing: 5),
                        itemCount: _bloc.quizList.length,
                        itemBuilder: (BuildContext ctx, index) {
                          return Container(
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.all(
                                Radius.circular(15),
                              ),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.white,
                                  blurRadius: 0,
                                  offset: Offset(0, 2), // Shadow position
                                ),
                              ],
                            ),
                            child: Column(
                              children: [
                                Container(
                                    width: MediaQuery.of(context).size.width,
                                    decoration: const BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(15),
                                      ),
                                    ),
                                    child: ClipRRect(
                                        borderRadius: const BorderRadius.only(
                                            topRight: Radius.circular(15),
                                            topLeft: Radius.circular(15)),
                                        child: SvgPicture.asset(
                                          "assets/img/svg/Quiz.svg",
                                          width: 50,
                                          height: 100,
                                        ))),
                                const SizedBox(
                                  height: 5,
                                ),
                                Padding(
                                  padding:
                                      EdgeInsetsDirectional.only(start: 2.w),
                                  child: SizedBox(
                                      width: MediaQuery.of(context).size.width,
                                      child: Text(_bloc.quizList[index].title)),
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Padding(
                                  padding:
                                      EdgeInsetsDirectional.only(start: 2.w),
                                  child: SizedBox(
                                      width: MediaQuery.of(context).size.width,
                                      child: Text(
                                        "- ${_bloc.quizList[index].description}",
                                        style: TextStyle(
                                            color: AppColors.lightPurple),
                                      )),
                                )
                              ],
                            ),
                          );
                        }),
                  ),
                ),
                SizedBox(
                  height: 10.sp,
                ),
                _bloc.quizList.isEmpty
                    ? SizedBox()
                    : Padding(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        child: Row(
                          children: [
                            Flexible(
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.push(context, MaterialPageRoute(
                                    builder: (context) {
                                      return BlocProvider.value(
                                        value: _bloc,
                                        child: EditQuizDetails(
                                            quiz: _bloc.quizList[0]),
                                      );
                                    },
                                  ));
                                  //EditQuizDetails
                                },
                                child: Container(
                                  width: MediaQuery.of(context).size.width / 3,
                                  decoration: BoxDecoration(
                                      color: AppColors.lightPurple,
                                      borderRadius: BorderRadius.circular(5)),
                                  child: Padding(
                                    padding:
                                        EdgeInsets.symmetric(vertical: 10.sp),
                                    child: Center(
                                      child: Text(
                                        "Edit Details",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 8.sp),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            Flexible(
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.push(context, MaterialPageRoute(
                                    builder: (context) {
                                      return BlocProvider.value(
                                        value: _bloc,
                                        child: AddQuestionPage(
                                            quizId: widget.lesson.id),
                                      );
                                    },
                                  ));
                                },
                                child: Container(
                                  width: MediaQuery.of(context).size.width / 3,
                                  decoration: BoxDecoration(
                                      color: AppColors.lightPurple,
                                      borderRadius: BorderRadius.circular(5)),
                                  child: Padding(
                                    padding:
                                        EdgeInsets.symmetric(vertical: 10.sp),
                                    child: Center(
                                      child: Text(
                                        "Add Question",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 8.sp),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            Flexible(
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.push(context, MaterialPageRoute(
                                    builder: (context) {
                                      return BlocProvider.value(
                                        value: _bloc,
                                        child: const QuestionManagement(),
                                      );
                                    },
                                  ));
                                },
                                child: Container(
                                  width: MediaQuery.of(context).size.width / 3,
                                  decoration: BoxDecoration(
                                      color: AppColors.lightPurple,
                                      borderRadius: BorderRadius.circular(5)),
                                  child: Padding(
                                    padding:
                                        EdgeInsets.symmetric(vertical: 10.sp),
                                    child: Center(
                                      child: Text(
                                        "Question Managament",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 8.sp),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                SizedBox(
                  height: 10.sp,
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  void _showPicker({BuildContext context, setState}) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return SafeArea(
          child: Wrap(
            children: <Widget>[
              ListTile(
                leading: const Icon(Icons.photo_library),
                title: const Text('Gallery'),
                onTap: () {
                  getVideo(ImageSource.gallery, setState);
                  Navigator.of(context).pop();
                },
              ),
              ListTile(
                leading: const Icon(Icons.photo_camera),
                title: const Text('Camera'),
                onTap: () {
                  getVideo(ImageSource.camera, setState);
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        );
      },
    );
  }

  Future getVideo(ImageSource img, setState) async {
    final pickedFile = await picker.pickVideo(
        source: img,
        preferredCameraDevice: CameraDevice.front,
        maxDuration: const Duration(minutes: 10));
    XFile xFilePick = pickedFile;
    ScaffoldMessenger.of(context).showSnackBar(// is this context <<<
        const SnackBar(
      behavior: SnackBarBehavior.floating,
      content: Text(
        "Wait to prepare ...",
        style: AppTextStyle.largeWhiteBold,
      ),
      backgroundColor: AppColors.green300,
    ));
    await VideoThumbnail.thumbnailData(
      video: pickedFile.path,
      imageFormat: ImageFormat.JPEG,
      // specify the width of the thumbnail, let the height auto-scaled to keep the source aspect ratio
      quality: 25,
    ).then((value) => {
          setState(
            () {
              if (xFilePick != null) {
                print("this is path" + pickedFile.path);
                print(value.toString());
                imageBytes = value;
                _bloc.videosBloc = File(pickedFile.path);
                ScaffoldMessenger.of(context)
                    .showSnackBar(// is this context <<<

                        const SnackBar(
                  behavior: SnackBarBehavior.floating,
                  content: Text(
                    "Ready",
                    style: AppTextStyle.largeWhiteBold,
                  ),
                  backgroundColor: AppColors.green300,
                ));
              } else {
                ScaffoldMessenger.of(context)
                    .showSnackBar(// is this context <<<
                        const SnackBar(content: Text('Nothing is selected!')));
              }
            },
          )
        });
  }

  Widget showDialog({Widget content, String title, fun, IconData icon}) {
    return myDialog(
        context,
        title ?? AppLocalization.of(context).trans("edit_course_information"),
        content ?? Container(),
        fun,
        icon: icon);
  }

  Widget _updateLessonInfo() {
    return Form(
        child: BlocProvider.value(
      value: _bloc,
      child: BlocBuilder<LessonCubit, LessonState>(
        buildWhen: (previous, current) =>
            current is UpdateLessonInfoAcceptState ||
            current is UpdateLessonInfoAwaitState ||
            current is UpdateLessonInfoErrorState,
        builder: (context, state) {
          return Container(
            height: MediaQuery.of(context).size.height / 3.5,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  buildTextFailedForm(
                      _bloc.titleLessonInfoController,
                      Icons.description,
                      _bloc.errorInfoTitle,
                      AppLocalization.of(context).trans("title"),
                      errorText: _bloc.errorInfoTitle),
                  buildTextFailedForm(
                      _bloc.titleLessonInfoArabicController,
                      Icons.description,
                      _bloc.errorInfoArabicTitle,
                      AppLocalization.of(context).trans("title_arabic"),
                      errorText: _bloc.errorInfoArabicTitle),
                  buildTextFailedForm(
                      _bloc.titleLessonInfoEnglishController,
                      Icons.description,
                      _bloc.errorInfoEnglishTitle,
                      AppLocalization.of(context).trans("title_english"),
                      errorText: _bloc.errorInfoEnglishTitle),
                  buildTextFailedForm(
                      _bloc.descriptionLessonInfoController,
                      Icons.description,
                      _bloc.errorInfoDescription,
                      AppLocalization.of(context).trans("description"),
                      errorText: _bloc.errorInfoDescription),
                  buildTextFailedForm(
                      _bloc.descriptionLessonInfoArabicController,
                      Icons.description,
                      _bloc.errorInfoArabicDescription,
                      AppLocalization.of(context).trans("description_arabic"),
                      errorText: _bloc.errorInfoArabicDescription),
                  buildTextFailedForm(
                      _bloc.descriptionLessonInfoEnglishController,
                      Icons.description,
                      _bloc.errorInfoEnglishDescription,
                      AppLocalization.of(context).trans("description_english"),
                      errorText: _bloc.errorInfoEnglishDescription),
                ],
              ),
            ),
          );
        },
      ),
    ));
  }

  Widget _buildAddVideoDialog() {
    return showDialog(
        fun: () => _bloc.addVideos(widget.lesson.id),
        title: AppLocalization.of(context).trans("add_video"),
        content: BlocProvider.value(
            value: _bloc,
            child: StatefulBuilder(builder: (context, StateSetter setState) {
              return BlocBuilder<LessonCubit, LessonState>(
                buildWhen: (context, state) {
                  return state is AddVideosAcceptState ||
                      state is AddVideosAcceptState ||
                      state is AddVideosErrorState ||
                      state is AddVideosValidateState;
                },
                builder: (context, state) {
                  return SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        buildTextFailedForm(
                            _bloc.titleController,
                            Icons.title,
                            _bloc.errorController,
                            AppLocalization.of(context).trans("title"),
                            errorText: _bloc.errorController),
                        buildTextFailedForm(
                            _bloc.titleArabicController,
                            Icons.title,
                            _bloc.errorArabicController,
                            AppLocalization.of(context).trans("title_arabic"),
                            errorText: _bloc.errorArabicController),
                        buildTextFailedForm(
                            _bloc.titleEnglishController,
                            Icons.title,
                            _bloc.errorEnglishController,
                            AppLocalization.of(context).trans("title_english"),
                            errorText: _bloc.errorEnglishController),
                        Divider(
                          color: Colors.black.withOpacity(0.4),
                        ),
                        buildTextFailedForm(
                            _bloc.descriptionController,
                            Icons.description,
                            _bloc.errorDescription,
                            AppLocalization.of(context).trans("description"),
                            errorText: _bloc.errorDescription),
                        buildTextFailedForm(
                          _bloc.descriptionArabicController,
                          Icons.description,
                          _bloc.errorArabicDescription,
                          AppLocalization.of(context)
                              .trans("description_arabic"),
                          errorText: _bloc.errorArabicDescription,
                        ),
                        buildTextFailedForm(
                          _bloc.descriptionEnglishController,
                          Icons.description,
                          _bloc.errorEnglishController,
                          AppLocalization.of(context)
                              .trans("description_english"),
                          errorText: _bloc.errorEnglishController,
                        ),
                        GestureDetector(
                          onTap: () {
                            _showPicker(context: context, setState: setState);
                            // _pickImageBase64(
                            //     setState);
                          },
                          child: Container(
                            margin: EdgeInsetsDirectional.only(
                                top: 15.sp, start: 3.w, end: 2.w),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                border:
                                    Border.all(color: AppColors.lighterGray)),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Center(
                                  child: Column(
                                children: [
                                  _bloc.videosBloc != null
                                      ? Stack(
                                          children: [
                                            Image.memory(
                                                Uint8List.fromList(imageBytes))
                                          ],
                                        )
                                      : Container(
                                          child: SvgPicture.asset(
                                            'assets/img/svg/bg.svg',
                                            height: 150.sp,
                                          ),
                                        ),
                                  SizedBox(
                                    height: 20.sp,
                                  ),
                                  Text(
                                    AppLocalization.of(context)
                                        .trans("Drop_Select_File"),
                                    style: AppTextStyle.xLargeBlackBold,
                                  ),
                                  SizedBox(
                                    height: 20.sp,
                                  ),
                                  Text(
                                    AppLocalization.of(context).trans(
                                      "browse_thorough_your_machine",
                                    ),
                                    style: AppTextStyle.smallBlack,
                                  ),
                                  SizedBox(
                                    height: 20.sp,
                                  ),
                                ],
                              )),
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                },
              );
            })));
  }

  Widget _buildAddQuizDialog() {
    return showDialog(
        fun: () => _bloc.addQuiz(widget.lesson.id),
        title: AppLocalization.of(context).trans("add_quiz"),
        content: BlocProvider.value(
            value: _bloc,
            child: StatefulBuilder(builder: (context, StateSetter setState) {
              return BlocBuilder<LessonCubit, LessonState>(
                buildWhen: (context, state) {
                  return state is AddQuizAcceptState ||
                      state is AddQuizErrorState ||
                      state is AddQuizAwaitState;
                },
                builder: (context, state) {
                  return SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        buildTextFailedForm(
                            _bloc.titleQuizController,
                            Icons.title,
                            _bloc.errorQuizTitle,
                            AppLocalization.of(context).trans("title"),
                            errorText: _bloc.errorQuizTitle),
                        buildTextFailedForm(
                            _bloc.titleQuizArabicController,
                            Icons.title,
                            _bloc.errorQuizTitleAr,
                            AppLocalization.of(context).trans("title_arabic"),
                            errorText: _bloc.errorQuizTitleAr),
                        buildTextFailedForm(
                            _bloc.titleQuizEnglishController,
                            Icons.title,
                            _bloc.errorQuizTitleEn,
                            AppLocalization.of(context).trans("title_english"),
                            errorText: _bloc.errorQuizTitleEn),
                        Divider(
                          color: Colors.black.withOpacity(0.4),
                        ),
                        buildTextFailedForm(
                            _bloc.descriptionQuizController,
                            Icons.description,
                            _bloc.errorQuizDescription,
                            AppLocalization.of(context).trans("description"),
                            errorText: _bloc.errorQuizDescription),
                        buildTextFailedForm(
                          _bloc.descriptionQuizArabicController,
                          Icons.description,
                          _bloc.errorQuizDescriptionAr,
                          AppLocalization.of(context)
                              .trans("description_arabic"),
                          errorText: _bloc.errorQuizDescriptionAr,
                        ),
                        buildTextFailedForm(
                          _bloc.descriptionQuizEnglishController,
                          Icons.description,
                          _bloc.errorQuizDescriptionEn,
                          AppLocalization.of(context)
                              .trans("description_english"),
                          errorText: _bloc.errorQuizDescriptionEn,
                        ),
                      ],
                    ),
                  );
                },
              );
            })));
  }
}
