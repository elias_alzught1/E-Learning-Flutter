import 'package:carousel_slider/carousel_controller.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:e_learning/bloc/teacher_cubit/teacher_cubit.dart';
import 'package:e_learning/controller/data_store.dart';
import 'package:e_learning/helpers/helpers.dart';
import 'package:e_learning/themes/app_colors.dart';
import 'package:e_learning/ui/widgets/appbar/abbBarAPP.dart';
import 'package:e_learning/ui/widgets/appbar/abbBottomBar.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:octo_image/octo_image.dart';

import '../bloc/profile_bloc/profile_bloc.dart';
import '../helpers/picker/custom_image_picker.dart';
import '../localization/app_localization.dart';
import '../models/response/teacher_response.dart';
import '../themes/app_text_style.dart';
import '../themes/elearning_icons.dart';
import 'course/widget/course_card.dart';

class TeacherPage extends StatefulWidget {
  final int id;

  const TeacherPage({Key key, this.id}) : super(key: key);

  @override
  State<TeacherPage> createState() => _TeacherPageState();
}

class _TeacherPageState extends State<TeacherPage> {
  CarouselController carouselController = CarouselController();
  TeacherCubit _bloc;
  TeacherResponse teacher;
  String rating;

  @override
  void initState() {
    _bloc = BlocProvider.of<TeacherCubit>(context);
    _bloc.getTeacherById(widget.id);
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBarApp(),
      body:  BlocConsumer<TeacherCubit, TeacherState>(
        listenWhen: (context, state) {
          return state is GetTeacherAcceptState;
        },
        listener: (context, state) {
          if (state is GetTeacherAcceptState) {
            setState(() {
              teacher = state.teacher;
              double temp = double.parse(teacher.data.rating.toString());
              rating = temp.floor().toString();
            });
            // Navigate to next screen

          }
        },
        buildWhen: (context, state) {
          return state is GetTeacherAwaitState ||
              state is GetTeacherErrorState ||
              state is GetTeacherAcceptState;
        },
        builder: (context, state) {
          if (state is GetTeacherAwaitState) {
            return Container(
                width: 100.w,
                height: 100.h,
                child: Center(
                    child: CircularProgressIndicator(
                  color: AppColors.deepBlue,
                )));
          } else if (state is GetTeacherErrorState) {
            return Container(child: Center(child: Text(state.message)));
          } else {
            return CustomScrollView(slivers: [
              SliverToBoxAdapter(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      color: AppColors.textFieldGray,
                      child: Padding(
                        padding: EdgeInsetsDirectional.only(
                            top: 4.w, end: 4.w, bottom: 4.w, start: 4.w),
                        child: Row(children: [
                          GestureDetector(
                            onTap: () {
                              // CustomImagePicker().openImagePicker(
                              //   context: context,
                              // );
                            },
                            child: Container(
                              width: 30.w,
                              alignment: Alignment.center,
                              child: Container(
                                height: 95.sp,
                                width: 95.sp,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                    color: AppColors.deepBlue,
                                    width: 3.sp,
                                  ),
                                ),
                                alignment: Alignment.center,
                                child: Container(
                                  height: 80.sp,
                                  width: 80.sp,
                                  decoration: const BoxDecoration(
                                    shape: BoxShape.circle,
                                  ),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(1000.sp),
                                    child: OctoImage(
                                        image:
                                        NetworkImage(teacher.data.profileImage),
                                        fit: BoxFit.cover,
                                        placeholderBuilder: (context) =>
                                            getShimmer(),
                                        errorBuilder: imageErrorBuilder2),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 8.w,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                DataStore.instance.lang == "en"
                                    ? "INSTRUCTOR"
                                    : "معلم",
                                style: AppTextStyle.largeGreenBold,
                              ),
                              Text(
                                "${teacher.data.firstName} ${teacher.data.lastName}",
                                style: AppTextStyle.largeGreenBold,
                              ),
                              Text(
                                DataStore.instance.lang == "en"
                                    ? teacher.data.metaData.positionEn
                                    : teacher.data.metaData.positionAr,
                                style: AppTextStyle.largeGreenBold,
                              ),
                            ],
                          )
                        ]),
                      ),
                    ),
                    Container(
                      child: Padding(
                        padding: EdgeInsetsDirectional.only(
                            top: 4.w, end: 4.w, bottom: 4.w, start: 6.w),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsetsDirectional.only(start: 4.w),
                                child: Text(
                                  rating,
                                  style: AppTextStyle.largeGreenBold,
                                ),
                              ),
                              SizedBox(
                                height: 1.h,
                              ),
                              Padding(
                                padding: EdgeInsetsDirectional.only(start: 2.w),
                                child: Center(
                                  child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: List.generate(
                                          5,
                                              (index) => buildStar(context, index,
                                              double.parse(teacher.data.rating.toString()),
                                              size: 21))),
                                ),
                              ),
                              SizedBox(
                                height: 1.h,
                              ),
                              Padding(
                                padding: EdgeInsetsDirectional.only(start: 4.w),
                                child: Text(
                                  teacher.data.email,
                                  style: AppTextStyle.mediumDeepGray,
                                ),
                              ),
                              SizedBox(
                                height: 1.h,
                              ),
                              Padding(
                                padding: EdgeInsetsDirectional.only(start: 4.w),
                                child: Text(
                                  teacher.data.mSISDN,
                                  style: AppTextStyle.mediumDeepGray,
                                ),
                              ),
                              SizedBox(
                                height: 2.h,
                              ),
                              Text(
                                AppLocalization.of(context).trans("About_me"),
                                style: AppTextStyle.largeGreenBold,
                              ),
                              Padding(
                                padding: EdgeInsetsDirectional.only(start: 3.w,end: 3.w),
                                child: Text(
                                  DataStore.instance.lang == "en"
                                      ? teacher.data.metaData.descriptionEn
                                      : teacher.data.metaData.descriptionAr,
                                  style: AppTextStyle.mediumDeepGray,
                                ),
                              ),


                            ]),
                      ),
                    )
                  ],
                ),
              ),
              SliverToBoxAdapter(child:
              Container(
                color: Color(0xFFF0F1F3),
                child: Expanded(
                  child: Column(
                    children: [
                      SizedBox(
                        child: Padding(
                          padding: EdgeInsetsDirectional.only(start: 3.w),
                          child: Row(
                            children: [
                              Text(
                                AppLocalization.of(context)
                                    .trans("Related_course"),
                                style: AppTextStyle.xlargeGreenBold,
                              ),
                              Spacer(),
                              Container(
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 8),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: AppColors.deepBlue,
                                        borderRadius:
                                        BorderRadius.circular(8)),
                                    width: 35,
                                    height: 35,
                                    alignment: Alignment.center,
                                    child: Ink(
                                      child: IconButton(
                                        icon: const Icon(
                                          Icons.arrow_back_ios_rounded,
                                          color: Colors.white,
                                        ),
                                        iconSize: 17,
                                        onPressed: () {
                                          carouselController.previousPage();
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 4.w,
                              ),
                              Container(
                                child: Padding(
                                  padding: EdgeInsetsDirectional.only(
                                      end: 2.w, top: 8),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: AppColors.deepBlue,
                                        borderRadius:
                                        BorderRadius.circular(8)),
                                    width: 35,
                                    height: 35,
                                    alignment: Alignment.center,
                                    child: Ink(
                                      child: IconButton(
                                        icon:const  Icon(
                                          Icons.arrow_forward_ios_rounded,
                                          color: Colors.white,
                                        ),
                                        iconSize: 17,
                                        onPressed: () {
                                          carouselController.nextPage();
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(

                        margin: EdgeInsetsDirectional.only(
                            top: 1.h, bottom: 1.h),
                        child:

                        CarouselSlider(
                          carouselController: carouselController, // Give the controller
                          options: CarouselOptions(
                            autoPlay: false,
                          ),
                          items: teacher.data.courses.map((e) {
                            return  Expanded(

                              child: Container(
                                height: 600,
                                child: CourseCard(

                                  course: e,

                                ),
                              ),
                            );
                          }).toList(),
                        ),
                      )
                    ],
                  ),
                ),
              ),),
              SliverToBoxAdapter(child: AppBottomBar())
            ],);
          }
        },
      ),
    );
  }

  Widget buildStar(BuildContext context, int index, double rating,
      {double size}) {
    Icon icon;
    if (index >= rating) {
      icon = Icon(
        Elearning.star_1,
        color: AppColors.deepBlue.withOpacity(0.3),
        size: size != null ? size : null,
      );
    } else if (index > rating - 1 && index < rating) {
      icon = Icon(Elearning.star_half_alt,
          color: AppColors.blue, size: size != null ? size : null);
    } else {
      icon = Icon(
        Elearning.star_1,
        color: AppColors.blue,
        size: size != null ? size : null,
      );
    }
    return InkResponse(
      onTap: null,
      child: Padding(
          padding: EdgeInsetsDirectional.only(start: 1, end: 0.5), child: icon),
    );
  }

  Widget imageErrorBuilder2(ctx, error, _) => Container(
        color: AppColors.gray,
        child: Center(
            child: Text(
          teacher.data.firstName[0] ?? "",
          style: AppTextStyle.xxxLargeBlack,
        )),
      );
}
