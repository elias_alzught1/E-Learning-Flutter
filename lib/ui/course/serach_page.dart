import 'package:e_learning/localization/app_localization.dart';
import 'package:e_learning/public/global.dart';
import 'package:e_learning/ui/course/course_details.dart';
import 'package:e_learning/ui/course/widget/custome_appBar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/course_bloc/course_bloc.dart';
import '../../bloc/search_bloc/search_cubit.dart';
import '../../themes/app_colors.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key key}) : super(key: key);

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  SearchCubit _bloc;
  final TextEditingController _searchController = TextEditingController();
  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    _bloc = BlocProvider.of<SearchCubit>(context);

    /*
     _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
 */
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: AppColors.white,
          leading: const Icon(Icons.arrow_back, color: Colors.black),
          title: SizedBox(
            height: 45,
            child: TextField(
              onChanged: (value) {
                _bloc.pagingController.addPageRequestListener((pageKey) {
                  _bloc.fetch(value, 1);
                });
              },
              decoration: InputDecoration(
                hintText: AppLocalization.of(context).trans("search"),
                contentPadding: const EdgeInsetsDirectional.only(start: 20),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey.shade100),
                    borderRadius: BorderRadius.circular(20)),
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey.shade100),
                    borderRadius: BorderRadius.circular(20)),
                border: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey.shade100),
                    borderRadius: BorderRadius.circular(20)),
                filled: true,
                fillColor: Colors.grey.shade200,
              ),
              controller: _searchController,
            ),
          )),
      body: BlocBuilder<SearchCubit, SearchState>(
        buildWhen: (_, current) =>
            current is SearchCourseAwaitState ||
            current is SearchCourseAcceptState ||
            current is SearchCourseErrorState,
        builder: (context, state){
          if (state is SearchCourseAwaitState){
            return const Center(
              child: CircularProgressIndicator(
                color: AppColors.purple,
              ),
            );
          } else if (state is SearchCourseErrorState) {
            return Container();
          } else if (state is SearchCourseAcceptState) {
            return Column(
              children: [
                Expanded(
                    child: ListView.builder(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                  itemCount: _bloc.searchResult.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CourseDetails(
                                    course: _bloc.searchResult[index],
                                  )),
                        );
                      },
                      child: Container(
                        color: AppColors.textFieldGray,
                        margin: const EdgeInsets.only(
                          top: 5,
                          bottom: 5,
                        ),
                        child: Padding(
                          padding: EdgeInsetsDirectional.only(start: 5),
                          child: ListTile(
                            contentPadding: EdgeInsets.zero,
                            leading: ClipRRect(
                              borderRadius: BorderRadius.circular(5),
                              child: Image.network(
                                _bloc.searchResult[index].image,
                                // Replace with your image URL
                                width: 80.0,
                                height: 48.0,
                                fit: BoxFit.fill,
                              ),
                            ),
                            title: Text(_bloc.searchResult[index].name),
                          ),
                        ),
                      ),
                    );
                  },
                )),
              ],
            );
          }
          return Container();
        },
      ),
    );
  }
}
