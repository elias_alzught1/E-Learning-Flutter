import 'package:cached_network_image/cached_network_image.dart';
import 'package:e_learning/bloc/course_bloc/course_bloc.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sprintf/sprintf.dart';

import '../../controller/data_store.dart';
import '../../helpers/helpers.dart';
import '../../localization/app_localization.dart';
import '../../public/global.dart';
import '../../themes/app_colors.dart';
import '../../themes/app_text_style.dart';
import '../widgets/appbar/abbBarAPP.dart';
import '../widgets/button/commit_button.dart';

class CartPage extends StatefulWidget {
  const CartPage({Key key}) : super(key: key);

  @override
  State<CartPage> createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  CourseBloc _bloc;

  @override
  void initState() {
    BlocProvider.of<CourseBloc>(context).add(GetCartCourses());
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarApp(),
      body: BlocBuilder<CourseBloc, CourseState>(
        builder: (context, state) {
          if (state is GetCartAwait) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is GetCartError) {
            return Center(
              child: Text(
                state.message,
                style: AppTextStyle.smallGreen,
              ),
            );
          } else if (state is GetCartAccept) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 25,
                ),
                Padding(
                  padding: const EdgeInsetsDirectional.only(start: 10),
                  child: Text(
                    sprintf(
                      AppLocalization.of(context).trans('number_in_cart'),
                      [
                        BlocProvider.of<CourseBloc>(context)
                            .cartCourses
                            .length
                            .toString()
                      ],
                    ),
                    style: AppTextStyle.xlargeGreenNormalBold,
                  ),
                ),
                const SizedBox(
                  height: 25,
                ),
                Expanded(
                  child: ListView.builder(
                    itemBuilder: (context, index) {
                      final item = BlocProvider.of<CourseBloc>(context)
                          .cartCourses[index];
                      return Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: Card(
                          elevation: 6,
                          // Adjust the elevation to control the shadow depth
                          shadowColor: Colors.grey,
                          // Set the shadow color to grey
                          child: Container(
                              margin:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              child: Container(
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border(
                                        bottom: BorderSide(
                                            color: colorList[index % 4],
                                            width: 5))),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10),
                                  child: Column(
                                    children: [
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(bottom: 5),
                                            child: CachedNetworkImage(
                                              imageUrl: item.image,
                                              height: 90,
                                              width: 90,
                                              fit: BoxFit.fill,
                                              placeholder: (context, url) =>
                                                  getShimmer(),
                                              errorWidget:
                                                  (context, url, error) =>
                                                      const Center(
                                                child: Icon(Icons.error),
                                              ),
                                            ),
                                          ),
                                          const SizedBox(width: 10),
                                          Expanded(
                                              child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 1,
                                                    bottom: 1,
                                                    right: 0.2,
                                                    left: 0.2),
                                                child: Row(
                                                  children: [
                                                    SvgPicture.asset(
                                                      'assets/img/svg/square_svg.svg',
                                                      width:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width *
                                                              0.04,
                                                      height:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width *
                                                              0.04,
                                                      color:
                                                          colorList[index % 4],
                                                    ),
                                                    Spacer(),
                                                    Expanded(
                                                      child: RichText(
                                                        text: TextSpan(
                                                          children: [
                                                            WidgetSpan(
                                                              child: Padding(
                                                                padding:
                                                                    const EdgeInsetsDirectional
                                                                            .only(
                                                                        end:
                                                                            2.0),
                                                                child: Icon(
                                                                    Icons
                                                                        .access_time,
                                                                    color: colorList[index %
                                                                            4] ??
                                                                        Colors
                                                                            .grey,
                                                                    size: 14),
                                                              ),
                                                            ),
                                                            TextSpan(
                                                                text: DataStore
                                                                            .instance
                                                                            .lang ==
                                                                        "en"
                                                                    ? item
                                                                        .durations
                                                                        .durationEn
                                                                        .toString()
                                                                    : item
                                                                        .durations
                                                                        .durationAr
                                                                        .toString()
                                                                        .toString(),
                                                                style: AppTextStyle
                                                                    .xSmallBlack),
                                                          ],
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                              const SizedBox(
                                                height: 5,
                                              ),
                                              Text(
                                                  DataStore.instance.lang ==
                                                          "en"
                                                      ? item.nameEn
                                                      : item.nameAr,
                                                  style: AppTextStyle
                                                      .mediumBlackBold,
                                                  maxLines: 1),
                                              const SizedBox(
                                                height: 5,
                                              ),
                                              Text(
                                                  DataStore.instance.lang ==
                                                          "en"
                                                      ? item.descriptionEn
                                                      : item.descriptionAr,
                                                  style:
                                                      AppTextStyle.mediumBlack,
                                                  maxLines: 2),
                                              Row(
                                                children: [
                                                  Container(
                                                    width: 30,
                                                    height: 30,
                                                    decoration: BoxDecoration(
                                                      shape: BoxShape.circle,
                                                      border: Border.all(
                                                        color: colorList[
                                                            index % 4],
                                                        // Border color
                                                        width:
                                                            2.0, // Border width
                                                      ),
                                                    ),
                                                    child: CircleAvatar(
                                                      backgroundColor:
                                                          Colors.white,
                                                      radius: 30,
                                                      backgroundImage:
                                                          NetworkImage(
                                                        item.authors[0]
                                                            .profileImage, // Replace with your image URL
                                                      ),
                                                    ),
                                                  ),
                                                  const SizedBox(
                                                    width: 5,
                                                  ),
                                                  Expanded(
                                                    child: Text(
                                                      item.authors[0].firstName,
                                                      style: AppTextStyle
                                                          .mediumGray,
                                                    ),
                                                  ),
                                                  Spacer(),
                                                  TextButton(
                                                    onPressed: () {},
                                                    child: Text(
                                                      "Remove",
                                                      style: AppTextStyle
                                                          .mediumRedBold,
                                                    ),
                                                  )
                                                ],
                                              ),
                                              SizedBox(
                                                height: 5,
                                              )
                                            ],
                                          )),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              )),
                        ),
                      );
                    },
                    itemCount:
                        BlocProvider.of<CourseBloc>(context).cartCourses.length,
                  ),
                ),
                Padding(
                  padding: const EdgeInsetsDirectional.only(start: 10),
                  child: Text(
                    AppLocalization.of(context).trans("subtotal"),
                    style: AppTextStyle.xlargeGreenNormalBold,
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Center(
                  child: Text(
                    "${BlocProvider.of<CourseBloc>(context).subTotal} ${AppLocalization.of(context).trans("Currency")}",
                    style: AppTextStyle.xlargeGreenNormalBold,
                  ),
                ),
                Container(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 35, vertical: 5),
                  width: 100.w,
                  height: 7.h,
                  child: CommitButton(
                    textStyle: AppTextStyle.largeWhiteBold,
                    backgroundColor: AppColors.normalGreen,
                    text: AppLocalization.of(context).trans("checkout"),
                    onPressed: () {},
                  ),
                ),
                const SizedBox(
                  height: 5,
                )
              ],
            );
          }
          return Container();
        },
      ),
    );
  }
}
