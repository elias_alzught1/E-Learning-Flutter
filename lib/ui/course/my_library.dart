import 'package:e_learning/bloc/course_bloc/course_bloc.dart';
import 'package:e_learning/controller/data_store.dart';
import 'package:e_learning/ui/course/widget/course_card.dart';
import 'package:e_learning/ui/widgets/appbar/abbBarAPP.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/wish_list_cubit/wish_list_cubit.dart';
import '../../localization/app_localization.dart';
import '../../public/global.dart';
import '../../themes/app_colors.dart';
import '../../themes/app_text_style.dart';
import '../../themes/font_family.dart';
import '../widgets/button/pagination_button.dart';

class MyLibrary extends StatefulWidget {
  const MyLibrary({Key key}) : super(key: key);

  @override
  State<MyLibrary> createState() => _MyLibraryState();
}

class _MyLibraryState extends State<MyLibrary> {
  final ScrollController _scrollController = ScrollController();
  WishListCubit _bloc;
  @override
  void initState() {
    _bloc= BlocProvider.of<WishListCubit>(context);
    _bloc.fetch();
    // TODO: implement initState
    super.initState();
  }


  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade50,
      appBar: const AppBarApp(),
      body: BlocBuilder<WishListCubit, WishListState>(
        builder: (context, state) {
          if (state is GetWishListAwait ){
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is GetWishListError){
            return Center(
              child: Text(
                state.errorMessage,
                style: AppTextStyle.smallGreen,
              ),
            );
          }
          return SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  color: Colors.black,
                  child: Padding(
                    padding: const EdgeInsetsDirectional.only(
                        start: 10, bottom: 10, top: 10),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            AppLocalization.of(context)
                                .trans("Enrolled_Courses"),
                            style: const TextStyle(
                                fontWeight: FontWeight.bold,
                                fontFamily: FontFamily.cairo,
                                color: Colors.yellow),
                          ),
                          Text(
                            AppLocalization.of(context)
                                .trans("enrolled_page_title"),
                            style: const TextStyle(
                                fontFamily: FontFamily.cairo,
                                color: Colors.white),
                          )
                        ]),
                  ),
                ),
                ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    final item =
                     _bloc.wishList[index];
                    return Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: CourseCard(
                        color: colorList[index % 4],
                        course: item,
                      ),
                    );
                  },
                  itemCount:
                  _bloc.wishList.length,
                ),
                 Center(
                    child: PaginationButton(
                  color: const Color(0xFF6E19D7),
                  pageNumber: _bloc.currentPage.toString(),
                      onLoadMore: ()=>   _bloc.fetchMore(),
                      onLoadLess:()=> _bloc.fetchLess(),
                )),
                const SizedBox(
                  height: 5,
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
