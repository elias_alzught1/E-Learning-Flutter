import 'package:e_learning/localization/app_localization.dart';
import 'package:e_learning/models/response/lessons_response.dart';
import 'package:e_learning/themes/app_text_style.dart';
import 'package:e_learning/ui/course/course_shower.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../bloc/course_bloc/course_bloc.dart';
import '../../../controller/data_store.dart';
import '../../../models/response/quizs_response.dart';
import '../../../themes/app_colors.dart';
import '../../../themes/app_font_size.dart';
import '../../../themes/elearning_icons.dart';
import '../../../themes/font_family.dart';

class LessonsCard extends StatefulWidget {
  final Lessons lesson;

  const LessonsCard({Key key, this.lesson}) : super(key: key);

  @override
  State<LessonsCard> createState() => _LessonsCardState();
}

class _LessonsCardState extends State<LessonsCard> {
  bool _isVisible = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String lang = DataStore.instance.lang;
    return InkWell(
      child: Container(
        decoration: BoxDecoration(
          color: AppColors.normalGreen.withOpacity(0.6),
        ),
        child:
            Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 4.w),
            child: Row(
              children: [
                Container(
                  child: Text(
                    lang == "en"
                        ? widget.lesson.title.toString()
                        : widget.lesson.titleAr,
                    style: AppTextStyle.largeWhiteBold,
                  ),
                ),
                Spacer(),
                Icon(
                  _isVisible == true
                      ? Icons.keyboard_arrow_up
                      : Icons.keyboard_arrow_down,
                  color: AppColors.white,
                ),
              ],
            ),
          ),
          Container(
            height: 3,
          ),
          Visibility(
              visible: _isVisible,
              child: Container(
                color: Colors.white,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 4.w),
                  child: Text(
                    lang == "en"
                        ? widget.lesson.descriptionEn
                        : widget.lesson.descriptionAr,
                    style: AppTextStyle.mediumGray,
                  ),
                ),
              )),
          Visibility(
            visible: _isVisible,
            child: Container(
              color: Colors.white,
              height: 8,
            ),
          ),
          Visibility(
              visible: _isVisible,
              child: Container(
                color: Colors.white,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 4.w),
                  child: Text(
                    lang == "en" ? "What is include" : "ماذا يتضمن ",
                    style: AppTextStyle.largeBlackBold,
                  ),
                ),
              )),
          Container(
            height: 1.h,
            color: Colors.white,
          ),
          Container(
            color: AppColors.white,
            child: Visibility(
              visible: _isVisible,
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4.w),
                    child: Row(
                      children: [
                        const Icon(Elearning.play_circle_outline,
                            color: AppColors.normalGreen),
                        SizedBox(
                          width: 1.w,
                        ),
                        Text(
                          lang == "en"
                              ? "${widget.lesson.videosNumber} Videos"
                              : "${widget.lesson.videosNumber} فيديو ",
                          style: TextStyle(
                            fontSize: AppFontSize.X_LARGE,
                            color: Colors.black,
                            fontWeight: FontWeight.normal,
                            fontFamily: FontFamily.cairo,
                          ),
                        ),
                        SizedBox(
                          width: 4.w,
                        ),
                        const Icon(Elearning.book,
                            color: AppColors.normalGreen),
                        SizedBox(
                          width: 1.w,
                        ),
                        Text(
                          lang == "en"
                              ? "${widget.lesson.readsNumber} Reading"
                              : "${widget.lesson.readsNumber} كتب ",
                          style: TextStyle(
                            fontSize: AppFontSize.X_LARGE,
                            color: Colors.black,
                            fontWeight: FontWeight.normal,
                            fontFamily: FontFamily.cairo,
                          ),
                        ),
                        SizedBox(
                          width: 4.w,
                        ),
                        Icon(Elearning.live_help, color: AppColors.normalGreen),
                        SizedBox(
                          width: 1.w,
                        ),
                        Text(
                          lang == "en"
                              ? "${widget.lesson.testsNumber} Quizs"
                              : "${widget.lesson.testsNumber} اختبارات ",
                          style: TextStyle(
                            fontSize: AppFontSize.X_LARGE,
                            color: Colors.black,
                            fontWeight: FontWeight.normal,
                            fontFamily: FontFamily.cairo,
                          ),
                        ),
                        SizedBox(
                          height: 2.h,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4.w),
                    child: GestureDetector(
                      child: Container(
                        height: 6.h,
                        width: 100.w,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            border: Border.all(color: AppColors.purple)),
                        child: Center(
                          child: Text(
                            AppLocalization.of(context).trans("Start"),
                            style: const TextStyle(
                              fontSize: AppFontSize.X_LARGE,
                              color: AppColors.purple,
                              fontWeight: FontWeight.normal,
                              fontFamily: FontFamily.cairo,
                            ),
                          ),
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => CourseShower(),
                          ),
                        );
                      },
                    ),
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                ],
              ),
            ),
          ),
        ]),
      ),
      onTap: () {
        setState(() {
          _isVisible = !_isVisible;
        });
      },
    );
  }
}
