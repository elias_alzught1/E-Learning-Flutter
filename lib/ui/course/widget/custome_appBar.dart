// import 'package:flutter/material.dart';
//
// class CustomAppBar extends StatelessWidget {
//   final TextEditingController searchController;
//   final VoidCallback onSearch;
//
//   CustomAppBar({  this.searchController,   this.onSearch});
//
//   @override
//   Widget build(BuildContext context) {
//     return AppBar(
//       title: TextField(
//         controller: searchController,
//         decoration: InputDecoration(
//           hintText: 'Search...',
//           suffixIcon: IconButton(
//             onPressed: () {
//               final query = searchController.text;
//               onSearch(query);
//             },
//             icon: Icon(Icons.search),
//           ),
//         ),
//       ),
//     );
//   }
// }