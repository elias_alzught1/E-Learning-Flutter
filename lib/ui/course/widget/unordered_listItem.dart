
import 'package:e_learning/themes/app_text_style.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';

class UnorderedListItem extends StatelessWidget {
     UnorderedListItem(this.text, this.icon, {Key key, this.padding,this.appTextStyle}) : super(key: key);
   final Icon icon;
  final String text;
  final double padding;
  final TextStyle appTextStyle;



  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,


      children: <Widget>[
        icon,
        SizedBox(width: 1.3.w,),

           Expanded(  child: Text(text,style: appTextStyle,)),


      ],
    );
  }
}