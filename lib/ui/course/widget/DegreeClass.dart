import 'package:flutter/material.dart';

class DegreeOperations {
  final double degree;

  DegreeOperations(this.degree);

  // Method to calculate color based on the degree
  Color calculateColor() {
    if (degree >= 0 && degree < 39) {
      return Colors.red;
    } else if (degree >= 40 && degree < 79) {
      return Colors.orange;
    } else if (degree >= 80 && degree < 100) {
      return Colors.green;
    } else {
      return Colors.yellow;
    }
  }

  // Method to generate a string based on the degree
  String generateGrade() {
    if (degree >= 0 && degree < 39) {
      return "F";
    } else if (degree >= 40 && degree < 59) {
      return "C";
    } else if (degree >= 60 && degree < 69) {
      return "B";
    } else {
      return "A";
    }
  }

  String generateMarksName() {
    if (degree >= 0 && degree < 39) {
      return "fail";
    } else if (degree >= 40 && degree < 59) {
      return "Bad";
    } else if (degree >= 60 && degree < 69) {
      return "Grade";
    } else {
      return "Very Excellent";
    }
  }
}
