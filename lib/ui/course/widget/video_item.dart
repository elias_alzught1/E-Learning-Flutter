import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class VideoListItem extends StatefulWidget {
  final String videoUrl;

  VideoListItem({this.videoUrl});

  @override
  _VideoListItemState createState() => _VideoListItemState();
}

class _VideoListItemState extends State<VideoListItem> {
  VideoPlayerController _videoPlayerController;
  ChewieController _chewieController;

  @override
  void initState() {
    super.initState();

    // Initialize video player controller
    _videoPlayerController = VideoPlayerController.network(widget.videoUrl);

    // Initialize Chewie controller
    _chewieController = ChewieController(
      videoPlayerController: _videoPlayerController,
      autoPlay: false, // Set to true if you want videos to auto-play
      looping: true,
      allowedScreenSleep: false,
      autoInitialize: true,
      isLive: true,

    );
  }

  @override
  Widget build(BuildContext context) {
    return    SizedBox(
      height: 200,
      child: Chewie(controller: _chewieController),
    );
  }

  @override
  void dispose() {
    _videoPlayerController.dispose();
    _chewieController.dispose();
    super.dispose();
  }
}
