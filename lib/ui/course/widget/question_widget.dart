import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../bloc/course_bloc/course_bloc.dart';
import '../../../localization/app_localization.dart';
import '../../../models/common/AnswerRequest.dart';
import '../../../models/response/quizs_response.dart';
import '../../../themes/app_colors.dart';
import '../../../themes/app_text_style.dart';

class QuestionWidget extends StatefulWidget {
  final List<Questions> form;
  final int id;

  const QuestionWidget({Key key, this.form, this.id}) : super(key: key);

  @override
  State<QuestionWidget> createState() => _QuestionWidgetState();
}

class _QuestionWidgetState extends State<QuestionWidget> {
  int indexItem = 0;
  int selectedIndex;
  List<String> groupsValue = [];
  AnswerRequest answerRequest = AnswerRequest();
  UserAnswers userAnswers = UserAnswers();
  List<UserAnswers> userAnswersList = [];
  List<Map<String, dynamic>> data = [
    // Add more items if needed
  ];
  List<IntPair> selectedAnswer = [];
  List<IntPair> currentChoice = [];

  @override
  void initState() {
    answerRequest.testId = 5;

    answerRequest.userAnswers = userAnswersList;
    for (int i = 0; i < widget.form.length; i++) {
      groupsValue.add("");
    }
    // TODO: implement initState
    super.initState();
  }

  bool isFinish = false;

  @override
  Widget build(BuildContext context) {
    String length = (widget.form.length - 1).toString();
    return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      Align(
        child: Text(
          widget.form[indexItem].questionText,
          textAlign: TextAlign.left,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18.0,
          ),
        ),
      ),
      ListView.builder(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: widget.form[indexItem].answers.length,
        itemBuilder: (context, i) {
          int currentSelectIndex;
          for (int i = 0; i < selectedAnswer.length; i++) {
            if (currentChoice[i].first == indexItem) {
              currentSelectIndex = currentChoice[i].second;
            }
          }
          return GestureDetector(
            onTap: () {
              for (int i = 0; i < selectedAnswer.length; i++) {
                if (selectedAnswer[i].first == widget.form[indexItem].id) {
                  selectedAnswer.removeWhere(
                      (element) => element.first == widget.form[indexItem].id);
                }
                if (currentChoice[i].first == indexItem) {
                  currentChoice
                      .removeWhere((element) => element.first == indexItem);
                }
              }
              setState(() {
                selectedAnswer.add(IntPair(widget.form[indexItem].id,
                    widget.form[indexItem].answers[i].id));
                currentChoice.add(IntPair(indexItem, i));
              });
              print(selectedAnswer.length);
            },
            child: Padding(
              padding:
                  EdgeInsetsDirectional.only(bottom: 1.h, start: 2.w, end: 2.w),
              child: Container(
                  decoration: BoxDecoration(
                      color: AppColors.lighterGray,
                      borderRadius: BorderRadius.circular(10)),
                  child: ListTile(
                    leading: GestureDetector(
                      onTap: () {},
                      child: SizedBox(
                        height: 25,
                        width: 25,
                        child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(100),
                                border: Border.all(color: AppColors.gray)),
                            child: currentSelectIndex == i
                                ? Center(
                                    child: Padding(
                                      padding: const EdgeInsets.all(4.0),
                                      child: Container(
                                        decoration: const BoxDecoration(
                                            color: AppColors.lightPurple,
                                            shape: BoxShape.circle),
                                      ),
                                    ),
                                  )
                                : null),
                      ),
                    ),
                    title: Text(
                      widget.form[indexItem].answers[i].value,
                    ),
                  )

                  // Row(
                  //   crossAxisAlignment: CrossAxisAlignment.start,
                  //   children: [
                  //     GestureDetector(
                  //       onTap: () {},
                  //       child: SizedBox(
                  //         height: 25,
                  //         width: 25,
                  //         child:
                  //         Container(
                  //             decoration: BoxDecoration(
                  //                 borderRadius: BorderRadius.circular(100),
                  //                 border: Border.all(color: AppColors.gray)),
                  //             child: currentSelectIndex == i
                  //                 ? Center(
                  //                     child: Padding(
                  //                       padding: const EdgeInsets.all(4.0),
                  //                       child: Container(
                  //                         decoration: const BoxDecoration(
                  //                             color: AppColors.lightPurple,
                  //                             shape: BoxShape.circle),
                  //                       ),
                  //                     ),
                  //                   )
                  //                 : null),
                  //       ),
                  //     ),
                  //     const SizedBox(
                  //       width: 10,
                  //     ),
                  //     Flexible(
                  //       child: Text(
                  //         widget.form[indexItem].answers[i].value,
                  //
                  //       ),
                  //     ),
                  //   ],
                  // ),
                  ),

              // RadioListTile<String>(
              //   value: widget.form[indexItem].answers[i].value,
              //   onChanged: (value) {
              //     /*
              //     questionId: widget.form[i].id,
              //         answerId: widget.form[indexItem].answers[i].id
              //     */
              //     userAnswers.answerId = widget.form[i].id;
              //     userAnswers.questionId = widget.form[i].id;
              //     userAnswersList.add(userAnswers);
              //
              //     setState(() {
              //       groupsValue[i] = value;
              //     });
              //
              //     data.add(
              //       {
              //         "answer_id": widget.form[i].id,
              //         "question_id": widget.form[indexItem].id
              //       },
              //     );
              //   },
              //   groupValue: groupsValue[i],
              //   secondary: Container(
              //     margin: EdgeInsetsDirectional.only(top: 15.sp),
              //     child:
              //     Text(
              //       widget.form[indexItem].answers[i].value,
              //       style: AppTextStyle.mediumBlack,
              //     ),
              //   ),
              //   activeColor: AppColors.deepBlue,
              // ),
            ),
          );
        },
      ),
      SizedBox(
        height: 2.h,
      ),
      Padding(
        padding: EdgeInsetsDirectional.only(start: 2.w, end: 2.w),
        child: Row(
          children: [
            Icon(
              Icons.arrow_back_ios,
              size: 12.sp,
              color: AppColors.deepBlue,
            ),
            InkWell(
                child: Text(
                  AppLocalization.of(context).trans("Back"),
                  style: AppTextStyle.mediumGreenBold,
                ),
                onTap: () {
                  setState(() {
                    if (indexItem == 0) {
                    } else {
                      if (isFinish) {
                        isFinish = false;
                      }
                      indexItem--;
                    }
                  });
                }),
            Spacer(),
            Text("$indexItem/$length"),
            Spacer(),
            InkWell(
              //Finish
              child: isFinish
                  ? Text(AppLocalization.of(context).trans("Finish"))
                  : Text(AppLocalization.of(context).trans("Next"),
                      style: AppTextStyle.mediumGreenBold),
              onTap: () {
                setState(() {
                  if (isFinish) {
                    for (int i = 0; i < selectedAnswer.length; i++) {
                      data.add(convertToJson(
                          selectedAnswer[i].second.toString(),
                          (selectedAnswer[i].first.toString())));

                    }
                    BlocProvider.of<CourseBloc>(context)
                        .add(TestEvents(widget.id, data));
                  } else {
                    if (indexItem + 1 == widget.form.length) {
                      // BlocProvider.of<CourseBloc>(context)
                      //     .add(TestEvents(widget.id, data));
                    } else {
                      if (indexItem + 2 == widget.form.length) {
                        isFinish = true;
                        indexItem++;
                      } else {
                        indexItem++;
                      }
                    }
                  }
                });
              },
            ),
            Icon(
              Icons.arrow_forward_ios,
              size: 12.sp,
              color: AppColors.deepBlue,
            ),
            SizedBox(
              height: 4.h,
            ),
          ],
        ),
      ),
      SizedBox(
        height: 2.h,
      ),
    ]);
  }
}

class IntPair {
  final int first;
  final int second;

  IntPair(this.first, this.second);
}

Map<String, dynamic> convertToJson(String answerId, String questionId) {
  Map<String, dynamic> jsonObject = {
    "answer_id": answerId,
    "question_id": questionId,
  };
  return jsonObject;
}
