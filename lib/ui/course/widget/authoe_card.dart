import 'package:e_learning/bloc/teacher_cubit/teacher_cubit.dart';
import 'package:e_learning/helpers/helpers.dart';
import 'package:e_learning/localization/app_localization.dart';
import 'package:e_learning/themes/app_colors.dart';
import 'package:e_learning/themes/app_text_style.dart';
import 'package:e_learning/themes/elearning_icons.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:octo_image/octo_image.dart';

import '../../../controller/data_store.dart';
import '../../../models/common/auther_model.dart';
import '../../teacher_ui.dart';

class AuthorCard extends StatelessWidget {
  final Authors authors;

  const AuthorCard({Key key, this.authors}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String lang = DataStore.instance.lang;
    return InkWell(
      child: Padding(
        padding: EdgeInsets.all(1.w),
        child: SizedBox(
          width: 100.w,
          child: Container(
            decoration: BoxDecoration(
              border: Border.all(color: AppColors.lighterGray),
              borderRadius: const BorderRadius.all(Radius.circular(5)),
              color: AppColors.white,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 3.h),
                  child: Center(
                    child: Container(
                      width: 100.w,
                      alignment: Alignment.center,
                      child: Container(
                        height: 100.sp,
                        width: 100.sp,
                        decoration: BoxDecoration(
                          border: Border.all(color: AppColors.purple),
                          shape: BoxShape.circle,
                        ),
                        alignment: Alignment.center,
                        child: Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: Container(
                            height: 95.sp,
                            width: 95.sp,
                            decoration: const BoxDecoration(
                              shape: BoxShape.circle,
                            ),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(1000.sp),
                              child: OctoImage(
                                image: NetworkImage(authors.profileImage),
                                fit: BoxFit.cover,
                                placeholderBuilder: (context) => getShimmer(),
                                errorBuilder: imageErrorBuilder,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 2.h,
                ),
                Center(
                  child: Text(
                    "${authors.firstName} ${authors.lastName}",

                    //yellowAccent
                    style: AppTextStyle.xlargeYellowBold,
                  ),
                ),
                Center(
                    child: Text(
                  lang == "en"
                      ? authors.metaData.positionEn == null
                          ? " "
                          : authors.metaData.positionEn
                      : authors.metaData.positionAr == null
                          ? " "
                          : authors.metaData.positionAr,
                  maxLines: 2,
                  style: AppTextStyle.mediumDeepGrayBold,
                )),
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              const Icon(
                                Icons.star,
                                color: Colors.yellow,
                              ),
                              const SizedBox(
                                width: 4,
                              ),
                              Text(
                                "4.9" +
                                    " " +
                                    AppLocalization.of(context)
                                        .trans("Course_instructor"),
                                style: AppTextStyle.mediumDeepGrayBold,
                              )
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Elearning.group,
                                color: AppColors.red,
                                size: 20.sp,
                              ),
                              const SizedBox(
                                width: 4,
                              ),
                              Text(
                                "4.9" +
                                    " " +
                                    AppLocalization.of(context)
                                        .trans("Students"),
                                style: AppTextStyle.mediumDeepGrayBold,
                              )
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Elearning.play_circled,
                                color: AppColors.red,
                                size: 20.sp,
                              ),
                              const SizedBox(
                                width: 4,
                              ),
                              Text(
                                "10" +
                                    " " +
                                    AppLocalization.of(context).trans("course"),
                                style: AppTextStyle.mediumDeepGrayBold,
                              )
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                Center(
                  child: Container(
                    margin: EdgeInsetsDirectional.only(start: 3.w, end: 3.w),
                    child: Text(
                      lang == "en"
                          ? authors.metaData.descriptionEn == null
                              ? " "
                              : authors.metaData.descriptionEn
                          : authors.metaData.descriptionAr == null
                              ? " "
                              : authors.metaData.descriptionAr,
                      style: AppTextStyle.mediumDeepGrayBold,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),

                  ),
                ),
                const SizedBox(
                  height: 4,
                ),
              ],
            ),
          ),
        ),
      ),
      onTap: () {
        //TeacherPage
        Navigator.of(context).push(CupertinoPageRoute(
          builder: (context) => BlocProvider(
            create: (context) => TeacherCubit(),
            child: TeacherPage(
              id: authors.id,
            ),
          ),
        ));
      },
    );
  }
}
