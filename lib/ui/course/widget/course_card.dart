import 'package:cached_network_image/cached_network_image.dart';
import 'package:e_learning/localization/app_localization.dart';
import 'package:e_learning/models/common/course_model.dart';
import 'package:e_learning/themes/app_text_style.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:octo_image/octo_image.dart';

import '../../../controller/data_store.dart';
import '../../../helpers/helpers.dart';
import '../../../models/common/auther_model.dart';
import '../../../themes/app_colors.dart';
import '../../../themes/app_font_size.dart';
import '../../../themes/font_family.dart';
import '../course_details.dart';

class CourseCard extends StatefulWidget {
  final Courses course;
  final double height;
  final VoidCallback fun;
  final Color color;

  const CourseCard({Key key, this.course, this.height, this.fun, this.color})
      : super(key: key);

  @override
  State<CourseCard> createState() => _CourseCardState();
}

class _CourseCardState extends State<CourseCard> {
  final searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        widget.fun ??
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      CourseDetails(
                        course: widget.course,
                      )),
            );
      },
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(0.0),
        ),
        elevation: 2,
        child: Container(
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border(
                  bottom: BorderSide(
                      color: widget.color ?? Colors.green, width: 5))),
          height: widget.height != null ? widget.height : 300,
          child: Padding(
            padding: const EdgeInsets.all(5.0),
            child: Stack(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 1, bottom: 1, right: 0.2, left: 0.2),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(15),
                          child: SizedBox(
                            child: CachedNetworkImage(
                              height: 50,
                              imageUrl: widget.course.image,
                              fit: BoxFit.fill,
                              placeholder: (context, url) => getShimmer(),
                              errorWidget: (context, url, error) =>
                              const Center(
                                child: Icon(Icons.error),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 0.3.h,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 1, bottom: 1, right: 0.2, left: 0.2),
                      child: Row(
                        children: [
                          SvgPicture.asset(
                            'assets/img/svg/square_svg.svg',
                            width: MediaQuery
                                .of(context)
                                .size
                                .width * 0.04,
                            height: MediaQuery
                                .of(context)
                                .size
                                .width * 0.04,
                            color: widget.color ?? Colors.grey,
                          ),
                          Spacer(),
                          DataStore.instance.lang == "en"
                              ? Expanded(
                            child: RichText(
                              text: TextSpan(
                                children: [
                                  WidgetSpan(
                                    child: Padding(
                                      padding: EdgeInsetsDirectional.only(
                                          end: 2.0),
                                      child: Icon(Icons.access_time,
                                          color:
                                          widget.color ?? Colors.grey,
                                          size: 14),
                                    ),
                                  ),
                                  TextSpan(
                                      text:
                                      DataStore.instance.lang == "en"
                                          ? widget.course.duartion
                                          .durationEn
                                          .toString()
                                          : widget.course.duartion
                                          .durationAr
                                          .toString(),
                                      style: AppTextStyle.xSmallBlack),
                                ],
                              ),
                            ),
                          )
                              : RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                    text: DataStore.instance.lang == "en"
                                        ? widget
                                        .course.duartion.durationEn
                                        .toString()
                                        : widget
                                        .course.duartion.durationAr
                                        .toString(),
                                    style:
                                    AppTextStyle.smallBlueNormalBold),
                                const WidgetSpan(
                                  child: Padding(
                                    padding: EdgeInsetsDirectional.only(
                                        start: 2.0),
                                    child: Icon(Icons.access_time,
                                        color: Color(0xFF9094A3),
                                        size: 14),
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsetsDirectional.only(start: 2.0),
                      child: Text(
                          DataStore.instance.lang == "en"
                              ? widget.course.shortname
                              : widget.course.shortnameEn,
                          style: AppTextStyle.smallBlackBold,
                          maxLines: 2),
                    ),
                    Padding(
                      padding: const EdgeInsetsDirectional.only(start: 2.0),
                      child: Text(
                          DataStore.instance.lang == "en"
                              ? widget.course.description
                              : widget.course.descriptionAr,
                          style: AppTextStyle.xSmallBlack,
                          maxLines: 2),
                    ),
                    Center(
                      child: Row(
                        children: [
                          Padding(
                            padding: EdgeInsetsDirectional.only(
                                top: 1.h, bottom: 1.5.h),
                            child: Container(
                              width: 30,
                              height: 30,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(
                                  color: widget.color ?? Colors.transparent,
                                  // Border color
                                  width: 1.0, // Border width
                                ),
                              ),
                              child: CircleAvatar(
                                backgroundColor: Colors.white,
                                radius: 30,
                                backgroundImage: NetworkImage(
                                  widget.course.authors[0]
                                      .profileImage, // Replace with your image URL
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            width: 2,
                          ),
                          SizedBox(
                            width: 60,
                            child: Text(
                              widget.course.authors[0].firstName,
                              style: TextStyle(
                                  fontFamily: "Cairo",
                                  color: widget.color ?? Colors.black),
                              maxLines: 1,
                            ),
                          ),
                          Spacer(),
                          DataStore.instance.lang == "en"
                              ? Text(
                            "${widget.course.price} SYP",
                            style: const TextStyle(
                                color: Color(0xFF08B055),
                                fontSize: AppFontSize.SMALL,
                                fontWeight: FontWeight.bold,
                                fontFamily: FontFamily.cairo),
                          )
                              : Text(
                            "ل.س${widget.course.price}",
                            style: const TextStyle(
                                color: Color(0xFF08B055),
                                fontSize: AppFontSize.SMALL,
                                fontWeight: FontWeight.bold,
                                fontFamily: FontFamily.cairo),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildAvatar(BuildContext context, List<Authors> authors) {
//     String name=authors.first.firstName;
//     return         Container(
// height: 20,
//       child: CircleAvatar(
//         backgroundColor: AppColors.deepBlue.withOpacity(0.2),
//         child: Text(
//           name[0],
//           style: TextStyle(
//             color: AppColors.deepBlue.withOpacity(0.4),
//           ),
//         ),
//       ),
//     );
  }
}
