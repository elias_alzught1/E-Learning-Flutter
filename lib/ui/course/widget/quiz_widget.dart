import 'package:e_learning/localization/app_localization.dart';
import 'package:e_learning/models/response/quizs_response.dart';
import 'package:e_learning/themes/app_text_style.dart';
import 'package:e_learning/ui/course/widget/question_widget.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';

import '../../../controller/data_store.dart';
import '../../../themes/app_colors.dart';
import '../../../themes/elearning_icons.dart';

class QuizWidget extends StatefulWidget {
  final List<QuizQuestions> data;

  const QuizWidget({Key key, this.data}) : super(key: key);

  @override
  State<QuizWidget> createState() => _QuizWidgetState();
}

class _QuizWidgetState extends State<QuizWidget> {
  int indexItem = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Row(children: [
            Padding(
              padding: EdgeInsetsDirectional.only(
                  start: 2.w, end: 2.w, bottom: 1.h, top: 1.h),
              child: Text(
                DataStore.instance.lang == "en"
                    ? widget.data[0].title
                    : widget.data[0].titleAr,
                style: AppTextStyle.mediumBlack,
              ),
            ),
            const Spacer(),
            Padding(
              padding: EdgeInsetsDirectional.only(
                  start: 2.w, end: 2.w, bottom: 1.h, top: 1.h),
              child: Text(
                widget.data[0].questionCount.toString(),
                style: AppTextStyle.mediumBlack,
              ),
            ),
            Padding(
              padding: EdgeInsetsDirectional.only(
                end: 1.w,
              ),
              child: const Icon(
                Elearning.live_help,
                color: AppColors.green300,
              ),
            )
          ]),
          Padding(
            padding: EdgeInsetsDirectional.only(
                start: 2.w, end: 2.w, bottom: 1.h, top: 1.h),
            child: Text(
              DataStore.instance.lang == "en"
                  ? widget.data[0].descriptionEn.toString()
                  : widget.data[0].descriptionAr,
              style: AppTextStyle.mediumGray,
            ),
          ),
          QuestionWidget(
            form: widget.data[0].questions,
            id: widget.data[0].id,
          ),
        ],
      ),
    );
  }
}
