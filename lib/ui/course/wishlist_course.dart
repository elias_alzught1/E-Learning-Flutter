import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:e_learning/bloc/course_bloc/course_bloc.dart';
import 'package:e_learning/localization/app_localization.dart';
import 'package:e_learning/themes/app_text_style.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';

import '../../bloc/application_bloc/application_cubit.dart';
import '../../bloc/profile_bloc/profile_bloc.dart';
import '../../controller/data_store.dart';
import '../../log_icons.dart';
import '../../models/common/language.dart';
import '../../themes/app_colors.dart';
import '../../themes/elearning_icons.dart';
import '../home/widget/course_enrolled.dart';
import '../widgets/appbar/abbBarAPP.dart';
import '../widgets/loader/inkdrop_loader.dart';
import '../widgets/loader/staggered_dots_wave.dart';

class WishListCourse extends StatefulWidget {
  const WishListCourse({Key key}) : super(key: key);

  @override
  State<WishListCourse> createState() => _WishListCourseState();
}

class _WishListCourseState extends State<WishListCourse> {
  CourseBloc _bloc;
 int page=1;
  @override
  void initState() {
    _bloc = BlocProvider.of<CourseBloc>(context);
    _bloc.add(GetWishList());
    // TODO: implement initState
    super.initState();
  }

  Language local;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //
      appBar: AppBarApp(),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
            SizedBox(
              height: 10,
            ),
            Container(
              color: AppColors.textFieldGray,
              child: Padding(
                padding: EdgeInsetsDirectional.only(
                    start: 4.w, end: 4.w, bottom: 4.w, top: 4.w),
                child: Center(
                    child: Text(
                  AppLocalization.of(context).trans("Wish_List_Title"),
                  style: AppTextStyle.mediumDeepGray,
                )),
              ),
            ),
            _listEnrolledCourse(context)
          ]),
        ),
      ),
    );
  }

  Widget _listEnrolledCourse(BuildContext context) {
    return BlocBuilder<CourseBloc, CourseState>(builder: (context, state) {
      if (state is GetWishListAwaitState) {
        return StaggeredDotsWave();
      }
      if (state is GetWishListErrorState) {
        return Center(
            child: SizedBox(

          width: 200,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 10.h,),
              Text(
                state.message,
                style: TextStyle(
                    fontSize: 12.sp,
                    fontWeight: FontWeight.w500,
                    color: Theme.of(context).textTheme.bodyText2.color),
              ),
              const SizedBox(
                height: 10,
              ),
              IconButton(
                  onPressed: () {
                    BlocProvider.of<CourseBloc>(context).add(GetCourses());
                  },
                  icon: Icon(
                    Log.retweet,
                    size: 18.sp,
                    color: Theme.of(context).textTheme.bodyText2.color,
                  ))
            ],
          ),
        ));
      }

      return BlocProvider.of<CourseBloc>(context).wishList.length == 0
          ? Container(
        height: 70.h,
            child: Center(
              child: Text(
                  "Empty Data",
                  style: AppTextStyle.smallGreenBold,
                ),
            ),
          )
          : Padding(
              padding:
                  EdgeInsetsDirectional.only(start: 2.w, top: 2.w, end: 5.w),
              child: Column(
                children: [
                  SizedBox(
                    child: ListView.separated(
                        shrinkWrap: true,
                        // controller: _scrollCourseController,
                        itemBuilder: (context, index) {
                          return CourseEnrolled(
                            course: context.read<CourseBloc>().wishList[index],
                          );
                        },
                        separatorBuilder: (context, index) {
                          return Divider(
                            color: Colors.grey[400],
                          );
                        },
                        itemCount: BlocProvider.of<CourseBloc>(context)
                            .wishList
                            .length),
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Row(
                    children: [
                      Spacer(),
                      Container(
                        child: Container(
                          width: 40,
                          height: 40,
                          alignment: Alignment.center,
                          child: Ink(
                            decoration: ShapeDecoration(
                              color: Color(0xFF0857DE),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                            ),
                            child: IconButton(
                              icon: Icon(
                                Icons.arrow_back_ios_rounded,
                                color: Colors.white,
                              ),
                              iconSize: 20,
                              onPressed: () {},
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 3.w,
                      ),
                      Container(
                        child: Container(
                          width: 40,
                          height: 40,
                          alignment: Alignment.center,
                          child: Ink(
                            decoration: ShapeDecoration(
                              color: Color(0xFF0857DE),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                            ),
                            child: IconButton(
                              icon: Icon(
                                Icons.arrow_forward_ios_rounded,
                                color: Colors.white,
                              ),
                              iconSize: 20,
                              onPressed: () {},
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            );
    });
  }
}
