import 'dart:developer';
import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_controller.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:e_learning/bloc/course_bloc/course_bloc.dart';
import 'package:e_learning/bloc/course_details/course_details_bloc.dart';
import 'package:e_learning/localization/app_localization.dart';
import 'package:e_learning/models/common/course_model.dart';
import 'package:e_learning/themes/app_text_style.dart';
import 'package:e_learning/ui/course/widget/authoe_card.dart';
import 'package:e_learning/ui/course/widget/course_card.dart';
import 'package:e_learning/ui/course/widget/lessons_card.dart';
import 'package:e_learning/ui/course/widget/unordered_listItem.dart';
import 'package:e_learning/ui/widgets/empty_content.dart';
import 'package:e_learning/utils/Utils.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/svg.dart';
import 'package:social_share/social_share.dart';

import '../../controller/data_store.dart';
import '../../helpers/helpers.dart';
import '../../models/response/lessons_response.dart';

import '../../themes/app_colors.dart';
import '../../themes/app_font_size.dart';
import '../../themes/app_icon.dart';
import '../../themes/elearning_icons.dart';
import '../../themes/font_family.dart';
import '../teacher/widget/dialog_show.dart';
import '../teacher/widget/text_input.dart';
import '../widgets/appbar/abbBarAPP.dart';

import '../widgets/common/Flush_bar.dart';
import '../widgets/widget.dart';

class CourseDetails extends StatefulWidget {
  final Courses course;
  final bool myCourse;

  const CourseDetails({Key key, this.course, this.myCourse = true})
      : super(key: key);

  @override
  State<CourseDetails> createState() => _CourseDetailsState();
}

class _CourseDetailsState extends State<CourseDetails>
    with SingleTickerProviderStateMixin {
  CourseBloc _bloc;
  List<Lessons> lessonList = [];
  CarouselController carouselController = CarouselController();
  List<Courses> relatedCourse = [];
  AnimationController _controller;
  Animation<Offset> _animation;
  Animation<Offset>  _animation2;
  @override
  void initState() {
    relatedCourse = widget.course.relatedCourses;
    _bloc = BlocProvider.of<CourseBloc>(context);
    _bloc.add(GetLessonsCourse());
    double temp = double.parse(widget.course.rating);
    widget.course.rating = temp.floor().toString();
    // TODO: implement initState
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 1),
    );

    _animation = Tween<Offset>(
      begin: Offset(-1.0, 0.0), // Start off-screen to the left
      end: Offset.zero, // End at the container's position
    ).animate(_controller);
    _animation2 = Tween<Offset>(
      begin: Offset(1.0, 0.0), // Start off-screen to the left
      end: Offset.zero, // End at the container's position
    ).animate(_controller);
    _controller.forward();
    print(widget.course.level);
    // relatedCourse=widget.course.r
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String lang = DataStore.instance.lang;
    return Scaffold(
        appBar: const AppBarApp(),
        body: Container(
          color: Theme.of(context).scaffoldBackgroundColor,
          child: Column(
            children: [
              Expanded(
                child: CustomScrollView(
                  slivers: [
                    SliverToBoxAdapter(
                      child: Stack(
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: CachedNetworkImageProvider(
                                    widget.course.image,
                                    errorListener: () {
                                      const Text(
                                        'Loading',
                                        style: TextStyle(
                                          fontSize: 20,
                                        ),
                                      );
                                    },
                                  )),
                            ),
                            child: Stack(
                              children: [
                                Positioned.fill(
                                  child: Container(
                                    width: 100.w,
                                    color: Colors.black.withOpacity(
                                        0.8), // Adjust opacity as needed
                                  ),
                                ),
                                Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    children: [
                                      Container(
                                        margin: EdgeInsetsDirectional.only(
                                            start: 2.w, top: 4.w),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Flexible(
                                              child: SlideTransition(
                                                position: _animation,
                                                child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  child: CachedNetworkImage(
                                                    width: 20.h,
                                                    height: 20.h,
                                                    imageUrl:
                                                        widget.course.image,
                                                    fit: BoxFit.fill,
                                                    placeholder:
                                                        (context, url) =>
                                                            getShimmer(),
                                                    errorWidget:
                                                        (context, url, error) =>
                                                            const Center(
                                                      child: Icon(Icons.error),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 2.w,
                                            ),
                                            Flexible(
                                                child: SlideTransition(
                                                  position: _animation2,
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    lang == "en"
                                                        ? widget.course.nameEn
                                                        : widget.course.nameAr,
                                                    style: AppTextStyle
                                                        .largeWhiteBold,
                                                  ),
                                                  Text(
                                                    lang == "en"
                                                        ? widget.course
                                                            .descriptionEn
                                                        : widget.course
                                                            .descriptionAr,
                                                    style: AppTextStyle
                                                        .mediumWhiteBold,
                                                  ),
                                                ],
                                              ),
                                            )),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 2.h,
                                      ),
                                      SizedBox(
                                        height: 1.h,
                                      ),
                                      Row(
                                        children: [
                                          Padding(
                                            padding: EdgeInsetsDirectional.only(
                                                start: 3.w),
                                            child: Text(
                                              AppLocalization.of(context)
                                                  .trans("Created_By"),
                                              style: AppTextStyle.mediumYellow,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Container(
                                        margin: EdgeInsetsDirectional.only(
                                            start: 2.w, end: 2.w),
                                        height: 20.sp,
                                        child: ListView.builder(
                                          scrollDirection: Axis.horizontal,
                                          itemCount:
                                              widget.course.authors.length,
                                          itemBuilder: (context, i) {
                                            return Container(
                                              alignment: Alignment.center,
                                              margin: const EdgeInsets.only(
                                                  left: 2, right: 2),
                                              child: Text(
                                                "  ${widget.course.authors[i].firstName} ${widget.course.authors[i].lastName}  ",
                                                style: AppTextStyle
                                                    .largeYellowAccent,
                                              ),
                                            );
                                          },
                                        ),
                                      ),
                                      SizedBox(
                                        height: 2.h,
                                      ),
                                      lang == "en"
                                          ? RichText(
                                              text: TextSpan(
                                                children: [
                                                  WidgetSpan(
                                                    child: Padding(
                                                      padding:
                                                          EdgeInsetsDirectional
                                                              .only(
                                                                  start: 3.w,
                                                                  end: 1.w),
                                                      child: Icon(
                                                        Icons.language,
                                                        size: 20,
                                                        color: AppColors.red,
                                                      ),
                                                    ),
                                                  ),
                                                  TextSpan(
                                                      text: widget
                                                          .course.language,
                                                      style: AppTextStyle
                                                          .largeWhite),
                                                ],
                                              ),
                                            )
                                          : RichText(
                                              text: TextSpan(
                                                children: [
                                                  TextSpan(
                                                      text: widget
                                                          .course.language,
                                                      style: AppTextStyle
                                                          .largeGreen),
                                                  WidgetSpan(
                                                    child: Padding(
                                                      padding:
                                                          EdgeInsetsDirectional
                                                              .only(
                                                                  start: 3.w,
                                                                  end: 1.w),
                                                      child: const Icon(
                                                        Icons.language,
                                                        size: 20,
                                                        color: AppColors.red,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                      SizedBox(
                                        height: 2.h,
                                      ),
                                      Container(
                                        width: 100.w,
                                        child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              SizedBox(
                                                height: 4.h,
                                              ),
                                              Padding(
                                                padding:
                                                    EdgeInsetsDirectional.only(
                                                        start: 4.w),
                                                child: Text(
                                                  widget.course.rating,
                                                  style:
                                                      AppTextStyle.largeWhite,
                                                ),
                                              ),
                                              SizedBox(
                                                height: 2.h,
                                              ),
                                              Padding(
                                                padding:
                                                    EdgeInsetsDirectional.only(
                                                        start: 2.w),
                                                child: Center(
                                                  child: Row(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      children: List.generate(
                                                          5,
                                                          (index) => buildStar(
                                                              context,
                                                              index,
                                                              double.parse(
                                                                  widget.course
                                                                      .rating),
                                                              size: 21))),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 8,
                                              ),
                                            ]),
                                      ),
                                    ]),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SliverToBoxAdapter(
                        child: SizedBox(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 25),
                        child: AnimatedContainer(
                          duration: Duration(seconds:10), // Animation duration
                          curve: Curves.easeInOut,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: 4.h,
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Icon(
                                    Icons.access_time,
                                    color: AppColors.red,
                                    size: 17.sp,
                                  ),
                                  SizedBox(width: 2.w),
                                  Text(
                                    AppLocalization.of(context)
                                        .trans("Course_Duration"),
                                    style: const TextStyle(
                                        color: AppColors.deepGray,
                                        fontSize: AppFontSize.LARGE,
                                        fontFamily: FontFamily.cairo,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(width: 4.w),
                                  Spacer(),
                                  Text(
                                    widget.course.duartion == null
                                        ? "1-4 Weeks"
                                        : lang == "en"
                                            ? widget.course.duartion.durationEn
                                            : widget.course.duartion.durationAr,
                                    style: const TextStyle(
                                        color: AppColors.lightPurple,
                                        fontSize: AppFontSize.LARGE,
                                        fontFamily: FontFamily.cairo),
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 1.h,
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Icon(
                                    Icons.leaderboard_sharp,
                                    color: AppColors.red,
                                    size: 17.sp,
                                  ),
                                  SizedBox(width: 2.w),
                                  Text(
                                    AppLocalization.of(context)
                                        .trans("Course_Level"),
                                    style: const TextStyle(
                                        color: AppColors.deepGray,
                                        fontSize: AppFontSize.LARGE,
                                        fontFamily: FontFamily.cairo,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(width: 4.w),
                                  Spacer(),
                                  Text(
                                    widget.course.level == null
                                        ? lang == "en"
                                            ? "Beginner"
                                            : "مبتدئ"
                                        : lang == "en"
                                            ? widget.course.level.levelEn
                                            : widget.course.level.levelAr,
                                    style: const TextStyle(
                                        color: AppColors.lightPurple,
                                        fontSize: AppFontSize.LARGE,
                                        fontFamily: FontFamily.cairo),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 1.h,
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Icon(
                                    Icons.supervised_user_circle_sharp,
                                    color: AppColors.red,
                                    size: 17.sp,
                                  ),
                                  SizedBox(width: 2.w),
                                  Text(
                                    AppLocalization.of(context)
                                        .trans("Students_Enrolled"),
                                    style: const TextStyle(
                                        color: AppColors.deepGray,
                                        fontSize: AppFontSize.LARGE,
                                        fontFamily: FontFamily.cairo,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(width: 4.w),
                                  const Spacer(),
                                  const Text(
                                    "44.99",
                                    style: const TextStyle(
                                        color: AppColors.lightPurple,
                                        fontSize: AppFontSize.LARGE,
                                        fontFamily: FontFamily.cairo),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 1.h,
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Icon(
                                    Icons.language,
                                    color: AppColors.red,
                                    size: 17.sp,
                                  ),
                                  SizedBox(width: 2.w),
                                  Text(
                                    AppLocalization.of(context).trans("language"),
                                    style: const TextStyle(
                                        color: AppColors.deepGray,
                                        fontSize: AppFontSize.LARGE,
                                        fontFamily: FontFamily.cairo,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(width: 4.w),
                                  const Spacer(),
                                  Text(
                                    widget.course.language,
                                    style: const TextStyle(
                                        color: AppColors.lightPurple,
                                        fontSize: AppFontSize.LARGE,
                                        fontFamily: FontFamily.cairo),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    )),

                    SliverToBoxAdapter(
                      child: Container(
                        width: 200.w,
                        child: Column(children: [
                          SizedBox(
                            height: 2.h,
                          ),
                          Container(
                            margin: EdgeInsetsDirectional.only(
                                start: 7.w, end: 7.w),
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 4.w, vertical: 3.w),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Container(
                                    width: 100.w,
                                    height: 7.h,
                                    child: ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(15),
                                          ),
                                          primary: AppColors.normalGreen
                                              .withOpacity(0.5),
                                          textStyle: AppTextStyle.largeWhite),
                                      onPressed: () {},
                                      child: Text(AppLocalization.of(context)
                                          .trans("Buy_now")),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 1.h,
                                  ),
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      BlocConsumer<CourseBloc, CourseState>(
                                        listenWhen: (context, state) {
                                          return state
                                                  is LikeCourseAcceptState ||
                                              state is UnLikeCourseAcceptState;
                                        },
                                        listener: (context, state) {
                                          if (state is LikeCourseAcceptState) {
                                            widget.course.isLike = state
                                                .courseDetailsResponse
                                                .data
                                                .isLike;
                                          } else if (state
                                              is UnLikeCourseAcceptState) {
                                            widget.course.isLike = state
                                                .courseDetailsResponse
                                                .data
                                                .isLike;
                                          }
                                        },
                                        buildWhen: (context, state) {
                                          return state
                                                  is LikeCourseAcceptState ||
                                              state is LikeCourseAwaitState ||
                                              state is LikeCourseErrorState ||
                                              state
                                                  is UnLikeCourseAcceptState ||
                                              state is UnLikeCourseAwaitState ||
                                              state is UnLikeCourseErrorState;
                                        },
                                        builder: (context, state) {
                                          if (state is LikeCourseAwaitState) {
                                            return Container(
                                                margin:
                                                    const EdgeInsetsDirectional
                                                        .only(end: 10),
                                                width: 15,
                                                height: 15,
                                                child:
                                                    const CircularProgressIndicator(
                                                  color: AppColors.deepBlue,
                                                ));
                                          } else if (state
                                              is UnLikeCourseAwaitState) {
                                            return Container(
                                                margin:
                                                    const EdgeInsetsDirectional
                                                        .only(end: 10),
                                                width: 15,
                                                height: 15,
                                                child:
                                                    const CircularProgressIndicator(
                                                  color: AppColors.deepBlue,
                                                ));
                                          } else {
                                            return Container(
                                              width: 25.w,
                                              decoration: BoxDecoration(
                                                  color: widget.course.isLike
                                                      ? AppColors.red
                                                          .withOpacity(0.2)
                                                      : AppColors.white,
                                                  border: Border.all(
                                                      color:
                                                          widget.course.isLike
                                                              ? AppColors.red
                                                              : AppColors.red),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          15)),
                                              child: IconButton(
                                                  onPressed: () {
                                                    widget.course.isLike
                                                        ? _bloc.add(
                                                            UnLikeCourse(widget
                                                                .course.id))
                                                        : _bloc.add(LikeCourse(
                                                            widget.course.id));
                                                  },
                                                  icon: Icon(
                                                      Icons
                                                          .favorite_border_outlined,
                                                      color:
                                                          widget.course.isLike
                                                              ? AppColors.red
                                                              : AppColors.red)),
                                            );
                                          }
                                        },
                                      ),
                                      Spacer(),
                                      BlocListener<CourseBloc, CourseState>(
                                        listenWhen: (_, current) =>
                                            current is ToggleCourseAwaitState ||
                                            current
                                                is ToggleCourseAcceptState ||
                                            current is ToggleCourseErrorState,
                                        listener: (context, state) async {
                                          if (state
                                              is ToggleCourseAcceptState) {
                                            await showCustomFlashbar(context,
                                                state.message, "Success_Add");
                                            setState(() {
                                              widget.course.inWishList =
                                                  !widget.course.inWishList;
                                            });
                                          } else if (state
                                              is ToggleCourseErrorState) {
                                            await showCustomFlashbar(context,
                                                state.message, "Failed_Add");
                                          } else if (state
                                              is ToggleCourseAwaitState) {}
                                        },
                                        /*
                                          "add_to_wishlist": "Add To Wish List",
  "remove_from_wishlist": "Remove From WishList"
                                        */
                                        child: GestureDetector(
                                          onTap: () {
                                            _bloc.add(ToggleWishList(
                                                widget.course.id));
                                          },
                                          child: Container(
                                            width: 50.w,
                                            height: 6.h,
                                            alignment: Alignment.center,
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    color:
                                                        AppColors.lightPurple),
                                                borderRadius:
                                                    BorderRadius.circular(15)),
                                            child: Text(
                                              widget.course.inWishList
                                                  ? AppLocalization.of(context)
                                                      .trans(
                                                          "remove_from_wishlist")
                                                  : AppLocalization.of(context)
                                                      .trans("add_to_wishlist"),
                                              style: AppTextStyle
                                                  .largeNormalLightPurple,
                                            ),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 2.w,
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 2.h,
                                  ),
                                  BlocConsumer<CourseBloc, CourseState>(
                                    listenWhen: (context, state) {
                                      return state is RatingCourseErrorState ||
                                          state is RatingCourseAcceptState;
                                    },
                                    listener: (context, state) async {
                                      if (state is RatingCourseErrorState) {
                                        await showCustomFlashbar(context,
                                            state.message, "Rating Failed");
                                        // Navigate to next screen
                                        Navigator.of(context)
                                            .pushNamed('OrderCompletedScreen');
                                      } else if (state
                                          is RatingCourseAcceptState) {
                                        // Report to analytics
                                        setState(() {
                                          widget.course.ratingDetails = state
                                              .ratingResponse
                                              .data
                                              .ratingDetails;
                                          widget.course.userRating = state
                                              .ratingResponse.data.userRating;
                                          widget.course.rating =
                                              state.ratingResponse.data.rating;
                                        });
                                      }
                                    },
                                    buildWhen: (context, state) {
                                      return state is RatingCourseAwaitState ||
                                          state is RatingCourseErrorState ||
                                          state is RatingCourseAcceptState;
                                    },
                                    builder: (context, state) {
                                      if (state is RatingCourseAwaitState) {
                                        return Row(
                                          children: [
                                            SizedBox(
                                              height: 4.h,
                                            ),
                                            Center(
                                              child: Container(
                                                  child: Text(
                                                'Please Wait..',
                                                style: AppTextStyle
                                                    .mediumGreenBold,
                                              )),
                                            ),
                                          ],
                                        );
                                      } else {
                                        return Container(
                                          child: Padding(
                                            padding: EdgeInsetsDirectional.only(
                                                start: 2.w, end: 2.w),
                                            child: Row(children: [
                                              SizedBox(
                                                height: 3.h,
                                              ),
                                              Text(
                                                AppLocalization.of(context)
                                                    .trans("Your_Rating"),
                                                style: AppTextStyle
                                                    .xLargeBlackBold,
                                              ),
                                              Spacer(),
                                              Center(
                                                child: RatingBar(
                                                  initialRating: double.parse(
                                                      widget.course.userRating
                                                          .toString()),
                                                  minRating: 0,
                                                  maxRating: 5,
                                                  itemPadding: EdgeInsets.only(
                                                      left: 1, right: 1),
                                                  allowHalfRating: false,
                                                  itemSize: 20.sp,
                                                  ratingWidget: RatingWidget(
                                                    full: const Icon(
                                                      Elearning.star_1,
                                                      color: AppColors.deepBlue,
                                                    ),
                                                    empty: const Icon(
                                                      Elearning.star_empty,
                                                      color: Colors.grey,
                                                    ),
                                                  ),
                                                  onRatingUpdate: (rating) {
                                                    // Rating is updated
                                                    _bloc.add(CourseRating(
                                                        widget.course.id,
                                                        rating.toInt()));
                                                    log('rating update to: $rating');
                                                  },
                                                ),
                                              ),
                                              SizedBox(
                                                height: 3.h,
                                              ),
                                            ]),
                                          ),
                                        );
                                      }
                                    },
                                  )
                                ],
                              ),
                            ),
                          )
                        ]),
                      ),
                    ),

                    //Share_this_Course
                    SliverToBoxAdapter(
                      child: Container(
                        child: Padding(
                          padding: EdgeInsetsDirectional.only(start: 2.w),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                SizedBox(
                                  height: 2.h,
                                ),
                                Text(
                                  AppLocalization.of(context)
                                      .trans("Course_included"),
                                  style: AppTextStyle.xLargeBlackBold,
                                ),
                                widget.course.includes != null &&
                                        widget.course.includes.isNotEmpty
                                    ? Container(
                                        margin: EdgeInsetsDirectional.only(
                                            start: 3.w),
                                        child: ListView.builder(
                                            physics:
                                                const NeverScrollableScrollPhysics(),
                                            scrollDirection: Axis.vertical,
                                            shrinkWrap: true,
                                            itemCount:
                                                widget.course.includes.length,
                                            itemBuilder: (BuildContext context,
                                                int index) {
                                              return UnorderedListItem(
                                                lang == "en"
                                                    ? widget
                                                        .course
                                                        .includes[index]
                                                        .includeEn
                                                        .toString()
                                                    : widget
                                                        .course
                                                        .includes[index]
                                                        .includeAr
                                                        .toString(),
                                                Icon(
                                                  Icons.circle,
                                                  color: AppColors.gray,
                                                  size: 15.sp,
                                                ),
                                                appTextStyle: const TextStyle(
                                                    color: Colors.grey,
                                                    fontSize:
                                                        AppFontSize.MEDIUM,
                                                    fontWeight: FontWeight.bold,
                                                    fontFamily:
                                                        FontFamily.cairo),
                                              );
                                            }),
                                      )
                                    : const EmptyContent(),
                                SizedBox(
                                  height: 3.h,
                                ),
                                widget.myCourse
                                    ? Container(
                                        child: InkWell(
                                            onTap: () {
                                              showDialog(
                                                content: Form(
                                                    child: BlocProvider.value(
                                                  value: BlocProvider.of<
                                                          CourseDetailsBloc>(
                                                      context),
                                                  child: BlocBuilder<
                                                      CourseDetailsBloc,
                                                      CourseDetailsState>(
                                                    buildWhen: (previous,
                                                            current) =>
                                                        current is AddIncludeErrorState ||
                                                        current
                                                            is AddIncludeValidateState ||
                                                        current
                                                            is AddIncludeAcceptState ||
                                                        current
                                                            is AddIncludeAwaitState,
                                                    builder: (context, state) {
                                                      return Container(
                                                        height: MediaQuery.of(
                                                                    context)
                                                                .size
                                                                .height /
                                                            3.5,
                                                        child: Column(
                                                          children: [
                                                            buildTextFailedForm(
                                                                BlocProvider.of<
                                                                            CourseDetailsBloc>(
                                                                        context)
                                                                    .arabicAddDescription,
                                                                Icons
                                                                    .description,
                                                                BlocProvider.of<
                                                                            CourseDetailsBloc>(
                                                                        context)
                                                                    .arabicErrorInclude,
                                                                AppLocalization.of(
                                                                        context)
                                                                    .trans(
                                                                        "arabic_description"),
                                                                errorText: BlocProvider.of<
                                                                            CourseDetailsBloc>(
                                                                        context)
                                                                    .arabicErrorInclude),
                                                            buildTextFailedForm(
                                                                BlocProvider.of<
                                                                            CourseDetailsBloc>(
                                                                        context)
                                                                    .englishAddDescription,
                                                                Icons
                                                                    .description,
                                                                BlocProvider.of<
                                                                            CourseDetailsBloc>(
                                                                        context)
                                                                    .englishErrorInclude,
                                                                AppLocalization.of(
                                                                        context)
                                                                    .trans(
                                                                        "english_description"),
                                                                errorText: BlocProvider.of<
                                                                            CourseDetailsBloc>(
                                                                        context)
                                                                    .englishErrorInclude),
                                                          ],
                                                        ),
                                                      );
                                                    },
                                                  ),
                                                )),
                                                fun: () => BlocProvider.of<
                                                            CourseDetailsBloc>(
                                                        context)
                                                    .add(AddIncludesEvent(
                                                        widget.course.id)),
                                                title: AppLocalization.of(
                                                        context)
                                                    .trans(
                                                        "edit_course_information"),
                                              );
                                            },
                                            child: Container()))
                                    : const SizedBox()
                              ]),
                        ),
                      ),
                    ),
                    SliverToBoxAdapter(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              decoration: const BoxDecoration(
                                color: AppColors.white,
                              ),
                              child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Padding(
                                      padding: EdgeInsetsDirectional.only(
                                          start: 2.w, top: 2.h),
                                      child: Text(
                                        AppLocalization.of(context)
                                            .trans("What_Will_Learn"),
                                        style: AppTextStyle.xLargeBlackBold,
                                      ),
                                    ),
                                    widget.course.aims == null ||
                                            widget.course.aims.isEmpty
                                        ? const EmptyContent()
                                        : Container(
                                            margin: EdgeInsetsDirectional.only(
                                                start: 3.w),
                                            child: ListView.builder(
                                                physics:
                                                    const NeverScrollableScrollPhysics(),
                                                scrollDirection: Axis.vertical,
                                                shrinkWrap: true,
                                                itemCount:
                                                    widget.course.aims.length,
                                                itemBuilder:
                                                    (BuildContext context,
                                                        int index) {
                                                  return UnorderedListItem(
                                                    lang == "en"
                                                        ? widget.course
                                                            .aims[index].aimEn
                                                            .toString()
                                                        : widget.course
                                                            .aims[index].aimAr
                                                            .toString(),
                                                    Icon(
                                                      Icons.circle,
                                                      color: AppColors.gray,
                                                      size: 15.sp,
                                                    ),
                                                    padding: 4,
                                                    appTextStyle:
                                                        const TextStyle(
                                                            color:
                                                                AppColors.gray,
                                                            fontSize:
                                                                AppFontSize
                                                                    .MEDIUM,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontFamily:
                                                                FontFamily
                                                                    .cairo),
                                                  );
                                                  Text(
                                                    widget
                                                        .course
                                                        .includes[index]
                                                        .includeEn,
                                                    style: AppTextStyle
                                                        .smallBlackBold,
                                                  );
                                                }),
                                          ),
                                    const SizedBox(
                                      height: 20,
                                    ),
                                    Container(
                                      child: Padding(
                                        padding: EdgeInsetsDirectional.only(
                                            start: 2.w, end: 3.w),
                                        child: Text(
                                          AppLocalization.of(context)
                                              .trans("who_this_course"),
                                          style: const TextStyle(
                                            fontSize: AppFontSize.X_LARGE,
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: FontFamily.cairo,
                                          ),
                                        ),
                                      ),
                                    ),
                                    widget.course.tarqetAudience != null &&
                                            widget.course.tarqetAudience
                                                .isNotEmpty
                                        ? Container(
                                            margin: EdgeInsetsDirectional.only(
                                                start: 3.w),
                                            child: ListView.builder(
                                                physics:
                                                    const NeverScrollableScrollPhysics(),
                                                scrollDirection: Axis.vertical,
                                                shrinkWrap: true,
                                                itemCount: widget.course
                                                    .tarqetAudience.length,
                                                itemBuilder:
                                                    (BuildContext context,
                                                        int index) {
                                                  return UnorderedListItem(
                                                    lang == "en"
                                                        ? widget
                                                            .course
                                                            .tarqetAudience[
                                                                index]
                                                            .targetEn
                                                            .toString()
                                                        : widget
                                                            .course
                                                            .tarqetAudience[
                                                                index]
                                                            .targetAr
                                                            .toString(),
                                                    Icon(
                                                      Icons.circle,
                                                      color: AppColors.gray,
                                                      size: 15.sp,
                                                    ),
                                                    appTextStyle:
                                                        const TextStyle(
                                                            color:
                                                                AppColors.gray,
                                                            fontSize:
                                                                AppFontSize
                                                                    .LARGE,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontFamily:
                                                                FontFamily
                                                                    .cairo),
                                                  );
                                                  Text(
                                                    widget
                                                        .course
                                                        .includes[index]
                                                        .includeEn,
                                                    style: AppTextStyle
                                                        .smallBlackBold,
                                                  );
                                                }),
                                          )
                                        : const EmptyContent(),
                                    SizedBox(
                                      height: 2.h,
                                    ),
                                    Container(
                                        child: Padding(
                                      padding: EdgeInsetsDirectional.only(
                                          start: 3.w, end: 3.w),
                                      child: Text(
                                        AppLocalization.of(context)
                                            .trans("course_requirements"),
                                        style: const TextStyle(
                                          fontSize: AppFontSize.X_LARGE,
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: FontFamily.cairo,
                                        ),
                                      ),
                                    )),
                                    widget.course.prerequisites != null &&
                                            widget
                                                .course.prerequisites.isNotEmpty
                                        ? Container(
                                            margin: EdgeInsetsDirectional.only(
                                                start: 3.w),
                                            child: ListView.builder(
                                                physics:
                                                    const NeverScrollableScrollPhysics(),
                                                scrollDirection: Axis.vertical,
                                                shrinkWrap: true,
                                                itemCount: widget.course
                                                    .prerequisites.length,
                                                itemBuilder:
                                                    (BuildContext context,
                                                        int index) {
                                                  return UnorderedListItem(
                                                    lang == "en"
                                                        ? widget
                                                            .course
                                                            .prerequisites[
                                                                index]
                                                            .prerequisiteEn
                                                            .toString()
                                                        : widget
                                                            .course
                                                            .prerequisites[
                                                                index]
                                                            .prerequisiteAr
                                                            .toString(),
                                                    Icon(
                                                      Icons.circle,
                                                      color: AppColors.gray,
                                                      size: 15.sp,
                                                    ),
                                                    appTextStyle:
                                                        const TextStyle(
                                                            color:
                                                                AppColors.gray,
                                                            fontSize:
                                                                AppFontSize
                                                                    .MEDIUM,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontFamily:
                                                                FontFamily
                                                                    .cairo),
                                                  );
                                                  Text(
                                                    widget
                                                        .course
                                                        .includes[index]
                                                        .includeEn,
                                                    style: AppTextStyle
                                                        .smallBlackBold,
                                                  );
                                                }),
                                          )
                                        : EmptyContent(),
                                    SizedBox(
                                      height: 4.h,
                                    ),
                                    Container(
                                      child: Padding(
                                        padding: EdgeInsetsDirectional.only(
                                            start: 3.w, end: 3.w),
                                        child: Text(
                                          AppLocalization.of(context)
                                              .trans("curriculum"),
                                          style: const TextStyle(
                                            fontSize: AppFontSize.X_LARGE,
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: FontFamily.cairo,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                        margin: EdgeInsetsDirectional.only(
                                            start: 3.w),
                                        height: 30.sp,
                                        width: 100.w,
                                        child: ListView(
                                            scrollDirection: Axis.horizontal,
                                            children: [
                                              Row(
                                                children: [
                                                  Icon(Elearning.folder_open,
                                                      color: AppColors
                                                          .normalGreen),
                                                  Text(
                                                    lang == "en"
                                                        ? " 1 Section"
                                                        : "1 قسم",
                                                    style: TextStyle(
                                                      fontSize:
                                                          AppFontSize.X_LARGE,
                                                      color: AppColors
                                                          .normalGreen
                                                          .withOpacity(0.6),
                                                      fontWeight:
                                                          FontWeight.normal,
                                                      fontFamily:
                                                          FontFamily.cairo,
                                                    ),
                                                  )
                                                ],
                                              ),
                                              SizedBox(
                                                width: 5.w,
                                              ),
                                              Row(
                                                children: [
                                                  const Icon(
                                                      Elearning.play_circled,
                                                      color: AppColors
                                                          .normalGreen),
                                                  Text(
                                                    lang == "en"
                                                        ? " 3 Lectures"
                                                        : "3 محاضرات",
                                                    style: TextStyle(
                                                      fontSize:
                                                          AppFontSize.X_LARGE,
                                                      color: AppColors
                                                          .normalGreen
                                                          .withOpacity(0.6),
                                                      fontWeight:
                                                          FontWeight.normal,
                                                      fontFamily:
                                                          FontFamily.cairo,
                                                    ),
                                                  )
                                                ],
                                              ),
                                              SizedBox(
                                                width: 5.w,
                                              ),
                                              Row(
                                                children: [
                                                  Icon(Elearning.clock,
                                                      color: AppColors
                                                          .normalGreen),
                                                  Text(
                                                    " 19h 32m",
                                                    style: TextStyle(
                                                      fontSize:
                                                          AppFontSize.X_LARGE,
                                                      color: AppColors
                                                          .normalGreen
                                                          .withOpacity(0.6),
                                                      fontWeight:
                                                          FontWeight.normal,
                                                      fontFamily:
                                                          FontFamily.cairo,
                                                    ),
                                                  )
                                                ],
                                              ),
                                              //twitter
                                            ])),
                                    const SizedBox(
                                      height: 20,
                                    ),
                                    BlocConsumer<CourseBloc, CourseState>(
                                      listenWhen: (context, state) {
                                        return state
                                            is GetCourseLessonsAcceptState;
                                      },
                                      listener: (context, state) {
                                        if (state
                                            is GetCourseLessonsAcceptState) {
                                          setState(() {
                                            lessonList = state.lessons;
                                          });
                                        }
                                      },
                                      buildWhen: (context, state) {
                                        return state
                                                is GetCourseLessonsAcceptState ||
                                            state
                                                is GetCourseLessonsErrorState ||
                                            state is GetCourseLessonsAwaitState;
                                      },
                                      builder: (context, state) {
                                        if (state
                                            is GetCourseLessonsAwaitState) {
                                          return const SizedBox();
                                        } else if (state
                                            is GetCourseLessonsErrorState) {
                                          return Center(
                                              child: Text(state.message));
                                        } else if (state
                                            is GetCourseLessonsAcceptState) {
                                          return Container(
                                            decoration: const BoxDecoration(
                                              boxShadow: [],
                                            ),
                                            child: ListView.builder(
                                                shrinkWrap: true,
                                                //    <-- Set this to true
                                                physics: ScrollPhysics(),
                                                itemCount: lessonList.length,
                                                itemBuilder:
                                                    (BuildContext context,
                                                        int index) {
                                                  return Padding(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            horizontal: 2.w),
                                                    child: LessonsCard(
                                                        lesson:
                                                            lessonList[index]),
                                                  );
                                                }),
                                          );
                                        } else {
                                          return Container();
                                        }
                                      },
                                    ),
                                  ]),
                            ),
                          ]),
                    ),

                    SliverToBoxAdapter(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            SizedBox(
                              height: 2.h,
                            ),
                            Row(
                              children: [
                                lang == "en"
                                    ? SvgPicture.asset(
                                        'assets/img/svg/tag.svg',
                                        color: AppColors.yellowAccent,
                                      )
                                    : Transform(
                                        alignment: Alignment.center,
                                        transform: Matrix4.identity()
                                          ..scale(-1.0, 1.0),
                                        // Rotate by 180 degrees (pi radians)
                                        child: SvgPicture.asset(
                                          'assets/img/svg/tag.svg',
                                          color: AppColors.yellowAccent,
                                        ),
                                      ),
                                Text(
                                  widget.course.authors != null
                                      ? "${"${AppLocalization.of(context).trans("Course_instructor")} "}(${widget.course.authors.length})"
                                      : AppLocalization.of(context)
                                          .trans("Course_instructor"),
                                  style: const TextStyle(
                                    fontSize: AppFontSize.X_LARGE,
                                    color: AppColors.black,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: FontFamily.cairo,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 2.h,
                            ),
                            widget.course.authors != null &&
                                    widget.course.authors.isNotEmpty
                                ? ListView.builder(
                                    physics:
                                        const NeverScrollableScrollPhysics(),
                                    scrollDirection: Axis.vertical,
                                    shrinkWrap: true,
                                    itemCount: widget.course.authors.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return AuthorCard(
                                        authors: widget.course.authors[index],
                                      );
                                      Text(
                                        widget.course.includes[index].includeEn,
                                        style: AppTextStyle.smallBlackBold,
                                      );
                                    })
                                : SizedBox(),
                          ]),
                    ),
                    SliverToBoxAdapter(
                      child: Container(
                        child: Padding(
                          padding: EdgeInsetsDirectional.only(start: 2.w),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                SizedBox(
                                  height: 2.h,
                                ),
                                SizedBox(
                                  height: 1.h,
                                ),
                                Container(
                                    height: 30.sp,
                                    width: 100.w,
                                    child: ListView(
                                        scrollDirection: Axis.horizontal,
                                        children: [
                                          Container(
                                            alignment: Alignment.center,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  const BorderRadius.all(
                                                      Radius.circular(8)),
                                              border: Border.all(
                                                color: AppColors.deepBlue
                                                    .withOpacity(0.4),
                                              ),
                                            ),
                                            child: InkWell(
                                              child: Container(
                                                child: Padding(
                                                  padding: EdgeInsetsDirectional
                                                      .only(
                                                          start: 1.w, end: 1.w),
                                                  child: Row(
                                                    children: [
                                                      Icon(
                                                        copy,
                                                        size: 15.sp,
                                                        color: AppColors
                                                            .deepBlue
                                                            .withOpacity(0.4),
                                                      ),
                                                      Text(
                                                        AppLocalization.of(
                                                                context)
                                                            .trans("Copy_link"),
                                                        style: TextStyle(
                                                            color: AppColors
                                                                .deepBlue
                                                                .withOpacity(
                                                                    0.6),
                                                            fontSize:
                                                                AppFontSize
                                                                    .MEDIUM,
                                                            fontFamily:
                                                                FontFamily
                                                                    .Cario_blod),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              onTap: () {
                                                SocialShare.copyToClipboard(
                                                  text: "https://",
                                                ).then((data) async {
                                                  await showCustomFlashbar(
                                                      context,
                                                      "Coppied Success",
                                                      "");
                                                });
                                                // Utils.shareDocumentLink(widget.course.id,name: widget.course.nameEn);
                                              },
                                            ),
                                          ),
                                          SizedBox(
                                            width: 8.w,
                                          ),
                                          InkWell(
                                            child: Icon(
                                              Icons.facebook_outlined,
                                              size: 20.sp,
                                              color: AppColors.deepBlue,
                                            ),
                                            onTap: () {
                                              Utils.shareDocumentLink(
                                                  widget.course.id,
                                                  name: widget.course.nameEn);
                                            },
                                          ),
                                          SizedBox(
                                            width: 8.w,
                                          ),
                                          InkWell(
                                            child: Icon(
                                              Elearning.twitter,
                                              size: 20.sp,
                                              color: AppColors.blueAccent,
                                            ),
                                            onTap: () {
                                              SocialShare.shareTwitter(
                                                "This is Social Share twitter example with link.  ",
                                                hashtags: [
                                                  "SocialSharePlugin",
                                                  "world",
                                                  "foo",
                                                  "bar"
                                                ],
                                                url: "https://google.com/hello",
                                                trailingText: "cool!!",
                                              );
                                            },
                                          ),
                                          SizedBox(
                                            width: 8.w,
                                          ),
                                          InkWell(
                                            child: Icon(
                                              Icons.email_outlined,
                                              size: 22.sp,
                                              color: AppColors.blue,
                                            ),
                                            onTap: () {
                                              Utils.shareDocumentLink(
                                                  widget.course.id,
                                                  name: widget.course.nameEn);
                                            },
                                          ),
                                          SizedBox(
                                            width: 8.w,
                                          ),
                                          InkWell(
                                            child: Icon(
                                              Elearning.whatsapp,
                                              size: 20.sp,
                                              color: Colors.green.shade900,
                                            ),
                                            onTap: () {
                                              SocialShare.shareWhatsapp(
                                                "Hello World \n https://google.com",
                                              );
                                            },
                                          ),
                                          // Icon(Icons.whatsapp,
                                          //     size: 20.sp, color: AppColors.green),
                                          //twitter
                                        ])),
                                SizedBox(
                                  height: 3.h,
                                )
                              ]),
                        ),
                      ),
                    ),
//Your_Rating
                    SliverToBoxAdapter(
                        child: Container(
                      color: Colors.white,
                      child: Column(
                        children: [
                          SizedBox(
                            child: Padding(
                              padding: EdgeInsetsDirectional.only(start: 3.w),
                              child: Row(
                                children: [
                                  Text(
                                    AppLocalization.of(context)
                                        .trans("Related_course"),
                                    style: AppTextStyle.xLargeBlackBold,
                                  ),
                                  Spacer(),
                                  Container(
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 8),
                                      child: Container(
                                        decoration: BoxDecoration(
                                            color: AppColors.deepBlue,
                                            borderRadius:
                                                BorderRadius.circular(8)),
                                        width: 35,
                                        height: 35,
                                        alignment: Alignment.center,
                                        child: Ink(
                                          child: IconButton(
                                            icon: const Icon(
                                              Icons.arrow_back_ios_rounded,
                                              color: Colors.white,
                                            ),
                                            iconSize: 17,
                                            onPressed: () {
                                              carouselController.previousPage();
                                            },
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 4.w,
                                  ),
                                  Container(
                                    child: Padding(
                                      padding: EdgeInsetsDirectional.only(
                                          end: 2.w, top: 8),
                                      child: Container(
                                        decoration: BoxDecoration(
                                            color: AppColors.deepBlue,
                                            borderRadius:
                                                BorderRadius.circular(8)),
                                        width: 35,
                                        height: 35,
                                        alignment: Alignment.center,
                                        child: Ink(
                                          child: IconButton(
                                            icon: const Icon(
                                              Icons.arrow_forward_ios_rounded,
                                              color: Colors.white,
                                            ),
                                            iconSize: 17,
                                            onPressed: () {
                                              carouselController.nextPage();
                                            },
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          relatedCourse.isEmpty
                              ? const Padding(
                                  padding: EdgeInsets.all(8.0),
                                  child: EmptyContent(),
                                )
                              : Container(
                                  margin: EdgeInsetsDirectional.only(
                                      top: 1.h, bottom: 1.h),
                                  child: CarouselSlider(
                                    carouselController: carouselController,
                                    // Give the controller
                                    options: CarouselOptions(
                                      height:
                                          MediaQuery.of(context).size.height /
                                              2.8,
                                      autoPlay: false,
                                    ),
                                    items: relatedCourse.map((e) {
                                      return Container(
                                        child: Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 7),
                                          child: CourseCard(
                                            course: e,
                                          ),
                                        ),
                                      );
                                    }).toList(),
                                  ),
                                )
                        ],
                      ),
                    )),
                    SliverToBoxAdapter(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Container(
                            color: Colors.black,
                            width: 100.w,
                            height: 10.h,
                            child: Padding(
                              padding: EdgeInsetsDirectional.only(
                                  start: 2.w, end: 2.w),
                              child: Row(children: [
                                SvgPicture.asset(
                                  'assets/img/svg/logo_icon.svg',
                                  width: 15.sp,
                                  height: 15.sp,
                                ),
                                SizedBox(
                                  width: 10.sp,
                                ),
                                Text(
                                  "E-Learning Platform",
                                  style: AppTextStyle.mediumWhiteBold,
                                ),
                                Spacer(),
                                Text(
                                  "2023 © E-Learing",
                                  style: AppTextStyle.mediumWhiteBold,
                                )
                              ]),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }

  Widget buildRatingReview(BuildContext context) {}

  Widget chartRow(BuildContext context, String label, int pct, double star) {
    return Row(
      children: [
        SizedBox(
          width: 5.w,
        ),
        Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: List.generate(
                5, (index) => buildStar(context, index, star, size: 18))),
        const SizedBox(width: 5),
        Text(label, style: AppTextStyle.largeBlack),
        Padding(
          padding: const EdgeInsetsDirectional.fromSTEB(8, 0, 8, 0),
          child: Stack(children: [
            Container(
              width: MediaQuery.of(context).size.width * 0.3,
              decoration: BoxDecoration(
                  color: Colors.orangeAccent.withOpacity(0.5),
                  borderRadius: BorderRadius.circular(20)),
              child: Text(''),
            ),
            Container(
              width: pct == null
                  ? MediaQuery.of(context).size.width * (0 / 100) * 0.3
                  : MediaQuery.of(context).size.width *
                      (int.parse(pct.toString()) / 100) *
                      0.15,
              decoration: BoxDecoration(
                  color: Colors.orange,
                  borderRadius: BorderRadius.circular(20)),
              child: Text(''),
            ),
          ]),
        ),
      ],
    );
  }

  Widget showDialog({Widget content, String title, fun, IconData icon}) {
    return myDialog(context, title ?? AppLocalization.of(context).trans(" "),
        content ?? Container(), fun,
        icon: icon);
  }
}
