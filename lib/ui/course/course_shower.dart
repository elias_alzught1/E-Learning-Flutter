import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:chewie/chewie.dart';
import 'package:e_learning/controller/data_store.dart';
import 'package:e_learning/localization/app_localization.dart';

import 'package:e_learning/themes/elearning_icons.dart';
import 'package:e_learning/ui/course/widget/DegreeClass.dart';

import 'package:e_learning/ui/course/widget/quiz_widget.dart';
import 'package:e_learning/ui/course/widget/video_item.dart';
import 'package:e_learning/ui/pdf/pdf_viewer_page.dart';

import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flick_video_player/flick_video_player.dart';
import 'package:flutter/material.dart';
import 'package:e_learning/themes/app_text_style.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';

import 'package:sprintf/sprintf.dart';
import 'package:video_player/video_player.dart';
import '../../bloc/course_bloc/course_bloc.dart';

import '../../helpers/Pdf/pdf_api.dart';
import '../../helpers/helpers.dart';
import '../../helpers/stack_loader_indicator.dart';
import '../../models/response/quiz_result_response.dart';
import '../../themes/app_colors.dart';
import '../widgets/appbar/abbBarAPP.dart';
import '../widgets/appbar/abbBottomBar.dart';

class CourseShower extends StatefulWidget {
  final int videoNumber;
  final int readNumber;
  final int quizNumber;

  const CourseShower(
      {Key key, this.videoNumber = 0, this.readNumber = 0, this.quizNumber = 0})
      : super(key: key);

  @override
  State<CourseShower> createState() => _CourseShowerState();
}

class _CourseShowerState extends State<CourseShower> {
  int _currentPage = 0;

  FlickManager flickManager;

  List<String> pages = [];
  var _playing = false;
  ChewieController _chewieController;

  bool get _isPlaying {
    return _playing;
  }

  set _isPlaying(bool value) {
    _playing = value;
    _timerVisibleControl?.cancel();
    if (value) {
      _timerVisibleControl = Timer(Duration(seconds: 2), () {
        if (_disposed) return;
        setState(() {
          _controlAlpha = 0.0;
        });
      });
    } else {
      _timerVisibleControl = Timer(Duration(milliseconds: 200), () {
        if (_disposed) return;
        setState(() {
          _controlAlpha = 1.0;
        });
      });
    }
  }

  final StackLoaderIndicator _loader = StackLoaderIndicator();

  @override
  void initState() {
    BlocProvider.of<CourseBloc>(context).add(FetchShowerContent());

    BlocProvider.of<CourseBloc>(context).add(FetchShowerQuiz());

    // TODO: implement initState
    super.initState();
  }

  VideoPlayerController _controller;

  bool _isVisibleVedios = false;
  bool _isVisibleReadings = false;
  bool _isVisibleQuiz = false;
  int indexShowVedio = 0;

  var _updateProgressInterval = 0.0;
  Duration _duration;
  Duration _position;
  var _playingIndex = -1;
  var _disposed = false;
  var _isFullScreen = false;
  var _isEndOfClip = false;
  var _progress = 0.0;
  var _showingDialog = false;
  Timer _timerVisibleControl;
  double _controlAlpha = 1.0;

  void _initializeAndPlay(List<String> _clips, int index) async {}

  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: AppBottomBar(),
      appBar: AppBarApp(),
      body: BlocConsumer<CourseBloc, CourseState>(
        listener: (context, state) {
          if (state is GetShowerContentAcceptState) {
            setState(() {});
            for (int i = 0;
                i < BlocProvider.of<CourseBloc>(context).videoList.length;
                i++) {
              pages.add(
                  BlocProvider.of<CourseBloc>(context).videoList[i].mediaUrl);
            }
          }
          if (state is TestAcceptState) {
            print(state.result.message);
          }
          // TODO: implement listener
        },
        builder: (context, state) {
          return BlocConsumer<CourseBloc, CourseState>(
            listenWhen: (_, current) =>
                current is TestAcceptState ||
                current is TestAwaitState ||
                current is TestErrorState,
            listener: (context, state) {
              if (state is TestAcceptState) {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      content: Container(
                        width: MediaQuery.of(context).size.width,
                        child: SingleChildScrollView(
                          child: Container(

                            child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [_buildDegree(state.result)]),
                          ),
                        ),
                      ),
                      actions: [
                        TextButton(
                          onPressed: () {
                            // Close the dialog when this button is pressed
                            Navigator.of(context).pop();
                          },
                          child: Text('Close'),
                        ),
                      ],
                    );
                  },
                );
              }
            },
            buildWhen: (_, current) =>
                current is GetShowerContentAcceptState ||
                current is GetShowerContentErrorState ||
                current is GetShowerContentAwaitState,
            builder: (context, state) {
              if (state is GetShowerContentAwaitState) {
                return Center(
                    child: Container(
                        height: 2.h,
                        width: 2.h,
                        child: CircularProgressIndicator()));
              }
              if (state is GetShowerContentErrorState) {
                return Container(
                  child: Text(state.message),
                );
              }
              if (state is GetShowerContentAcceptState) {
                return Container(
                    color: Theme.of(context).scaffoldBackgroundColor,
                    child: CustomScrollView(
                      slivers: [
                        SliverToBoxAdapter(
                          child: Container(
                            color: const Color(0xFFF0F1F3),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    height: 5.h,
                                  ),
                                  Padding(
                                    padding:
                                        EdgeInsetsDirectional.only(start: 6.w),
                                    child: Text(
                                      AppLocalization.of(context)
                                          .trans("Lesson_Content"),
                                      style: AppTextStyle.largeRedBold,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 1.h,
                                  ),
                                  const Divider(color: AppColors.gray),
                                  InkWell(
                                    child: Padding(
                                      padding: EdgeInsetsDirectional.only(
                                          start: 6.w),
                                      child: Row(
                                        children: [
                                          Icon(
                                            Icons.view_carousel_sharp,
                                            color: AppColors.black,
                                            size: 18.sp,
                                          ),
                                          SizedBox(
                                            width: 1.5.w,
                                          ),
                                          Text(
                                            AppLocalization.of(context)
                                                .trans('Overview'),
                                            style: AppTextStyle.largeBlackBold,
                                          ),
                                        ],
                                      ),
                                    ),
                                    onTap: () {
                                      setState(() {
                                        _currentPage = 0;
                                      });
                                    },
                                  ),
                                  SizedBox(
                                    height: 1.h,
                                  ),
                                  const Divider(color: AppColors.gray),
                                  Padding(
                                    padding:
                                        EdgeInsetsDirectional.only(start: 6.w),
                                    child: InkWell(
                                      child: Row(
                                        children: [
                                          Icon(
                                            Elearning.play_circle_outline,
                                            color: AppColors.black,
                                            size: 18.sp,
                                          ),
                                          SizedBox(
                                            width: 1.5.w,
                                          ),
                                          Text(
                                            sprintf(
                                              AppLocalization.of(context)
                                                  .trans('videos_number'),
                                              [
                                                BlocProvider.of<CourseBloc>(
                                                        context)
                                                    .videoList
                                                    .length
                                                    .toString()
                                              ],
                                            ),
                                            style: AppTextStyle.largeBlackBold,
                                          ),
                                          Spacer(),
                                          Padding(
                                            padding: EdgeInsetsDirectional.only(
                                                end: 3.w, top: 4.sp),
                                            child: Align(
                                              alignment: Alignment.center,
                                              child: Icon(
                                                _isVisibleVedios == true
                                                    ? Icons.keyboard_arrow_up
                                                    : Icons.keyboard_arrow_down,
                                                color: AppColors.black,
                                                size: 18.sp,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      onTap: () {
                                        setState(() {
                                          _isVisibleVedios = !_isVisibleVedios;
                                        });
                                      },
                                    ),
                                  ),
                                  Visibility(
                                    visible: _isVisibleVedios,
                                    child: ListView.builder(
                                        padding: EdgeInsetsDirectional.only(
                                            top: 3.h, start: 8.w, end: 8.w),
                                        physics:
                                            const NeverScrollableScrollPhysics(),
                                        scrollDirection: Axis.vertical,
                                        shrinkWrap: true,
                                        itemCount:
                                            BlocProvider.of<CourseBloc>(context)
                                                .videoList
                                                .length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return InkWell(
                                            child: Padding(
                                              padding:
                                                  EdgeInsets.only(bottom: 4.sp),
                                              child: Container(
                                                child: Column(
                                                  children: [
                                                    Row(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: <Widget>[
                                                        Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  top: 7.sp),
                                                          child: Icon(
                                                            Icons.circle,
                                                            color: AppColors
                                                                .deepGray,
                                                            size: 10.sp,
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          width: 1.3.w,
                                                        ),
                                                        Expanded(
                                                          child: Text(
                                                            DataStore.instance
                                                                        .lang ==
                                                                    "en"
                                                                ? BlocProvider.of<
                                                                            CourseBloc>(
                                                                        context)
                                                                    .videoList[
                                                                        index]
                                                                    .title
                                                                    .toString()
                                                                : BlocProvider.of<
                                                                            CourseBloc>(
                                                                        context)
                                                                    .videoList[
                                                                        index]
                                                                    .titleAr
                                                                    .toString(),
                                                            style: AppTextStyle
                                                                .largeDeepGray,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    VideoListItem(
                                                        videoUrl: BlocProvider
                                                                .of<CourseBloc>(
                                                                    context)
                                                            .videoList[index]
                                                            .mediaUrl)
                                                  ],
                                                ),
                                              ),
                                            ),
                                            onTap: () {
                                              // setState(() {
                                              //   indexShowVedio = index;
                                              //   _currentPage = 1;
                                              //   _initializeAndPlay(
                                              //       pages, index);
                                              //
                                              //   print("xxxs");
                                              // });
                                            },
                                          );
                                        }),
                                  ),
                                  SizedBox(
                                    height: 1.h,
                                  ),
                                  const Divider(color: AppColors.gray),
                                  Padding(
                                    padding:
                                        EdgeInsetsDirectional.only(start: 6.w),
                                    child: InkWell(
                                      child: Row(
                                        children: [
                                          Row(
                                            children: [
                                              Icon(
                                                Elearning.book,
                                                color: AppColors.black,
                                                size: 18.sp,
                                              ),
                                              SizedBox(
                                                width: 1.5.w,
                                              ),
                                              Text(
                                                sprintf(
                                                  AppLocalization.of(context)
                                                      .trans('readings_number'),
                                                  [
                                                    BlocProvider.of<CourseBloc>(
                                                            context)
                                                        .readList
                                                        .length
                                                        .toString()
                                                  ],
                                                ),
                                                style:
                                                    AppTextStyle.largeBlackBold,
                                              ),
                                            ],
                                          ),
                                          Spacer(),
                                          Padding(
                                            padding: EdgeInsetsDirectional.only(
                                                end: 3.w, top: 4.sp),
                                            child: Align(
                                              alignment: Alignment.center,
                                              child: Icon(
                                                _isVisibleReadings == true
                                                    ? Icons.keyboard_arrow_up
                                                    : Icons.keyboard_arrow_down,
                                                color: AppColors.black,
                                                size: 18.sp,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      onTap: () {
                                        setState(() {
                                          _isVisibleReadings =
                                              !_isVisibleReadings;
                                        });
                                      },
                                    ),
                                  ),
                                  Visibility(
                                    visible: _isVisibleReadings,
                                    child: ListView.builder(
                                        padding: EdgeInsetsDirectional.only(
                                            top: 3.h, start: 8.w),
                                        physics:
                                            const NeverScrollableScrollPhysics(),
                                        scrollDirection: Axis.vertical,
                                        shrinkWrap: true,
                                        itemCount:
                                            BlocProvider.of<CourseBloc>(context)
                                                .readList
                                                .length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return InkWell(
                                            child: Padding(
                                              padding:
                                                  EdgeInsets.only(bottom: 4.sp),
                                              child: Container(
                                                child: Row(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Padding(
                                                      padding: EdgeInsets.only(
                                                          top: 7.sp),
                                                      child: Icon(
                                                        Icons.circle,
                                                        color:
                                                            AppColors.deepGray,
                                                        size: 10.sp,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      width: 1.3.w,
                                                    ),
                                                    Expanded(
                                                      child: Text(
                                                        DataStore.instance
                                                                    .lang ==
                                                                "en"
                                                            ? BlocProvider.of<
                                                                        CourseBloc>(
                                                                    context)
                                                                .readList[index]
                                                                .title
                                                                .toString()
                                                            : BlocProvider.of<
                                                                        CourseBloc>(
                                                                    context)
                                                                .readList[index]
                                                                .titleAr
                                                                .toString(),
                                                        style: AppTextStyle
                                                            .largeDeepGray,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            onTap: () async {
                                              print(BlocProvider.of<CourseBloc>(
                                                      context)
                                                  .readList[index]
                                                  .mediaUrl
                                                  .toString());
                                              _loader.show(context);
                                              final file = await PDFApi.loadNetwork(
                                                  "http://185.194.124.88:8889/Media/125_1688650212bed1cc93-cb59-4e91-8453-4652b313c10a.pdf");
                                              _loader.dismiss();
                                              openPDF(context, file);
                                            },
                                          );
                                        }),
                                  ),
                                  SizedBox(
                                    height: 1.h,
                                  ),
                                  const Divider(color: AppColors.gray),
                                  Padding(
                                    padding:
                                        EdgeInsetsDirectional.only(start: 6.w),
                                    child: InkWell(
                                      child: Row(
                                        children: [
                                          Icon(
                                            Elearning.live_help,
                                            color: AppColors.black,
                                            size: 18.sp,
                                          ),
                                          SizedBox(
                                            width: 1.5.w,
                                          ),
                                          Text(
                                            sprintf(
                                              AppLocalization.of(context)
                                                  .trans('quiz_number'),
                                              [
                                                BlocProvider.of<CourseBloc>(
                                                        context)
                                                    .result
                                                    .length
                                                    .toString()
                                              ],
                                            ),
                                            style: AppTextStyle.largeBlackBold,
                                          ),
                                          Spacer(),
                                          Padding(
                                            padding: EdgeInsetsDirectional.only(
                                                end: 3.w, top: 4.sp),
                                            child: Align(
                                              alignment: Alignment.center,
                                              child: Icon(
                                                _isVisibleQuiz == true
                                                    ? Icons.keyboard_arrow_up
                                                    : Icons.keyboard_arrow_down,
                                                color: AppColors.deepBlue,
                                                size: 18.sp,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      onTap: () {
                                        setState(() {
                                          _isVisibleQuiz = !_isVisibleQuiz;
                                        });
                                      },
                                    ),
                                  ),
                                  Visibility(
                                    visible: _isVisibleQuiz,
                                    child: ListView.builder(
                                        padding: EdgeInsetsDirectional.only(
                                            top: 3.h, start: 8.w),
                                        physics:
                                            const NeverScrollableScrollPhysics(),
                                        scrollDirection: Axis.vertical,
                                        shrinkWrap: true,
                                        itemCount:
                                            BlocProvider.of<CourseBloc>(context)
                                                .result
                                                .length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return InkWell(
                                            child: Padding(
                                              padding:
                                                  EdgeInsets.only(bottom: 4.sp),
                                              child: Container(
                                                child: Row(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Padding(
                                                      padding: EdgeInsets.only(
                                                          top: 7.sp),
                                                      child: Icon(
                                                        Icons.circle,
                                                        color:
                                                            AppColors.deepGray,
                                                        size: 10.sp,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      width: 1.3.w,
                                                    ),
                                                    Expanded(
                                                      child: Text(
                                                        DataStore.instance
                                                                    .lang ==
                                                                "en"
                                                            ? BlocProvider.of<
                                                                        CourseBloc>(
                                                                    context)
                                                                .result[index]
                                                                .descriptionEn
                                                                .toString()
                                                            : BlocProvider.of<
                                                                        CourseBloc>(
                                                                    context)
                                                                .result[index]
                                                                .descriptionAr
                                                                .toString(),
                                                        style: AppTextStyle
                                                            .largeDeepGray,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            onTap: () {
                                              setState(() {
                                                _currentPage = 3;
                                              });
                                            },
                                          );
                                        }),
                                  ),
                                  SizedBox(
                                    height: 1.h,
                                  ),
                                  const Divider(color: AppColors.gray),
                                ]),
                          ),
                        ),
                        SliverToBoxAdapter(
                          child: _currentPage == 0
                              ? Padding(
                                  padding: EdgeInsetsDirectional.only(
                                      top: 2.h,
                                      bottom: 2.h,
                                      end: 4.w,
                                      start: 4.w),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    children: [
                                      ClipRRect(
                                        child: SizedBox(
                                          width: 100.w,
                                          child: AspectRatio(
                                            aspectRatio: 3 / 2,
                                            child: CachedNetworkImage(
                                              imageUrl:
                                                  BlocProvider.of<CourseBloc>(
                                                          context)
                                                      .info
                                                      .data
                                                      .image,
                                              imageBuilder:
                                                  (context, imageProvider) =>
                                                      Container(
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(8)),
                                                  image: DecorationImage(
                                                    image: imageProvider,
                                                    fit: BoxFit.fill,
                                                  ),
                                                ),
                                              ),
                                              placeholder: (context, url) =>
                                                  getShimmer(),
                                              errorWidget:
                                                  (context, url, error) =>
                                                      const Center(
                                                child: Icon(Icons.error),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Text(
                                        DataStore.instance.lang == "en"
                                            ? BlocProvider.of<CourseBloc>(
                                                    context)
                                                .info
                                                .data
                                                .titleEn
                                            : BlocProvider.of<CourseBloc>(
                                                    context)
                                                .info
                                                .data
                                                .titleAr,
                                        style: AppTextStyle.xlargeGreenBold,
                                      ),
                                      Text(
                                        DataStore.instance.lang == "en"
                                            ? BlocProvider.of<CourseBloc>(
                                                    context)
                                                .info
                                                .data
                                                .descriptionEn
                                            : BlocProvider.of<CourseBloc>(
                                                    context)
                                                .info
                                                .data
                                                .descriptionAr,
                                        style:
                                            AppTextStyle.mediumNormalBlueBold,
                                      ),
                                    ],
                                  ),
                                )
                              : _currentPage == 1
                                  ? Container(
                                      margin: EdgeInsetsDirectional.only(
                                          top: 2.h,
                                          bottom: 2.h,
                                          end: 4.w,
                                          start: 4.w),
                                      child: Column(
                                        children: [
                                          // _playView(context),
                                          Text(
                                            DataStore.instance.lang == "en"
                                                ? BlocProvider.of<CourseBloc>(
                                                        context)
                                                    .videoList[indexShowVedio]
                                                    .title
                                                : BlocProvider.of<CourseBloc>(
                                                        context)
                                                    .videoList[indexShowVedio]
                                                    .titleAr,
                                            style: AppTextStyle
                                                .largeNormalBlueBold,
                                          ),
                                          Text(
                                            DataStore.instance.lang == "en"
                                                ? BlocProvider.of<CourseBloc>(
                                                        context)
                                                    .videoList[indexShowVedio]
                                                    .descriptionEn
                                                : BlocProvider.of<CourseBloc>(
                                                        context)
                                                    .videoList[indexShowVedio]
                                                    .descriptionAr,
                                            style: AppTextStyle.largeNormalBlue,
                                          ),
                                        ],
                                      ))
                                  : _currentPage == 3
                                      ? BlocBuilder<CourseBloc, CourseState>(
                                          buildWhen: (previous, current) =>
                                              current is FetchShowerQuizAcceptState ||
                                              current
                                                  is FetchShowerQuizErrorState ||
                                              current
                                                  is FetchShowerQuizAwaitState,
                                          builder: (context, state) {
                                            if (state
                                                is FetchShowerQuizAwaitState) {
                                              return const SizedBox(
                                                  height: 15,
                                                  width: 15,
                                                  child: Center(
                                                    child:
                                                        CircularProgressIndicator(
                                                            color: AppColors
                                                                .deepBlue),
                                                  ));
                                            }
                                            if (state
                                                is FetchShowerQuizErrorState) {
                                              return Container(
                                                child: Text(
                                                  state.message,
                                                  style:
                                                      AppTextStyle.smallGreen,
                                                ),
                                              );
                                            }
                                            return Container(
                                              margin:
                                                  EdgeInsetsDirectional.only(
                                                      top: 2.h,
                                                      bottom: 2.h,
                                                      end: 4.w,
                                                      start: 4.w),
                                              child: QuizWidget(
                                                data:
                                                    BlocProvider.of<CourseBloc>(
                                                            context)
                                                        .result,
                                              ),
                                            );
                                          },
                                        )
                                      : Container(),
                        ),

                        // SliverToBoxAdapter(
                        //   child: Column(
                        //     crossAxisAlignment: CrossAxisAlignment.stretch,
                        //     children: [
                        //       SizedBox(
                        //         height: 5.h,
                        //       ),
                        //       ClipRRect(
                        //         child: SizedBox(
                        //           width: 100.w,
                        //           child: AspectRatio(
                        //             aspectRatio: 3 / 2,
                        //             child: CachedNetworkImage(
                        //               imageUrl:
                        //                   BlocProvider.of<CourseBloc>(context)
                        //                       .info
                        //                       .data
                        //                       .image,
                        //               imageBuilder: (context, imageProvider) =>
                        //                   Container(
                        //                 decoration: BoxDecoration(
                        //                   borderRadius: BorderRadius.all(
                        //                       Radius.circular(8)),
                        //                   image: DecorationImage(
                        //                     image: imageProvider,
                        //                     fit: BoxFit.fill,
                        //                   ),
                        //                 ),
                        //               ),
                        //               placeholder: (context, url) =>
                        //                   getShimmer(),
                        //               errorWidget: (context, url, error) =>
                        //                   const Center(
                        //                 child: Icon(Icons.error),
                        //               ),
                        //             ),
                        //           ),
                        //         ),
                        //       ),
                        //       Text(
                        //         BlocProvider.of<CourseBloc>(context)
                        //             .info
                        //             .data
                        //             .titleEn,
                        //         style: AppTextStyle.largeBlackBold,
                        //       ),
                        //       Text(
                        //         BlocProvider.of<CourseBloc>(context)
                        //             .info
                        //             .data
                        //             .descriptionEn,
                        //         style: AppTextStyle.mediumBlackBold,
                        //       ),
                        //       SizedBox(
                        //         height: 4.h,
                        //       ),
                        //       Text(
                        //         AppLocalization.of(context)
                        //             .trans("Lesson_Includes"),
                        //         style: AppTextStyle.xLargeBlackBold,
                        //       ),
                        //       SizedBox(
                        //         height: 2.h,
                        //       ),
                        //     ],
                        //   ),
                        // ),
                        // SliverToBoxAdapter(
                        //   child: Container(
                        //     decoration: BoxDecoration(
                        //         color: AppColors.green100.withOpacity(0.4),
                        //         borderRadius: BorderRadius.circular(3)),
                        //     child: Column(
                        //         crossAxisAlignment: CrossAxisAlignment.stretch,
                        //         children: [
                        //           Padding(
                        //             padding: EdgeInsetsDirectional.only(
                        //                 start: 2.w, top: 1.h),
                        //             child: Row(
                        //               children: [
                        //                 Icon(
                        //                   Elearning.play_circle_outline,
                        //                   color: AppColors.green300,
                        //                   size: 18.sp,
                        //                 ),
                        //                 SizedBox(
                        //                   width: 1.w,
                        //                 ),
                        //                 Text(
                        //                   BlocProvider.of<CourseBloc>(context)
                        //                       .videoList
                        //                       .length
                        //                       .toString(),
                        //                   style: AppTextStyle.largeBlack,
                        //                 ),
                        //                 SizedBox(
                        //                   width: 1.w,
                        //                 ),
                        //                 Text(
                        //                   AppLocalization.of(context)
                        //                       .trans("videos"),
                        //                   style: AppTextStyle.largeBlack,
                        //                 ),
                        //               ],
                        //             ),
                        //           ),
                        //           ListView.builder(
                        //               padding: EdgeInsetsDirectional.only(
                        //                   top: 3.h, start: 5.w),
                        //               physics:
                        //                   const NeverScrollableScrollPhysics(),
                        //               scrollDirection: Axis.vertical,
                        //               shrinkWrap: true,
                        //               itemCount:
                        //                   BlocProvider.of<CourseBloc>(context)
                        //                       .videoList
                        //                       .length,
                        //               itemBuilder:
                        //                   (BuildContext context, int index) {
                        //                 return UnorderedListItem(
                        //                     BlocProvider.of<CourseBloc>(context)
                        //                         .videoList[index]
                        //                         .title
                        //                         .toString(),
                        //                     Icon(
                        //                       Icons.check_circle,
                        //                       color: AppColors.green300,
                        //                       size: 20.sp,
                        //                     ));
                        //               }),
                        //           Padding(
                        //             padding: EdgeInsetsDirectional.only(
                        //                 start: 2.w, top: 1.h),
                        //             child: Row(
                        //               children: [
                        //                 Icon(
                        //                   Elearning.play_circle_outline,
                        //                   color: AppColors.green300,
                        //                   size: 18.sp,
                        //                 ),
                        //                 SizedBox(
                        //                   width: 1.w,
                        //                 ),
                        //                 Text(
                        //                   BlocProvider.of<CourseBloc>(context)
                        //                       .readList
                        //                       .length
                        //                       .toString(),
                        //                   style: AppTextStyle.largeBlack,
                        //                 ),
                        //                 SizedBox(
                        //                   width: 1.w,
                        //                 ),
                        //                 Text(
                        //                   AppLocalization.of(context)
                        //                       .trans("readings"),
                        //                   style: AppTextStyle.largeBlack,
                        //                 ),
                        //               ],
                        //             ),
                        //           ),
                        //           BlocProvider.of<CourseBloc>(context)
                        //                       .readList
                        //                       .length >
                        //                   0
                        //               ? ListView.builder(
                        //                   padding: EdgeInsetsDirectional.only(
                        //                       top: 3.h, start: 5.w),
                        //                   physics:
                        //                       const NeverScrollableScrollPhysics(),
                        //                   scrollDirection: Axis.vertical,
                        //                   shrinkWrap: true,
                        //                   itemCount:
                        //                       BlocProvider.of<CourseBloc>(
                        //                               context)
                        //                           .readList
                        //                           .length,
                        //                   itemBuilder: (BuildContext context,
                        //                       int index) {
                        //                     return UnorderedListItem(
                        //                         BlocProvider.of<CourseBloc>(
                        //                                 context)
                        //                             .readList[index]
                        //                             .title
                        //                             .toString(),
                        //                         Icon(
                        //                           Icons.check_circle,
                        //                           color: AppColors.green300,
                        //                           size: 20.sp,
                        //                         ));
                        //                   })
                        //               : SizedBox(),
                        //           SizedBox(
                        //             height: 1.h,
                        //           ),
                        //         ]),
                        //   ),
                        // ),
                        // SliverToBoxAdapter(
                        //   child: Column(
                        //       crossAxisAlignment: CrossAxisAlignment.stretch,
                        //       children: [
                        //         SizedBox(
                        //           height: 4.h,
                        //         ),
                        //         Text(
                        //           AppLocalization.of(context).trans("videos"),
                        //           style: AppTextStyle.xLargeBlackBold,
                        //         ),
                        //         SizedBox(
                        //           height: 4.h,
                        //         ),
                        //         Container(
                        //             margin: EdgeInsets.all(5),
                        //             child: Center(
                        //                 child: FlickVideoPlayer(
                        //               flickManager: flickManager,
                        //             ))),
                        //         SizedBox(
                        //           height: 2.h,
                        //         ),
                        //         Text(
                        //           BlocProvider.of<CourseBloc>(context)
                        //               .videoList[0]
                        //               .title,
                        //           style: AppTextStyle.largeBlackBold,
                        //         ),
                        //         Text(
                        //           BlocProvider.of<CourseBloc>(context)
                        //               .videoList[0]
                        //               .descriptionEn,
                        //           style: AppTextStyle.largeBlack,
                        //         ),
                        //         SizedBox(
                        //           height: 5.h,
                        //         ),
                        //         PlayPage(clips: pages, index: 0),
                        //         SizedBox(
                        //           height: 4.h,
                        //         ),
                        //       ]),
                        // ),

                        // SliverToBoxAdapter(
                        //   child: Container(
                        //     height: 250,
                        //     width: 100.w,
                        //     child: ListView.builder(
                        //         controller: _scrollController,
                        //         scrollDirection: Axis.horizontal,
                        //         itemCount: _videoUrls.length,
                        //         shrinkWrap: true,
                        //         itemBuilder: (ctx, index) {
                        //           print(
                        //               " $index: Offset ($_offset) is between  ${(250 * index) - 140} and  ${(250 * index)}");
                        //           return MyVideoWidget(
                        //             videoUrl: _videoUrls[index],
                        //             index: index,
                        //             thumbNailUrl: "thumbNail_$index",
                        //             isPositioned: _offset <= (150 * index) &&
                        //                 _offset >= (150 * index) - 140,
                        //             key: ObjectKey(_videoUrls[index]),
                        //           );
                        //         }),
                        //   ),
                        // ),

                        // SliverToBoxAdapter(
                        //   child: Column(
                        //     crossAxisAlignment: CrossAxisAlignment.stretch,
                        //     children: [
                        //       SizedBox(
                        //         height: 2.h,
                        //       ),
                        //       Text(
                        //         "${AppLocalization.of(context).trans("quiz")} :",
                        //         style: AppTextStyle.xLargeBlackBold,
                        //       ),
                        //       SizedBox(
                        //         height: 2.h,
                        //       ),
                        //
                        //     ],
                        //   ),
                        // ),
                      ],
                    ));
              }
              return Container();
            },
          );
        },
      ),
    );
  }

  Widget _buildDegree(TestResult result) {
    double degree = result.data.correctAnswersNumber *
        100 /
        (result.data.correctAnswersNumber +
            result.data.incorrectAnswersNumber +
            result.data.incorrectAnswersNumber);
    final degreeOperations = DegreeOperations(degree);
    final color = degreeOperations.calculateColor();
    final stringMarksName = degreeOperations.generateMarksName();
    final stringGrade = degreeOperations.generateGrade();
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            decoration: BoxDecoration(
                shape: BoxShape.circle, border: Border.all(color: color)),
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Text(
                stringGrade,
                style: TextStyle(color: color, fontSize: 35),
              ),
            ),
          ),
          Text(
            sprintf(
              AppLocalization.of(context).trans('test_result'),
              [stringMarksName],
            ),
            style: const TextStyle(
                color: Colors.black, fontSize: 12, fontWeight: FontWeight.bold),
          ),
          const Divider(
            color: AppColors.gray,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SvgPicture.asset("assets/img/svg/correct_answer.svg",
                  color: Colors.green, height: 15, width: 15),
              const SizedBox(
                width: 5,
              ),
              Expanded(
                child: Text(
                  sprintf(
                    AppLocalization.of(context).trans('number_correct_answer'),
                    [result.data.correctAnswersNumber],
                  ),
                  style: const TextStyle(
                      color: Colors.black,
                      fontSize: 12,
                      fontWeight: FontWeight.normal),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          for (int i = 0; i < result.data.report.length; i++)
            Container(
              child: result.data.report[i].isCorrect == false
                  ? const SizedBox()
                  : Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                          SvgPicture.asset("assets/img/svg/arrow_tag.svg",
                              color: Colors.black, height: 15, width: 15),
                          Expanded(
                              child: Text(
                            result.data.report[i].question,
                            style: TextStyle(fontSize: 13),
                          )),
                          const Icon(
                            Icons.check,
                            color: Colors.green,
                          )
                        ]),
            ),
          const Divider(
            color: AppColors.gray,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SvgPicture.asset("assets/img/svg/correct_answer.svg",
                  color: AppColors.red, height: 15, width: 15),
              const SizedBox(
                width: 5,
              ),
              Expanded(
                child: Text(
                  sprintf(
                    AppLocalization.of(context)
                        .trans('number_in_correct_answer'),
                    [result.data.incorrectAnswersNumber],
                  ),
                  style: const TextStyle(
                      color: Colors.black,
                      fontSize: 12,
                      fontWeight: FontWeight.normal),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          for (int i = 0; i < result.data.report.length; i++)
            Container(
              child: result.data.report[i].isCorrect == true
                  ? const SizedBox()
                  : Column(
                      children: [
                        Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              SvgPicture.asset("assets/img/svg/arrow_tag.svg",
                                  color: Colors.black, height: 15, width: 15),
                              Expanded(
                                  child: Text(
                                result.data.report[i].question,
                                style: TextStyle(fontSize: 13),
                              )),
                              const Icon(
                                Icons.cancel_outlined,
                                color: AppColors.red,
                              )
                            ]),
                        Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),   color: Colors.green,),

                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10),
                            child: Center(
                                child: Text(result.data.report[i].expectedAnswer
                                        .contains("\n")
                                    ? result.data.report[i].expectedAnswer
                                        .replaceRange(result.data.report[i].expectedAnswer.length - 1, result.data.report[i].expectedAnswer.length, "")
                                    : result.data.report[i].expectedAnswer)),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),   color: AppColors.red,),
                          alignment: Alignment.center,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10),
                            child: Center(
                                child: Text(result.data.report[i].userAnswer
                                        .contains("\n")
                                    ? result.data.report[i].userAnswer
                                        .replaceAll("\n", '')
                                    : result.data.report[i].userAnswer)),
                          ),
                        ),
                        SizedBox(
                          height: 8,
                        )
                      ],
                    ),
            ),
        ],
      ),
    );
  }

  void openPDF(BuildContext context, File file) => Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => PDFViewerPage(file: file)),
      );
}
