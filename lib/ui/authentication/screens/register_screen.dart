import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/utils.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../../bloc/authentication/register_bloc/register_bloc.dart';
import '../../../localization/app_localization.dart';
import '../../../public/constants.dart';
import '../../../themes/app_colors.dart';
import '../../../themes/app_text_style.dart';
import '../../../themes/font_family.dart';
import '../../navigation/navigation.dart';
import '../../widgets/common/Flush_bar.dart';
import '../../widgets/textfield/DirectionalTextField.dart';

class RegisterPage extends StatefulWidget {
  final VoidCallback toggleView;

  RegisterPage({this.toggleView});

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController test = TextEditingController();
  FocusNode textFieldFocus = FocusNode();
  RegisterBloc _bloc;

  hideKeyboard() => textFieldFocus.unfocus();

  @override
  void initState() {
    // TODO: implement initState
    _bloc = BlocProvider.of<RegisterBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.bGray,
      body: BlocListener<RegisterBloc, RegisterState>(
          listenWhen: (_, current) =>
              current is RegisterErrorState || current is RegisterAcceptState,
          listener: (context, state) async {
            if (state is RegisterErrorState) {
              await showCustomFlashbar(
                  context, state.message, "Register Failed");
            }
            if (state is RegisterAcceptState) {
              Navigator.of(context, rootNavigator: true).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (context) => Navigation()),
                  (route) => false);
            }
          },
          child: SizedBox(
            height: 100.h,
            width: 100.w,
            child: Form(
              key: _formKey,
              child: NotificationListener<OverscrollIndicatorNotification>(
                onNotification: (overscroll) {
                  return true;
                },
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 32.h,
                        width: 100.w,
                        margin: EdgeInsets.fromLTRB(
                          10.w,
                          8.8.h - 12.sp,
                          10.w,
                          12.sp,
                        ),
                        child: Image(image: Constants().primaryImage),
                      ),
                      Padding(
                        padding: EdgeInsetsDirectional.only(start: 4.w),
                        child: Text(
                          AppLocalization.of(context).trans("Sign_Up"),
                          style: AppTextStyle.largeBlackBold,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsetsDirectional.only(start: 4.w),
                        child: Text(
                          AppLocalization.of(context)
                              .trans("Enter_your_Information"),
                          style: AppTextStyle.smallDeepGrayBold,
                        ),
                      ),
                      SizedBox(
                        height: 2.h,
                      ),
                      BlocBuilder<RegisterBloc, RegisterState>(
                        buildWhen: (previous, current) {
                          return current is RegisterValidationState;
                        },
                        builder: (context, state) {
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsetsDirectional.only(start: 4.w),
                                child: Text(
                                  AppLocalization.of(context)
                                      .trans("first_name"),
                                  style: AppTextStyle.smallDeepGray,
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.fromLTRB(
                                    12.sp, 4.sp, 16.sp, 2.5.sp),
                                child: BlocBuilder<RegisterBloc, RegisterState>(
                                  buildWhen: (previous, current) {
                                    return current is RegisterValidationState;
                                  },
                                  builder: (context, state) {
                                    return DirectionalTextField(
                                      controller: _bloc.firstNameController,
                                      decoration: InputDecoration(
                                        errorText: !_bloc.isValidFirstName
                                            ? AppLocalization.of(context)
                                                .trans(_bloc.errorFirstName)
                                            : null,
                                        floatingLabelBehavior:
                                            FloatingLabelBehavior.always,
                                        contentPadding: const EdgeInsets.only(
                                          left: 12.0,
                                        ),
                                        labelStyle:
                                            const TextStyle(color: Colors.grey),
                                        prefixIcon: const Icon(
                                          Icons.person,
                                          color: AppColors.gray,
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: const BorderSide(
                                              color: AppColors.gray),
                                          borderRadius:
                                              BorderRadius.circular(25.0),
                                        ),
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(25.0),
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ),
                            ],
                          );
                        },
                      ),
                      BlocBuilder<RegisterBloc, RegisterState>(
                        buildWhen: (previous, current) {
                          return current is RegisterValidationState;
                        },
                        builder: (context, state) {
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsetsDirectional.only(start: 4.w),
                                child: Text(
                                  AppLocalization.of(context)
                                      .trans("last_name"),
                                  style: AppTextStyle.smallDeepGray,
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.fromLTRB(
                                    12.sp, 4.sp, 16.sp, 2.5.sp),
                                child: DirectionalTextField(
                                  controller: _bloc.lastNameController,
                                  decoration: InputDecoration(
                                    errorText: !_bloc.isValidLastName
                                        ? AppLocalization.of(context)
                                            .trans(_bloc.errorFirstName)
                                        : null,
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.always,
                                    contentPadding: const EdgeInsets.only(
                                      left: 12.0,
                                    ),
                                    labelStyle:
                                        const TextStyle(color: Colors.grey),
                                    prefixIcon: const Icon(
                                      Icons.person,
                                      color: AppColors.gray,
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                          color: AppColors.gray),
                                      borderRadius: BorderRadius.circular(25.0),
                                    ),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(25.0),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          );
                        },
                      ),
                      BlocBuilder<RegisterBloc, RegisterState>(
                        buildWhen: (previous, current) {
                          return current is RegisterValidationState;
                        },
                        builder: (context, state) {
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsetsDirectional.only(start: 4.w),
                                child: Text(
                                  AppLocalization.of(context)
                                      .trans("phone_number"),
                                  style: AppTextStyle.smallDeepGray,
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.fromLTRB(
                                    12.sp, 4.sp, 16.sp, 2.5.sp),

                                child: DirectionalTextField(
                                  keyboardType: TextInputType.number,
                                  controller: _bloc.mobileNumberController,
                                  decoration: InputDecoration(
                                    errorText: !_bloc.isValidMobileNumber
                                        ? AppLocalization.of(context)
                                            .trans(_bloc.errorMobileNumber)
                                        : null,
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.always,
                                    contentPadding: const EdgeInsets.only(
                                      left: 12.0,
                                    ),
                                    labelStyle:
                                        const TextStyle(color: Colors.grey),
                                    prefixIcon: const Icon(
                                      Icons.person,
                                      color: AppColors.gray,
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                          color: AppColors.gray),
                                      borderRadius: BorderRadius.circular(25.0),
                                    ),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(25.0),
                                    ),
                                  ),
                                ),
                                // DirectionalTextField(
                                //   colorTitle: AppColors.gray,
                                //   controller:
                                //       _bloc.mobileNumberController,
                                //   keyboardType: TextInputType.number,
                                //   inputFormatters: [
                                //     FilteringTextInputFormatter.digitsOnly
                                //   ],
                                //   decoration: InputDecoration(
                                //     errorText: _bloc.errorMobileNumber,
                                //     floatingLabelBehavior:
                                //         FloatingLabelBehavior.always,
                                //     contentPadding: const EdgeInsets.only(
                                //       left: 12.0,
                                //     ),
                                //     border: InputBorder.none,
                                //     labelText: AppLocalization.instance
                                //         .trans('phone_number'),
                                //     labelStyle: TextStyle(
                                //       color: Theme.of(context)
                                //           .textTheme
                                //           .bodyText1
                                //           .color
                                //           .withOpacity(.8),
                                //       fontSize: 11.sp,
                                //       fontWeight: FontWeight.w600,
                                //     ),
                                //   ),
                                // ),
                              ),
                            ],
                          );
                        },
                      ),
                      SizedBox(height: 2.h),
                      GestureDetector(
                        onTap: () async {},
                        child: Container(
                            height: 40.sp,
                            margin: EdgeInsets.symmetric(horizontal: 16.sp),
                            decoration: BoxDecoration(
                              boxShadow:  [
                                BoxShadow(
                                  color: AppColors.gray,
                                  blurRadius: 8.0,
                                  // soften the shadow
                                  spreadRadius: 2.0,
                                  //extend the shadow
                                  offset: Offset(0.5, 0.5)
                                )
                              ],
                              borderRadius: BorderRadius.circular(25.sp),
                              color: AppColors.deepBlue,
                            ),
                            child: BlocBuilder<RegisterBloc, RegisterState>(
                              buildWhen: (_, current) =>
                                  current is RegisterValidationState ||
                                  current is RegisterAcceptState ||
                                  current is RegisterAwaitState ||
                                  current is RegisterErrorState,
                              builder: (context, state) {
                                return InkWell(
                                  child: Center(
                                    child: state is RegisterAwaitState
                                        ? const CircularProgressIndicator(
                                            color: Colors.white,
                                            strokeWidth: 1.5,
                                          )
                                        : Text(
                                            AppLocalization.of(context)
                                                .trans("register_now"),
                                            style:
                                                AppTextStyle.mediumWhiteBold),
                                  ),
                                  onTap: () {
                                    _bloc.add(RegisterCommitEvent());
                                  },
                                );
                              },
                            )),
                      ),
                      SizedBox(height: 0.5.h),
                      GestureDetector(
                        onTap: () {
                          widget.toggleView();
                        },
                        child: Container(
                          margin: EdgeInsets.only(
                            top: 18.sp,
                            bottom: 12.sp,
                          ),
                          alignment: Alignment.center,
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(
                              style: TextStyle(
                                fontFamily: FontFamily.lato,
                                fontSize: 10.sp,
                                color:
                                    Theme.of(context).textTheme.bodyText2.color,
                              ),
                              children: [
                                TextSpan(
                                    text: AppLocalization.of(context)
                                        .trans("have_account"),
                                    style: AppTextStyle.smallBlackBold),
                                const WidgetSpan(
                                  child: SizedBox(width: 5),
                                ),
                                TextSpan(
                                    text: AppLocalization.of(context)
                                        .trans("login"),
                                    style: AppTextStyle.smallGreenBold),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )),
    );
  }

  // Widget _buildLineInfo(context, title, valid, controller) {
  //   return Container(
  //     padding: EdgeInsets.fromLTRB(12.sp, 16.sp, 16.sp, 2.5.sp),
  //     child: TextFormField(
  //       controller: controller,
  //       cursorColor: colorTitle,
  //       cursorRadius: Radius.circular(30.0),
  //       style: TextStyle(
  //         color: Theme.of(context).textTheme.bodyText1.color.withOpacity(.95),
  //         fontSize: 11.sp,
  //         fontWeight: FontWeight.w500,
  //       ),
  //       validator: (val) {
  //         // if (title == 'Email') {
  //         //   return !GetUtils.isEmail(val) ? valid : null;
  //         // } else if (title == 'Mật khẩu') {
  //         //   return val.length < 6 ? valid : null;
  //         // } else {
  //         //   return val.isEmpty ? valid : null;
  //         // }
  //       },
  //       onChanged: (val) {
  //         setState(() {
  //           if (title == 'Họ của bạn') {
  //             firstName = val.trim();
  //           } else if (title == 'Tên của bạn') {
  //             lastName = val.trim();
  //           } else if (title == 'Email') {
  //             email = val.trim();
  //           } else if (title == 'Mật khẩu') {
  //             password = val.trim();
  //           } else {}
  //         });
  //       },
  //       inputFormatters: [
  //         title == 'Số điện thoại'
  //             ? FilteringTextInputFormatter.digitsOnly
  //             : FilteringTextInputFormatter.singleLineFormatter,
  //       ],
  //       obscureText: title == 'Mật khẩu' ? true : false,
  //       decoration: InputDecoration(
  //         floatingLabelBehavior: FloatingLabelBehavior.always,
  //         contentPadding: const EdgeInsets.only(
  //           left: 12.0,
  //         ),
  //         border: InputBorder.none,
  //         labelText: title,
  //         labelStyle: TextStyle(
  //           color: Theme.of(context).textTheme.bodyText1.color.withOpacity(.8),
  //           fontSize: 11.sp,
  //           fontWeight: FontWeight.w600,
  //         ),
  //       ),
  //     ),
  //   );
  // }

  Widget _buildDivider(context) {
    return Divider(
      thickness: .25,
      height: .25,
      indent: 22.sp,
      endIndent: 22.sp,
    );
  }
}
