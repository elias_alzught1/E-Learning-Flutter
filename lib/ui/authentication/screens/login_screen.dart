import 'package:e_learning/themes/app_text_style.dart';
import 'package:e_learning/ui/authentication/screens/verification_layout.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import '../../../bloc/authentication/login_bloc/authentication_bloc.dart';
import '../../../bloc/authentication/login_bloc/authentication_state.dart';
import '../../../bloc/authentication/login_bloc/authentication_event.dart';
import '../../../bloc/verification_bloc/verification_bloc.dart';
import '../../../controller/data_store.dart';
import '../../../localization/app_localization.dart';
import '../../../public/constants.dart';
import '../../../themes/app_colors.dart';
import '../../../themes/app_decorations.dart';
import '../../../themes/font_family.dart';
import '../../common/widget/get_snack_bar.dart';
import '../../navigation/navigation.dart';
import '../../widgets/common/Flush_bar.dart';
import '../../widgets/textfield/DirectionalTextField.dart';

class LoginPage extends StatefulWidget {
  final VoidCallback toggleView;

  LoginPage({this.toggleView});

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  FocusNode usernameFocus = FocusNode();

  String email = '';

  hideKeyboard() => usernameFocus.unfocus();
  AuthBloc _bloc;

  @override
  void initState() {
    _bloc = BlocProvider.of<AuthBloc>(context);
  }

  bool _isChecked = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: AppColors.bGray,
      body: BlocListener<AuthBloc, AuthState>(
        listenWhen: (previous, current) =>
            current is LoginAcceptState || current is LoginErrorState,
        listener: (context, state) async {
          if (state is LoginAcceptState) {
            Navigator.of(context).push(CupertinoPageRoute(
              builder: (context) => BlocProvider(
                // this.data, this.phoneNumber, this.channelId, this.planId, this.language
                create: (context) => VerificationBloc(
                    state.response.data.sessionId,
                    state.response.data.mSISDN,
                    2,
                    0,
                    DataStore.instance.lang,
                   ),
                child: const VerificationLayout(),
              ),
            ));

            // Navigator.of(context, rootNavigator: true).pushAndRemoveUntil(
            //     MaterialPageRoute(builder: (context) => Navigation()),
            //     (route) => false);
            print("yes");
          }
          if (state is LoginErrorState) {
            await showCustomFlashbar(context, state.message, "Login Failed");
          }
        },
        child: SizedBox(
          height: 100.h,
          width: 100.w,
          child: Form(
            autovalidateMode: AutovalidateMode.disabled,
            key: _formKey,
            child: NotificationListener<OverscrollIndicatorNotification>(
              onNotification: (overscroll) {
                return true;
              },
              child: Column(
                children: [
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.only(
                          bottom: MediaQuery.of(context).viewInsets.bottom),
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(height: 12.sp),
                            //Sign_in
                            Container(
                              height: 42.h,
                              width: 100.w,
                              margin: EdgeInsets.fromLTRB(
                                10.w,
                                8.8.h - 12.sp,
                                10.w,
                                12.sp,
                              ),
                              child: Image(image: Constants().primaryImage),
                            ),
                            Padding(
                              padding: EdgeInsetsDirectional.only(start: 4.w),
                              child: Text(
                                AppLocalization.of(context).trans("SignIn"),
                                style: AppTextStyle.largeBlackBold,
                              ),
                            ), //Enter_Mobile_Number
                            Padding(
                              padding: EdgeInsetsDirectional.only(start: 4.w),
                              child: Text(
                                AppLocalization.of(context)
                                    .trans("Enter_Mobile_Number"),
                                style: AppTextStyle.smallDeepGrayBold,
                              ),
                            ), //Enter_Mobile_Number
                            Padding(
                              padding: EdgeInsets.fromLTRB(
                                2.w,
                                3.h,
                                2.w,
                                MediaQuery.of(context).viewInsets.bottom,
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  SizedBox(
                                    child: BlocBuilder<AuthBloc, AuthState>(
                                      buildWhen: (previous, current) {
                                        return current is LoginValidationState;
                                      },
                                      builder: (context, state) {
                                        return DirectionalTextField(
                                          controller:
                                              _bloc.mobileNumberController,
                                          decoration: InputDecoration(
                                            errorText: !_bloc
                                                    .isValidMobileNumber
                                                ? AppLocalization.of(context)
                                                    .trans(_bloc
                                                        .errorMessageNumber)
                                                : null,
                                            labelText:
                                                AppLocalization.of(context)
                                                    .trans("phone_number"),
                                            labelStyle:
                                                const TextStyle(color: Colors.grey),
                                            prefixIcon: const Icon(
                                              Icons.person,
                                              color: AppColors.gray,
                                            ),
                                            focusedBorder: OutlineInputBorder(
                                              borderSide: const BorderSide(
                                                  color: AppColors.gray),
                                              borderRadius:
                                                  BorderRadius.circular(25.0),
                                            ),
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(25.0),
                                            ),
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                                  SizedBox(height: 2.h),
                                  Padding(
                                    padding:
                                        EdgeInsetsDirectional.only(start: 2.w),
                                    child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          SizedBox(
                                              height: 24.0,
                                              width: 24.0,
                                              child: Theme(
                                                data: ThemeData(
                                                    unselectedWidgetColor:
                                                        AppColors
                                                            .gray // Your color
                                                    ),
                                                child: Checkbox(
                                                  activeColor: AppColors.deepBlue,
                                                  value: _isChecked,
                                                  onChanged: _handleRemeberme,
                                                ),
                                              )),
                                          const SizedBox(width: 10.0),
                                          Text(
                                            AppLocalization.of(context)
                                                .trans("Remember_Me"),
                                            style:
                                                AppTextStyle.smallDeepGrayBold,
                                          ),
                                        ]),
                                  ),
                                  SizedBox(
                                    height: 2.h,
                                  ),
                                  GestureDetector(
                                    onTap: () async {
                                      if (_formKey.currentState.validate()) {}
                                    },
                                    child: Container(
                                        height: 40.sp,
                                        margin: EdgeInsets.symmetric(
                                            horizontal: 16.sp),
                                        decoration: BoxDecoration(
                                          boxShadow: const [
                                             BoxShadow(
                                              color: AppColors.gray,
                                              blurRadius: 8.0,
                                              // soften the shadow
                                              spreadRadius: 2.0,
                                              //extend the shadow
                                              offset: Offset(
                                                5.0,
                                                // Move to right 5  horizontally
                                                5.0, // Move to bottom 5 Vertically
                                              ),
                                            )
                                          ],
                                          borderRadius:
                                              BorderRadius.circular(25.sp),
                                          color: AppColors.deepBlue,
                                        ),
                                        child: BlocBuilder<AuthBloc, AuthState>(
                                          buildWhen: (_, current) =>
                                              current is LoginValidationState ||
                                              current is LoginAcceptState ||
                                              current is LoginAwaitState ||
                                              current is LoginErrorState,
                                          builder: (context, state) {
                                            return InkWell(
                                              child: Center(
                                                child: state is LoginAwaitState
                                                    ? const CircularProgressIndicator(
                                                        color: Colors.white,
                                                        strokeWidth: 1.5,
                                                      )
                                                    : Text(
                                                        AppLocalization.of(
                                                                context)
                                                            .trans("login"),
                                                        style: AppTextStyle
                                                            .mediumWhiteBold),
                                              ),
                                              onTap: () {
                                                state is LoginAwaitState
                                                    ? null
                                                    : _bloc.add(LoginEvent());
                                              },
                                            );
                                          },
                                        )
                                        /*
                                      Center(
                                      child: Text(
                                        AppLocalization.of(context)
                                            .trans("login"),
                                        style: TextStyle(
                                          color: mC,
                                          fontSize: 10.sp,
                                          fontFamily: FontFamily.lato,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                    ),
                                      */
                                        ),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      widget.toggleView();
                                    },
                                    child: Container(
                                      margin: EdgeInsets.only(
                                        top: 18.sp,
                                        bottom: 12.sp,
                                      ),
                                      alignment: Alignment.center,
                                      child: RichText(
                                        textAlign: TextAlign.center,
                                        text: TextSpan(
                                          style: TextStyle(
                                            fontFamily: FontFamily.lato,
                                            fontSize: 10.sp,
                                            color: Theme.of(context)
                                                .textTheme
                                                .bodyText2
                                                .color,
                                          ),
                                          children: [
                                            TextSpan(
                                                text: AppLocalization.of(
                                                        context)
                                                    .trans(
                                                        "Do_not_have_account"),
                                                style: AppTextStyle
                                                    .smallBlackBold),
                                            const WidgetSpan(
                                              child: SizedBox(width: 5),
                                            ),
                                            TextSpan(
                                                text: AppLocalization.of(
                                                        context)
                                                    .trans("create_account"),
                                                style: AppTextStyle
                                                    .smallGreenBold),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Text(
                    '@DigitalHorizon ${DateTime.now().year} ',
                    style: TextStyle(
                      fontSize: 7.sp,
                      fontFamily: FontFamily.lato,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  SizedBox(height: 12.sp),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

//handle remember me function
  void _handleRemeberme(bool value) {
    _isChecked = value;

    setState(() {
      _isChecked = value;
    });
  }

  Widget _buildLineInfo(context, title, valid, focusNode) {
    return Container(
        padding: const EdgeInsets.all(8),
        child: BlocBuilder<AuthBloc, AuthState>(
          buildWhen: (previous, current) {
            return current is LoginValidationState;
          },
          builder: (context, state) {
            return DirectionalTextField(
              controller: context.read<AuthBloc>().mobileNumberController,
              keyboardType: TextInputType.phone,
              // inputFormatters: [phoneNumberFormatter],
              decoration: InputDecoration(
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  contentPadding: const EdgeInsets.only(
                    left: 12.0,
                  ),
                  border: InputBorder.none,
                  labelText: title,
                  labelStyle: TextStyle(
                    color: Theme.of(context)
                        .textTheme
                        .bodyText1
                        .color
                        .withOpacity(.8),
                    fontSize: 11.sp,
                    fontWeight: FontWeight.w500,
                  )),
            );
          },
        ));
  }
}
