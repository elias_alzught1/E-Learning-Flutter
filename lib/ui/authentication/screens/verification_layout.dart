import 'package:e_learning/themes/app_text_style.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';
import '../../../bloc/verification_bloc/verification_bloc.dart';
import '../../../helpers/helpers.dart';
import '../../../localization/app_localization.dart';
import '../../../themes/app_colors.dart';
import '../../navigation/navigation.dart';

class VerificationLayout extends StatelessWidget {
  const VerificationLayout({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<VerificationBloc, VerificationState>(
      listenWhen: (_, current) =>
          current is VerificationErrorState ||
          current is VerificationLoginErrorState ||
          current is VerificationAcceptState ||
          current is VerificationLoginAcceptState,
      listener: (context, state) {
        if (state is VerificationAcceptState) {
          Navigator.of(context, rootNavigator: true).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => Navigation()),
              (route) => false);
        } else if (state is VerificationErrorState) {
        } else if (state is VerificationLoginErrorState) {}
      },
      child: Scaffold(
        body: ExpandedSingleChildScrollView(
          child: Padding(
            padding:const EdgeInsetsDirectional.only(top: 4),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsetsDirectional.only(start: 4.w, end: 4.w),
                  child: Text(
                    AppLocalization.of(context)
                        .trans('verification_description'),
                  ),
                ),
                getPinField(),
                Column(
                  children: [
                    Text(
                      AppLocalization.of(context).trans("not_receive_code"),
                      style: AppTextStyle.mediumBlack,
                    ),
                    getResendButton()
                  ],
                ),
                getCommitButton()
              ],
            ),
          ),
        ),
      ),
    );
  }

/*
      Navigator.of(context).push(CupertinoPageRoute(
              builder: (context) => BlocProvider(
                // this.data, this.phoneNumber, this.channelId, this.planId, this.language
                create: (context) => VerificationBloc(
                    state.data!.sessionId.toString(),
                    state.phoneNumber.e164.replaceAll('+', ''),
                    2,
                    0,
                    DataStore.instance.lang,
                    VerificationOnAuthFactory(context)),
                child: const VerificationLayout(isLogin: true),
              ),
            ));
*/
  Widget getResendButton() {
    return SizedBox(
      height: 40,
      child: BlocBuilder<VerificationBloc, VerificationState>(
        buildWhen: (_, current) =>
            current is VerificationResendErrorState ||
            current is VerificationResendAwaitState ||
            current is VerificationResendAcceptState,
        builder: (context, state) {
          if (state is VerificationResendAwaitState) {
            return const CircularProgressIndicator();
          }
          return TextButton(
            onPressed: () {
              var _bloc = BlocProvider.of<VerificationBloc>(context);
              _bloc.add(VerificationResendCommitEvent());
            },
            child: Text(
              AppLocalization.of(context).trans("resend"),
              style: AppTextStyle.mediumGreen,
            ),
          );
        },
      ),
    );
  }

  Widget getPinField() {
    return BlocBuilder<VerificationBloc, VerificationState>(
      buildWhen: (_, current) => current is VerificationValidationState,
      builder: (context, state) {
        var _bloc = BlocProvider.of<VerificationBloc>(context);

        return Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 10,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              PinCodeTextField(
                maxLength: 4,
                pinBoxBorderWidth: 1,
                pinBoxRadius: 10,
                defaultBorderColor: Colors.transparent,
                hasTextBorderColor: AppColors.deepBlue,
                controller: _bloc.controller,
                pinBoxWidth: 12.w,
                pinBoxHeight: 15.h,
                hasError: !_bloc.isValid,
                errorBorderColor: AppColors.red,
                wrapAlignment: WrapAlignment.spaceAround,
                pinBoxColor: AppColors.grayAccent,
                pinTextStyle: AppTextStyle.mediumGreen,
                pinTextAnimatedSwitcherTransition:
                    ProvidedPinBoxTextAnimation.scalingTransition,
                pinTextAnimatedSwitcherDuration:
                    const Duration(milliseconds: 100),
                keyboardType: TextInputType.number,
              ),
              const   SizedBox(height: 10),
              Text(
                _bloc.otpMessage ?? "",
              ),
            ],
          ),
        );
      },
    );
  }

  Widget getCommitLoginButton() {
    return BlocBuilder<VerificationBloc, VerificationState>(
      buildWhen: (_, current) =>
          current is VerificationValidationState ||
          current is VerificationLoginAwaitState ||
          current is VerificationLoginAcceptState ||
          current is VerificationLoginErrorState,
      builder: (context, state) {
        var _bloc = BlocProvider.of<VerificationBloc>(context);
        return ElevatedButton(
          onPressed: state is VerificationLoginAwaitState
              ? null
              : () {
                  _bloc.add(VerificationPinLoginCommitEvent());
                },
          child: state is VerificationLoginAwaitState
              ? const CupertinoActivityIndicator()
              : Text(
                  AppLocalization.of(context).trans("finish"),
                ),
        );
      },
    );
  }

  Widget getCommitButton() {
    return BlocBuilder<VerificationBloc, VerificationState>(
      buildWhen: (_, current) =>
          current is VerificationValidationState ||
          current is VerificationErrorState ||
          current is VerificationAwaitState ||
          current is VerificationAcceptState,
      builder: (context, state) {
        var _bloc = BlocProvider.of<VerificationBloc>(context);
        return ElevatedButton(
          style: ElevatedButton.styleFrom(
            backgroundColor: AppColors.deepBlue, // background (button) color
            // foreground (text) color
          ),
          onPressed: state is VerificationAwaitState
              ? null
              : () {
                  _bloc.add(VerificationCommitEvent());
                },
          child: state is VerificationAwaitState
              ? const CupertinoActivityIndicator()
              : Text(
                  AppLocalization.of(context).trans("finish"),
                  style: AppTextStyle.mediumWhite,
                ),
        );
      },
    );
  }
}
