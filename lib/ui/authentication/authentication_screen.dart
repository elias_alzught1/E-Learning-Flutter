import 'package:e_learning/ui/authentication/screens/login_screen.dart';
import 'package:e_learning/ui/authentication/screens/register_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/authentication/register_bloc/register_bloc.dart';

class AuthenticateScreen extends StatefulWidget {
  const AuthenticateScreen({key});

  @override
  State<StatefulWidget> createState() => _AuthenticateScreenState();
}

class _AuthenticateScreenState extends State<AuthenticateScreen> {
  bool signIn = true;

  @override
  void initState() {
    super.initState();
  }

  switchScreen() {
    setState(() {
      signIn = !signIn;
    });
  }

  @override
  Widget build(BuildContext context) {
    return signIn
        ? LoginPage(
            toggleView: switchScreen,
          )
        : BlocProvider(
            lazy: false,
            create: (BuildContext context) => RegisterBloc(),
            child: RegisterPage(
              toggleView: switchScreen,
            )
          );
  }
}
