
import 'package:e_learning/localization/app_localization.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';

import '../../../themes/font_family.dart';

class UpdateProfile extends StatefulWidget {
  const UpdateProfile({Key key}) : super(key: key);

  @override
  State<UpdateProfile> createState() => _UpdateProfileState();
}

class _UpdateProfileState extends State<UpdateProfile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
            appBar: AppBar(
        centerTitle: true,
        elevation: .0,
        // systemOverlayStyle: ThemeService.systemBrightness,
        leading: IconButton(
          onPressed: () => Navigator.of(context).pop(),
          icon: Icon(
            Icons.arrow_back_outlined,
            size: 20.sp,
          ),
        ),
        title: Text(
          AppLocalization.of(context).trans("update_profile"),
          style: TextStyle(
            fontSize: 15.sp,
            fontFamily: FontFamily.lato,
            fontWeight: FontWeight.w600,
            color: Theme.of(context).textTheme.bodyText1.color,
          ),
        ),
        actions: [

        ],
      ),
    );
  }
}
