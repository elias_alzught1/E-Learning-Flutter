import 'package:e_learning/localization/app_localization.dart';
import 'package:e_learning/themes/app_text_style.dart';
import 'package:e_learning/ui/widgets/appbar/abbBarAPP.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:octo_image/octo_image.dart';

import '../../bloc/profile_bloc/profile_bloc.dart';
import '../../helpers/helpers.dart';
import '../../helpers/picker/custom_image_picker.dart';
import '../../themes/app_colors.dart';
import '../common/dialog_loading.dart';
import '../widgets/buttom_sheet/verify_cange_mobile_number.dart';
import '../widgets/textfield/DirectionalTextField.dart';

class UserProfile extends StatefulWidget {
  const UserProfile({Key key}) : super(key: key);

  @override
  State<UserProfile> createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  @override
  void initState() {
    BlocProvider.of<ProfileBloc>(context).firstName.text =
        BlocProvider.of<ProfileBloc>(context).user.firstName ?? " ";
    BlocProvider.of<ProfileBloc>(context).lastName.text =
        BlocProvider.of<ProfileBloc>(context).user.lastName;
    BlocProvider.of<ProfileBloc>(context).userName.text =
        BlocProvider.of<ProfileBloc>(context).user.username;
    BlocProvider.of<ProfileBloc>(context).email.text =
        BlocProvider.of<ProfileBloc>(context).user.email;
    BlocProvider.of<ProfileBloc>(context).phoneNumber.text =
        BlocProvider.of<ProfileBloc>(context).user.mSISDN;
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarApp(),
        body: Container(
          child: SingleChildScrollView(
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Container(
                color: AppColors.textFieldGray,
                child: Padding(
                  padding: EdgeInsetsDirectional.only(
                      top: 4.w, end: 4.w, bottom: 4.w, start: 4.w),
                  child: Row(children: [
                    GestureDetector(
                      onTap: () {
                        CustomImagePicker().openImagePicker(
                          context: context,
                        );
                      },
                      child: Container(
                        width: 30.w,
                        alignment: Alignment.center,
                        child: Container(
                          height: 95.sp,
                          width: 95.sp,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                              color: AppColors.deepBlue,
                              width: 3.sp,
                            ),
                          ),
                          alignment: Alignment.center,
                          child: Container(
                            height: 80.sp,
                            width: 80.sp,
                            decoration: const BoxDecoration(
                              shape: BoxShape.circle,
                            ),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(1000.sp),
                              child: OctoImage(
                                image: NetworkImage(
                                    BlocProvider.of<ProfileBloc>(context)
                                        .user
                                        .profileImage),
                                fit: BoxFit.cover,
                                placeholderBuilder: (context) => getShimmer(),
                                errorBuilder: imageErrorBuilder2,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 8.w,
                    ),
                    Text(
                      "${AppLocalization.of(context).trans("welcome_name_profile")}${BlocProvider.of<ProfileBloc>(context).user.firstName} ${BlocProvider.of<ProfileBloc>(context).user.lastName}",
                      style: AppTextStyle.largeGreenBold,
                    )
                  ]),
                ),
              ),
              Container(
                  margin: EdgeInsetsDirectional.only(start: 5.w, top: 4.w),
                  child: Text(
                    AppLocalization.of(context).trans("Personal_information"),
                    style: AppTextStyle.largeGreenBold,
                  )),
              Container(
                  margin: EdgeInsetsDirectional.only(
                      start: 5.w, top: 4.w, end: 5.w),
                  child: Text(
                    AppLocalization.of(context).trans("Profile_title"),
                    style: AppTextStyle.mediumDeepGrayBold,
                  )),
              SizedBox(
                height: 2.h,
              ),
              Container(
                margin: EdgeInsetsDirectional.only(start: 5.w),
                child: Text(
                  AppLocalization.of(context).trans("first_name"),
                  style: AppTextStyle.largeGreenBold,
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Container(
                margin: EdgeInsetsDirectional.only(start: 5.w, end: 5.w),
                child: DirectionalTextField(
                  controller: BlocProvider.of<ProfileBloc>(context).firstName,
                  decoration: InputDecoration(
                    // errorText: !_bloc
                    //     .isValidMobileNumber
                    //     ? AppLocalization.of(context)
                    //     .trans(_bloc
                    //     .errorMessageNumber)
                    //     : null,
                    // labelText:
                    // AppLocalization.of(context)
                    //     .trans("phone_number"),
                    labelStyle: const TextStyle(color: Colors.grey),
                    prefixIcon: const Icon(
                      Icons.person,
                      color: AppColors.gray,
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: AppColors.gray),
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Container(
                margin: EdgeInsetsDirectional.only(start: 5.w),
                child: Text(
                  AppLocalization.of(context).trans("last_name"),
                  style: AppTextStyle.largeGreenBold,
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Container(
                margin: EdgeInsetsDirectional.only(start: 5.w, end: 5.w),
                child: DirectionalTextField(
                  controller: BlocProvider.of<ProfileBloc>(context).lastName,
                  decoration: InputDecoration(
                    // errorText: !_bloc
                    //     .isValidMobileNumber
                    //     ? AppLocalization.of(context)
                    //     .trans(_bloc
                    //     .errorMessageNumber)
                    //     : null,
                    // labelText:
                    // AppLocalization.of(context)
                    //     .trans("phone_number"),
                    labelStyle: const TextStyle(color: Colors.grey),
                    prefixIcon: const Icon(
                      Icons.person,
                      color: AppColors.gray,
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: AppColors.gray),
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Container(
                margin: EdgeInsetsDirectional.only(start: 5.w),
                child: Text(
                  AppLocalization.of(context).trans("userName"),
                  style: AppTextStyle.largeGreenBold,
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Container(
                margin: EdgeInsetsDirectional.only(start: 5.w, end: 5.w),
                child: DirectionalTextField(
                  controller: BlocProvider.of<ProfileBloc>(context).userName,
                  decoration: InputDecoration(
                    // errorText: !_bloc
                    //     .isValidMobileNumber
                    //     ? AppLocalization.of(context)
                    //     .trans(_bloc
                    //     .errorMessageNumber)
                    //     : null,
                    // labelText:
                    // AppLocalization.of(context)
                    //     .trans("phone_number"),
                    labelStyle: const TextStyle(color: Colors.grey),
                    prefixIcon: const Icon(
                      Icons.perm_contact_cal_outlined,
                      color: AppColors.gray,
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: AppColors.gray),
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              //phone_number
              Container(
                margin: EdgeInsetsDirectional.only(start: 5.w),
                child: Text(
                  AppLocalization.of(context).trans("email"),
                  style: AppTextStyle.largeGreenBold,
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Container(
                margin: EdgeInsetsDirectional.only(start: 5.w, end: 5.w),
                child: DirectionalTextField(
                  controller: BlocProvider.of<ProfileBloc>(context).email,
                  decoration: InputDecoration(
                    // errorText: !_bloc
                    //     .isValidMobileNumber
                    //     ? AppLocalization.of(context)
                    //     .trans(_bloc
                    //     .errorMessageNumber)
                    //     : null,
                    // labelText:
                    // AppLocalization.of(context)
                    //     .trans("phone_number"),
                    labelStyle: const TextStyle(color: Colors.grey),
                    prefixIcon: const Icon(
                      Icons.email,
                      color: AppColors.gray,
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: AppColors.gray),
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              //phone_number
              Container(
                margin: EdgeInsetsDirectional.only(start: 5.w),
                child: Text(
                  AppLocalization.of(context).trans("phone_number"),
                  style: AppTextStyle.largeGreenBold,
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Container(
                margin: EdgeInsetsDirectional.only(start: 5.w, end: 5.w),
                child: DirectionalTextField(
                  readOnly: true,
                  controller: BlocProvider.of<ProfileBloc>(context).phoneNumber,
                  decoration: InputDecoration(
                    labelStyle: const TextStyle(color: Colors.grey),
                    prefixIcon: const Icon(
                      Icons.phone_android,
                      color: AppColors.gray,
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: AppColors.gray),
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 4.h,
              ),
              BlocBuilder<ProfileBloc, ProfileState>(
                buildWhen: (_, current) =>
                    current is UpdateUserInfoAwaitState ||
                    current is UpdateUserInfoErrorState ||
                    current is UpdateUserInfoAcceptState,
                builder: (context, state) {
                  return GestureDetector(
                    onTap: () {
                      BlocProvider.of<ProfileBloc>(context)
                          .add(UpdateUserInfo());
                    },
                    child: Container(
                      margin: EdgeInsetsDirectional.only(
                          start: 6.w, end: 6.w, bottom: 2.w),
                      decoration: const BoxDecoration(
                        color: AppColors.blue,
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(15),
                          topLeft: Radius.circular(15),
                          bottomRight: Radius.circular(15),
                          bottomLeft: Radius.circular(15),
                        ),
                      ),
                      height: 45,
                      width: 100.w,
                      child: Center(
                        child: Text(
                          state is UpdateUserInfoAwaitState
                              ? "Please Waite ..."
                              : AppLocalization.of(context).trans("saveChange"),
                          style: AppTextStyle.largeWhiteBold,
                        ),
                      ),
                    ),
                  );
                },
              ),
              SizedBox(
                height: 2.h,
              ),
              Container(
                  margin: EdgeInsetsDirectional.only(start: 5.w, end: 5.w),
                  child: const Divider(
                    color: Colors.grey,
                  )),
              SizedBox(
                height: 2.h,
              ),
              Container(
                  margin: EdgeInsetsDirectional.only(start: 5.w, top: 4.w),
                  child: Text(
                    AppLocalization.of(context).trans("Security"),
                    style: AppTextStyle.largeGreenBold,
                  )),
              Container(
                  margin: EdgeInsetsDirectional.only(
                      start: 5.w, top: 4.w, end: 5.w),
                  child: Text(
                    AppLocalization.of(context).trans("securityDesc"),
                    style: AppTextStyle.mediumDeepGrayBold,
                  )),
              SizedBox(
                height: 1.h,
              ),
              Container(
                  margin: EdgeInsetsDirectional.only(start: 5.w, top: 4.w),
                  child: Text(
                    AppLocalization.of(context).trans("change_number"),
                    style: AppTextStyle.xlargeGreenBold,
                  )),
              Container(
                  margin: EdgeInsetsDirectional.only(start: 5.w, top: 4.w),
                  child: Text(
                    AppLocalization.of(context).trans("phone_number"),
                    style: AppTextStyle.largeGreenBold,
                  )),

              SizedBox(
                height: 2.h,
              ),
              Container(
                margin: EdgeInsetsDirectional.only(start: 5.w, end: 5.w),
                child: DirectionalTextField(
                  controller: BlocProvider.of<ProfileBloc>(context).phoneNumber,
                  decoration: InputDecoration(
                    labelStyle: const TextStyle(color: Colors.grey),
                    prefixIcon: const Icon(
                      Icons.phone_android,
                      color: AppColors.gray,
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: AppColors.gray),
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              GestureDetector(
                onTap: () async {
                  var pinResult = await ChangeMobileNumberBottomSheet.show(
                    context,
                  );
                },
                child: Container(
                  margin: EdgeInsetsDirectional.only(
                      start: 6.w, end: 6.w, bottom: 2.w),
                  decoration: const BoxDecoration(
                    color: AppColors.blue,
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(15),
                      topLeft: Radius.circular(15),
                      bottomRight: Radius.circular(15),
                      bottomLeft: Radius.circular(15),
                    ),
                  ),
                  height: 45,
                  width: 100.w,
                  child: Center(
                    child: Text(
                      AppLocalization.of(context).trans("Next"),
                      style: AppTextStyle.largeWhiteBold,
                    ),
                  ),
                ),
              ),
              /////////////////////////////////////////////////////////////////
              Container(
                  margin: EdgeInsetsDirectional.only(start: 5.w, top: 4.w),
                  child: Text(
                    AppLocalization.of(context).trans("changeEmail"),
                    style: AppTextStyle.xlargeGreenBold,
                  )),
              Container(
                  margin: EdgeInsetsDirectional.only(start: 5.w, top: 4.w),
                  child: Text(
                    AppLocalization.of(context).trans("email"),
                    style: AppTextStyle.largeGreenBold,
                  )),

              SizedBox(
                height: 2.h,
              ),
              Container(
                margin: EdgeInsetsDirectional.only(start: 5.w, end: 5.w),
                child: DirectionalTextField(
                  controller: BlocProvider.of<ProfileBloc>(context).email,
                  decoration: InputDecoration(
                    // errorText: !_bloc
                    //     .isValidMobileNumber
                    //     ? AppLocalization.of(context)
                    //     .trans(_bloc
                    //     .errorMessageNumber)
                    //     : null,
                    // labelText:
                    // AppLocalization.of(context)
                    //     .trans("phone_number"),
                    labelStyle: const TextStyle(color: Colors.grey),
                    prefixIcon: const Icon(
                      Icons.phone_android,
                      color: AppColors.gray,
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: AppColors.gray),
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Container(
                margin: EdgeInsetsDirectional.only(
                    start: 6.w, end: 6.w, bottom: 2.w),
                decoration: const BoxDecoration(
                  color: AppColors.blue,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(15),
                    topLeft: Radius.circular(15),
                    bottomRight: Radius.circular(15),
                    bottomLeft: Radius.circular(15),
                  ),
                ),
                height: 45,
                width: 100.w,
                child: Center(
                  child: Text(
                    AppLocalization.of(context).trans("Next"),
                    style: AppTextStyle.largeWhiteBold,
                  ),
                ),
              ),
              SizedBox(
                height: 4.h,
              ),
              Container(
                width: 100.w,
                height: 10.h,
                color: Color(0xFF031E4D),
                child: Padding(
                  padding: EdgeInsetsDirectional.only(start: 2.w, end: 2.w),
                  child: Row(children: [
                    SvgPicture.asset(
                      'assets/img/svg/white_logo.svg',
                      width: 15.sp,
                      height: 15.sp,
                    ),
                    SizedBox(
                      width: 10.sp,
                    ),
                    Text(
                      "E-Learning Platform",
                      style: AppTextStyle.mediumWhiteBold,
                    ),
                    Spacer(),
                    Text(
                      "2023 © E-Learing",
                      style: AppTextStyle.mediumWhiteBold,
                    )
                  ]),
                ),
              ),
              //change_number
            ]),
          ),
        ));
  }

  Widget imageErrorBuilder2(ctx, error, _) => Container(
        color: AppColors.gray,
        child:
            Center(child: Text(AppLocalization.instance.trans("upload_photo"))),
      );
}
