import 'dart:io';

import 'package:e_learning/bloc/authentication/login_bloc/authentication_bloc.dart';
import 'package:e_learning/bloc/authentication/login_bloc/authentication_state.dart';
import 'package:e_learning/bloc/profile_bloc/profile_bloc.dart';
import 'package:e_learning/helpers/stack_loader_indicator.dart';
import 'package:e_learning/localization/app_localization.dart';
import 'package:e_learning/models/common/user_model.dart';
import 'package:e_learning/ui/profile/screens/update_profile.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:octo_image/octo_image.dart';
import 'package:phone_number/phone_number.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

import '../../bloc/authentication/login_bloc/authentication_event.dart';
import '../../helpers/helpers.dart';
import '../../helpers/picker/custom_image_picker.dart';


import '../../themes/app_colors.dart';

import '../../themes/font_family.dart';


import '../authentication/authentication_screen.dart';
import '../common/dialog_loading.dart';
import '../common/screen/loading_screen.dart';
import '../widgets/loader/inkdrop_loader.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen>
    with TickerProviderStateMixin {
  ScrollController scrollController = ScrollController();

  AuthBloc _bloc;
  final StackLoaderIndicator _loader = StackLoaderIndicator();

  @override
  void initState() {
    super.initState();
    _bloc = BlocProvider.of<AuthBloc>(context);
    // if (!Application.isProductionMode) {
    //   // AppBloc.transactionBloc.add(GetTransactionEvent());
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // systemOverlayStyle: ThemeService.systemBrightness,
        backgroundColor: Colors.transparent,
        elevation: .0,
        centerTitle: true,
        leading: IconButton(
          splashColor: AppColors.deepBlue,
          splashRadius: 5.0,
          icon: Icon(
            PhosphorIcons.slidersHorizontal,
            size: 22.5.sp,
          ),
          onPressed: () {
          //  UpdateProfile
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => const UpdateProfile(),
              ),
            );
            // AppNavigator.push(
            //   AppRoutes.EDIT_INFO_USER,
            //   arguments: {
            //     'slide': SlideMode.left,
            //   },
            // );
          },
        ),
        actions: [
          IconButton(
            splashRadius: 10.0,
            icon: Icon(
              PhosphorIcons.signOutBold,
              size: 20.sp,
              color: AppColors.red,
            ),
            onPressed: () => _bloc.add(LogOutEvent()),
          ),
        ],
      ),
      body: BlocListener<AuthBloc, AuthState>(
        listenWhen: (context, state) =>
            state is LogoutAwaitState ||
            state is LogoutErrorState ||
            state is LogoutAcceptState,
        listener: (context, state) {
          if (state is LogoutAcceptState) {
            Navigator.of(context).pushAndRemoveUntil(
              CupertinoPageRoute(
                builder: (context) => BlocProvider<AuthBloc>(
                  child: const AuthenticateScreen(),
                  create: (context) => AuthBloc(),
                ),
              ),
              (route) => false,
            );
          } else if (state is LogoutAwaitState) {
            _loader.show(context);
          } else if (state is LogoutErrorState) {
            _loader.dismiss();
            Fluttertoast.showToast(
                msg: state.message,
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.TOP,
                timeInSecForIosWeb: 1,
                backgroundColor: AppColors.black.withOpacity(1),
                textColor: Colors.white,
                fontSize: 16.0);
          }
        },
        child: Container(
          height: 100.h,
          width: 100.w,
          child: Column(
            children: [
              GestureDetector(
                onTap: () {
                  CustomImagePicker().openImagePicker(
                    context: context,
                    handleFinish: (File image) async {
                      showDialogLoading(context);
                      // AppBloc.authBloc.add(
                      //   UpdateAvatarUser(avatar: image),
                      // );
                    },
                  );
                },
                child: Container(
                  width: 100.w,
                  alignment: Alignment.center,
                  child: Container(
                    height: 105.sp,
                    width: 105.sp,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: AppColors.deepBlue,
                        width: 3.sp,
                      ),
                    ),
                    alignment: Alignment.center,
                    child: Container(
                      height: 95.sp,
                      width: 95.sp,
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                      ),
                      child:
                      ClipRRect(
                        borderRadius: BorderRadius.circular(1000.sp),
                        child:     OctoImage(
                          image: NetworkImage(    BlocProvider.of<ProfileBloc>(context).user.profileImage),
                          fit: BoxFit.cover,
                          placeholderBuilder: (context) => getShimmer(),
                          errorBuilder: imageErrorBuilder,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 8.sp),
              //first_name

              Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  //Center Column contents vertically,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  //Center Column contents horizontally,
                  textDirection: TextDirection.ltr,
                  children: [
                    Text(
                      BlocProvider.of<ProfileBloc>(context).user.firstName,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontFamily: FontFamily.lato,
                          fontSize: 16.sp,
                          color: Theme.of(context).textTheme.bodyText2.color),
                    ),
                    SizedBox(width: 1.w),
                    Text(
                      BlocProvider.of<ProfileBloc>(context).user.lastName,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontFamily: FontFamily.lato,
                          fontSize: 16.sp,
                          color: Theme.of(context).textTheme.bodyText2.color),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 6.sp),
              Padding(
                padding: EdgeInsets.all(4.w),
                child: ListTile(
                  shape: RoundedRectangleBorder(
                    side: BorderSide(
                        width: 0.7.sp,
                        color: Theme.of(context).textTheme.bodyText2.color),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  title: Text(
                    "${AppLocalization.of(context).trans("phone_number")} :",
                    style: TextStyle(
                      fontFamily: FontFamily.lato,
                      fontSize: 16.sp,
                      color: Theme.of(context).textTheme.bodyText2.color,
                    ),
                  ),
                  subtitle: Directionality(
                    textDirection: TextDirection.ltr,
                    child: Row(
                      children: [
                        const Spacer(),
                        Text(
                          BlocProvider.of<ProfileBloc>(context).user.mSISDN,
                          style: TextStyle(
                            fontFamily: FontFamily.lato,
                            fontSize: 14.sp,
                            color: Theme.of(context).textTheme.bodyText2.color,
                          ),
                        ),
                      ],
                    ),
                  ),
                  trailing: Icon(Icons.phone_android_outlined),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(4.w),
                child: ListTile(
                  shape: RoundedRectangleBorder(
                    //<-- SEE HERE
                    side: BorderSide(
                        width: 0.7.sp,
                        color: Theme.of(context).textTheme.bodyText2.color),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  title: Text(
                    "${AppLocalization.of(context).trans("email")} :",
                    style: TextStyle(
                      fontFamily: FontFamily.lato,
                      fontSize: 16.sp,
                      color: Theme.of(context).textTheme.bodyText2.color,
                    ),
                  ),
                  subtitle: Text(
                    BlocProvider.of<ProfileBloc>(context).user.email,
                    style: TextStyle(
                      fontFamily: FontFamily.lato,
                      fontSize: 14.sp,
                      color: Theme.of(context).textTheme.bodyText2.color,
                    ),
                  ),
                  trailing: Icon(
                    Icons.email_outlined,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

Future<String> getPhoneNumber(String phoneNumber1) async {
  final phoneNumberUtil = PhoneNumberUtil();
  String nationalNumber = phoneNumber1;
  var phoneNumber = await phoneNumberUtil.parse(
    nationalNumber,
    regionCode: 'SY',
  );
  return phoneNumber.e164;
}

_buildTitle(title, icon, color) {
  return Container(
    padding: EdgeInsets.only(left: 12.sp, right: 4.sp),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            Icon(
              icon,
              size: 16.sp,
              color: color,
            ),
            SizedBox(width: 6.sp),
            Text(
              title,
              style: TextStyle(
                fontSize: 14.sp,
                fontWeight: FontWeight.w600,
                fontFamily: FontFamily.lato,
                color: color,
              ),
            ),
          ],
        ),
        IconButton(
          onPressed: () => null,
          icon: Icon(
            null,
            size: 18.sp,
          ),
        ),
      ],
    ),
  );
}
