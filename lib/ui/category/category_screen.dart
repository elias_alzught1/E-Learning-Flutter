// import 'dart:async';
//
// import 'package:e_learning/localization/app_localization.dart';
// import 'package:e_learning/themes/app_colors.dart';
// import 'package:e_learning/ui/category/widget/category_card.dart';
// import 'package:e_learning/utils/sizer_custom/sizer.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:pull_to_refresh/pull_to_refresh.dart';
//
// import '../../bloc/category_bloc/category_cubit.dart';
// import '../../helpers/errors/reloade_widget.dart';
// import '../../models/common/category_model.dart';
// import '../../themes/font_family.dart';
// import '../widgets/loader/inkdrop_loader.dart';
// import '../widgets/loader/staggered_dots_wave.dart';
// import '../widgets/textfield/DirectionalTextField.dart';
//
// class CategoryScreen extends StatefulWidget {
//   const CategoryScreen({Key key}) : super(key: key);
//
//   @override
//   State<CategoryScreen> createState() => _CategoryScreenState();
// }
//
// class _CategoryScreenState extends State<CategoryScreen> {
//   CategoryCubit _bloc;
//   ScrollController _scrollController;
//   TextEditingController t = new TextEditingController();
//
//   FocusNode myFocusNode;
//   bool isSearch;
//
//   @override
//   void initState() {
//     myFocusNode = FocusNode();
//     _bloc = BlocProvider.of<CategoryCubit>(context);
//     _scrollController = ScrollController();
//     _bloc.fetch();
//     isSearch = false;
//     myFocusNode.addListener(_onFocusChange);
//     // TODO: implement initState
//     super.initState();
//   }
//
//   @override
//   void dispose() {
//     // Clean up the focus node when the Form is disposed.
//     myFocusNode.removeListener(_onFocusChange);
//     myFocusNode.dispose();
//
//     super.dispose();
//   }
//
//   bool isEnglish;
//   bool isVisible = false;
//
//   void _onFocusChange() {
//     if (myFocusNode.hasFocus) {
//       setState(() {
//         isSearch = true;
//       });
//     } else {
//       setState(() {
//         isSearch = false;
//       });
//     }
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     isEnglish = Directionality.of(context) == TextDirection.ltr;
//     var size = MediaQuery.of(context).size;
//     final double itemHeight = (size.height) / 2;
//     final double itemWidth = size.width;
//
//     return Scaffold(
//       body: Column(
//         children: [
//           Container(
//             height: 160.0,
//             child: Stack(
//               children: <Widget>[
//                 Container(
//                   decoration: BoxDecoration(
//                       borderRadius: BorderRadius.circular(1.0),
//                       border: Theme.of(context).brightness == Brightness.light
//                           ? Border.all(
//                               color: Colors.grey.withOpacity(0.5), width: 1.0)
//                           : null,
//                       color: Colors.white),
//                   width: MediaQuery.of(context).size.width,
//                   height: 110.0,
//                   child: Container(
//                     color: Theme.of(context).brightness == Brightness.light
//                         ? Colors.transparent
//                         : colorBlack,
//                     child: Center(
//                       child: Text(
//                         AppLocalization.of(context).trans("categories"),
//                         style: TextStyle(
//                           fontFamily: FontFamily.lato,
//                           fontSize: 18.sp,
//                           fontWeight: FontWeight.w800,
//                           color: Theme.of(context).textTheme.bodyText2.color,
//                         ),
//                       ),
//                     ),
//                     //     Stack(
//                     //   children: [
//                     //     Center(
//                     //       child: Text(
//                     //         AppLocalization.of(context).trans("categories"),
//                     //         style: TextStyle(
//                     //           fontFamily: FontFamily.lato,
//                     //           fontSize: 18.sp,
//                     //           fontWeight: FontWeight.w800,
//                     //           color: Theme.of(context).textTheme.bodyText2.color,
//                     //         ),
//                     //       ),
//                     //     ),
//                     //     Positioned(
//                     //       top: 4.h,
//                     //       left: isEnglish ? 0 : 95.w,
//                     //       right: isEnglish ? 100.w : 0,
//                     //       child:
//                     //       Icon(
//                     //         Icons.arrow_back_outlined,
//                     //         color: bC,
//                     //
//                     //       ),
//                     //
//                     //     ),
//                     //   ],
//                     // )
//                   ),
//                 ),
//                 Positioned(
//                   top: 90.0,
//                   left: 0.0,
//                   right: 0.0,
//                   child: Container(
//                     padding: EdgeInsets.symmetric(horizontal: 20.0),
//                     child: DecoratedBox(
//                       decoration: BoxDecoration(
//                           borderRadius: BorderRadius.circular(1.0),
//                           border: Border.all(
//                               color: Colors.grey.withOpacity(0.5), width: 1.0),
//                           color: Colors.white),
//                       child: Row(
//                         children: [
//                           IconButton(
//                             icon: Icon(
//                               Icons.menu,
//                               color: bC,
//                             ),
//                             onPressed: () {
//                               print("your menu action here");
//                             },
//                           ),
//                           Expanded(
//                             child: InkWell(
//                               child: DirectionalTextField(
//                                   focusNode: myFocusNode,
//                                   onSubmit: (_) {
//                                     _bloc.applySearch(
//                                         _bloc.searchController.text.trim());
//                                   },
//                                   controller: _bloc.searchController,
//                                   fontSize: 15.sp,
//
//                                   //  focusNode: FocusScope.of(context),
//                                   fontWeight: FontWeight.w400,
//                                   keyboardType: TextInputType.text,
//
//                                   // inputFormatters: [phoneNumberFormatter],
//                                   decoration: InputDecoration(
//                                     floatingLabelBehavior:
//                                         FloatingLabelBehavior.auto,
//                                     contentPadding: const EdgeInsets.only(
//                                       left: 12.0,
//                                     ),
//                                     hintText: Directionality.of(context) ==
//                                             TextDirection.ltr
//                                         ? "Search"
//                                         : "ابحث هنا",
//                                     hintStyle: TextStyle(fontSize: 15.sp),
//                                     border: InputBorder.none,
//                                   )),
//                             ),
//                           ),
//                           IconButton(
//                             icon: isSearch
//                                 ? InkWell(
//                                     child: Icon(
//                                       Icons.close,
//                                       color: bC,
//                                     ),
//                                     onTap: () {
//                                       setState(() {
//                                         _bloc.searchController.clear();
//                                         _bloc.categoriesSearch.clear();
//                                         _bloc.pageSearch = 1;
//                                         myFocusNode.unfocus();
//                                       });
//                                     },
//                                   )
//                                 : Icon(
//                                     Icons.search,
//                                     color: bC,
//                                   ),
//                           ),
//                         ],
//                       ),
//                     ),
//                   ),
//                 )
//               ],
//             ),
//           ),
//           Expanded(
//             child: BlocBuilder<CategoryCubit, CategoryState>(
//                 builder: (context, state) {
//               if (state is CategoryAwaitState && state.isFirstFetch == true ||
//                   state is SearchAwaitState && state.isFirstFetch == true) {
//                 return const StaggeredDotsWave();
//               }
//               if (state is CategoryErrorState) {
//                 return Center();
//               } else if (state is SearchErrorState) {
//                 return Center(child: errorDialog(20, state.message));
//               }
//
//               return SmartRefresher(
//                   controller: _bloc.controller,
//                   onRefresh: () => _bloc.refresh(),
//                   onLoading: () => isSearch ? _bloc.search() : _bloc.fetch(),
//                   enablePullUp: true,
//                   enablePullDown: true,
//                   footer: ClassicFooter(
//                     onClick: () {
//                       if (_bloc.controller.footerStatus == LoadStatus.failed) {
//                         _bloc.controller.requestLoading();
//                       }
//                     },
//                   ),
//                   child: GridView.builder(
//                       controller: _scrollController,
//                       //   shrinkWrap: true,
//
//                       //   physics: const AlwaysScrollableScrollPhysics(),
//                       gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//                           crossAxisCount: 2,
//                           mainAxisSpacing: 4.w,
//                           crossAxisSpacing: 2.h),
//                       itemCount: isSearch
//                           ? context
//                               .read<CategoryCubit>()
//                               .categoriesSearch
//                               .length
//                           : context.read<CategoryCubit>().categories.length,
//                       itemBuilder: (context, index) {
//                         return CategoryCard(isSearch
//                             ? context
//                                 .read<CategoryCubit>()
//                                 .categoriesSearch[index]
//                             : context.read<CategoryCubit>().categories[index]);
//                       }));
//             }),
//           )
//         ],
//       ),
//     );
//   }
//
//   Widget errorDialog(double size, String messageError) {
//     return SizedBox(
//       height: 180,
//       width: 200,
//       child: Column(
//         mainAxisAlignment: MainAxisAlignment.center,
//         children: [
//           Text(
//             messageError,
//             style: TextStyle(
//               color: Theme.of(context).textTheme.bodyText1.color,
//               fontSize: 12.sp,
//               fontWeight: FontWeight.w600,
//             ),
//           ),
//           const SizedBox(
//             height: 10,
//           ),
//           ElevatedButton(
//               onPressed: () {
//                 _bloc.fetch();
//               },
//               style: ElevatedButton.styleFrom(backgroundColor: bC),
//               child: Text(
//                 AppLocalization.of(context).trans("retry"),
//                 style: TextStyle(fontSize: 15.sp, color: mC),
//               )),
//         ],
//       ),
//     );
//   }
// }
