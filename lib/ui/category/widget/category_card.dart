import 'package:e_learning/models/common/category_model.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';

import '../../../localization/app_localization.dart';
import '../../../themes/font_family.dart';

class CategoryCard extends StatefulWidget {
  final Category category;

  const CategoryCard(this.category, {Key key}) : super(key: key);

  @override
  State<CategoryCard> createState() => _CategoryCardState();
}

class _CategoryCardState extends State<CategoryCard> {
  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(

        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(10),
              topRight: Radius.circular(10),
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10)),
          //     border: Border.all(color: Colors.blue.shade200),
          boxShadow: [
            Theme.of(context).brightness == Brightness.light
                ? BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 2,
              blurRadius: 2,
              offset:  Offset(
                  0, 3), // changes position of shadow
            )
                : BoxShadow(
              color: Colors.grey.withOpacity(0.5),
            ),
          ],
        ),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 5.h),
                child: const Icon(
                  Icons.computer_outlined,
                  size: 30,
                  color: Colors.blue,
                ),
              ),
           Padding(
             padding:  EdgeInsets.only(top: 5.h),
             child: Center(
                      child: Text(

                          widget.category.nameEn,
                          maxLines: 1,
                          style: TextStyle(
                            color: Colors.black,
                            fontFamily: FontFamily.lato,
                            fontSize: 13.sp,
                            fontWeight: FontWeight.bold,
                          )),
             ),
           )
            ]),
      ),
    );
  }
}
