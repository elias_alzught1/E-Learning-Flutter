import 'package:e_learning/localization/app_localization.dart';
import 'package:e_learning/themes/app_colors.dart';
import 'package:e_learning/themes/app_text_style.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class EmptyContent extends StatelessWidget {
  const EmptyContent({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
          color: Colors.white.withOpacity(0.5),
          spreadRadius: 5,
          blurRadius: 7,
          offset: Offset(0, 3), // changes position of shadow
        ),
      ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: 3.h,
          ),
          SvgPicture.asset(
            'assets/img/svg/empty-content.svg',
            width: 15.h,
            height: 15.h,
          ),
          SizedBox(
            height: 2.h,
          ),
          Text(
            AppLocalization.of(context).trans("empty_content"),
            style: AppTextStyle.largeRedBold,
          )
        ],
      ),
    );
  }
}
