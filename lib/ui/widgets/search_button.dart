
import 'package:e_learning/ui/widgets/custom_search_delegate.dart';
import 'package:flutter/material.dart';

import '../../models/common/course_model.dart';

class SearchButton extends StatelessWidget {
  final List<Courses> courses;

  const SearchButton({Key key, this.courses}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.search,color: Colors.white),
      onPressed: () async {
        final selectedResult = await showSearch(
          context: context,
          delegate: CustomSearchDelegate(courses),
        );

        // Do something with the selected result if needed
        if (selectedResult != null) {
          // Handle the selected result
          print("Selected result: $selectedResult");
        }
      },
    );
  }
}