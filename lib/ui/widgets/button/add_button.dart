import 'package:e_learning/localization/app_localization.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';

import '../../../themes/app_colors.dart';
import '../../../themes/app_text_style.dart';

class AddButton extends StatelessWidget {
  const AddButton({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: AppColors.normalGreen.withOpacity(0.6),
          borderRadius: BorderRadius.circular(8)),
      margin: EdgeInsetsDirectional.only(start: 4.w, end: 4.w),
      width: MediaQuery.of(context).size.width,
      height: 6.h,
      child: Center(
        child: Text(
          AppLocalization.instance.trans("add"),
          style: AppTextStyle.mediumWhiteBold,
        ),
      ),
    );
  }
}
