import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';

class PaginationButton extends StatelessWidget {
  final String pageNumber;
  final VoidCallback onLoadMore;
  final VoidCallback onLoadLess;
  final Color color;

  const PaginationButton(
      {Key key, this.pageNumber, this.onLoadMore, this.onLoadLess, this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Spacer(),
          Container(
            width: 10.w,
            height: 10.w,
            decoration: BoxDecoration(
                border: Border.all(color: color),
                borderRadius: BorderRadius.circular(5)),
            child: Center(
                child: Padding(
              padding: EdgeInsetsDirectional.only(start: 0.5.w),
              child: IconButton(
                  icon: Icon(Icons.arrow_back_ios, color: color, size: 20),
                onPressed: () {
                  onLoadLess();
                },),
            )),
          ),
          const SizedBox(
            width: 2,
          ),
          Container(
            width: 10.w,
            height: 10.w,
            decoration: BoxDecoration(
                border: Border.all(color: color),
                borderRadius: BorderRadius.circular(5)),
            child:
                Center(child: Text(pageNumber, style: TextStyle(color: color))),
          ),
          const SizedBox(
            width: 2,
          ),
          Container(
            width: 10.w,
            height: 10.w,
            decoration: BoxDecoration(
                border: Border.all(color: color),
                borderRadius: BorderRadius.circular(5)),
            child: Center(
                child: IconButton(
              onPressed: () {
                onLoadMore();
              },
              icon: Icon(Icons.arrow_forward_ios, color: color, size: 20),
            )),
          ),
          Spacer(),
        ],
      ),
    );
  }
}
