
import 'package:flutter/material.dart';

import '../../../themes/app_colors.dart';

class CancelButton extends StatelessWidget {
  final String text;
  final VoidCallback onPressed;
  final Color color;

  const CancelButton({
    Key key,
    this.onPressed,
    this.text,
    this.color = AppColors.gray,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
        elevation: 0,
        shape: RoundedRectangleBorder(
          side: BorderSide(color: color),
          borderRadius: BorderRadius.circular(10),
        ),
        primary: Theme.of(context).scaffoldBackgroundColor,
        minimumSize: Size(MediaQuery.of(context).size.width * 0.25, 0.0),
      ),
      child: Text(
        text,
        style: Theme.of(context)
            .textTheme
            .bodyText1
            .copyWith(color: color),
      ),
    );
  }
}
