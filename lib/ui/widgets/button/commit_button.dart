
import 'package:flutter/material.dart';

import '../../../themes/app_colors.dart';

class CommitButton extends StatelessWidget {
  final String text;
  final VoidCallback onPressed;
  final Color backgroundColor;
  final TextStyle textStyle;
  const CommitButton({
    this.backgroundColor,
    Key key,
    this.onPressed,
    this.text, this.textStyle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed:  onPressed,
      style: ElevatedButton.styleFrom(
        backgroundColor:backgroundColor ?? AppColors.blueAccent,
        elevation: 0,
        minimumSize: Size(MediaQuery.of(context).size.width * 0.25, 0.0),
      ),
      child: Text(
        text,
        style: textStyle??Theme.of(context)
            .textTheme
            .bodyText1
            .copyWith(color: AppColors.white),
      ),
    );
  }
}
