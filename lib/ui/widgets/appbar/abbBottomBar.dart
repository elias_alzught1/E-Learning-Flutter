
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../localization/app_localization.dart';
import '../../../themes/app_text_style.dart';

class AppBottomBar extends StatelessWidget {
  const AppBottomBar({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Container(
      width: 100.w,
      height: 10.h,
      color: Colors.black,
      child: Padding(
        padding: EdgeInsetsDirectional.only(
            start: 2.w, end: 2.w),
        child: Row(children: [
          SvgPicture.asset(
            'assets/img/svg/logo_icon.svg',
            width: 15.sp,
            height: 15.sp,
          ),
          SizedBox(
            width: 10.sp,
          ),
          Text(
            AppLocalization.of(context).trans("e_learning"),
            style: AppTextStyle.mediumWhiteBold,
          ),
          Spacer(),
          Text(
            "2023 © E-Learing",
            style: AppTextStyle.mediumWhiteBold,
          )
        ]),
      ),
    );
  }
}
