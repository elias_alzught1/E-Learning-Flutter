import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:e_learning/bloc/search_bloc/search_cubit.dart';
import 'package:e_learning/bloc/teacher_cubit/teacher_cubit.dart';
import 'package:e_learning/localization/app_localization.dart';
import 'package:e_learning/models/common/language.dart';
import 'package:e_learning/ui/course/cart_page.dart';
import 'package:e_learning/ui/course/serach_page.dart';
import 'package:e_learning/ui/teacher/my_course.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:textfield_search/textfield_search.dart';
import '../../../bloc/application_bloc/application_cubit.dart';
import '../../../bloc/authentication/login_bloc/authentication_bloc.dart';
import '../../../bloc/authentication/login_bloc/authentication_event.dart';
import '../../../bloc/course_bloc/course_bloc.dart';
import '../../../bloc/teacher/add_course/add_course_bloc.dart';
import '../../../bloc/wish_list_cubit/wish_list_cubit.dart';
import '../../../controller/data_store.dart';
import '../../../models/common/course_model.dart';
import '../../../themes/app_colors.dart';
import '../../../themes/app_font_size.dart';
import '../../../themes/app_text_style.dart';
import '../../../themes/elearning_icons.dart';
import '../../../themes/font_family.dart';
import '../../course/my_library.dart';
import '../../course/wishlist_course.dart';
import '../../profile/user_pfrofile.dart';
import 'package:blurrycontainer/blurrycontainer.dart';

import '../../teacher/add_course.dart';
import '../search_button.dart';

class AppBarApp extends StatefulWidget implements PreferredSizeWidget {
  const AppBarApp({Key key}) : super(key: key);

  @override
  State<AppBarApp> createState() => _AppBarAppState();

  @override
  // TODO: implement preferredSize
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

class _AppBarAppState extends State<AppBarApp> {
  Language local;
  TextEditingController myController = TextEditingController();
  bool isSreach = false;
  String _selectedResult = "";

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black,
      child: Padding(
        padding: EdgeInsets.only(top: 25.sp, bottom: 5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: EdgeInsetsDirectional.only(top: 4.sp, start: 10.sp),
              child: SvgPicture.asset(
                'assets/img/svg/logo_icon.svg',
                width: 18.sp,
                height: 18.sp,
              ),
            ),
            const SizedBox(
              width: 5,
            ),
            // SearchButton(courses: [],),
            Flexible(
                child: GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                  CupertinoPageRoute(
                    builder: (context) => BlocProvider(
                      create:   (context) =>  SearchCubit(),
                      child:  SearchPage(),
                    ),
                  ),
                );

              },
              child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: AppColors.deepGray.withOpacity(0.8),
                      border: Border.all(color: AppColors.purple),
                      borderRadius: BorderRadius.circular(20)),
                  margin: EdgeInsetsDirectional.only(start: 1.w, end: 1.w),
                  height: 30.sp,
                  child: Padding(
                    child:   Row(children: [
                      Text(
                        AppLocalization.of(context).trans("search"),
                        style: AppTextStyle.xSmallWhiteBold,
                      ),
                      Spacer(),
                      Icon(Icons.search, color: Colors.white, size: 18)
                    ]),
                    padding: EdgeInsetsDirectional.only(start: 3.w, end: 3.w),
                  )),
            )),

            const SizedBox(
              width: 8,
            ),

            DropdownButtonHideUnderline(
              child: DropdownButton2<Language>(
                hint: Row(
                  children: [
                    Container(
                      margin: EdgeInsetsDirectional.only(bottom: 2.sp),
                      height: 25.sp,
                      width: 25.sp,
                      decoration: BoxDecoration(
                        color: AppColors.white,
                        border: Border.all(
                          color: AppColors.normalBlue.withOpacity(0.2),
                          width: 1.8.sp,
                        ),
                        shape: BoxShape.circle,
                      ),
                      child: Center(
                        child: Text(
                          DataStore.instance.lang,
                          style: const TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: AppColors.normalBlue,
                          ),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ),
                  ],
                ),
                dropdownStyleData: DropdownStyleData(
                  maxHeight: 200,
                  width: MediaQuery.of(context).size.width / 2,
                  padding: const EdgeInsetsDirectional.only(top: 0),
                  elevation: 8,
                ),
                value: local,
                menuItemStyleData: MenuItemStyleData(
                  height: MediaQuery.of(context).size.height / 20,
                ),
                buttonStyleData: ButtonStyleData(
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height / 95),
                ),
                iconStyleData: const IconStyleData(
                  icon: Icon(
                    Icons.arrow_forward_ios_outlined,
                    size: 0.0,
                  ),
                ),
                selectedItemBuilder: (BuildContext context) {
                  return Language.languageList().map((Language item) {
                    return Container(
                      alignment: Alignment.center,
                      child: Text(
                        item.languageCode,
                      ),
                    );
                  }).toList();
                },
                style: const TextStyle(color: Colors.black),
                onChanged: (value) async {
                  setState(() async {
                    await BlocProvider.of<ApplicationCubit>(context)
                        .changeLanguage(value.languageCode);
                  });
                },
                items: Language.languageList().map((Language items) {
                  return DropdownMenuItem(
                    value: items,
                    child: Row(children: <Widget>[
                      Text(items.flag),
                      const Spacer(),
                      Text(
                        items.name,
                        style: AppTextStyle.smallGreenBold,
                      ),
                    ]),
                  );
                }).toList(),
              ),
            ),

            const SizedBox(
              width: 15,
            ),
            Container(
              margin: EdgeInsetsDirectional.only(end: 2.w, top: 5.sp),
              child: DropdownButtonHideUnderline(
                child: DropdownButton2(
                  customButton: Container(
                    height: 24.sp,
                    width: 24.sp,
                    decoration: BoxDecoration(
                      color: AppColors.gray,
                      border: Border.all(
                        color: AppColors.white,
                      ),
                      shape: BoxShape.circle,
                    ),
                    child: Center(
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(12.sp),
                          child: Icon(
                            Icons.person,
                            color: AppColors.black,
                            size: 18.sp,
                          )),
                    ),
                  ),
                  items: [
                    ...MenuItems.firstItems.map(
                      (item) => DropdownMenuItem<MenuItem>(
                        value: item,
                        child: MenuItems.buildItem(item),
                      ),
                    ),
                    const DropdownMenuItem<Divider>(
                        enabled: false, child: Divider()),
                    ...MenuItems.secondItems.map(
                      (item) => DropdownMenuItem<MenuItem>(
                        value: item,
                        child: MenuItems.buildItem(item),
                      ),
                    ),
                  ],
                  onChanged: (value) {
                    MenuItems.onChanged(context, value as MenuItem);
                  },
                  dropdownStyleData: DropdownStyleData(
                    width: 160,
                    padding: const EdgeInsets.symmetric(vertical: 6),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: Colors.black.withOpacity(0.99),
                    ),
                    offset: const Offset(0, 8),
                  ),
                  menuItemStyleData: MenuItemStyleData(
                    customHeights: [
                      ...List<double>.filled(MenuItems.firstItems.length, 48),
                      8,
                      ...List<double>.filled(MenuItems.secondItems.length, 48),
                    ],
                    padding: const EdgeInsets.only(left: 16, right: 16),
                  ),
                ),
              ),
            ),

            // InkWell(
            //   child: Container(
            //     margin: EdgeInsetsDirectional.only(top: 10.sp,end: 2.w),
            //     child: _buildItemBottomAccount(
            //         BlocProvider.of<ProfileBloc>(context)
            //             .user
            //             .profileImage ??
            //             "",
            //         "",
            //         4),
            //   ),onTap: (){
            //
            //     //UserProfile
            // },
            // )
          ],
        ),
      ),
    );
  }

  Widget _buildItemBottomAccount(
    urlToImage,
    blurHash,
    index,
  ) {
    return Expanded(
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => const UserProfile(),
            ),
          );
        },
        child: Container(
          color: Colors.transparent,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 24.sp,
                    width: 24.sp,
                    decoration: BoxDecoration(
                      color: AppColors.lighterGray,
                      border: Border.all(
                        color: AppColors.deepBlue.withOpacity(0.2),
                        width: 1.8.sp,
                      ),
                      shape: BoxShape.circle,
                    ),
                    child: Center(
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(12.sp),
                          child: Icon(
                            Icons.person,
                            color: AppColors.deepGray,
                            size: 18.sp,
                          )),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 2.5.sp),
              Container(
                height: 4.sp,
                width: 4.sp,
                decoration: BoxDecoration(
                  color: index == 2 ? AppColors.deepBlue : Colors.transparent,
                  shape: BoxShape.circle,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class MenuItem {
  const MenuItem({
    this.color,
    this.text,
    this.icon,
  });

  final String text;
  final IconData icon;
  final Color color;
}
//MyLibrary
abstract class MenuItems {
  static const List<MenuItem> firstItems = [
    profile,
    addCourse,
    myCourse,
    favorit,
    myLibrary,
    cart,

  ];
  static const List<MenuItem> secondItems = [logout];

  static const profile = MenuItem(
      text: "My profile", icon: Icons.person, color: Color(0xFFD7195B));
  static const addCourse = MenuItem(
      text: "Add Course",
      icon: Icons.add_box_outlined,
      color: Color(0xFFD7B719));
  static const myCourse = MenuItem(
      text: "My Course",
      icon: Elearning.graduation_hat,
      color: Color(0xFF19D765));
  static const favorit =
      MenuItem(text: "WishList", icon: Icons.favorite, color: Colors.red);
  static const cart =
      MenuItem(text: "cart", icon: Elearning.basket, color: AppColors.normalBlue);
  static const logout =
      MenuItem(text: 'Log Out', icon: Icons.logout, color: Color(0xFF6E19D7));
  static const myLibrary = MenuItem(text: 'my Library', icon: Elearning.book, color: Color(
      0xFF77B4F6));

  static Widget buildItem(MenuItem item) {
    return Row(
      children: [
        Icon(item.icon, color: item.color, size: 22),
        const SizedBox(
          width: 10,
        ),
        Expanded(
          child: Text(item.text, style: AppTextStyle.mediumWhiteBold),
        ),
      ],
    );
  }

  static void onChanged(BuildContext context, MenuItem item) {
    switch (item) {
      case MenuItems.profile:
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => UserProfile(),
          ),
        );
        //Do something
        break;
      case MenuItems.addCourse:
        Navigator.of(context).push(
          CupertinoPageRoute(
            builder: (context) => BlocProvider(
              create: (context) => AddCourseBloc(context),
              child: AddCourse(),
            ),
          ),
        );
        //Do something
        break;
      case MenuItems.myCourse:
        Navigator.of(context).push(
          CupertinoPageRoute(
            builder: (context) => MultiBlocProvider(
              providers: [
                BlocProvider<TeacherCubit>(
                  create: (BuildContext context) => TeacherCubit(),
                ),
              ],
              child: MyCourse(),
            ),
          ),
        );
        //Do something
        break;
      case MenuItems.favorit:
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) =>const WishListCourse(),
          ),
        );

        //Do something
        break;
      case MenuItems.cart:
        Navigator.of(context).push(
          MaterialPageRoute(builder: (context) => const CartPage(),)
        );
        break;
      case MenuItems.myLibrary:
        Navigator.of(context).push(
          CupertinoPageRoute(
            builder: (context) => BlocProvider(
               create:   (context) =>  WishListCubit(),
              child:  MyLibrary(),
            ),
          ),
        );

        break;
      case MenuItems.logout:
        BlocProvider.of<AuthBloc>(context).add(LogOutEvent());
        //Do something
        break;
    }
  }
}
