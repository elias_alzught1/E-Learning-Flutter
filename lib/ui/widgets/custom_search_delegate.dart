import 'package:flutter/material.dart';

import '../../models/common/course_model.dart';

class CustomSearchDelegate extends SearchDelegate<String> {
  final List<Courses> courses;

  CustomSearchDelegate(this.courses);

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    // Implement how to display search results here
    final searchResults = courses
        .where((item) => item.name.toLowerCase().contains(query.toLowerCase()));

    return ListView(
      children: searchResults
          .map((result) => ListTile(
                title: Text(result.name),
                onTap: () {
                  close(context, result.name);
                },
              ))
          .toList(),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final suggestionList = courses
        .where((item) => item.name.toLowerCase().contains(query.toLowerCase()));

    return ListView(
      children: suggestionList
          .map((suggestion) => ListTile(
                title: Text(suggestion.name),
                onTap: () {
                  query = suggestion.name;
                  showResults(context);
                },
              ))
          .toList(),
    );
  }
}
