import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart' as intl;

import '../../../config/application.dart';

class DirectionalTextField extends StatefulWidget {
  final FocusNode focusNode;
  final TextEditingController controller;
  final TextInputType keyboardType;
  final bool obscureText;
  final InputDecoration decoration;
  final List<TextInputFormatter> inputFormatters;
  final Color colorTitle;
  final double fontSize;
  final FontWeight fontWeight;
  final Function(String value) onSubmit;
  final double borderCircular;
  final bool readOnly;

  const DirectionalTextField({
    Key key,
    this.focusNode,
    this.controller,
    this.keyboardType,
    this.obscureText = false,
    this.decoration,
    this.inputFormatters,
    this.colorTitle,
    this.fontSize,
    this.fontWeight,
    this.onSubmit,
  this.readOnly = false,
    this.borderCircular,
  }) : super(key: key);

  @override
  _DirectionalTextFieldState createState() => _DirectionalTextFieldState();
}

class _DirectionalTextFieldState extends State<DirectionalTextField> {
  TextDirection _direction;
  RegExp _exp = RegExp('^[0-9]');

  bool isLTR(String value) {
    return intl.Bidi.startsWithLtr(value) || _exp.hasMatch(value);
  }

  @override
  void initState() {
    if (widget.controller.text.isNotEmpty) {
      var _isLTR = intl.Bidi.startsWithLtr(widget.controller.text);
      _direction = _isLTR ? TextDirection.ltr : TextDirection.rtl;
    } else {
      _direction = Directionality.of(Application.instance.context);
    }
    widget.controller.addListener(_listener);
    super.initState();
  }

  @override
  void didUpdateWidget(covariant DirectionalTextField oldWidget) {
    if (widget.controller.text.isEmpty) {
      _direction = Directionality.of(Application.instance.context);
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    widget.controller.removeListener(_listener);
    super.dispose();
  }

  void _listener() {
    if (!mounted) return;
    if (widget.controller.text.isEmpty) {
      _direction = Directionality.of(Application.instance.context);
    } else {
      var _isLTR = isLTR(widget.controller.text);
      if (_direction == TextDirection.rtl && _isLTR) {
        setState(() => _direction = TextDirection.ltr);
      } else if (_direction == TextDirection.ltr && !_isLTR) {
        setState(() => _direction = TextDirection.rtl);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    /*
    TextStyle(
          color: Theme.of(context).textTheme.bodyText1!.color!.withOpacity(.95),
          fontSize: 11.sp,
          fontWeight: FontWeight.w500,
        ),
        inputFormatters: [
          FilteringTextInputFormatter.singleLineFormatter,
        ],
        validator: (val) {
          if (title == 'Email') {
            return !GetUtils.isEmail(val!.trim()) ? valid : null;
          } else {
            return val!.trim().length < 6 ? valid : null;
          }
        },
        onChanged: (val) {
          if (title == 'Email') {
            email = val.trim();
          } else {
            password = val.trim();
          }
        },
        obscureText: title == 'Mật khẩu' ? hidePassword : false,
        decoration: InputDecoration(
          floatingLabelBehavior: FloatingLabelBehavior.always,
          contentPadding: EdgeInsets.only(
            left: 12.0,
          ),
          border: InputBorder.none,
          labelText: title,
          labelStyle: TextStyle(
            color:
                Theme.of(context).textTheme.bodyText1!.color!.withOpacity(.8),
            fontSize: 11.sp,
            fontWeight: FontWeight.w500,
          ),
          suffixIcon: title == 'Mật khẩu'
              ? IconButton(
                  onPressed: () {
                    setState(() {
                      hidePassword = !hidePassword;
                    });
                  },
                  icon: Icon(
                    hidePassword ? PhosphorIcons.eyeSlash : PhosphorIcons.eye,
                    color: colorPrimary,
                    size: 16.sp,
                  ),
                )
              : null,
        ),
      ),
    */
    return TextField(
      readOnly: widget.readOnly,
      style: TextStyle(
        color: Theme.of(context).textTheme.bodyText1.color.withOpacity(.95),
        fontSize: widget.fontSize ?? 11.sp,
        fontWeight: widget.fontWeight ?? FontWeight.w500,
      ),
      cursorColor: widget.colorTitle,
      focusNode: widget.focusNode,
      controller: widget.controller,
      keyboardType: widget.keyboardType,
      decoration: widget.decoration,
      inputFormatters: widget.inputFormatters,
      textDirection: _direction,
      onSubmitted: widget.onSubmit,
    );
  }
}
