
import 'package:flutter/material.dart';

import '../../themes/app_colors.dart';

class CommitButton extends StatelessWidget {
  final String text;
  final VoidCallback onPressed;

  const CommitButton({
    Key key,
    this.onPressed,
    this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
        elevation: 0,
        minimumSize: Size(MediaQuery.of(context).size.width * 0.25, 0.0),
      ),
      child: Text(
        text,
        style: Theme.of(context)
            .textTheme
            .bodyText1
            .copyWith(color: AppColors.white),
      ),
    );
  }
}
