import 'package:e_learning/themes/app_colors.dart';
import 'package:e_learning/themes/elearning_icons.dart';
import 'package:flutter/material.dart';

Widget buildStar(BuildContext context, int index, double rating,
    {double size}) {
  Icon icon;
  if (index >= rating) {
    icon = Icon(
      Elearning.star_1,
      color: AppColors.gray.withOpacity(0.3),
      size: size != null ? size : null,
    );
  } else if (index > rating - 1 && index < rating) {
    icon = Icon(Elearning.star_half_alt,
        color: AppColors.yellowAccent, size: size != null ? size : null);
  } else {
    icon = Icon(
      Elearning.star_1,
      color: AppColors.yellowAccent,
      size: size != null ? size : null,
    );
  }
  return InkResponse(
    onTap: null,
    child: Padding(
        padding: EdgeInsetsDirectional.only(start: 1, end: 0.5), child: icon),
  );
}
Widget buildRotateWidget(Widget widget){
  return Transform(
    alignment: Alignment.center,
    transform: Matrix4.identity()
      ..scale(-1.0, 1.0),
    // Rotate by 180 degrees (pi radians)
    child: widget,
  );
}