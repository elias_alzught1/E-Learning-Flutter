import 'package:e_learning/bloc/level_cubit/level_cubit.dart';
import 'package:e_learning/localization/app_localization.dart';

import 'package:e_learning/models/response/durations_response.dart';
import 'package:e_learning/themes/app_text_style.dart';
import 'package:e_learning/themes/font_family.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';
import '../../../bloc/category_bloc/category_cubit.dart';
import '../../../bloc/course_bloc/course_bloc.dart';
import '../../../bloc/duration_cubit/duration_cubit.dart';
import '../../../models/common/category_model.dart';
import '../../../models/request/filter_request.dart';
import '../../../models/response/levels_response.dart';
import '../../../themes/app_colors.dart';
import '../../function/function.dart';
import '../widget.dart';
class MyBottomSheet extends StatefulWidget {
  @override
  State<MyBottomSheet> createState() => _MyBottomSheetState();
}

class _MyBottomSheetState extends State<MyBottomSheet> {
  CategoryCubit _bloc;
  List<String> selectedDataString;
  String selectedString;
  RangeValues _currentRangeValues = const RangeValues(0, 100);
  List<int> selectedCategories = [];
  List<int> selectedLevels = [];
  List<int> selectedDurations = [];
  List<double> selectedPriceRange = [];
  @override
  void initState() {
    _bloc = BlocProvider.of<CategoryCubit>(context);
    // TODO: implement initState
    super.initState();
  }
  Widget build(BuildContext context) {
    final filterController = TextEditingController();
    final courseBloc = BlocProvider.of<CourseBloc>(context);

    return   SizedBox(
      height: MediaQuery.of(context).size.height / 1.3,
      child: Material(
        child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 4),
                  child: Center(
                    child: Text(
                      AppLocalization.of(context).trans("Add_Filters"),
                      style: AppTextStyle.xlargeBlueNormal,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsetsDirectional.only(start: 10.sp, top: 4.sp),
                  child: Row(
                    children: [
                      checkIsEnglish()
                          ? SvgPicture.asset(
                        "assets/img/svg/arrow_filter_tag.svg",
                        width: 4.w,
                        height: 2.h,
                        color: Colors.blue,
                      )
                          : buildRotateWidget(SvgPicture.asset(
                        "assets/img/svg/arrow_filter_tag.svg",
                        width: 4.w,
                        height: 2.h,
                        color: Colors.blue,
                      )),
                      SizedBox(
                        width: 2.sp,
                      ),
                      InkWell(
                        child: Text(
                          AppLocalization.of(context).trans("categories"),
                          style: AppTextStyle.xLargeBlack,
                        ),
                        onTap: () {},
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsetsDirectional.only(end: 10.sp, start: 10.sp),
                  child: MultiSelectDialogField<Category>(
                    items: _bloc.categories
                        .map((e) => MultiSelectItem(e, e.name))
                        .toList(),
                    title: Text(
                      AppLocalization.of(context).trans("categories"),
                      style: AppTextStyle.largeBlackBold,
                    ),
                    selectedColor: Colors.blue,
                    decoration: BoxDecoration(
                      color: AppColors.white,
                      borderRadius: const BorderRadius.all(Radius.circular(12)),
                      border: Border.all(
                        color: AppColors.gray,
                        width: 1,
                      ),
                    ),
                    buttonIcon: const Icon(
                      Icons.keyboard_arrow_down,
                      color: AppColors.gray,
                    ),
                    buttonText: Text(
                      AppLocalization.of(context).trans("select_categories"),
                      style: AppTextStyle.smallDeepGray,
                    ),
                    onConfirm: (results) {
                      selectedCategories.clear();
                      for (int i = 0; i < results.length; i++) {
                        selectedCategories.add(results[i].id);
                      }
                      //_selectedAnimals = results;
                    },
                  ),
                ),
                SizedBox(
                  height: 15.sp,
                ),
                const Divider(
                  color: AppColors.gray,
                ),
                SizedBox(
                  height: 5.sp,
                ),
                Padding(
                  padding: EdgeInsetsDirectional.only(start: 10.sp, top: 4.sp),
                  child: Row(
                    children: [
                      checkIsEnglish()
                          ? SvgPicture.asset(
                        "assets/img/svg/arrow_filter_tag.svg",
                        width: 4.w,
                        height: 2.h,
                        color: Colors.blue,
                      )
                          : buildRotateWidget(SvgPicture.asset(
                        "assets/img/svg/arrow_filter_tag.svg",
                        width: 4.w,
                        height: 2.h,
                        color: Colors.blue,
                      )),
                      SizedBox(
                        width: 2.sp,
                      ),
                      Text(
                        AppLocalization.of(context).trans("Level"),
                        style: AppTextStyle.xLargeBlack,
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 15.sp,
                ),
                Padding(
                  padding: EdgeInsetsDirectional.only(end: 10.sp, start: 10.sp),
                  child: MultiSelectDialogField<Level>(
                    items: BlocProvider.of<LevelCubit>(context)
                        .levels
                        .map((e) => MultiSelectItem(e, e.levelEn))
                        .toList(),
                    title: Text(
                      AppLocalization.of(context).trans("Level"),
                      style: AppTextStyle.largeBlackBold,
                    ),
                    selectedColor: Colors.blue,
                    decoration: BoxDecoration(
                      color: AppColors.white,
                      borderRadius: const BorderRadius.all(Radius.circular(12)),
                      border: Border.all(
                        color: AppColors.gray,
                        width: 1,
                      ),
                    ),
                    buttonIcon: const Icon(
                      Icons.keyboard_arrow_down,
                      color: AppColors.gray,
                    ),
                    buttonText: Text(
                      AppLocalization.of(context).trans("select_levels"),
                      style: AppTextStyle.smallDeepGray,
                    ),
                    onConfirm: (results) {
                      if(selectedLevels.isNotEmpty)  selectedLevels.clear();
                      for (int i = 0; i < results.length; i++) {
                        selectedLevels.add(results[i].id);
                      }
                    },
                  ),
                ),
                SizedBox(
                  height: 15.sp,
                ),
                const Divider(
                  color: AppColors.gray,
                ),
                SizedBox(
                  height: 15.sp,
                ),
                Padding(
                  padding: EdgeInsetsDirectional.only(start: 10.sp, top: 4.sp),
                  child: Row(
                    children: [
                      checkIsEnglish()
                          ? SvgPicture.asset(
                        "assets/img/svg/arrow_filter_tag.svg",
                        width: 4.w,
                        height: 2.h,
                        color: Colors.blue,
                      )
                          : buildRotateWidget(SvgPicture.asset(
                        "assets/img/svg/arrow_filter_tag.svg",
                        width: 4.w,
                        height: 2.h,
                        color: Colors.blue,
                      )),
                      SizedBox(
                        width: 2.sp,
                      ),
                      Text(
                        AppLocalization.of(context).trans("price"),
                        style: AppTextStyle.xLargeBlack,
                      ),
                    ],
                  ),
                ),
                Column(
                  children: [
                    SliderTheme(
                      data: const SliderThemeData(
                        trackHeight: 2,
                        rangeThumbShape:
                        RoundRangeSliderThumbShape(enabledThumbRadius: 7),
                      ),
                      child: RangeSlider(
                        values: _currentRangeValues,
                        max: 100,
                        activeColor: Colors.blue,
                        inactiveColor: AppColors.deepBlue.withOpacity(0.2),
                        divisions: 5,
                        labels: RangeLabels(
                          _currentRangeValues.start.round().toString(),
                          _currentRangeValues.end.round().toString(),
                        ),
                        onChanged: (RangeValues values) {
                          setState(() {
                            _currentRangeValues = values;
                            // if(selectedPriceRange.isNotEmpty)  selectedPriceRange.clear();
                            //   selectedPriceRange.add(
                            //       _currentRangeValues.start  );
                            //   selectedPriceRange
                            //       .add( _currentRangeValues.end );
                          });
                        },
                      ),
                    ),
                    Directionality.of(context) == TextDirection.ltr
                        ? Padding(
                      padding:
                      EdgeInsetsDirectional.only(start: 4.w, end: 4.w),
                      child: Container(
                        alignment: Alignment.center,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text(
                              "Min",
                              style: TextStyle(
                                  fontSize: 17,
                                  color: AppColors.black,
                                  fontWeight: FontWeight.w300,
                                  fontFamily: FontFamily.cairo),
                            ),
                            SizedBox(
                              width: 10.sp,
                            ),
                            Container(
                              alignment: AlignmentDirectional.centerStart,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                  border: Border.all(
                                      color: AppColors.lighterGray),
                                  color: Colors.white),
                              width: 70.w,
                              height: 40,
                              child: Padding(
                                padding:
                                EdgeInsetsDirectional.only(start: 10.sp),
                                child: Text(
                                  _currentRangeValues.start.toString(),
                                  style: const TextStyle(
                                      color: AppColors.gray,
                                      fontWeight: FontWeight.w300,
                                      fontFamily: FontFamily.cairo),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                        : Padding(
                      padding:
                      EdgeInsetsDirectional.only(start: 4.w, end: 4.w),
                      child: Container(
                        alignment: Alignment.center,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              alignment: AlignmentDirectional.centerStart,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                  border: Border.all(
                                      color: AppColors.lighterGray),
                                  color: Colors.white),
                              width: 70.w,
                              height: 40,
                              child: Padding(
                                padding:
                                EdgeInsetsDirectional.only(start: 10.sp),
                                child: Text(
                                  _currentRangeValues.start.toString(),
                                  style: const TextStyle(
                                      color: AppColors.gray,
                                      fontWeight: FontWeight.w300,
                                      fontFamily: FontFamily.cairo),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.sp,
                            ),
                            const Text(
                              "Min",
                              style: TextStyle(
                                  fontSize: 17,
                                  color: AppColors.black,
                                  fontWeight: FontWeight.w300,
                                  fontFamily: FontFamily.cairo),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    Directionality.of(context) == TextDirection.ltr
                        ? Padding(
                      padding:
                      EdgeInsetsDirectional.only(start: 4.w, end: 4.w),
                      child: Container(
                        alignment: Alignment.center,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text(
                              "Max",
                              style: TextStyle(
                                  fontSize: 17,
                                  color: AppColors.black,
                                  fontWeight: FontWeight.w300,
                                  fontFamily: FontFamily.cairo),
                            ),
                            SizedBox(
                              width: 10.sp,
                            ),
                            Container(
                              alignment: AlignmentDirectional.centerStart,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                  border: Border.all(
                                      color: AppColors.lighterGray),
                                  color: Colors.white),
                              width: 70.w,
                              height: 40,
                              child: Padding(
                                padding:
                                EdgeInsetsDirectional.only(start: 10.sp),
                                child: Text(
                                  _currentRangeValues.end.toString(),
                                  style: const TextStyle(
                                      color: AppColors.gray,
                                      fontWeight: FontWeight.w300,
                                      fontFamily: FontFamily.cairo),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                        : Padding(
                      padding:
                      EdgeInsetsDirectional.only(start: 4.w, end: 4.w),
                      child: Container(
                        alignment: Alignment.center,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              alignment: Alignment.center,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    alignment:
                                    AlignmentDirectional.centerStart,
                                    decoration: BoxDecoration(
                                        borderRadius:
                                        BorderRadius.circular(10.0),
                                        border: Border.all(
                                            color: AppColors.lighterGray),
                                        color: Colors.white),
                                    width: 70.w,
                                    height: 40,
                                    child: Padding(
                                      padding: EdgeInsetsDirectional.only(
                                          start: 10.sp),
                                      child: Text(
                                        _currentRangeValues.end.toString(),
                                        style: const TextStyle(
                                            color: AppColors.gray,
                                            fontWeight: FontWeight.w300,
                                            fontFamily: FontFamily.cairo),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              width: 10.sp,
                            ),
                            const Text(
                              "Max",
                              style: TextStyle(
                                  fontSize: 17,
                                  color: AppColors.black,
                                  fontWeight: FontWeight.w300,
                                  fontFamily: FontFamily.cairo),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 2.h,
                ),
                const Divider(
                  color: AppColors.gray,
                ),
                Padding(
                  padding: EdgeInsetsDirectional.only(start: 10.sp, top: 4.sp),
                  child: Row(
                    children: [
                      checkIsEnglish()
                          ? SvgPicture.asset(
                        "assets/img/svg/arrow_filter_tag.svg",
                        width: 4.w,
                        height: 2.h,
                        color: Colors.blue,
                      )
                          : buildRotateWidget(SvgPicture.asset(
                        "assets/img/svg/arrow_filter_tag.svg",
                        width: 4.w,
                        height: 2.h,
                        color: Colors.blue,
                      )),
                      // Image.asset("assets/img/images-right-tag.png",
                      //     width: 4.w, height: 2.h),
                      SizedBox(
                        width: 2.sp,
                      ),
                      Text(
                        AppLocalization.of(context).trans("Duration"),
                        style: AppTextStyle.xLargeBlack,
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 15.sp,
                ),
                Padding(
                  padding: EdgeInsetsDirectional.only(end: 10.sp, start: 10.sp),
                  child: MultiSelectDialogField<Durations>(
                    items: BlocProvider.of<DurationCubit>(context)
                        .durations
                        .map((e) => MultiSelectItem(e, e.durationEn))
                        .toList(),
                    title: Text(
                      AppLocalization.of(context).trans("Duration"),
                      style: AppTextStyle.largeBlackBold,
                    ),
                    selectedColor: Colors.blue,
                    decoration: BoxDecoration(
                      color: AppColors.white,
                      borderRadius: const BorderRadius.all(Radius.circular(12)),
                      border: Border.all(
                        color: AppColors.gray,
                        width: 1,
                      ),
                    ),
                    buttonIcon: const Icon(
                      Icons.keyboard_arrow_down,
                      color: AppColors.gray,
                    ),
                    buttonText: Text(
                      AppLocalization.of(context).trans("select_duration"),
                      style: AppTextStyle.smallDeepGray,
                    ),
                    onConfirm: (results) {
                      // if(selectedDataString.isNotEmpty) selectedDurations.clear();
                      for (int i = 0; i < results.length; i++) {
                        print(results[i].id);
                        selectedDurations.add(results[i].id);
                      }
                      //_selectedAnimals = results;
                    },
                  ),
                ),
                SizedBox(
                  height: 15.sp,
                ),
                Container(
                  height: 8.h,
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Expanded(
                        // Place 2 `Expanded` mean: they try to get maximum size and they will have same size
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 2.w),
                          child: ElevatedButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            style: ElevatedButton.styleFrom(
                                primary: AppColors.white,
                                textStyle: const TextStyle(
                                    fontSize: 30, fontWeight: FontWeight.bold)),
                            child: Text(
                              AppLocalization.of(context).trans("cancel"),
                              style: AppTextStyle.mediumNormalBlue,
                            ), // Every button need a callback
                          ),
                        ),
                      ),
                      Expanded(
                        // Place `Expanded` inside `Row`
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 2.w),
                          child: ElevatedButton(
                            onPressed: () {
                              FilterRequest ts = FilterRequest(
                                  selectedCategories: selectedCategories,
                                  selectedDurations: selectedDurations,
                                  selectedLevels: selectedLevels,
                                  selectedPriceRange: [0,1000]);
                              ts.toJson();

                             courseBloc
                                  .add(GetFilterCourses(filterRequest: ts.toJson()));
                              Navigator.pop(context);
                            }, //
                            style: ElevatedButton.styleFrom(
                                primary: Colors.blue,
                                textStyle: const TextStyle(
                                    fontSize: 30, fontWeight: FontWeight.bold)),
                            child: Text(
                              AppLocalization.of(context).trans("Filter"),
                              style: AppTextStyle.mediumWhite,
                            ), // Every button need a callback
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 15.sp,
                ),
              ],
            )),
      ),
    );
  }
}
class FilterBottomSheet extends StatefulWidget {
  const FilterBottomSheet({key}) : super(key: key);

  static Future<dynamic> show(BuildContext context, CategoryCubit bloc) {
    return showCupertinoModalBottomSheet(
        expand: false,
        isDismissible: false,
        enableDrag: false,
        context: context,
        backgroundColor: Colors.transparent,
        builder: (context) => BlocProvider.value(
              value: bloc,
              child: const FilterBottomSheet(),
            ));
  }

  @override
  State<FilterBottomSheet> createState() => _FilterBottomSheetState();
}

class _FilterBottomSheetState extends State<FilterBottomSheet> {
  CategoryCubit _bloc;
  List<String> selectedDataString;
  String selectedString;
  RangeValues _currentRangeValues = const RangeValues(0, 100);
  List<int> selectedCategories = [];
  List<int> selectedLevels = [];
  List<int> selectedDurations = [];
  List<double> selectedPriceRange = [];

  @override
  void initState() {
    _bloc = BlocProvider.of<CategoryCubit>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return
      SizedBox(
      height: MediaQuery.of(context).size.height / 1.3,
      child: Material(
        child: SingleChildScrollView(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              margin: EdgeInsets.only(top: 4),
              child: Center(
                child: Text(
                  AppLocalization.of(context).trans("Add_Filters"),
                  style: AppTextStyle.xlargeBlueNormal,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsetsDirectional.only(start: 10.sp, top: 4.sp),
              child: Row(
                children: [
                  checkIsEnglish()
                      ? SvgPicture.asset(
                          "assets/img/svg/arrow_filter_tag.svg",
                          width: 4.w,
                          height: 2.h,
                          color: Colors.blue,
                        )
                      : buildRotateWidget(SvgPicture.asset(
                          "assets/img/svg/arrow_filter_tag.svg",
                          width: 4.w,
                          height: 2.h,
                          color: Colors.blue,
                        )),
                  SizedBox(
                    width: 2.sp,
                  ),
                  InkWell(
                    child: Text(
                      AppLocalization.of(context).trans("categories"),
                      style: AppTextStyle.xLargeBlack,
                    ),
                    onTap: () {},
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsetsDirectional.only(end: 10.sp, start: 10.sp),
              child: MultiSelectDialogField<Category>(
                items: _bloc.categories
                    .map((e) => MultiSelectItem(e, e.name))
                    .toList(),
                title: Text(
                  AppLocalization.of(context).trans("categories"),
                  style: AppTextStyle.largeBlackBold,
                ),
                selectedColor: Colors.blue,
                decoration: BoxDecoration(
                  color: AppColors.white,
                  borderRadius: const BorderRadius.all(Radius.circular(12)),
                  border: Border.all(
                    color: AppColors.gray,
                    width: 1,
                  ),
                ),
                buttonIcon: const Icon(
                  Icons.keyboard_arrow_down,
                  color: AppColors.gray,
                ),
                buttonText: Text(
                  AppLocalization.of(context).trans("select_categories"),
                  style: AppTextStyle.smallDeepGray,
                ),
                onConfirm: (results) {
                  selectedCategories.clear();
                  for (int i = 0; i < results.length; i++) {
                    selectedCategories.add(results[i].id);
                  }
                  //_selectedAnimals = results;
                },
              ),
            ),
            SizedBox(
              height: 15.sp,
            ),
            const Divider(
              color: AppColors.gray,
            ),
            SizedBox(
              height: 5.sp,
            ),
            Padding(
              padding: EdgeInsetsDirectional.only(start: 10.sp, top: 4.sp),
              child: Row(
                children: [
                  checkIsEnglish()
                      ? SvgPicture.asset(
                          "assets/img/svg/arrow_filter_tag.svg",
                          width: 4.w,
                          height: 2.h,
                          color: Colors.blue,
                        )
                      : buildRotateWidget(SvgPicture.asset(
                          "assets/img/svg/arrow_filter_tag.svg",
                          width: 4.w,
                          height: 2.h,
                          color: Colors.blue,
                        )),
                  SizedBox(
                    width: 2.sp,
                  ),
                  Text(
                    AppLocalization.of(context).trans("Level"),
                    style: AppTextStyle.xLargeBlack,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 15.sp,
            ),
            Padding(
              padding: EdgeInsetsDirectional.only(end: 10.sp, start: 10.sp),
              child: MultiSelectDialogField<Level>(
                items: BlocProvider.of<LevelCubit>(context)
                    .levels
                    .map((e) => MultiSelectItem(e, e.levelEn))
                    .toList(),
                title: Text(
                  AppLocalization.of(context).trans("Level"),
                  style: AppTextStyle.largeBlackBold,
                ),
                selectedColor: Colors.blue,
                decoration: BoxDecoration(
                  color: AppColors.white,
                  borderRadius: const BorderRadius.all(Radius.circular(12)),
                  border: Border.all(
                    color: AppColors.gray,
                    width: 1,
                  ),
                ),
                buttonIcon: const Icon(
                  Icons.keyboard_arrow_down,
                  color: AppColors.gray,
                ),
                buttonText: Text(
                  AppLocalization.of(context).trans("select_levels"),
                  style: AppTextStyle.smallDeepGray,
                ),
                onConfirm: (results) {
                if(selectedLevels.isNotEmpty)  selectedLevels.clear();
                  for (int i = 0; i < results.length; i++) {
                    selectedLevels.add(results[i].id);
                  }
                },
              ),
            ),
            SizedBox(
              height: 15.sp,
            ),
            const Divider(
              color: AppColors.gray,
            ),
            SizedBox(
              height: 15.sp,
            ),
            Padding(
              padding: EdgeInsetsDirectional.only(start: 10.sp, top: 4.sp),
              child: Row(
                children: [
                  checkIsEnglish()
                      ? SvgPicture.asset(
                          "assets/img/svg/arrow_filter_tag.svg",
                          width: 4.w,
                          height: 2.h,
                          color: Colors.blue,
                        )
                      : buildRotateWidget(SvgPicture.asset(
                          "assets/img/svg/arrow_filter_tag.svg",
                          width: 4.w,
                          height: 2.h,
                          color: Colors.blue,
                        )),
                  SizedBox(
                    width: 2.sp,
                  ),
                  Text(
                    AppLocalization.of(context).trans("price"),
                    style: AppTextStyle.xLargeBlack,
                  ),
                ],
              ),
            ),
            Column(
              children: [
                SliderTheme(
                  data: const SliderThemeData(
                    trackHeight: 2,
                    rangeThumbShape:
                        RoundRangeSliderThumbShape(enabledThumbRadius: 7),
                  ),
                  child: RangeSlider(
                    values: _currentRangeValues,
                    max: 100,
                    activeColor: Colors.blue,
                    inactiveColor: AppColors.deepBlue.withOpacity(0.2),
                    divisions: 5,
                    labels: RangeLabels(
                      _currentRangeValues.start.round().toString(),
                      _currentRangeValues.end.round().toString(),
                    ),
                    onChanged: (RangeValues values) {
                      setState(() {
                        _currentRangeValues = values;
                      // if(selectedPriceRange.isNotEmpty)  selectedPriceRange.clear();
                      //   selectedPriceRange.add(
                      //       _currentRangeValues.start  );
                      //   selectedPriceRange
                      //       .add( _currentRangeValues.end );
                      });
                    },
                  ),
                ),
                Directionality.of(context) == TextDirection.ltr
                    ? Padding(
                        padding:
                            EdgeInsetsDirectional.only(start: 4.w, end: 4.w),
                        child: Container(
                          alignment: Alignment.center,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                "Min",
                                style: TextStyle(
                                    fontSize: 17,
                                    color: AppColors.black,
                                    fontWeight: FontWeight.w300,
                                    fontFamily: FontFamily.cairo),
                              ),
                              SizedBox(
                                width: 10.sp,
                              ),
                              Container(
                                alignment: AlignmentDirectional.centerStart,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10.0),
                                    border: Border.all(
                                        color: AppColors.lighterGray),
                                    color: Colors.white),
                                width: 70.w,
                                height: 40,
                                child: Padding(
                                  padding:
                                      EdgeInsetsDirectional.only(start: 10.sp),
                                  child: Text(
                                    _currentRangeValues.start.toString(),
                                    style: const TextStyle(
                                        color: AppColors.gray,
                                        fontWeight: FontWeight.w300,
                                        fontFamily: FontFamily.cairo),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      )
                    : Padding(
                        padding:
                            EdgeInsetsDirectional.only(start: 4.w, end: 4.w),
                        child: Container(
                          alignment: Alignment.center,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                alignment: AlignmentDirectional.centerStart,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10.0),
                                    border: Border.all(
                                        color: AppColors.lighterGray),
                                    color: Colors.white),
                                width: 70.w,
                                height: 40,
                                child: Padding(
                                  padding:
                                      EdgeInsetsDirectional.only(start: 10.sp),
                                  child: Text(
                                    _currentRangeValues.start.toString(),
                                    style: const TextStyle(
                                        color: AppColors.gray,
                                        fontWeight: FontWeight.w300,
                                        fontFamily: FontFamily.cairo),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 10.sp,
                              ),
                              const Text(
                                "Min",
                                style: TextStyle(
                                    fontSize: 17,
                                    color: AppColors.black,
                                    fontWeight: FontWeight.w300,
                                    fontFamily: FontFamily.cairo),
                              ),
                            ],
                          ),
                        ),
                      ),
                SizedBox(
                  height: 2.h,
                ),
                Directionality.of(context) == TextDirection.ltr
                    ? Padding(
                        padding:
                            EdgeInsetsDirectional.only(start: 4.w, end: 4.w),
                        child: Container(
                          alignment: Alignment.center,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                "Max",
                                style: TextStyle(
                                    fontSize: 17,
                                    color: AppColors.black,
                                    fontWeight: FontWeight.w300,
                                    fontFamily: FontFamily.cairo),
                              ),
                              SizedBox(
                                width: 10.sp,
                              ),
                              Container(
                                alignment: AlignmentDirectional.centerStart,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10.0),
                                    border: Border.all(
                                        color: AppColors.lighterGray),
                                    color: Colors.white),
                                width: 70.w,
                                height: 40,
                                child: Padding(
                                  padding:
                                      EdgeInsetsDirectional.only(start: 10.sp),
                                  child: Text(
                                    _currentRangeValues.end.toString(),
                                    style: const TextStyle(
                                        color: AppColors.gray,
                                        fontWeight: FontWeight.w300,
                                        fontFamily: FontFamily.cairo),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    : Padding(
                        padding:
                            EdgeInsetsDirectional.only(start: 4.w, end: 4.w),
                        child: Container(
                          alignment: Alignment.center,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                alignment: Alignment.center,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      alignment:
                                          AlignmentDirectional.centerStart,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                          border: Border.all(
                                              color: AppColors.lighterGray),
                                          color: Colors.white),
                                      width: 70.w,
                                      height: 40,
                                      child: Padding(
                                        padding: EdgeInsetsDirectional.only(
                                            start: 10.sp),
                                        child: Text(
                                          _currentRangeValues.end.toString(),
                                          style: const TextStyle(
                                              color: AppColors.gray,
                                              fontWeight: FontWeight.w300,
                                              fontFamily: FontFamily.cairo),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                width: 10.sp,
                              ),
                              const Text(
                                "Max",
                                style: TextStyle(
                                    fontSize: 17,
                                    color: AppColors.black,
                                    fontWeight: FontWeight.w300,
                                    fontFamily: FontFamily.cairo),
                              ),
                            ],
                          ),
                        ),
                      )
              ],
            ),
            SizedBox(
              height: 2.h,
            ),
            const Divider(
              color: AppColors.gray,
            ),
            Padding(
              padding: EdgeInsetsDirectional.only(start: 10.sp, top: 4.sp),
              child: Row(
                children: [
                  checkIsEnglish()
                      ? SvgPicture.asset(
                          "assets/img/svg/arrow_filter_tag.svg",
                          width: 4.w,
                          height: 2.h,
                          color: Colors.blue,
                        )
                      : buildRotateWidget(SvgPicture.asset(
                          "assets/img/svg/arrow_filter_tag.svg",
                          width: 4.w,
                          height: 2.h,
                          color: Colors.blue,
                        )),
                  // Image.asset("assets/img/images-right-tag.png",
                  //     width: 4.w, height: 2.h),
                  SizedBox(
                    width: 2.sp,
                  ),
                  Text(
                    AppLocalization.of(context).trans("Duration"),
                    style: AppTextStyle.xLargeBlack,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 15.sp,
            ),
            Padding(
              padding: EdgeInsetsDirectional.only(end: 10.sp, start: 10.sp),
              child: MultiSelectDialogField<Durations>(
                items: BlocProvider.of<DurationCubit>(context)
                    .durations
                    .map((e) => MultiSelectItem(e, e.durationEn))
                    .toList(),
                title: Text(
                  AppLocalization.of(context).trans("Duration"),
                  style: AppTextStyle.largeBlackBold,
                ),
                selectedColor: Colors.blue,
                decoration: BoxDecoration(
                  color: AppColors.white,
                  borderRadius: const BorderRadius.all(Radius.circular(12)),
                  border: Border.all(
                    color: AppColors.gray,
                    width: 1,
                  ),
                ),
                buttonIcon: const Icon(
                  Icons.keyboard_arrow_down,
                  color: AppColors.gray,
                ),
                buttonText: Text(
                  AppLocalization.of(context).trans("select_duration"),
                  style: AppTextStyle.smallDeepGray,
                ),
                onConfirm: (results) {
                 // if(selectedDataString.isNotEmpty) selectedDurations.clear();
                  for (int i = 0; i < results.length; i++) {
                    print(results[i].id);
                    selectedDurations.add(results[i].id);
                  }
                  //_selectedAnimals = results;
                },
              ),
            ),
            SizedBox(
              height: 15.sp,
            ),
            Container(
              height: 8.h,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Expanded(
                    // Place 2 `Expanded` mean: they try to get maximum size and they will have same size
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 2.w),
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        style: ElevatedButton.styleFrom(
                            primary: AppColors.white,
                            textStyle: const TextStyle(
                                fontSize: 30, fontWeight: FontWeight.bold)),
                        child: Text(
                          AppLocalization.of(context).trans("cancel"),
                          style: AppTextStyle.mediumNormalBlue,
                        ), // Every button need a callback
                      ),
                    ),
                  ),
                  Expanded(
                    // Place `Expanded` inside `Row`
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 2.w),
                      child: ElevatedButton(
                        onPressed: () {
                          FilterRequest ts = FilterRequest(
                              selectedCategories: selectedCategories,
                              selectedDurations: selectedDurations,
                              selectedLevels: selectedLevels,
                              selectedPriceRange: [0,1000]);
                          ts.toJson();
                          Navigator.pop(context);
                          BlocProvider.of<CourseBloc>(this.context)
                              .add(GetCourses(filterRequest: ts.toJson()));
                        }, //
                        style: ElevatedButton.styleFrom(
                            primary: Colors.blue,
                            textStyle: const TextStyle(
                                fontSize: 30, fontWeight: FontWeight.bold)),
                        child: Text(
                          AppLocalization.of(context).trans("Filter"),
                          style: AppTextStyle.mediumWhite,
                        ), // Every button need a callback
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 15.sp,
            ),
          ],
        )),
      ),
    );
  }

  void _onCountriesSelectionComplete(value) {
    selectedDataString?.addAll(value);
    setState(() {});
  }
}
