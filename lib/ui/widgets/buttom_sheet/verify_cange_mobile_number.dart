import 'package:e_learning/bloc/profile_bloc/profile_bloc.dart';
import 'package:e_learning/themes/app_text_style.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import '../../../helpers/stack_loader_indicator.dart';
import '../../../localization/app_localization.dart';

import '../button/cancel_button.dart';
import '../button/commit_button.dart';
import '../common/Flush_bar.dart';

const OTP_HINT = "****";

class ChangeMobileNumberBottomSheet extends StatefulWidget {
  const ChangeMobileNumberBottomSheet({Key key}) : super(key: key);

  static Future<dynamic> show(
      BuildContext context) {
    return showCupertinoModalBottomSheet(
        expand: false,
        isDismissible: false,
        enableDrag: false,
        context: context,
        topRadius: Radius.circular(35),
        backgroundColor: Colors.transparent,
        builder: (context) => ChangeMobileNumberBottomSheet());
  }

  @override
  State<ChangeMobileNumberBottomSheet> createState() =>
      _ChangeMobileNumberBottomSheet();
}

class _ChangeMobileNumberBottomSheet
    extends State<ChangeMobileNumberBottomSheet> {
  final _loader = StackLoaderIndicator();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var height = size.height;
    var vSpacing = size.height * 0.06;
    var hSpacing = size.width * 0.06;
    return BlocListener<ProfileBloc, ProfileState>(
      listener: (context, state) async {
        if (state is VerifyChangeMobileNumberAwaitState) {
          _loader.show(context);
        } else if (state is VerifyChangeMobileNumberAcceptState) {
          _loader.dismiss();
          Navigator.of(context).pop(state);
        } else if (state is VerifyChangeMobileNumberErrorState) {
          _loader.dismiss();
          Navigator.pop(context);
          await showCustomFlashbar(context, state.message, "Verify Failed");
        }
      },
      child: Material(
        child: SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(
            height * 0.04,
            vSpacing,
            height * 0.04,
            height * 0.03 + MediaQuery.of(context).viewInsets.bottom,
          ),
          child: BlocBuilder<ProfileBloc, ProfileState>(
            builder: (context, state) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    AppLocalization.of(context).trans("verification_title"),
                    style: AppTextStyle.largeGreenBold,
                  ),
                  SizedBox(height: hSpacing),
                  Text(
                    AppLocalization.of(context).trans("enter_otp_to_proceed"),
                    style: AppTextStyle.mediumDeepGray,
                  ),
                  SizedBox(height: vSpacing),
                  TextField(
                    controller:
                        BlocProvider.of<ProfileBloc>(context).pinController,
                    autofillHints: [AutofillHints.oneTimeCode],
                    keyboardType: TextInputType.number,
                    autofocus: false,
                    inputFormatters: [
                      LengthLimitingTextInputFormatter(5),
                      FilteringTextInputFormatter.digitsOnly,
                    ],
                    decoration: InputDecoration(
                      errorText:
                          BlocProvider.of<ProfileBloc>(context).pinMessage,
                      hintText: OTP_HINT,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: height * 0.03),
                    child: Row(
                      children: [
                        Expanded(
                          child: CancelButton(
                            text: AppLocalization.of(context).trans("cancel"),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                        SizedBox(width: height * 0.02),
                        Expanded(
                          child: CommitButton(
                            text: AppLocalization.of(context).trans("verify"),
                            onPressed: () {
                              FocusScope.of(context).unfocus();
                           Navigator.pop(context);
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
