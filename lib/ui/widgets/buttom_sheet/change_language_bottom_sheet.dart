import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import '../../../bloc/application_bloc/application_cubit.dart';
import '../../../controller/data_store.dart';
import '../../../localization/app_localization.dart';
import '../../../resources/provider/user_provider.dart';
import '../../../themes/app_colors.dart';
import '../../../themes/font_family.dart';

class ChangeLanguageBottomSheet extends StatelessWidget {
  final Map<String, String> alternative = {
    "ar": "العربية",
    "en": "English",
  };

  Widget getLanguageButton(BuildContext context, String locale) {
    var mediaQuery = MediaQuery.of(context);
    return InkWell(
      onTap: () async {
        await BlocProvider.of<ApplicationCubit>(context).changeLanguage(locale);
        // GetIt.I.get<UserProvider>().changeLanguage();
        Navigator.of(context).pop();
      },
      child: ListTile(
        contentPadding: EdgeInsets.zero,
        title: Text(
          alternative[locale] ?? "Unknown",
          style: TextStyle(
            fontFamily: FontFamily.lato,
            fontSize: 15.sp,
            color: Theme.of(context).textTheme.bodyText2.color,
          ),
        ),
        trailing: DataStore.instance.lang == locale
            ? Icon(
                Icons.check,
                color: AppColors.deepBlue,
                size: 22.sp,
              )
            : null,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    var hSpacing = mediaQuery.size.width * 0.08;
    return Material(
      child: ListView(
        padding: EdgeInsets.symmetric(
          horizontal: hSpacing,
        ),
        shrinkWrap: true,
        children: [
          // LineBottomSheetWidget(),
          SizedBox(height: 10.h),
          Text(
            AppLocalization.of(context).trans("language"),
            style: TextStyle(
              fontFamily: FontFamily.lato,
              fontSize: 20.sp,
              color: Theme.of(context).textTheme.titleMedium.color,
            ),
          ),
          Divider(height: 1),
          getLanguageButton(context, "ar"),
          Divider(height: 1),
          getLanguageButton(context, "en"),
          Divider(height: 1),
          SizedBox(height: 5.h),
        ],
      ),
    );
  }
}

Future<dynamic> showChangeLanguageBottomSheet(BuildContext context) {
  return showCupertinoModalBottomSheet(
    expand: false,
    context: context,
    topRadius: Radius.circular(35),
    builder: (context) => ChangeLanguageBottomSheet(),
  );
}
