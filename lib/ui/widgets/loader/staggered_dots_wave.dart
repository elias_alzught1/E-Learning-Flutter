import 'package:e_learning/themes/app_colors.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';

class StaggeredDotsWave extends StatelessWidget {
  const StaggeredDotsWave({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: LoadingAnimationWidget.staggeredDotsWave(
          color: AppColors.gray,
          size: 30.sp,
        ));
  }
}
