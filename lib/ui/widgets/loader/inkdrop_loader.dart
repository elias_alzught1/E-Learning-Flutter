import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';

import '../../../themes/app_colors.dart';

class InkDropLoader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  LoadingAnimationWidget.inkDrop(
      color: AppColors.gray,
      size: 20.sp,
    );
  }
}
