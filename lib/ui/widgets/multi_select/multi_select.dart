// import 'package:e_learning/localization/app_localization.dart';
// import 'package:e_learning/ui/widgets/textfield/DirectionalTextField.dart';
// import 'package:e_learning/utils/sizer_custom/sizer.dart';
// import 'package:flutter/material.dart';
//
// import '../../../themes/app_colors.dart';
//
// /// A model class used to represent a selectable item.
// class MultiSelectItem<T> {
//   final T value;
//   final String label;
//   bool selected = false;
//
//   MultiSelectItem(this.value, this.label);
// }
//
// class MultiSelect extends StatefulWidget {
//   final  List<MultiSelectItem> items;
//   final String title;
//   final BuildContext context;
//
//   const MultiSelect({key, this.items, this.title, this.context})
//       : super(key: key);
//
//   @override
//   State<StatefulWidget> createState() => _MultiSelectState();
// }
//
// class _MultiSelectState extends State<MultiSelect> {
//   // this variable holds the selected items
//   final List<MultiSelectItem> _selectedItems = [];
//
// // This function is triggered when a checkbox is checked or unchecked
//   final TextEditingController _controller = TextEditingController();
//
//   void _itemChange(MultiSelectItem itemValue, bool isSelected) {
//     setState(() {
//       if (isSelected) {
//         setState(() {
//           _selectedItems.add(itemValue);
//         });
//       } else {
//         _selectedItems.remove(itemValue);
//       }
//     });
//   }
//
//   // this function is called when the Cancel button is pressed
//   void _cancel() {
//     Navigator.pop(context);
//   }
//
// // this function is called when the Submit button is tapped
//   void _submit() {
//     Navigator.pop(context, _selectedItems);
//     for (int i = 0; i < _selectedItems.length; i++) {
//       _controller.text + _selectedItems[i].toString();
//     }
//     print(_controller.text);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: EdgeInsetsDirectional.only(start: 10.sp, end: 10.0.sp),
//       child: InkWell(
//         child: Column(
//           children: [
//             DirectionalTextField(
//                 controller: _controller,
//                 readOnly: true,
//                 decoration: InputDecoration(
//                   floatingLabelBehavior: FloatingLabelBehavior.always,
//                   contentPadding: const EdgeInsets.only(
//                     left: 12.0,
//                   ),
//                   labelStyle: const TextStyle(color: Colors.grey),
//                   suffixIcon: const Icon(
//                     Icons.keyboard_arrow_down,
//                     color: AppColors.gray,
//                   ),
//                   labelText: AppLocalization.of(context)
//                       .trans(widget.title.toString()),
//                   border: OutlineInputBorder(
//                       borderRadius: BorderRadius.circular(13.0),
//                       borderSide: BorderSide(color: AppColors.gray)),
//                 )),
//             ElevatedButton(
//               child: Text("sss"),
//               onPressed: () {
//                 showDialog(
//                     context: context,
//                     builder: (context) {
//                       return AlertDialog(
//                         title: const Text('Select Topics'),
//                         content: SingleChildScrollView(
//                           child: ListBody(
//                             children: widget.items
//                                 .map((item) => CheckboxListTile(
//                                     value: _selectedItems.contains(item),
//                                     title: Text(item.label),
//                                     controlAffinity:
//                                         ListTileControlAffinity.leading,
//                                     onChanged: (isChecked) async {
//                                     await  setState(() {
//                                          _itemChange(item, isChecked);
//                                       });
//                                     }))
//                                 .toList(),
//                           ),
//                         ),
//                         actions: [
//                           const TextButton(),
//                           ElevatedButton(
//                             onPressed: _submit,
//                             child: const Text('Submit'),
//                           ),
//                         ],
//                       );
//                     }
//                     );
//               },
//             )
//           ],
//         ),
//       ),
//     );
//   }
//
//   Widget _showDialog(BuildContext context) {
//     return Builder(
//       builder: (context) {
//         return AlertDialog(
//           title: const Text('Select Topics'),
//           content: SingleChildScrollView(
//             child: ListBody(
//               children: widget.items
//                   .map((item) => CheckboxListTile(
//                         value: _selectedItems.contains(item),
//                         title: Text(item.label),
//                         controlAffinity: ListTileControlAffinity.leading,
//                         onChanged: (isChecked) {
//                           _itemChange(item, isChecked);
//                         },
//                       ))
//                   .toList(),
//             ),
//           ),
//           actions: [
//             TextButton(
//               child: const Text('Cancel'),
//               onPressed: _cancel,
//             ),
//             ElevatedButton(
//               onPressed: _submit,
//               child: const Text('Submit'),
//             ),
//           ],
//         );
//       },
//     );
//   }
// }
