import 'package:e_learning/localization/app_localization.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flash/flash.dart';
import 'package:flutter/material.dart';
import '../../../themes/app_colors.dart';

Future<void> showCustomFlashbar(
    BuildContext context, String message, String title,
    {Duration duration}) async {
  await showFlash(
      context: context,
      duration: const Duration(seconds: 5),
      transitionDuration: const Duration(milliseconds: 400),
      builder: (_, controller) {
        return CustomFlashbar(
          controller: controller,
          message: message,
          title: title,
        );
      });

  return;
}

class CustomFlashbar extends StatelessWidget {
  final FlashController controller;
  final String message;
  final String title;

  const CustomFlashbar({Key key, this.controller, this.message, this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(
        16.sp,
        16.sp,
        6.sp,
        14.sp,
      ),
      child: Flash(
        controller: controller,
        behavior: FlashBehavior.floating,
        position: FlashPosition.top,
        backgroundColor: AppColors.gray,
        child: FlashBar(
          title: Text(
              title==""?"":     AppLocalization.of(context).trans(title),
            style: TextStyle(
              fontSize: 12.sp,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
          content: Text(
            message,
            style: TextStyle(
              fontSize: 10.5.sp,
              color: Colors.white.withOpacity(.85),
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
      ),
    );
  }
}
