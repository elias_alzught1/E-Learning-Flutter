import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';

import '../../../themes/app_colors.dart';
import '../../../themes/font_family.dart';

class ActionButton extends StatelessWidget {
  final IconData icon;
  final String title;
  final Function() onTap;
  final bool showTile;

  const ActionButton({
    Key key,
    this.title,
    this.icon,
    this.onTap,
    this.showTile = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    return Column(
      children: [
        ListTile(
          leading: Icon(icon,color:Theme.of(context).textTheme.bodyText1.color ),
          title: Text(
            title,
            style:TextStyle(          fontSize: 14.sp,
              fontWeight: FontWeight.w600,
               fontFamily: FontFamily.lato,
              color: Theme.of(context).textTheme.bodyText2.color,),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
          trailing: showTile
              ? Icon(

            Icons.arrow_forward_ios_outlined,
            size: 5.w,
            color: Theme.of(context).textTheme.bodyText1.color,
          )
              : null,
          onTap: onTap,
        ),
        Divider(
          color: Colors.grey,
        )
      ],
    );
  }
}
