// import 'package:e_learning/themes/font_family.dart';
// import 'package:e_learning/utils/sizer_custom/sizer.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:fluttertoast/fluttertoast.dart';
//
// import '../../bloc/profile_bloc/profile_bloc.dart';
// import '../../bloc/settings/change_mobile_number/change_mobile_number_bloc.dart';
// import '../../localization/app_localization.dart';
// import '../../themes/app_colors.dart';
// import '../widgets/button/app_back_button.dart';

// import '../widgets/textfield/DirectionalTextField.dart';
//
// class ChangeNumberPhone extends StatefulWidget {
//   const ChangeNumberPhone({Key key}) : super(key: key);
//
//   @override
//   State<ChangeNumberPhone> createState() => _ChangeNumberPhoneState();
// }
//
// class _ChangeNumberPhoneState extends State<ChangeNumberPhone> {
//   ChangeMobileNumberBloc _bloc;
//
//   @override
//   void initState() {
//     _bloc = BlocProvider.of<ChangeMobileNumberBloc>(context);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         leading: AppBackButton(),
//         foregroundColor: Theme.of(context).textTheme.bodyText1.color,
//         elevation: 0.0,
//       ),
//       body: BlocListener<ChangeMobileNumberBloc, ChangeMobileNumberState>(
//         listenWhen: (context, state) =>
//             state is ChangeMobileNumberErrorState ||
//             state is ChangeMobileNumberAcceptState,
//         listener: (context, state) {
//           if (state is ChangeMobileNumberErrorState) {
//             Fluttertoast.showToast(
//                 msg: state.message,
//                 toastLength: Toast.LENGTH_SHORT,
//                 gravity: ToastGravity.TOP,
//                 timeInSecForIosWeb: 1,
//                 backgroundColor: colorBlack.withOpacity(1),
//                 textColor: Colors.white,
//                 fontSize: 16.0);
//           } else if (state is ChangeMobileNumberAcceptState) {
//             setState(() {
//               BlocProvider.of<ProfileBloc>(context).user.mSISDN=_bloc.mobileNumberController.text;
//             });
//             Fluttertoast.showToast(
//                 msg: state.message,
//                 toastLength: Toast.LENGTH_SHORT,
//                 gravity: ToastGravity.TOP,
//                 timeInSecForIosWeb: 1,
//                 backgroundColor: colorBlack.withOpacity(1),
//                 textColor: Colors.white,
//                 fontSize: 16.0);
//           }
//         },
//         child: LayoutBuilder(
//           builder: (context, constraint) {
//             return SingleChildScrollView(
//               child: ConstrainedBox(
//                 constraints: BoxConstraints(
//                   minHeight: constraint.maxHeight,
//                 ),
//                 child: Padding(
//                   padding: EdgeInsets.all(1),
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.stretch,
//                     children: [
//                       Column(
//                         crossAxisAlignment: CrossAxisAlignment.stretch,
//                         children: [
//                           Center(
//                             child: Text(
//                               AppLocalization.of(context)
//                                   .trans('change_number'),
//                               style: TextStyle(
//                                 fontSize: 16.sp,
//                                 // fontFamily: FontFamily.lato,
//                                 fontWeight: FontWeight.w600,
//                                 color:
//                                     Theme.of(context).textTheme.bodyText1.color,
//                               ),
//                             ),
//                           ),
//                           SizedBox(
//                             height: 5.h,
//                           ),
//                           Center(
//                               child: Text(
//                             BlocProvider.of<ProfileBloc>(context).user.mSISDN,
//                             style: TextStyle(
//                               color: fCL,
//                               fontSize: 13.sp,
//                               fontWeight: FontWeight.w500,
//                             ),
//                           )),
//                           SizedBox(
//                             height: 15.h,
//                           ),
//                           BlocBuilder<ChangeMobileNumberBloc,
//                               ChangeMobileNumberState>(
//                             buildWhen: (_, current) =>
//                                 current is ChangeMobileNumberValidationState,
//                             builder: (context, state) {
//                               return DirectionalTextField(
//                                 controller: _bloc.mobileNumberController,
//                                 decoration: InputDecoration(
//                                     border: OutlineInputBorder(),
//                                     labelText: AppLocalization.of(context)
//                                         .trans("phone_number"),
//                                     hintText: AppLocalization.of(context)
//                                         .trans("Enter_mobile_number")),
//                               );
//                             },
//                           )
//                         ],
//                       ),
//                       SizedBox(
//                         height: 10.h,
//                       ),
//                       Padding(
//                         padding: const EdgeInsets.all(8.0),
//                         child: getCommetButtom(),
//                       )
//                     ],
//                   ),
//                 ),
//               ),
//             );
//           },
//         ),
//       ),
//     );
//   }
//
//   Widget getCommetButtom() {
//     return Container(
//       height: 7.h,
//       width: 5.w,
//       child: BlocBuilder<ChangeMobileNumberBloc, ChangeMobileNumberState>(
//         buildWhen: (_, current) =>
//             current is ChangeMobileNumberAwaitState ||
//             current is ChangeMobileNumberAcceptState ||
//             current is ChangeMobileNumberErrorState ||
//             current is ChangeMobileNumberValidationState,
//         builder: (context, state) {
//           return ElevatedButton(
//             onPressed: state is ChangeMobileNumberAwaitState
//                 ? null
//                 : () {
//                     _bloc.add(ChangeMobileNumber());
//                     //   AppBloc.changeNumBloc.add(ChangeMobileNumber());
//                   },
//             style: ElevatedButton.styleFrom(
//               shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.circular(
//                   4,
//                 ),
//               ),
//             ),
//             child: state is ChangeMobileNumberAwaitState
//                 ? const CupertinoActivityIndicator()
//                 : Text(
//                     AppLocalization.of(context).trans("change"),
//                   ),
//           );
//         },
//       ),
//     );
//   }
// }
