// import 'package:e_learning/bloc/settings/change_mobile_number/change_mobile_number_bloc.dart';
// import 'package:e_learning/config/application.dart';
// import 'package:e_learning/controller/data_store.dart';
// import 'package:e_learning/localization/app_localization.dart';
// import 'package:e_learning/ui/setting/widget/action_buton.dart';
// import 'package:e_learning/utils/sizer_custom/sizer.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
// import 'package:phosphor_flutter/phosphor_flutter.dart';
//
// import '../../themes/app_colors.dart';
//
// // import '../../themes/theme_service.dart';
// import '../../themes/font_family.dart';
// import '../widgets/buttom_sheet/change_language_bottom_sheet.dart';
// import '../widgets/button/app_back_button.dart';
// import 'change_number_phone.dart';
//
// class SettingScreen extends StatefulWidget {
//   const SettingScreen({Key key}) : super(key: key);
//
//   @override
//   State<SettingScreen> createState() => _SettingScreenState();
// }
//
// class _SettingScreenState extends State<SettingScreen> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         leading: AppBackButton(),
//         foregroundColor: Theme.of(context).textTheme.bodyText1.color,
//         elevation: 0.0,
//       ),
//       body: LayoutBuilder(
//         builder: (BuildContext, BoxConstraints) {
//           return SingleChildScrollView(
//             child: Column(children: [
//               Center(
//                 child: Text(
//                   AppLocalization.of(context).trans('setting'),
//                   style: TextStyle(
//                     fontFamily: FontFamily.lato,
//                     fontSize: 16.sp,
//                     fontWeight: FontWeight.w600,
//                     color: Theme.of(context).textTheme.bodyText1.color,
//                   ),
//                 ),
//               ),
//               SizedBox(height: 10.h),
//               //change_language
//               Container(
//                   margin: const EdgeInsets.only(left: 20, right: 40),
//                   child: ActionButton(
//                     onTap: () {
//                       Navigator.push(context,
//                           CupertinoPageRoute(builder: (context) {
//                         return BlocProvider<ChangeMobileNumberBloc>(
//                           child: const ChangeNumberPhone(),
//                           create: (context) => ChangeMobileNumberBloc(),
//                         );
//                       }));
//                     },
//                     icon: Icons.phone_android_outlined,
//                     title: AppLocalization.of(context).trans('change_number'),
//                   )),
//
//               Container(
//                   margin: const EdgeInsets.only(left: 20, right: 40),
//                   child: ActionButton(
//                     onTap: () {
//                       showChangeLanguageBottomSheet(context);
//                     },
//                     icon: Icons.language,
//                     title: AppLocalization.of(context).trans('change_language'),
//                   )),
//             ]),
//           );
//         },
//       ),
//     );
//   }
//
//   Future<dynamic> showChangeLanguageBottomSheet(BuildContext context) {
//     return showCupertinoModalBottomSheet(
//       expand: false,
//       context: context,
//       topRadius: Radius.circular(35),
//       backgroundColor: Colors.transparent,
//       builder: (context) => ChangeLanguageBottomSheet(),
//     );
//   }
// }
