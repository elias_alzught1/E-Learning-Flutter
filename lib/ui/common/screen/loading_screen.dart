
import 'package:e_learning/themes/font_family.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';

import '../../../public/constants.dart';
import '../../../themes/app_colors.dart';

class LoadingScreen extends StatelessWidget {
  final bool isShowText;
  const LoadingScreen({Key key, this.isShowText = true}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: 100.h,
        width: 100.w,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: isShowText ? 70.w : 50.w,
              height: isShowText ? 70.w : 50.w,
              child: Constants().loadingLottie,
            ),
            isShowText
                ? RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: 'Load',
                    style: TextStyle(
                      fontSize: 26.sp,
                      fontWeight: FontWeight.w600,
                      fontFamily: FontFamily.dancing,
                      color: AppColors.deepBlue,
                    ),
                  ),
                  TextSpan(
                    text: 'ing',
                    style: TextStyle(
                      fontSize: 26.sp,
                      fontWeight: FontWeight.w600,
                      fontFamily: FontFamily.dancing,
                      color: Theme.of(context).textTheme.bodyText1.color,
                    ),
                  ),
                ],
              ),
            )
                : Container(),
          ],
        ),
      ),
    );
  }
}
