import 'package:e_learning/bloc/ads_bloc/ads_cubit.dart';
import 'package:e_learning/bloc/authentication/login_bloc/authentication_event.dart';
import 'package:e_learning/bloc/category_bloc/category_cubit.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:octo_image/octo_image.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

import '../../bloc/authentication/login_bloc/authentication_bloc.dart';
import '../../bloc/authentication/login_bloc/authentication_state.dart';
import '../../bloc/course_bloc/course_bloc.dart';
import '../../bloc/profile_bloc/profile_bloc.dart';
import '../../helpers/helpers.dart';
import '../../public/constants.dart';
import '../../themes/app_colors.dart';
import '../home/home_screen.dart';
import '../profile/profile_screen.dart';

class Navigation extends StatefulWidget {
  final int initialIndex;

  Navigation({this.initialIndex = 0});

  @override
  State<StatefulWidget> createState() => _NavigationState();
}

class _NavigationState extends State<Navigation> {
  AuthBloc _bloc;
  int currentPage = 0;
  final _pages = [
    MultiBlocProvider(
      providers: [
        BlocProvider<CategoryCubit>(
          create: (BuildContext context) => CategoryCubit(),
        ),
        BlocProvider<CourseBloc>(
          create: (BuildContext context) => CourseBloc(),
        ),
        BlocProvider<AdsCubit>(
          create: (BuildContext context) => AdsCubit(),
        ),
      ],
      child: const HomeScreen(),
    ),
    const HomeScreen(),
    const HomeScreen(),
    const HomeScreen(),
    const ProfileScreen(),
  ];

  @override
  void initState() {
    super.initState();
    _bloc = BlocProvider.of<AuthBloc>(context);
    // _bloc.add(GetInfoUser());
    currentPage = widget.initialIndex;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomAppBar(
        clipBehavior: Clip.antiAliasWithSaveLayer,
        color: Theme.of(context).scaffoldBackgroundColor,
        elevation: .0,
        child: Container(
          height: 48.sp,
          padding: EdgeInsets.symmetric(horizontal: 6.5.sp),
          alignment: Alignment.center,
          decoration: BoxDecoration(
            border: Border(
              top: BorderSide(
                color: Theme.of(context).dividerColor,
                width: .2,
              ),
            ),
          ),
          child: Row(
            children: [
              _buildItemBottomBar(
                PhosphorIcons.house,
                PhosphorIcons.houseFill,
                0,
                'Home',
              ),
              _buildItemBottomBar(
                PhosphorIcons.graduationCap,
                PhosphorIcons.graduationCapFill,
                1,
                'Classes',
              ),
              _buildItemBottomBar(
                PhosphorIcons.chatsTeardrop,
                PhosphorIcons.chatsTeardropFill,
                2,
                'Message',
              ),
              _buildItemBottomBar(
                PhosphorIcons.clock,
                PhosphorIcons.alarmBold,
                3,
                'Calendar',
              ),

            ],
          ),
        ),
      ),
      body: _pages[currentPage],
    );
  }

  Widget _buildItemBottomBar(inActiveIcon, activeIcon, index, title) {
    return Expanded(
      child: GestureDetector(
        onTap: () {
          setState(() {
            currentPage = index;
          });
        },
        child: Container(
          color: Colors.transparent,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                color: Colors.transparent,
                child: Icon(
                  index == currentPage ? activeIcon : inActiveIcon,
                  size: 21.5.sp,
                  color: index == currentPage
                      ? AppColors.gray
                      : Theme.of(context).textTheme.bodyText2.color,
                ),
              ),
              SizedBox(height: 2.5.sp),
              Container(
                height: 4.sp,
                width: 4.sp,
                decoration: BoxDecoration(
                  color: index == 10 ? AppColors.gray : Colors.transparent,
                  shape: BoxShape.circle,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildItemBottomAccount(
    urlToImage,
    blurHash,
    index,
  ) {
    return Expanded(
      child: GestureDetector(
        onTap: () {
          setState(() {
            currentPage = index;
          });
        },
        child: Container(
          color: Colors.transparent,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 24.sp,
                    width: 24.sp,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: currentPage == index
                            ? AppColors.gray
                            : Colors.transparent,
                        width: 1.8.sp,
                      ),
                      shape: BoxShape.circle,
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(12.sp),
                      child:
                      OctoImage(
                        image: NetworkImage(    urlToImage),
                        fit: BoxFit.cover,
                        placeholderBuilder: (context) => getShimmer(),
                        errorBuilder: imageErrorBuilder,
                      ),

                    ),
                  ),
                ],
              ),
              SizedBox(height: 2.5.sp),
              Container(
                height: 4.sp,
                width: 4.sp,
                decoration: BoxDecoration(
                  color: index == 2 ? AppColors.gray : Colors.transparent,
                  shape: BoxShape.circle,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
