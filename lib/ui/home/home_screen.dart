import 'dart:async';
import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:e_learning/bloc/ads_bloc/ads_cubit.dart';
import 'package:e_learning/bloc/authentication/login_bloc/authentication_bloc.dart';
import 'package:e_learning/bloc/authentication/login_bloc/authentication_state.dart';
import 'package:e_learning/bloc/category_bloc/category_cubit.dart';
import 'package:e_learning/bloc/course_bloc/course_bloc.dart';
import 'package:e_learning/bloc/duration_cubit/duration_cubit.dart';
import 'package:e_learning/themes/app_text_style.dart';

import 'package:e_learning/ui/home/widget/ads_card_course.dart';
import 'package:e_learning/ui/widgets/loader/staggered_dots_wave.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:number_paginator/number_paginator.dart';
import '../../bloc/level_cubit/level_cubit.dart';
import '../../controller/data_store.dart';
import '../../helpers/stack_loader_indicator.dart';
import '../../localization/app_localization.dart';
import '../../log_icons.dart';
import '../../models/common/language.dart';
import '../../public/global.dart';
import '../../themes/app_colors.dart';
import '../../themes/app_font_size.dart';
import '../../themes/font_family.dart';
import 'package:blurrycontainer/blurrycontainer.dart';
import '../authentication/authentication_screen.dart';
import '../course/widget/course_card.dart';
import '../widgets/appbar/abbBarAPP.dart';
import '../widgets/appbar/abbBottomBar.dart';
import '../widgets/buttom_sheet/filter_bottom_sheet.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _HomeScreenState();
}

Random random = Random();
AuthBloc _bloc;
AdsCubit _adsBloc;
ScrollController _scrollController;
ScrollController _scrollCourseController;
CategoryCubit _categoryBloc;
LevelCubit _levelCubit;
String selectedValue;
Language local;
int page = 1;
int _current = 0;
int tempIndex = -1;

class _HomeScreenState extends State<HomeScreen> {
  int previousValue = -1;

  final StackLoaderIndicator _loader = StackLoaderIndicator();

// instantiate the controller in your state
  final NumberPaginatorController _controller = NumberPaginatorController();

  @override
  void initState() {
    BlocProvider.of<DurationCubit>(context).fetch();
    _bloc = BlocProvider.of<AuthBloc>(context);
    _levelCubit = BlocProvider.of<LevelCubit>(context);
    _scrollController = ScrollController();
    _scrollCourseController = ScrollController();
    _categoryBloc = BlocProvider.of<CategoryCubit>(context);
    BlocProvider.of<CategoryCubit>(context).fetch();
    BlocProvider.of<CourseBloc>(context).add(GetCourses());
    BlocProvider.of<AdsCubit>(context).fetchAds();
    _levelCubit.fetch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String lang = DataStore.instance.lang;
    return GestureDetector(
      onTap: () {
        if (FocusScope.of(context).hasFocus) {
          FocusScope.of(context).unfocus();
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: const AppBarApp(),
        bottomNavigationBar: const AppBottomBar(),
        body: SingleChildScrollView(
          child: BlocListener<AuthBloc, AuthState>(
            listenWhen: (context, state) =>
                state is LogoutAwaitState ||
                state is LogoutErrorState ||
                state is LogoutAcceptState,
            listener: (context, state) {
              if (state is LogoutAcceptState) {
                Navigator.of(context).pushAndRemoveUntil(
                  CupertinoPageRoute(
                    builder: (context) => BlocProvider<AuthBloc>(
                      child: const AuthenticateScreen(),
                      create: (context) => AuthBloc(),
                    ),
                  ),
                  (route) => false,
                );
              } else if (state is LogoutAwaitState) {
                _loader.show(context);
              } else if (state is LogoutErrorState) {
                _loader.dismiss();
                Fluttertoast.showToast(
                    msg: state.message,
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.TOP,
                    timeInSecForIosWeb: 1,
                    backgroundColor: AppColors.black.withOpacity(1),
                    textColor: Colors.white,
                    fontSize: 16.0);
              }
            },
            child: SizedBox(
                width: 100.w,
                child: SafeArea(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        color: AppColors.white,
                        height: 90.h,
                        width: 100.w,
                        child: BlocBuilder<AdsCubit, AdsState>(
                          buildWhen: (_, current) =>
                              current is GetAdsAwaitState ||
                              current is GetAdsAcceptState ||
                              current is GetAdsErrorState,
                          builder: (context, state) {
                            if (state is GetAdsAwaitState) {
                              return Container();
                            } else if (state is GetAdsErrorState) {
                              return Container();
                            } else if (state is GetAdsAcceptState) {
                              return Stack(
                                children: <Widget>[
                                  Positioned.fill(
                                    child: Align(
                                      alignment: Alignment.topCenter,
                                      child: CarouselSlider(
                                          items:
                                              BlocProvider.of<AdsCubit>(context)
                                                  .adsItem
                                                  .map((e) => Stack(
                                                        children: [
                                                          Container(
                                                            height: 30.h,
                                                            width: 100.w,
                                                            child: ClipRRect(
                                                              child:
                                                                  CachedNetworkImage(
                                                                imageUrl:
                                                                    e.mediaUrl,
                                                                errorWidget: (context,
                                                                        url,
                                                                        error) =>
                                                                    const Icon(Icons
                                                                        .error),
                                                                progressIndicatorBuilder:
                                                                    (context,
                                                                            url,
                                                                            downloadProgress) =>
                                                                        Center(
                                                                  child:
                                                                      CircularProgressIndicator(
                                                                    value: downloadProgress
                                                                        .progress,
                                                                  ),
                                                                ),
                                                                fit: BoxFit
                                                                    .cover,
                                                                width: 1000,
                                                              ),
                                                            ),
                                                          ),
                                                          Container(
                                                            height: 30.h,
                                                            width: 100.w,
                                                            color: Colors.black
                                                                .withOpacity(
                                                                    0.5), // Adjust opacity as needed
                                                          ),
                                                          Positioned(
                                                            top: 2.h,
                                                            left: 11.w,
                                                            child: Column(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              children: [
                                                                Container(
                                                                    child: Text
                                                                        .rich(
                                                                  TextSpan(
                                                                    children: [
                                                                      const TextSpan(
                                                                          text:
                                                                              'By ',
                                                                          style: TextStyle(
                                                                              fontWeight: FontWeight.bold,
                                                                              fontFamily: FontFamily.cairo,
                                                                              color: Colors.yellow)),
                                                                      TextSpan(
                                                                          text: e
                                                                              .owner
                                                                              .firstName,
                                                                          style: const TextStyle(
                                                                              fontFamily: FontFamily.cairo,
                                                                              fontWeight: FontWeight.bold,
                                                                              color: Color(0xffb0a12c))),
                                                                    ],
                                                                  ),
                                                                )),
                                                                SizedBox(
                                                                  height: 1.h,
                                                                ),
                                                                Text(
                                                                  lang == "en"
                                                                      ? e.titleEn
                                                                      : e.titleAr,
                                                                  style: const TextStyle(
                                                                      fontSize:
                                                                          AppFontSize
                                                                              .LARGE,
                                                                      fontFamily:
                                                                          FontFamily
                                                                              .cairo,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      color: Color(
                                                                          0xFF3EDA82)),
                                                                ),
                                                                SizedBox(
                                                                  height: 1.h,
                                                                ),
                                                                Container(
                                                                  width: 80.w,
                                                                  child: Text(
                                                                    lang == "en"
                                                                        ? e.descriptionEn
                                                                        : e.descriptionAr,
                                                                    style: const TextStyle(
                                                                        fontSize:
                                                                            AppFontSize
                                                                                .SMALL,
                                                                        fontFamily:
                                                                            FontFamily
                                                                                .cairo,
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .bold,
                                                                        color: Colors
                                                                            .white),
                                                                    maxLines: 3,
                                                                  ),
                                                                ),
                                                                SizedBox(
                                                                  height: 1.h,
                                                                ),
                                                                Container(
                                                                  decoration: BoxDecoration(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              8),
                                                                      color: Colors
                                                                          .red
                                                                          .withOpacity(
                                                                              0.8)),
                                                                  child: Center(
                                                                      child:
                                                                          Padding(
                                                                    padding:
                                                                        const EdgeInsets.all(
                                                                            8.0),
                                                                    child: Text(
                                                                      AppLocalization.of(
                                                                              context)
                                                                          .trans(
                                                                              "start_learning"),
                                                                      style: const TextStyle(
                                                                          fontSize: AppFontSize
                                                                              .SMALL,
                                                                          fontFamily: FontFamily
                                                                              .cairo,
                                                                          fontWeight: FontWeight
                                                                              .bold,
                                                                          color:
                                                                              Colors.white),
                                                                    ),
                                                                  )),
                                                                )
                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      ))
                                                  .toList(),
                                          options: CarouselOptions(
                                              autoPlay: true,
                                              autoPlayAnimationDuration:
                                                  Duration(seconds: 1),
                                              aspectRatio: 14 / 9,
                                              viewportFraction: 1,
                                              onPageChanged: (index, reason) {
                                                setState(() {
                                                  _current = index;
                                                });
                                              })),
                                    ),
                                  ),
                                  Positioned(
                                    top: 26.h,
                                    child: BlurryContainer(
                                      height: 700,
                                      width: 100.w,
                                      child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                                AppLocalization.of(context)
                                                    .trans("welcome_back"),
                                                style: const TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontFamily:
                                                        FontFamily.cairo,
                                                    color: Colors.yellow)),
                                            Container(
                                              child: GridView.builder(
                                                  shrinkWrap: true,
                                                  physics:
                                                      NeverScrollableScrollPhysics(),
                                                  itemBuilder:
                                                      (context, index) {
                                                    int randomNumber =
                                                        random.nextInt(4);
                                                    return AdsCourseCard(
                                                      color:
                                                          colorList[index % 4],
                                                      course: context
                                                          .read<AdsCubit>()
                                                          .adsItem[index],
                                                    );
                                                  },
                                                  gridDelegate:
                                                      const SliverGridDelegateWithFixedCrossAxisCount(
                                                    crossAxisCount: 1,
                                                    childAspectRatio: 1.0,
                                                    crossAxisSpacing: 10.0,
                                                    mainAxisSpacing: 5,
                                                    mainAxisExtent: 145,
                                                  ),
                                                  itemCount:
                                                      BlocProvider.of<AdsCubit>(
                                                              context)
                                                          .adsItem
                                                          .length),
                                            ),
                                          ]),
                                    ),
                                  )
                                ],
                              );
                            }
                            return Container();
                          },
                        ),
                      ),
                      SizedBox(
                        height: 2.h,
                      ),
                      Padding(
                        padding:
                            EdgeInsetsDirectional.only(start: 4.w, end: 4.w),
                        child: Row(
                          children: [
                            lang == "en"
                                ? SvgPicture.asset('assets/img/svg/tag.svg')
                                : Transform(
                                    alignment: Alignment.center,
                                    transform: Matrix4.identity()
                                      ..scale(-1.0, 1.0),
                                    // Rotate by 180 degrees (pi radians)
                                    child: SvgPicture.asset(
                                        'assets/img/svg/tag.svg'),
                                  ),
                            Text(AppLocalization.of(context).trans("course"),
                                style: AppTextStyle.largeBlackBold),
                            Spacer(),
                            InkWell(
                              onTap: () {
                                showModalBottomSheet(
                                  context: context,
                                  builder: (context) => MyBottomSheet(),
                                );
                              },
                              child: SvgPicture.asset(
                                'assets/img/svg/search-advanced.svg',
                                color: Colors.red.withOpacity(0.9),
                              ),
                            )
                          ],
                        ),
                      ),
                      BlocBuilder<CourseBloc, CourseState>(
                          builder: (context, state) {
                        if (state is GetCourseAwaitState ||
                            state is GetFilterCourseAwaitState) {
                          return const StaggeredDotsWave();
                        } else if (state is GetCourseErrorState) {
                          return Center(
                              child: SizedBox(
                            height: 180,
                            width: 200,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  state.message,
                                  style: TextStyle(
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.w500,
                                      color: Theme.of(context)
                                          .textTheme
                                          .bodyText2
                                          .color),
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                IconButton(
                                    onPressed: () {
                                      BlocProvider.of<CourseBloc>(context)
                                          .add(GetCourses());
                                    },
                                    icon: Icon(
                                      Log.retweet,
                                      size: 18.sp,
                                      color: Theme.of(context)
                                          .textTheme
                                          .bodyText2
                                          .color,
                                    ))
                              ],
                            ),
                          ));
                        } else if (state is GetCourseAcceptState ||
                            state is GetFilterCourseAcceptState) {
                          return SingleChildScrollView(
                            child: Container(
                              color: Colors.white,
                              child: GridView.builder(
                                  shrinkWrap: true,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemBuilder: (context, index) {
                                    return CourseCard(
                                      color: colorList[index % 4],
                                      course:
                                          BlocProvider.of<CourseBloc>(context)
                                              .courses[index],
                                    );
                                  },
                                  gridDelegate:
                                      SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 2,
                                    childAspectRatio: 1.0,
                                    crossAxisSpacing: 0.0,
                                    mainAxisSpacing: 5,
                                    mainAxisExtent:
                                        MediaQuery.of(context).size.height /
                                            3.1,
                                  ),
                                  itemCount:
                                      BlocProvider.of<CourseBloc>(context)
                                          .courses
                                          .length),
                            ),
                          );
                        }

                        return Container();
                      }),
                      SizedBox(
                        height: 3.h,
                        child: Container(
                          color: Colors.white,
                        ),
                      ),
                      // NumberPaginator(
                      //   numberPages:
                      //       BlocProvider.of<CourseBloc>(context).totalPages,
                      //   onPageChange: (int index) {
                      //     setState(() {
                      //       BlocProvider.of<CourseBloc>(context).page =
                      //           index + 1;
                      //       BlocProvider.of<CourseBloc>(context)
                      //           .add(GetCourses());
                      //     });
                      //   },
                      //   showPrevButton: true,
                      //   config: const NumberPaginatorUIConfig(
                      //     buttonSelectedBackgroundColor: Color(0xFF6E19D7),
                      //     buttonUnselectedForegroundColor: Color(0xFF6E19D7),
                      //   ),
                      //   showNextButton: true,
                      //   nextButtonContent: TextButton(
                      //     onPressed: _controller.currentPage > 0
                      //         ? () => _controller.prev()
                      //         : null,
                      //     // _controller must be passed to NumberPaginator
                      //     child: Icon(Icons.chevron_right),
                      //   ),
                      //   // custom prev/next buttons using builder (ignored if showPrevButton/showNextButton is false)
                      //   prevButtonBuilder: (context) => TextButton(
                      //     onPressed: _controller.currentPage > 0
                      //         ? () => _controller.prev()
                      //         : null,
                      //     // _controller must be passed to NumberPaginator
                      //     child: Icon(Icons.chevron_left),
                      //   ),
                      // )
                    ],
                  ),
                )),
          ),
        ),
      ),
    );
  }
}
