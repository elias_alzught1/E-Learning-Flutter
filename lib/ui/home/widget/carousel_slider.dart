// import 'package:e_learning/themes/app_colors.dart';
// import 'package:e_learning/utils/sizer_custom/sizer.dart';
// import 'package:flutter/material.dart';
// import 'package:carousel_slider/carousel_slider.dart';
//
// class CarouselSliderWidget extends StatefulWidget {
//   const CarouselSliderWidget(
//       {Key key, CarouselOptions options, List<ClipRRect> items})
//       : super(key: key);
//
//   @override
//   State<CarouselSliderWidget> createState() => _CarouselSliderWidgetState();
// }
//
// final List<String> imageList = [
//   "assets/img/slider-1.png",
//   "assets/img/slider-2.png",
//   "assets/img/slider-3.png",
//   "assets/img/slider-4.png",
// ];
//
// class _CarouselSliderWidgetState extends State<CarouselSliderWidget> {
//   final CarouselController _controller = CarouselController();
//   int _current = 0;
//
//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       children: [
//         Center(
//           child: CarouselSlider(
//             carouselController: _controller,
//             options: CarouselOptions(
//                 enlargeCenterPage: true,
//                 enableInfiniteScroll: false,
//                 autoPlay: false,
//                 aspectRatio: 1.8,
//                 onPageChanged: (index, reason) {
//                   setState(() {
//                     _current = index;
//                   });
//                 }),
//             items: imageList
//                 .map((e) => ClipRRect(
//                       borderRadius: BorderRadius.circular(4),
//                       child: Container(
//                         decoration: BoxDecoration(color: cCH),
//                         child: Stack(
//                           fit: StackFit.loose,
//                           children: <Widget>[
//                             Image.asset(
//                               e,
//                               width: 80.w,
//                               height: 350,
//                               fit: BoxFit.cover,
//                             )
//                           ],
//                         ),
//                       ),
//                     ))
//                 .toList(),
//           ),
//         ),
//         SizedBox(
//           height: 1.h,
//         ),
//         Row(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: imageList.asMap().entries.map((entry) {
//             return GestureDetector(
//               onTap: () => _controller.animateToPage(entry.key),
//               child: Container(
//                 width: 6.0,
//                 height: 6.0,
//                 margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
//                 decoration: BoxDecoration(
//                     shape: BoxShape.circle,
//                     color: (Theme.of(context).brightness == Brightness.dark
//                             ? Colors.white
//                             : Colors.blue)
//                         .withOpacity(_current == entry.key ? 0.9 : 0.4)),
//               ),
//             );
//           }).toList(),
//         ),
//       ],
//     );
//   }
// }
