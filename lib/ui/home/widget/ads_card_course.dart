import 'package:cached_network_image/cached_network_image.dart';
import 'package:e_learning/models/common/course_model.dart';
import 'package:e_learning/themes/app_text_style.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';

import '../../../helpers/helpers.dart';
import '../../../models/response/ads_response.dart';
import '../../../themes/app_colors.dart';

class AdsCourseCard extends StatefulWidget {
  final AdsItem course;
  final Color color;

  const AdsCourseCard({Key key, this.course, this.color}) : super(key: key);

  @override
  State<AdsCourseCard> createState() => _AdsCourseCardState();
}

class _AdsCourseCardState extends State<AdsCourseCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border(bottom: BorderSide(color: widget.color, width: 5))),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Expanded(
                  child: Column(
                    children: [
                      Text(
                        widget.course.titleEn,
                        style: AppTextStyle.largeBlackBold,
                      ),
                      Row(
                        children: [
                          Container(
                            width: 30,
                            height: 30,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                color: widget.color, // Border color
                                width: 2.0, // Border width
                              ),
                            ),
                            child: CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 30,
                              backgroundImage: NetworkImage(
                                widget.course.owner
                                    .profileImage, // Replace with your image URL
                              ),
                            ),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Text(
                            widget.course.owner.firstName,
                            style: AppTextStyle.mediumGray,
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Container(
                        width: 100.w,
                        height: 4,
                        decoration: const BoxDecoration(
                            color: AppColors.deepGray,
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                      ),
                      Row(
                        children: const [
                          Spacer(),
                          Text(
                            "Lesson 0 of 0",
                            style: AppTextStyle.smallDeepGrayBold,
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                const SizedBox(width: 15),
                Expanded(
                  child: CachedNetworkImage(
                    imageUrl: widget.course.mediaUrl,
                    height: 90,
                    width: 90,
                    fit: BoxFit.fill,
                    placeholder: (context, url) => getShimmer(),
                    errorWidget: (context, url, error) => const Center(
                      child: Icon(Icons.error),
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
