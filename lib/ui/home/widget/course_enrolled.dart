import 'package:cached_network_image/cached_network_image.dart';
import 'package:e_learning/helpers/helpers.dart';
import 'package:e_learning/utils/sizer_custom/sizer.dart';
import 'package:flutter/material.dart';
import 'package:sprintf/sprintf.dart';

import '../../../controller/data_store.dart';
import '../../../localization/app_localization.dart';
import '../../../models/common/auther_model.dart';
import '../../../models/common/course_model.dart';
import '../../../themes/app_colors.dart';
import '../../../themes/app_text_style.dart';
import '../../course/course_details.dart';

class CourseEnrolled extends StatelessWidget {
  final Courses course;

  const CourseEnrolled({Key key, this.course}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Padding(
        padding: EdgeInsetsDirectional.only(start: 4.w),
        child: SizedBox(
          width: 300.w,
          child: Card(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            elevation: 4,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              //mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Stack(
                  children: [
                    Padding(
                      padding: EdgeInsets.all(2.h),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: SizedBox(
                          height: 180.sp,
                          width: 100.w,

                          child: CachedNetworkImage(
                            imageUrl: course.image,
                            fit: BoxFit.fill,
                            placeholder: (context, url) => getShimmer(),
                            errorWidget: (context, url, error) => const Center(
                              child: Icon(Icons.error),
                            ),
                          ),
                          // child: OctoImage(
                          //   image: Image.network(widget.course.image),
                          //   fit: BoxFit.cover,
                          //   placeholderBuilder: (context) => getShimmer(),
                          //   errorBuilder: imageErrorBuilder,
                          // ),
                        ),
                      ),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 1.h,
                    ),

                    Padding(
                      padding: EdgeInsetsDirectional.only(start: 4.w),
                      child: Text( DataStore.instance.lang=="en"?course.nameEn :course.nameAr ,
                          style: AppTextStyle.largeGreenBold, maxLines: 2),
                    ),
                    //Course_start
                    Padding(
                      padding: EdgeInsetsDirectional.only(
                          start: 4.w, top: 1.h, bottom: 1.5.h),
                      child: _buildAvatar(context, course.authors),
                    ),
                    chartLine(
                        context, course.currentLesson, course.lessonsCount),
                    SizedBox(
                      height: 2.h,
                    ),

                    Padding(
                      padding:
                          EdgeInsetsDirectional.only(end: 4.w, bottom: 2.h),
                      child: Row(
                        children: [
                          Spacer(),
                          Padding(
                            padding: EdgeInsetsDirectional.only(end: 1.w),
                            child: Directionality(
                              textDirection: DataStore.instance.lang=="en"?TextDirection.ltr:TextDirection.rtl,
                              child: Text(
                                sprintf(
                                  AppLocalization.of(context).trans('lesson'),
                                  [course.currentLesson, course.lessonsCount],
                                ),
                                style: const TextStyle(
                                    fontSize: 13,
                                    color: Color(0xFF707070),
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
      onTap: () {
        //CourseDetails
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => CourseDetails(
                    course: course,
                  )),
        );
      },
    );
  }

  Widget chartLine(BuildContext context, int pct, int count) {
    return Padding(
      padding: EdgeInsetsDirectional.only(start: 4.w, end: 4.w),
      child: Stack(children: [
        Container(
          height: 1.h,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              color: AppColors.gray.withOpacity(0.5),
              borderRadius: BorderRadius.circular(20)),
          child: const Text(''),
        ),
        Container(
          height: 1.h,
          width: pct == null || pct == 0
              ? MediaQuery.of(context).size.width * (0 / 100)
              : MediaQuery.of(context).size.width *
                  (int.parse(pct.toString()) / count) *
                  0.9,
          decoration: BoxDecoration(
              color: AppColors.blueAccent,
              borderRadius: BorderRadius.circular(20)),
          child: Text(''),
        ),
      ]),
    );
  }

  Widget _buildAvatar(BuildContext context, List<Authors> authors) {

    return Container(width: MediaQuery.of(context).size.width,child: Stack(
      children: [

        // authors.length>0 ? Positioned(left: 8.w,child: CircleAvatar(
        //   backgroundColor: AppColors.deepBlue.withOpacity(0.05),
        //   child: Text(
        //     "+" + authors.length.toString(),
        //     style: TextStyle(
        //       color: AppColors.deepBlue.withOpacity(0.4),
        //     ),
        //   ),
        // )):SizedBox(),
        // CircleAvatar(
        //   backgroundColor: AppColors.deepBlue.withOpacity(0.2),
        //   child: Text(
        //     authors.first.firstName[0]??"",
        //     style: TextStyle(
        //       color: AppColors.deepBlue.withOpacity(0.4),
        //     ),
        //   ),
        // ),
      ],
    ),);
  }
}
